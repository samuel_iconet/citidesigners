<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtisansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artisans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('skill_id');
            $table->string('phone_number');
            $table->string('account_number');
            $table->string('account_name');
            $table->string('guarantor');
            $table->string('guarantor_number');
            $table->string('profile_image');
            $table->string('address');
            $table->integer('area_id');
            $table->string('state_id');
            $table->timestamp('date_of_birth');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artisans');
    }
}
