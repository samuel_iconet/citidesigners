-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 09, 2020 at 11:22 AM
-- Server version: 10.3.24-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `empirhal_empireklassic2`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Aba', 6, 1, '2020-06-04 12:38:04', '2020-06-04 12:38:04'),
(7, 'Arochukwu', 6, 1, '2020-06-04 12:39:21', '2020-06-04 12:39:21'),
(8, 'Umuahia', 6, 1, '2020-06-04 12:39:37', '2020-06-04 12:39:37'),
(9, 'Jimeta', 7, 1, '2020-06-04 12:39:55', '2020-06-04 12:39:55'),
(10, 'Mubi', 7, 1, '2020-06-04 12:40:06', '2020-06-04 12:40:06'),
(11, 'Numan', 7, 1, '2020-06-04 12:40:17', '2020-06-04 12:40:17'),
(12, 'Yola', 7, 1, '2020-06-04 12:40:28', '2020-06-04 12:40:28'),
(13, 'Ikot Abasi', 8, 1, '2020-06-04 12:40:47', '2020-06-04 12:40:47'),
(14, 'Ikot Ekpene', 8, 1, '2020-06-04 12:40:57', '2020-06-04 12:40:57'),
(15, 'Oron', 8, 1, '2020-06-04 12:41:08', '2020-06-04 12:41:08'),
(16, 'Uyo', 8, 1, '2020-06-04 12:41:18', '2020-06-04 12:41:18'),
(17, 'Awka', 10, 1, '2020-06-04 12:41:36', '2020-06-04 12:41:36'),
(18, 'Onitsha', 10, 1, '2020-06-04 12:41:50', '2020-06-04 12:41:50'),
(19, 'Azare', 9, 1, '2020-06-04 12:42:01', '2020-06-04 12:42:01'),
(20, 'Bauchi', 9, 1, '2020-06-04 12:42:13', '2020-06-04 12:42:13'),
(21, 'Jama′are', 9, 1, '2020-06-04 12:42:25', '2020-06-04 12:42:25'),
(22, 'Katagum', 9, 1, '2020-06-04 12:42:37', '2020-06-04 12:42:37'),
(23, 'Misau', 9, 1, '2020-06-04 12:42:47', '2020-06-04 12:42:47'),
(24, 'Brass', 12, 1, '2020-06-04 12:43:00', '2020-06-04 12:43:00'),
(25, 'Makurdi', 13, 1, '2020-06-04 12:43:14', '2020-06-04 12:43:14'),
(26, 'Biu', 14, 1, '2020-06-04 12:43:27', '2020-06-04 12:43:27'),
(27, 'Dikwa', 14, 1, '2020-06-04 12:43:37', '2020-06-04 12:43:37'),
(28, 'Maiduguri', 14, 1, '2020-06-04 12:43:48', '2020-06-04 12:43:48'),
(29, 'Calabar', 15, 1, '2020-06-04 12:51:29', '2020-06-04 12:51:29'),
(30, 'Ogoja', 15, 1, '2020-06-05 01:20:06', '2020-06-05 01:20:06'),
(31, 'Ogoja', 15, 1, '2020-06-05 01:20:06', '2020-06-05 01:20:06'),
(32, 'Asaba', 16, 1, '2020-06-05 01:20:31', '2020-06-05 01:20:31'),
(33, 'Burutu', 16, 1, '2020-06-05 01:20:49', '2020-06-05 01:20:49'),
(34, 'Koko', 16, 1, '2020-06-05 01:21:05', '2020-06-05 01:21:05'),
(35, 'Sapele', 16, 1, '2020-06-08 11:46:41', '2020-06-08 11:46:41'),
(37, 'Ughelli', 16, 1, '2020-06-08 11:47:25', '2020-06-08 11:47:25'),
(38, 'Warri', 16, 1, '2020-06-08 12:19:05', '2020-06-08 12:19:05'),
(39, 'Abakaliki', 17, 1, '2020-06-08 12:19:19', '2020-06-08 12:19:19'),
(40, 'Benin City', 18, 1, '2020-06-08 12:19:35', '2020-06-08 12:19:35'),
(41, 'Ado-Ekiti', 19, 1, '2020-06-08 12:19:53', '2020-06-08 12:19:53'),
(42, 'Effon-Alaiye', 19, 1, '2020-06-08 12:20:05', '2020-06-08 12:20:05'),
(43, 'Ikere-Ekiti', 19, 1, '2020-06-08 12:20:18', '2020-06-08 12:20:18'),
(45, 'Enugu', 20, 1, '2020-06-08 12:21:15', '2020-06-08 12:21:15'),
(46, 'Nsukka', 20, 1, '2020-06-08 12:21:26', '2020-06-08 12:21:26'),
(47, 'Abuja', 21, 1, '2020-06-08 12:21:42', '2020-06-08 12:21:42'),
(48, 'Asokoro', 21, 1, '2020-06-08 12:22:39', '2020-06-08 12:22:39'),
(49, 'Central Area', 21, 1, '2020-06-08 12:22:50', '2020-06-08 12:22:50'),
(50, 'Garki', 21, 1, '2020-06-08 12:23:01', '2020-06-08 12:23:01'),
(51, 'Guzape', 21, 1, '2020-06-08 12:23:13', '2020-06-08 12:23:13'),
(52, 'Maitama', 21, 1, '2020-06-08 12:23:24', '2020-06-08 12:23:24'),
(53, 'Wuse', 21, 1, '2020-06-08 12:23:41', '2020-06-08 12:23:41'),
(54, 'Apo-Dutse', 21, 1, '2020-06-08 12:23:54', '2020-06-08 12:23:54'),
(55, 'Dakibiyu', 21, 1, '2020-06-08 12:24:04', '2020-06-08 12:24:04'),
(56, 'Duboyi', 21, 1, '2020-06-08 12:24:14', '2020-06-08 12:24:14'),
(57, 'Durumi', 21, 1, '2020-06-08 12:24:24', '2020-06-08 12:24:24'),
(58, 'Gaduwa', 21, 1, '2020-06-08 12:24:34', '2020-06-08 12:24:34'),
(59, 'Gudu', 21, 1, '2020-06-08 12:24:45', '2020-06-08 12:24:45'),
(60, 'Jabi', 21, 1, '2020-06-08 12:24:57', '2020-06-08 12:24:57'),
(61, 'Jahi', 21, 1, '2020-06-08 12:25:07', '2020-06-08 12:25:07'),
(62, 'Kado', 21, 1, '2020-06-08 12:25:17', '2020-06-08 12:25:17'),
(63, 'Katampe', 21, 1, '2020-06-08 12:25:27', '2020-06-08 12:25:27'),
(64, 'Kaura', 21, 1, '2020-06-08 12:25:40', '2020-06-08 12:25:40'),
(65, 'Kukwaba', 21, 1, '2020-06-08 12:25:51', '2020-06-08 12:25:51'),
(66, 'Mabushi', 21, 1, '2020-06-08 12:26:01', '2020-06-08 12:26:01'),
(67, 'Utako', 21, 1, '2020-06-08 12:26:12', '2020-06-08 12:26:12'),
(68, 'Wuye', 21, 1, '2020-06-08 12:26:27', '2020-06-08 12:26:27'),
(69, 'Bunkoro', 21, 1, '2020-06-08 12:26:48', '2020-06-08 12:26:48'),
(70, 'Dakwo', 21, 1, '2020-06-08 12:26:57', '2020-06-08 12:26:57'),
(71, 'Dape', 21, 1, '2020-06-08 12:27:07', '2020-06-08 12:27:07'),
(72, 'Galadimawa', 21, 1, '2020-06-08 12:27:20', '2020-06-08 12:27:20'),
(73, 'Gwarinpa', 21, 1, '2020-06-08 12:27:29', '2020-06-08 12:27:29'),
(74, 'Industrial Area', 21, 1, '2020-06-08 12:27:40', '2020-06-08 12:27:40'),
(75, 'Kabusa', 21, 1, '2020-06-08 12:27:52', '2020-06-08 12:27:52'),
(76, 'Kafe', 21, 1, '2020-06-08 12:28:02', '2020-06-08 12:28:02'),
(77, 'Karmo', 21, 1, '2020-06-08 12:28:13', '2020-06-08 12:28:13'),
(78, 'Life Camp', 21, 1, '2020-06-08 12:28:27', '2020-06-08 12:28:27'),
(79, 'Lokogoma', 21, 1, '2020-06-08 12:28:38', '2020-06-08 12:28:38'),
(80, 'Nbora', 21, 1, '2020-06-08 12:28:49', '2020-06-08 12:28:49'),
(81, 'Okanje', 21, 1, '2020-06-08 12:28:59', '2020-06-08 12:28:59'),
(82, 'Pyakasa', 21, 1, '2020-06-08 12:29:10', '2020-06-08 12:29:10'),
(83, 'Saraji', 21, 1, '2020-06-08 12:29:24', '2020-06-08 12:29:24'),
(84, 'Wumba', 21, 1, '2020-06-08 12:29:37', '2020-06-08 12:29:37'),
(85, 'Wupa', 21, 1, '2020-06-08 12:29:48', '2020-06-08 12:29:48'),
(86, 'Bude', 21, 1, '2020-06-08 12:30:12', '2020-06-08 12:30:12'),
(87, 'Burun', 21, 1, '2020-06-08 12:30:23', '2020-06-08 12:30:23'),
(88, 'Chafe', 21, 1, '2020-06-08 12:30:34', '2020-06-08 12:30:34'),
(89, 'Gidari Bahagwo', 21, 1, '2020-06-08 12:30:47', '2020-06-08 12:30:47'),
(90, 'Gwagwa', 21, 1, '2020-06-08 12:30:59', '2020-06-08 12:30:59'),
(91, 'Gwari', 21, 1, '2020-06-08 12:31:10', '2020-06-08 12:31:10'),
(92, 'Idogwari', 21, 1, '2020-06-08 12:32:32', '2020-06-08 12:32:32'),
(93, 'Idu', 21, 1, '2020-06-08 12:32:43', '2020-06-08 12:32:43'),
(94, 'Jaite', 21, 1, '2020-06-08 12:32:53', '2020-06-08 12:32:53'),
(95, 'Jaite', 21, 1, '2020-06-08 12:33:33', '2020-06-08 12:33:33'),
(96, 'Kaba', 21, 1, '2020-06-08 12:33:42', '2020-06-08 12:33:42'),
(97, 'Kagini', 21, 1, '2020-06-08 12:33:52', '2020-06-08 12:33:52'),
(98, 'Karsana', 21, 1, '2020-06-08 12:34:03', '2020-06-08 12:34:03'),
(99, 'Ketti', 21, 1, '2020-06-08 12:34:14', '2020-06-08 12:34:14'),
(100, 'Kpoto', 21, 1, '2020-06-08 12:34:23', '2020-06-08 12:34:23'),
(101, 'Mamusa', 21, 1, '2020-06-08 12:34:34', '2020-06-08 12:34:34'),
(102, 'Parfun', 21, 1, '2020-06-08 12:34:45', '2020-06-08 12:34:45'),
(103, 'Sabo Gida', 21, 1, '2020-06-08 12:34:55', '2020-06-08 12:34:55'),
(104, 'Sheretti', 21, 1, '2020-06-08 12:35:04', '2020-06-08 12:35:04'),
(105, 'Tasha', 21, 1, '2020-06-08 12:35:14', '2020-06-08 12:35:14'),
(106, 'Waru-Pozema', 21, 1, '2020-06-08 12:35:25', '2020-06-08 12:35:25'),
(107, 'Kyami', 21, 1, '2020-06-08 12:35:37', '2020-06-08 12:35:37'),
(108, 'Lugbe', 21, 1, '2020-06-08 12:35:48', '2020-06-08 12:35:48'),
(109, 'Dawaki', 21, 1, '2020-06-08 13:32:05', '2020-06-08 13:32:05'),
(110, 'Gwagwalada', 21, 1, '2020-06-08 13:32:15', '2020-06-08 13:32:15'),
(111, 'Karu', 21, 1, '2020-06-08 13:32:25', '2020-06-08 13:32:25'),
(112, 'Kubwa', 21, 1, '2020-06-08 13:32:35', '2020-06-08 13:32:35'),
(113, 'Kuje', 21, 1, '2020-06-08 13:32:44', '2020-06-08 13:32:44'),
(114, 'Mpape', 21, 1, '2020-06-08 13:32:54', '2020-06-08 13:32:54'),
(115, 'Nyanya', 21, 1, '2020-06-08 13:33:07', '2020-06-08 13:33:07'),
(116, 'Deba Habe', 22, 1, '2020-06-08 16:42:38', '2020-06-08 16:42:38'),
(117, 'Gombe', 22, 1, '2020-06-08 16:42:51', '2020-06-08 16:42:51'),
(118, 'Kumo', 22, 1, '2020-06-08 16:43:02', '2020-06-08 16:43:02'),
(119, 'Owerri', 23, 1, '2020-06-08 16:43:15', '2020-06-08 16:43:15'),
(120, 'Birnin Kudu', 24, 1, '2020-06-08 17:20:14', '2020-06-08 17:20:14'),
(121, 'Dutse', 24, 1, '2020-06-08 19:15:40', '2020-06-08 19:15:40'),
(122, 'Gumel', 24, 1, '2020-06-08 19:15:55', '2020-06-08 19:15:55'),
(123, 'Hadejia', 24, 1, '2020-06-08 19:16:06', '2020-06-08 19:16:06'),
(124, 'Kazaure', 24, 1, '2020-06-08 19:16:16', '2020-06-08 19:16:16'),
(125, 'Jemaa', 25, 1, '2020-06-08 19:16:31', '2020-06-08 19:16:31'),
(126, 'Kaduna', 25, 1, '2020-06-08 19:16:44', '2020-06-08 19:16:44'),
(127, 'Zaria', 25, 1, '2020-06-08 19:16:56', '2020-06-08 19:16:56'),
(128, 'Kano', 26, 1, '2020-06-08 19:17:07', '2020-06-08 19:17:07'),
(129, 'Daura', 27, 1, '2020-06-08 19:17:19', '2020-06-08 19:17:19'),
(130, 'Katsina', 27, 1, '2020-06-08 19:17:41', '2020-06-08 19:17:41'),
(131, 'Argungu', 28, 1, '2020-06-08 19:18:09', '2020-06-08 19:18:09'),
(132, 'Birnin Kebbi', 28, 1, '2020-06-08 19:18:22', '2020-06-08 19:18:22'),
(133, 'Gwandu', 28, 1, '2020-06-08 19:18:35', '2020-06-08 19:18:35'),
(134, 'Yelwa', 28, 1, '2020-06-08 19:18:47', '2020-06-08 19:18:47'),
(135, 'Idah', 29, 1, '2020-06-09 12:18:14', '2020-06-09 12:18:14'),
(136, 'Kabba', 29, 1, '2020-06-09 12:18:27', '2020-06-09 12:18:27'),
(137, 'Lokoja', 29, 1, '2020-06-09 12:18:39', '2020-06-09 12:18:39'),
(138, 'Okene', 29, 1, '2020-06-09 12:18:51', '2020-06-09 12:18:51'),
(139, 'Ilorin', 30, 1, '2020-06-09 12:19:10', '2020-06-09 12:19:10'),
(140, 'Jebba', 30, 1, '2020-06-09 12:19:26', '2020-06-09 12:19:26'),
(141, 'Lafiagi', 30, 1, '2020-06-09 12:19:40', '2020-06-09 12:19:40'),
(142, 'Offa', 30, 1, '2020-06-09 12:19:52', '2020-06-09 12:19:52'),
(143, 'Pategi', 30, 1, '2020-06-09 12:20:06', '2020-06-09 12:20:06'),
(144, 'Badagry', 31, 1, '2020-06-09 12:20:25', '2020-06-09 12:20:25'),
(145, 'Epe', 31, 1, '2020-06-09 12:30:12', '2020-06-09 12:30:12'),
(146, 'Ikeja', 31, 1, '2020-06-09 12:30:26', '2020-06-09 12:30:26'),
(147, 'Ikorodu', 31, 1, '2020-06-09 12:30:43', '2020-06-09 12:30:43'),
(148, 'Lagos', 31, 1, '2020-06-09 12:30:56', '2020-06-09 12:30:56'),
(149, 'Mushin', 31, 1, '2020-06-09 12:34:18', '2020-06-09 12:34:18'),
(150, 'Shomolu', 31, 1, '2020-06-09 12:34:28', '2020-06-09 12:34:28'),
(151, 'Keffi', 32, 1, '2020-06-09 12:34:47', '2020-06-09 12:34:47'),
(152, 'Lafia', 32, 1, '2020-06-09 12:35:04', '2020-06-09 12:35:04'),
(153, 'Nasarawa', 32, 1, '2020-06-09 12:35:16', '2020-06-09 12:35:16'),
(154, 'Agaie', 33, 1, '2020-06-09 12:35:35', '2020-06-09 12:35:35'),
(155, 'Baro', 33, 1, '2020-06-09 12:35:46', '2020-06-09 12:35:46'),
(156, 'Bida', 33, 1, '2020-06-09 12:35:57', '2020-06-09 12:35:57'),
(157, 'Kontagora', 33, 1, '2020-06-09 12:36:08', '2020-06-09 12:36:08'),
(158, 'Lapai', 33, 1, '2020-06-09 12:36:17', '2020-06-09 12:36:17'),
(159, 'Minna', 33, 1, '2020-06-09 12:36:29', '2020-06-09 12:36:29'),
(160, 'Suleja', 33, 1, '2020-06-09 12:36:43', '2020-06-09 12:36:43'),
(161, 'Abeokuta', 34, 1, '2020-06-11 14:22:54', '2020-06-11 14:22:54'),
(162, 'Ijebu-Ode', 34, 1, '2020-06-11 14:23:05', '2020-06-11 14:23:05'),
(163, 'Ilaro', 34, 1, '2020-06-11 14:23:17', '2020-06-11 14:23:17'),
(164, 'Shagamu', 34, 1, '2020-06-11 14:23:30', '2020-06-11 14:23:30'),
(165, 'Akure', 35, 1, '2020-06-11 14:23:44', '2020-06-11 14:23:44'),
(166, 'Ikare', 35, 1, '2020-06-11 14:23:58', '2020-06-11 14:23:58'),
(167, 'Oka-Akoko', 35, 1, '2020-06-11 14:24:11', '2020-06-11 14:24:11'),
(168, 'Ondo', 35, 1, '2020-06-11 14:24:22', '2020-06-11 14:24:22'),
(169, 'Owo', 35, 1, '2020-06-11 14:24:32', '2020-06-11 14:24:32'),
(170, 'Ede', 36, 1, '2020-06-11 14:24:45', '2020-06-11 14:24:45'),
(171, 'Ikire', 36, 1, '2020-06-11 14:24:58', '2020-06-11 14:24:58'),
(172, 'Ikirun', 36, 1, '2020-06-11 14:25:08', '2020-06-11 14:25:08'),
(173, 'Ila', 36, 1, '2020-06-11 14:25:19', '2020-06-11 14:25:19'),
(174, 'Ile-Ife', 36, 1, '2020-06-11 14:25:31', '2020-06-11 14:25:31'),
(175, 'Ilesha', 36, 1, '2020-06-11 14:25:43', '2020-06-11 14:25:43'),
(176, 'Ilobu', 36, 1, '2020-06-11 14:25:55', '2020-06-11 14:25:55'),
(177, 'Inisa', 36, 1, '2020-06-11 14:26:13', '2020-06-11 14:26:13'),
(178, 'Iwo', 36, 1, '2020-06-11 14:26:25', '2020-06-11 14:26:25'),
(179, 'Oshogbo', 36, 1, '2020-06-11 14:26:39', '2020-06-11 14:26:39'),
(180, 'Ibadan', 37, 1, '2020-06-11 14:26:50', '2020-06-11 14:26:50'),
(181, 'Iseyin', 37, 1, '2020-06-11 14:27:01', '2020-06-11 14:27:01'),
(182, 'Ogbomosho', 37, 1, '2020-06-11 14:27:12', '2020-06-11 14:27:12'),
(183, 'Oyo', 37, 1, '2020-06-11 14:27:22', '2020-06-11 14:27:22'),
(184, 'Saki', 37, 1, '2020-06-11 14:27:33', '2020-06-11 14:27:33'),
(185, 'Bukuru', 38, 1, '2020-06-11 14:27:48', '2020-06-11 14:27:48'),
(186, 'Jos', 38, 1, '2020-06-11 14:27:59', '2020-06-11 14:27:59'),
(187, 'Vom', 38, 1, '2020-06-11 14:28:09', '2020-06-11 14:28:09'),
(188, 'Wase', 38, 1, '2020-06-11 14:28:20', '2020-06-11 14:28:20'),
(189, 'Bonny', 39, 1, '2020-06-11 14:29:44', '2020-06-11 14:29:44'),
(190, 'Degema', 39, 1, '2020-06-11 14:29:54', '2020-06-11 14:29:54'),
(191, 'Okrika', 39, 1, '2020-06-11 14:30:04', '2020-06-11 14:30:04'),
(192, 'Port Harcourt', 39, 1, '2020-06-11 14:30:14', '2020-06-11 14:30:14'),
(193, 'Sokoto', 40, 1, '2020-06-11 14:30:26', '2020-06-11 14:30:26'),
(194, 'Ibi', 41, 1, '2020-06-11 14:30:38', '2020-06-11 14:30:38'),
(195, 'Jalingo', 41, 1, '2020-06-11 14:30:49', '2020-06-11 14:30:49'),
(196, 'Muri', 41, 1, '2020-06-11 14:31:00', '2020-06-11 14:31:00'),
(197, 'Damaturu', 42, 1, '2020-06-11 14:31:16', '2020-06-11 14:31:16'),
(198, 'Nguru', 42, 1, '2020-06-11 14:31:26', '2020-06-11 14:31:26'),
(199, 'Gusau', 43, 1, '2020-06-11 14:31:38', '2020-06-11 14:31:38'),
(200, 'Kaura Namoda', 43, 1, '2020-06-11 14:31:51', '2020-06-11 14:31:51');

-- --------------------------------------------------------

--
-- Table structure for table `artisans`
--

CREATE TABLE `artisans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantor_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artisans`
--

INSERT INTO `artisans` (`id`, `user_id`, `skill_id`, `phone_number`, `account_number`, `account_name`, `guarantor`, `guarantor_number`, `profile_image`, `address`, `area_id`, `state_id`, `state_name`, `area_name`, `date_of_birth`, `created_at`, `updated_at`) VALUES
(1, 0, 0, '0', '0', '0', '0', '0', '0', '0', '6', '6', 'Abia State', 'Aba', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 2, 7, '9768', '9089804848', 'inuh', 'Samuel bakare', '8884949494', 'profile_1596541516.png', 'Abuja', '41', '19', 'Ekiti State', 'Ado-Ekiti', '1993-03-09 05:00:00', '2020-03-15 23:24:10', '2020-08-04 10:45:17'),
(8, 21, 10, '07038927440', '022238927440', 'GT bank', '0', '0', 'profile_1590852646.jpg', 'games', '4', '1', 'Abia State', 'Aba', NULL, '2020-05-30 19:28:56', '2020-05-30 19:30:46'),
(9, 22, 4, '07038927440', '055', '055', '0gh', '0980', NULL, 'gy', '4', '1', 'Abia State', 'Aba', NULL, '2020-05-30 21:24:48', '2020-05-30 21:24:48'),
(10, 35, 24, '07038927440', '0222384053', 'Gt bank', 'mr obinna', '07085376346', NULL, '0sggsgg', '34', '16', 'Delta State', 'Koko', NULL, '2020-06-12 17:09:48', '2020-06-12 17:09:48'),
(11, 43, 25, '07038927440', '0222384053', 'gtbank', 'kelvin', '08038927440', NULL, 'sabon garri', '54', '21', 'Abia State', 'Apo-Dutse', '2016-01-11 05:00:00', '2020-06-19 03:06:32', '2020-06-19 03:15:45'),
(12, 56, 10, '090393939', '0222222211', 'mini', '0cdzxcsdz', '3534534324', NULL, 'sauka area', '24', '12', 'Bayelsa State', 'Brass', '2009-06-07 04:00:00', '2020-07-05 14:16:38', '2020-07-15 00:04:11'),
(13, 60, 9, '09034273833', '0909909999', 'hwdi', 'nAIWJO', '09999999999', NULL, 'askl', '41', '19', 'Ekiti State', 'Ado-Ekiti', '1989-04-07 04:00:00', '2020-07-19 17:22:53', '2020-07-19 17:22:53'),
(14, 69, 7, '07545455', '05654352435', '032r34qt43t3t', '0vvvfd', '04554546346354', 'profile_1596758502.png', '0sasad', '119', '23', 'Imo State', 'Owerri', '2020-05-05 23:00:00', '2020-08-06 23:01:14', '2020-08-06 23:01:43'),
(15, 72, 4, '07878889', '087867686', 'yffuy', 'ftyv', '07866789', 'profile_1597138428.png', 'yyug', '45', '20', 'Enugu State', 'Enugu', '2020-08-17 23:00:00', '2020-08-11 08:33:35', '2020-08-11 08:33:48'),
(16, 87, 4, '07080168504', '3121922189', 'First Bank', 'Engr. Alhasan Dakwoyi', '08035619529', 'profile_1598167374.jpg', 'Alheri Baptist Church Rubochi -kuje Abuja', '113', '21', 'Federal Capital Territory', 'Kuje', '1991-11-18 05:00:00', '2020-08-23 11:19:28', '2020-11-04 01:46:52'),
(17, 93, 9, '08069324142', '6015973143', 'Fidelity Bank', 'IYIDA SAMSON', '2347032396917', 'profile_1599372324.jpg', 'Ogrute Enugu Ezike', '46', '20', 'Enugu State', 'Nsukka', '1990-03-23 05:00:00', '2020-09-06 10:04:49', '2020-11-02 22:33:17'),
(18, 104, 9, '07068089376', '0038320268', 'Owoho Boniface', 'Mr Ariku Igbebi', '08029325238', 'profile_1599495705.jpg', 'Suit C2X Amara Mall After City College Karu - Abuja', '111', '21', 'Federal Capital Territory', 'Karu', '1991-09-27 04:00:00', '2020-09-07 20:20:12', '2020-09-07 20:21:45'),
(19, 142, 10, '08030708859', '0137269218', 'Gtbank', 'Olanrewaju opeyemi', '08166141580', 'profile_1600587265.jpg', 'No6,ifelodun street oke ogba akure', '165', '35', 'Ondo State', 'Akure', '1989-04-10 04:00:00', '2020-09-20 11:30:45', '2020-09-20 13:29:48'),
(20, 183, 14, '09047349061', '0168152499', 'OLUBAYO OLUMIDE SUNDAY', 'Abiola Olubayo', '08162488466', NULL, 'Lagos', '35', '16', 'Delta State', 'Sapele', '1989-02-12 05:00:00', '2020-11-03 20:54:17', '2020-11-07 16:34:25'),
(21, 184, 11, '07033593196', '0042323445', 'Diamond access', 'Mr. Onwumere Cyriacus chukwuma', '08137206534', NULL, 'House 12 umuoyo Irete Owerri', '119', '23', 'Imo State', 'Owerri', '1970-01-01 05:00:00', '2020-11-04 00:05:02', '2020-11-04 00:05:02'),
(22, 184, 11, '07033593196', '0042323445', 'Diamond access', 'Mr. Onwumere Cyriacus chukwuma', '08137206534', NULL, 'House 12 umuoyo Irete Owerri', '119', '23', 'Imo State', 'Owerri', '1970-01-01 05:00:00', '2020-11-04 00:05:02', '2020-11-04 00:05:02'),
(23, 203, 17, '08130735668', '6232730927', 'Fidelity', 'Thankgod', '07015243107', 'profile_1604592036.jpg', '0', '40', '18', 'Edo State', 'Benin City', '1970-01-01 05:00:00', '2020-11-05 20:51:43', '2020-11-05 21:03:32');

-- --------------------------------------------------------

--
-- Table structure for table `contact_uses`
--

CREATE TABLE `contact_uses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `itemcategories`
--

CREATE TABLE `itemcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `status`, `amount`, `profile_image`, `created_at`, `updated_at`) VALUES
(22, 'MINI ESTATE IN MPAPE', 'Almost completed mini estate in the Mpape area of the FcT', 1, '28000000', 'store_1597918017.jpeg', '2020-08-20 14:06:57', '2020-08-20 14:06:57'),
(31, 'TERRACED HOUSE', '3 Bed Trerraced house for sale in Gwarinpa. a new constructed home to be proud of.', 1, '58000000', 'store_1597918432.jpg', '2020-08-20 14:13:52', '2020-10-29 16:52:08'),
(37, 'LAND FOR SALE', 'Land for sale at imperial Park, GRA Sagamu Ogun state.\nper plot.', 1, '850000', 'store_1597919657.jpg', '2020-08-20 14:34:17', '2020-08-20 14:34:17'),
(38, 'LAND FOR SALE', 'Land for sale at ajo Garden, beside UBA Port harcourt', 1, '900000', 'store_1597920040.jpeg', '2020-08-20 14:40:40', '2020-08-20 14:40:40'),
(39, 'HOUSE FOR SALE', 'One story detached for sale, behind Mega fuel station at aja Lagos state', 1, '35000000', 'store_1597920369.jpg', '2020-08-20 14:46:09', '2020-08-20 14:46:09'),
(41, '3 BEDROOM', '3 bedroom detached bungalow for sale. university Dadin-kowa Area jos', 1, '15000000', 'store_1603974844.jpg', '2020-10-29 16:34:04', '2020-10-29 16:34:04'),
(42, '5 BEDROOM FLAT', 'House for sale Jos plateau state', 1, '38000000', 'store_1603975033.webp', '2020-10-29 16:37:13', '2020-10-29 16:37:13'),
(43, '5 BEDROOM', 'Detached duplex for sale, Nwafia street off Agric Bank junction Enugu Independence Layout, Enugu state', 1, '72000000', 'store_1603975397.jpeg', '2020-10-29 16:43:17', '2020-10-29 16:43:17'),
(45, 'LAND', '2 hectare Before mees palace Resort Road Rayfied jos south Plateau', 0, '54000000', 'store_1603975826.jpg', '2020-10-29 16:50:26', '2020-10-29 16:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

CREATE TABLE `item_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobmerges`
--

CREATE TABLE `jobmerges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artisan_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artisan_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `supervisor_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `amount` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobmerges`
--

INSERT INTO `jobmerges` (`id`, `job_id`, `job_name`, `artisan_id`, `supervisor_id`, `user_id`, `status`, `artisan_status`, `supervisor_status`, `amount`, `payment_status`, `created_at`, `updated_at`) VALUES
(9, 6, 'Architect', 2, 68, 11, 'Cancelled', 'Rejected', 'Rejected', 0, 0, '2020-03-15 23:50:16', '2020-08-04 11:45:09'),
(10, 6, 'Architect', 72, 71, 11, 'Cancelled', 'Pending', 'Pending', 0, 0, '2020-03-16 00:00:06', '2020-08-11 16:23:51'),
(11, 7, 'Contractor', 69, 68, 12, 'Pending', 'Pending', 'Accepted', 0, 0, '2020-04-05 15:09:54', '2020-08-06 23:03:02'),
(12, 14, 'Contractor', 2, 68, 2, 'Completed', 'Rejected', 'Pending', 0, 0, '2020-04-07 20:35:09', '2020-09-07 03:11:46'),
(13, 14, 'Contractor', 2, 68, 2, 'Pending', 'Accepted', 'Pending', 0, 0, '2020-04-07 20:35:09', '2020-09-07 03:12:24'),
(14, 10, 'Contractor', 2, 68, 12, 'Rejected', 'Rejected', 'Pending', 0, 0, '2020-04-16 22:12:16', '2020-09-07 03:12:35'),
(15, 25, 'Plaster Ceiling Maker (POP)', 35, 68, 36, 'Cancelled', 'Pending', 'Pending', 0, 0, '2020-06-16 22:26:06', '2020-06-16 22:27:04'),
(16, 27, 'Architect', 22, 68, 2, 'Pending', 'Pending', 'Rejected', 0, 0, '2020-06-17 04:46:22', '2020-08-04 11:27:06'),
(17, 30, 'Architect', 22, 68, 44, 'Pending', 'Pending', 'Pending', 0, 0, '2020-06-20 01:11:08', '2020-06-20 01:11:08'),
(18, 24, 'Contractor', 2, 68, 17, 'Cancelled', 'Pending', 'Pending', 0, 0, '2020-07-12 23:11:44', '2020-07-12 23:12:10'),
(19, 19, 'Contractor', 2, 68, 1, 'Pending', 'Rejected', 'Pending', 0, 0, '2020-07-12 23:12:42', '2020-09-07 03:13:39'),
(20, 32, 'Architect', 22, 68, 48, 'Cancelled', 'Pending', 'Pending', 0, 0, '2020-07-12 23:13:32', '2020-07-12 23:14:00'),
(21, 24, 'Contractor', 2, 68, 17, 'Pending', 'Rejected', 'Pending', 0, 0, '2020-07-12 23:29:20', '2020-09-07 03:16:54'),
(22, 6, 'Architect', 22, 68, 11, 'Pending', 'Pending', 'Pending', 0, 0, '2020-07-19 17:28:49', '2020-07-19 17:28:49'),
(23, 29, 'Electrician', 60, 68, 44, 'Completed', 'Pending', 'Pending', 0, 0, '2020-07-19 17:30:26', '2020-08-09 06:15:44'),
(24, 8, 'Contractor', 2, 68, 12, 'Pending', 'Accepted', 'Pending', 0, 0, '2020-08-04 10:05:32', '2020-09-07 03:18:52'),
(25, 6, 'Architect', 72, 71, 11, 'Pending', 'Pending', 'Pending', 0, 0, '2020-08-11 08:37:44', '2020-08-11 08:37:44'),
(26, 6, 'Architect', 72, 71, 11, 'Pending', 'Pending', 'Pending', 0, 0, '2020-08-11 08:37:52', '2020-08-11 08:37:52'),
(27, 15, 'Architect', 72, 71, 1, 'Pending', 'Pending', 'Pending', 0, 0, '2020-08-11 08:38:57', '2020-08-11 08:38:57'),
(28, 9, 'Contractor', 2, 85, 12, 'Completed', 'Rejected', 'Accepted', 0, 0, '2020-08-22 12:42:15', '2020-09-07 03:54:19'),
(29, 33, 'Architect', 72, 85, 50, 'Pending', 'Pending', 'Rejected', 0, 0, '2020-08-28 13:39:23', '2020-08-28 13:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jobber_id` int(11) NOT NULL,
  `skills_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `jobber_id`, `skills_id`, `status`, `created_at`, `updated_at`, `description`, `address`, `state`, `area`, `phone_number`) VALUES
(6, 11, 4, 'merged', '2020-03-15 22:43:43', '2020-08-11 08:37:44', 'NICE ONE', 'abuja, fct', 2, 3, '0908685785'),
(7, 12, 7, 'merged', '2020-04-03 11:14:11', '2020-04-05 15:09:54', 'po0fiujpoisdjsld', 'oufhpiufd;bpdsubs', 1, 2, '09893939393'),
(8, 12, 7, 'merged', '2020-04-03 11:44:20', '2020-08-04 10:05:32', 'po0fiujpoisdjsld', 'oufhpiufd;bpdsubsvrs', 1, 2, '09893939393'),
(9, 12, 7, 'Completed', '2020-04-03 11:47:02', '2020-09-07 03:54:19', 'po0fiujpoisdjsld', 'oufhpiufd;bpdsubsvrs', 1, 2, '09893939393'),
(10, 12, 7, 'Pending', '2020-04-03 11:55:40', '2020-04-16 22:17:12', 'po0fiujpoisdjsld', 'oufhpiufd;bpdsubsvrs', 1, 2, '09893939393'),
(11, 12, 7, 'Pending', '2020-04-03 12:04:20', '2020-04-03 12:04:20', 'dsoiudsdsvcsdv', 'rsiugifdiushosdisd', 1, 4, 'u932y89e883'),
(12, 13, 7, 'Pending', '2020-04-03 12:05:46', '2020-04-03 12:05:46', 'sjdbf jkbdscjuxbudsb', 'dfidsbfiesb', 1, 4, 'cjisjidjdji'),
(13, 14, 7, 'Pending', '2020-04-03 12:11:11', '2020-04-03 12:11:11', 'qreouh9furphuioerws', 'gdvckmpd\nfk\'l df', 1, 2, '1fknonkfkfk'),
(14, 2, 7, 'Completed', '2020-04-07 19:52:04', '2020-08-09 06:17:11', 'hirwoijegim', 'dfojnohrkogh', 1, 2, '09609606906'),
(15, 1, 4, 'merged', '2020-04-10 21:33:17', '2020-08-11 08:38:57', 'i need a good Architect', 'sauka Airport', 1, 4, '08169401926'),
(16, 22, 4, 'Pending', '2020-05-30 21:40:27', '2020-05-30 21:40:27', 'gffsgg', 'hfhhfh', 1, 2, '69'),
(17, 1, 6, 'Pending', '2020-06-03 00:34:06', '2020-06-03 00:34:06', 'tq4hb et wghnh', 'awesome get cgreds re', 1, 2, '09849403039'),
(18, 1, 6, 'Pending', '2020-06-03 00:50:24', '2020-06-03 00:50:24', 'ergwe get groceries g wrt', 'ergsrtgvcrwt yh hehet', 1, 2, '23232345644'),
(19, 1, 7, 'merged', '2020-06-03 00:56:57', '2020-07-12 23:12:42', 'dytej. re ehthyt nhg', 'by tdj. you jt jyj', 1, 2, '75789089786'),
(20, 26, 4, 'Pending', '2020-06-03 16:17:59', '2020-06-03 16:17:59', 'G', 'Dere', 1, 2, '99'),
(21, 26, 4, 'Pending', '2020-06-03 16:18:04', '2020-06-03 16:18:04', 'G', 'Dere', 1, 2, '99'),
(22, 28, 16, 'Pending', '2020-06-03 17:16:10', '2020-06-03 17:16:10', 'I need your services', 'Enugu', 1, 2, '077000'),
(23, 28, 16, 'Pending', '2020-06-03 17:16:14', '2020-06-03 17:16:14', 'I need your services', 'Enugu', 1, 2, '077000'),
(24, 17, 7, 'merged', '2020-06-10 18:32:39', '2020-07-12 23:29:20', 'I need one now', 'Games Village', 21, 64, '08039202123'),
(25, 36, 24, 'pending', '2020-06-12 17:21:30', '2020-06-16 22:27:04', 'here in sokoto', 'kebi', 6, 6, '09038927440'),
(26, 37, 4, 'Pending', '2020-06-12 17:57:47', '2020-06-12 17:57:47', 'i need this artisan', 'abia', 6, 6, '88888'),
(27, 2, 4, 'merged', '2020-06-17 03:45:34', '2020-06-17 04:46:22', 'samufg  g hg', 'abuja', 15, 30, '65464656546'),
(28, 42, 5, 'Pending', '2020-06-17 10:15:32', '2020-06-17 10:15:32', 'jkgkjgkj', 'abuja', 21, 47, '08039202123'),
(29, 44, 9, 'Completed', '2020-06-19 03:37:48', '2020-08-09 06:15:44', 'needs an electrician right now', 'bwari', 31, 150, '07038927440'),
(30, 44, 4, 'merged', '2020-06-19 04:06:46', '2020-06-20 01:11:08', 'ff', 'ggfff', 20, 45, '-1'),
(31, 37, 4, 'Pending', '2020-06-20 19:29:09', '2020-06-20 19:29:09', 'My self', 'Enugu', 20, 46, '07038927440'),
(32, 48, 4, 'pending', '2020-06-22 11:35:23', '2020-07-12 23:14:00', 'Ok', 'Ogun', 6, 6, '000'),
(33, 50, 4, 'merged', '2020-06-23 23:51:59', '2020-08-28 13:39:23', 'building', 'abuja', 21, 47, '090'),
(34, 51, 5, 'Pending', '2020-06-24 14:39:40', '2020-06-24 14:39:40', 'I want peoe', 'gghhhhh', 15, 30, '08033333358'),
(35, 37, 9, 'Pending', '2020-07-08 06:17:03', '2020-07-08 06:17:03', 'To get my house wired', 'Ali Street', 12, 24, '07038927440'),
(36, 57, 8, 'Pending', '2020-07-08 06:19:04', '2020-07-08 06:19:04', 'For furniture contract', 'I do street', 9, 22, '07085376346'),
(37, 43, 11, 'Pending', '2020-07-30 00:37:45', '2020-07-30 00:37:45', 'tile my place', 'abj', 21, 60, '08038927440'),
(38, 1, 4, 'Pending', '2020-08-11 08:18:39', '2020-08-11 08:18:39', 'an ew', 'abuja', 21, 64, '0909939393'),
(39, 71, 4, 'Pending', '2020-08-11 08:25:31', '2020-08-11 08:25:31', 'gdf', 'abuja', 6, 6, '6547676456456'),
(40, 98, 5, 'Pending', '2020-09-06 19:56:31', '2020-09-06 19:56:31', 'Employment', '25 Gidan Daya Resettlement Estate Abuja', 21, 47, '08028822630'),
(41, 98, 5, 'Pending', '2020-09-06 19:56:51', '2020-09-06 19:56:51', 'Employment', '25 Gidan Daya Resettlement Estate Abuja', 21, 47, '08028822630'),
(42, 102, 16, 'Pending', '2020-09-07 11:43:36', '2020-09-07 11:43:36', 'To have the best experience', '19 Mohammed mustapha way yola', 7, 9, '07041490975'),
(43, 117, 15, 'Pending', '2020-09-18 10:29:20', '2020-09-18 10:29:20', 'I\'m pro in wallpapers fixing and 3d panel insulation with epoxy floor selling and wall, I can do all of this,', 'Airport road lugbe abuja', 29, 137, '09020882138'),
(44, 117, 15, 'Pending', '2020-09-18 10:29:44', '2020-09-18 10:29:44', 'I\'m pro in wallpapers fixing and 3d panel insulation with epoxy floor selling and wall, I can do all of this,', 'Airport road lugbe abuja', 29, 137, '09020882138'),
(45, 117, 15, 'Pending', '2020-09-18 10:29:58', '2020-09-18 10:29:58', 'I\'m pro in wallpapers fixing and 3d panel insulation with epoxy floor selling and wall, I can do all of this,', 'Airport road lugbe abuja', 29, 137, '09020882138'),
(46, 163, 4, 'Pending', '2020-10-29 15:56:51', '2020-10-29 15:56:51', 'juda', 'duwa', 6, 6, '07034927440'),
(47, 164, 5, 'Pending', '2020-10-29 15:59:56', '2020-10-29 15:59:56', 'wada', 'duwa', 6, 6, '07034927440'),
(48, 165, 4, 'Pending', '2020-10-29 16:01:25', '2020-10-29 16:01:25', 'lada', 'lowa', 26, 128, '07034927440'),
(49, 180, 7, 'Pending', '2020-11-03 17:31:19', '2020-11-03 17:31:19', 'All kind of Aluminum Projects/Properties Management/Building Materials supplies and General Contractor', '13 Imole Ayo Street Agbede Ikorodu Lagos', 31, 147, '08030775571'),
(50, 180, 7, 'Pending', '2020-11-03 17:31:45', '2020-11-03 17:31:45', 'All kind of Aluminum Projects/Properties Management/Building Materials supplies and General Contractor', '13 Imole Ayo Street Agbede Ikorodu Lagos', 31, 147, '08030775571'),
(51, 180, 7, 'Pending', '2020-11-05 00:12:52', '2020-11-05 00:12:52', 'All kinds of Aluminum Projects/ Windows,Doors, Partitions, roofing.\nProperties Management.\nBuilding Materials Supplies', '13 Imole Ayo Street Agbede Ikorodu Lagos State', 31, 147, '08030775571'),
(52, 180, 7, 'Pending', '2020-11-05 00:13:07', '2020-11-05 00:13:07', 'All kinds of Aluminum Projects/ Windows,Doors, Partitions, roofing.\nProperties Management.\nBuilding Materials Supplies', '13 Imole Ayo Street Agbede Ikorodu Lagos State', 31, 147, '08030775571'),
(53, 202, 4, 'Pending', '2020-11-05 14:58:55', '2020-11-05 14:58:55', 'I would like to work with this great organisation as a site supervisor/Architect. Thanks.', 'No 2, Olayinka Sanusi street, Arab Road, Kubwa Abuja.', 21, 112, '07069324371');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(9, '2020_02_24_114746_create_artisans_table', 3),
(10, '2020_02_24_115545_create_skills_table', 4),
(11, '2020_02_24_120533_create_jobs_table', 4),
(12, '2020_02_24_121217_create_jobmerges_table', 4),
(13, '2020_02_24_121951_create_itemcategories_table', 4),
(14, '2020_02_24_122000_create_items_table', 4),
(15, '2020_02_24_122018_create_orders_table', 4),
(16, '2020_02_24_122040_create_subscriptions_table', 5),
(17, '2020_02_24_122059_create_contact_uses_table', 5),
(18, '2020_02_24_122505_create_item_images_table', 5),
(19, '2020_02_24_122845_create_user_datas_table', 5),
(20, '2020_02_24_124407_create_areas_table', 5),
(21, '2020_02_24_124438_create_states_table', 5),
(22, '2020_03_03_190648_create_proofs_table', 6),
(23, '2020_05_18_213831_create_tips_table', 7),
(24, '2020_08_04_024628_create_supervisor_data_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('004b22b272c905508ceeaa85ab84bed3279202b20c97a66713c0ab6bf361b6624a45f767e8a9fc29', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:43:04', '2020-04-28 02:43:04', '2021-04-27 22:43:04'),
('005206c6e52f2a339139f4067d4923f33754d6662b284e141fcc2a15adc561b9d952282a2dcd4e2f', 56, 2, NULL, '[\"*\"]', 0, '2020-07-03 22:19:43', '2020-07-03 22:19:43', '2021-07-03 18:19:43'),
('008cd3a9dcce04e9cb902209bcc073f88163b4bbed256b47a77b0ef97e3e7473517190af9bd65689', 1, 2, NULL, '[]', 0, '2020-04-15 21:52:17', '2020-04-15 21:52:17', '2021-04-15 22:52:17'),
('00b924d59fba3d1ab240aa29ef0352dbc1ebf4953181c4c8be366f321bcc8b322e38151f60c5cb9c', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 16:46:57', '2020-04-15 16:46:57', '2021-04-15 17:46:57'),
('010972e34c82640ffc26b39a8e4f8438cf9a2df565065765504bed1053718b72f72cf60eb1675ce0', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 20:13:29', '2020-04-07 20:13:29', '2021-04-07 21:13:29'),
('0113c194ed3cf58e7e3398bec311648e3c65eab89afef7700d2d134b5c112148bdc01d159d6290f3', 16, 4, NULL, '[]', 0, '2020-08-22 12:38:14', '2020-08-22 12:38:14', '2021-08-22 08:38:14'),
('012acb2243beadbb16e841d2032b2ff7ff2ae8369bcf1fdbdacab53fa8b6082d3937bd50a8a15550', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:01:25', '2020-04-06 22:01:25', '2021-04-06 23:01:25'),
('014bab181c80264f14f0651888f8bb19c460f3e2439b689f4a4cad59d7b05541ca0be43aa6bd92e3', 78, 4, NULL, '[]', 0, '2020-08-16 00:28:38', '2020-08-16 00:28:38', '2021-08-15 20:28:38'),
('0168bd3aeaae9fcd02f8ab741af33e2ddad9b7c024fdd4fcae720b8fb44b6785f04304f22eb3ce34', 1, 2, NULL, '[]', 0, '2020-06-16 19:28:23', '2020-06-16 19:28:23', '2021-06-16 15:28:23'),
('01a4de3415da8906a15f4e595d35c0686ddc96eb7434a9e2d96e59d99c85c3ad3c3383d99203bffa', 1, 2, NULL, '[]', 0, '2020-04-16 11:45:27', '2020-04-16 11:45:27', '2021-04-16 12:45:27'),
('020a0f55c4a9171a44cd9737d0ce9d7b7a55e4ad3dc408a55590d94120ca95935feb6fd5fb8ad74e', 1, 2, NULL, '[]', 0, '2020-04-10 17:32:42', '2020-04-10 17:32:42', '2021-04-10 18:32:42'),
('0244df89f638e96882ea062b1d957d945f9eb8718844919dbc530b71699f5665fadf8453247a2330', 1, 2, NULL, '[]', 0, '2020-06-16 19:30:25', '2020-06-16 19:30:25', '2021-06-16 15:30:25'),
('02fb294012a19fb60c0d177720f9ab708b5805060e568bde49dae0e577dabc7db12fe92bf8647688', 1, 2, NULL, '[]', 0, '2020-04-27 18:59:36', '2020-04-27 18:59:36', '2021-04-27 14:59:36'),
('031d9fdcfe474bb73398f932f1c6ebe0d7458b741d6f345b2159502a240f3b58dc9a1c36189b81d3', 16, 2, NULL, '[]', 0, '2020-06-10 18:43:59', '2020-06-10 18:43:59', '2021-06-10 14:43:59'),
('03ebdb29f8ccaaa4a88823d6719de0772d25c5aafc5bce0f83bec40a1cc37d7a67299d249e9a3fad', 1, 2, NULL, '[]', 0, '2020-03-06 14:15:18', '2020-03-06 14:15:18', '2021-03-06 15:15:18'),
('04233e78bb73f77c528b6c5bf2a0a6904a6aeaa01c426e91ef20289f50b4319ef5178e9b83853902', 2, 2, NULL, '[\"*\"]', 0, '2020-06-03 00:42:48', '2020-06-03 00:42:48', '2021-06-02 20:42:48'),
('04c7d04f0e7e9f6d5023577d4ebe3473a3b3dd58cd27c441377afee2b33425e7f7f1df2ca4c80e41', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:38:54', '2020-04-28 02:38:54', '2021-04-27 22:38:54'),
('05e736c0bb3b064d65ffe11b9b065d707d40a28cc01784a0c3918a8b02b419875f4edcfefde5b768', 205, 4, NULL, '[]', 0, '2020-11-05 22:22:23', '2020-11-05 22:22:23', '2021-11-05 17:22:23'),
('068f850a35a67991871cd79d2c8f9a90d1093becf499d9f2d976dbd9f35da4dd5dd927ce9360ca44', 87, 4, NULL, '[]', 0, '2020-08-23 11:09:08', '2020-08-23 11:09:08', '2021-08-23 07:09:08'),
('06dd7ed2b2057a0ff73d574e73c706df042987afde0dd0bb4ef8db3d88933380874ae709f3867e42', 21, 2, NULL, '[]', 0, '2020-05-30 19:20:41', '2020-05-30 19:20:41', '2021-05-30 15:20:41'),
('074a2a237deaa40d77b4141475e3cf949e85768a6b11f59aeee11e27136929772c4d4fb12e700013', 1, 2, NULL, '[]', 0, '2020-06-16 19:19:58', '2020-06-16 19:19:58', '2021-06-16 15:19:58'),
('07bb128c97ef1d02b1e944cbf3d42c93c0fc6ee8494183fa775046e94d8564978187cab640e7b20f', 186, 4, NULL, '[]', 0, '2020-11-04 01:28:00', '2020-11-04 01:28:00', '2021-11-03 20:28:00'),
('08ba4b54f579f0ccd3c23a6add712f939752d938865707c9531cbed53630ad3c949402d6e2feaf5c', 1, 2, NULL, '[]', 0, '2020-04-28 15:07:05', '2020-04-28 15:07:05', '2021-04-28 11:07:05'),
('08e183ac74d8f848e927618ed4be3dd354b68c21378c2bf15cded17b755c6456abbd6594ce5188c9', 16, 2, NULL, '[]', 0, '2020-07-19 17:32:52', '2020-07-19 17:32:52', '2021-07-19 13:32:52'),
('099f052c89fb6feca9b3f6403f977f2167ae3db7cfbd7c9463c9d00a6ab7ccd92d8ba2c40052b43c', 71, 4, NULL, '[]', 0, '2020-08-11 08:25:06', '2020-08-11 08:25:06', '2021-08-11 09:25:06'),
('09a45dcf716f2b7b177c2fe2a568fcd43dc29f7c1df6406c91e275603688151404f104eeca5a7680', 16, 2, NULL, '[]', 0, '2020-04-28 13:37:39', '2020-04-28 13:37:39', '2021-04-28 09:37:39'),
('09e03ffe5d12edc4976f01383b81a9197b0c4b20ac335a761e5b392597d6868b964f0379405c4fb4', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 14:13:13', '2020-04-05 14:13:13', '2021-04-05 15:13:13'),
('0b39f9b307e3a4a2bd1bd736fd52746565fb9c91021a91081270bdbfc8f94ff526e7f11ef02ea9b8', 142, 2, NULL, '[\"*\"]', 0, '2020-09-20 16:49:27', '2020-09-20 16:49:27', '2021-09-20 12:49:27'),
('0b452f49f979aef99c6bfba6c4a2466f3293f9786644cd66c6f741985dfc9889b9bd9611352eb217', 175, 4, NULL, '[]', 0, '2020-11-03 09:41:18', '2020-11-03 09:41:18', '2021-11-03 04:41:18'),
('0ba83fb6266ac94aff65e3eb24906bcb67008e6e4f7d63fb155d9ad8f9f35fc9b7660b4f143b414a', 186, 2, NULL, '[\"*\"]', 0, '2020-11-04 01:21:00', '2020-11-04 01:21:00', '2021-11-03 20:21:00'),
('0cd857cfbe584eb381f8ecdc2a09222e65bdce0292a25492021abc0b3188c4db5fc44dae9f796305', 1, 2, NULL, '[]', 0, '2020-04-27 14:12:29', '2020-04-27 14:12:29', '2021-04-27 10:12:29'),
('0cea37b0508c0afe86410625a23d0cce6b4bd75d6193c7c7acfe794c5a3a001dcf480333d93095b6', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 20:36:30', '2020-04-07 20:36:30', '2021-04-07 21:36:30'),
('0d0f04a598b9a8fe321737e172151778e0e7560a0b9ebb7cf99387c27286aeee5af815c50bf6c55a', 1, 4, NULL, '[]', 0, '2020-08-09 06:17:32', '2020-08-09 06:17:32', '2021-08-09 07:17:32'),
('0e4c7971a50b34b7ef9c4ee671e6ff1bab7d36058c9ad8e6324c40cf42ff34358a48f5afd20237c6', 1, 2, NULL, '[]', 0, '2020-06-17 04:34:46', '2020-06-17 04:34:46', '2021-06-17 00:34:46'),
('0e70fcb9d9b89c5500a0b9e2aa437b0a41a197b52247882f9d8a77718e88d5989c13cdf7015b9324', 1, 4, NULL, '[]', 0, '2020-08-28 13:36:05', '2020-08-28 13:36:05', '2021-08-28 09:36:05'),
('0e9fb6750c1deefc940d4b4a64a3bd6e8cb7588170b8a40f2c799e14211e83de7981206f691587ee', 114, 4, NULL, '[]', 0, '2020-09-18 09:33:09', '2020-09-18 09:33:09', '2021-09-18 05:33:09'),
('0ef99444e4e91fe2fda488173704fc682e6e1d2abcd790f546eaead2d72ea31c9b8fa81a4bf89348', 16, 2, NULL, '[]', 0, '2020-04-28 14:50:35', '2020-04-28 14:50:35', '2021-04-28 10:50:35'),
('0f63cf19e594c83a62a493f3d96df069e45642b4f03dc4513fd3a573af5ca0f065bd15b7978d80a7', 194, 4, NULL, '[]', 0, '2020-11-06 14:05:22', '2020-11-06 14:05:22', '2021-11-06 09:05:22'),
('1049f1e9ae04f614c31e83fcf61a0200a3b976bdac9bb4257b86d71a5a08beb433c2f570e103d18e', 85, 2, NULL, '[\"*\"]', 0, '2020-09-05 01:18:11', '2020-09-05 01:18:11', '2021-09-04 21:18:11'),
('106ba4e09b02ed2e5fd7fd79b23f279ec9edec7f05bbf7cb8a3e5d0e1249be43e17dedbdd801c2a9', 1, 2, NULL, '[]', 0, '2020-06-16 19:29:59', '2020-06-16 19:29:59', '2021-06-16 15:29:59'),
('10e9519b9be586d04396077f99617ef64a245e4fcba85cb56e5b9edf09c75b0c1a32ad15501a3ca4', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 20:04:54', '2020-04-15 20:04:54', '2021-04-15 21:04:54'),
('1231e63004c97e74defcf13f7f7a1e66698d12f2bca7f3194fd53e9792335b79b3cd39bc268c26d3', 153, 4, NULL, '[]', 0, '2020-10-29 14:02:11', '2020-10-29 14:02:11', '2021-10-29 10:02:11'),
('1242967e126589a0501e777020d4e87f20cc09ff3fe180453dd36c500f657ea6f0cf0edb40c0443c', 1, 2, NULL, '[]', 0, '2020-04-26 01:33:11', '2020-04-26 01:33:11', '2021-04-25 21:33:11'),
('1507c4dbd3bafd001d1cecfa49e33ddef5ce1e0eb4ee2664c51e247cf05d72b20ebfc24cc744e6ac', 60, 2, NULL, '[]', 0, '2020-07-19 17:24:31', '2020-07-19 17:24:31', '2021-07-19 13:24:31'),
('16e5ded995bc2e9a88f5a269ec8692eccd4f1bd8b30fa2ed6ea741b3626ecf90349aabc8170c071e', 16, 2, NULL, '[]', 0, '2020-06-16 18:51:05', '2020-06-16 18:51:05', '2021-06-16 14:51:05'),
('1757b6e725cf2bd7393c24f08c9b7fcd5dff9db2df3b8ad2fb0f7574862785502e2439e59beb54b7', 166, 4, NULL, '[]', 0, '2020-11-07 02:46:21', '2020-11-07 02:46:21', '2021-11-06 21:46:21'),
('17755625be89e8b8dc68452de0d5b1fb06c61b3170266b71fef5cdab9e1c11b8c96286befcdec48c', 2, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:17:55', '2020-04-06 22:17:55', '2021-04-06 23:17:55'),
('17a5c5324cf4b5b64780769fac763deb0c5f4f674ea05bc7c16b475fb2cb297e7ed0c2bcccf14ed2', 2, 4, NULL, '[]', 0, '2020-10-27 09:10:44', '2020-10-27 09:10:44', '2021-10-27 05:10:44'),
('17b59c84ce54f56877e074b30f8010bf954420cc1de82d1a35dbe3703aadcdefb8360c7baee69221', 1, 2, NULL, '[\"*\"]', 0, '2020-04-09 08:23:05', '2020-04-09 08:23:05', '2021-04-09 09:23:05'),
('186e9cd85ab9f90e2081510ed24cc4b3b6568b878159e91010c2b809a5fda2f4efec9860b63abd52', 166, 4, NULL, '[]', 0, '2020-10-31 00:57:43', '2020-10-31 00:57:43', '2021-10-30 20:57:43'),
('18f57720f87820762168ce81df93b18d3fdf5f94248f9349fbf50f7a25ed3f26962a17a593a1ada4', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 20:57:40', '2020-03-28 20:57:40', '2021-03-28 21:57:40'),
('1a5bd0ef32e55473d63d94c781a0290fc10538d35d679630f5f6dc517de16220ac2e409ba0fda20a', 191, 4, NULL, '[]', 0, '2020-11-04 11:50:41', '2020-11-04 11:50:41', '2021-11-04 06:50:41'),
('1a7b9236d2acc567d3c4970bcb3ce120569d00adb87aae3bb53479f399f4eccac35eaaeec82178dc', 68, 4, NULL, '[]', 0, '2020-08-17 23:12:37', '2020-08-17 23:12:37', '2021-08-17 19:12:37'),
('1a862bb77eac3adfdbef1e9ac796e0cee30f47ed1e1e3fc761abbbab5846fc47534a0577cd645c66', 212, 4, NULL, '[]', 0, '2020-11-06 16:49:16', '2020-11-06 16:49:16', '2021-11-06 11:49:16'),
('1b1e1003318ee69c6183733031472b431aa5f8590b3eb54a5a3225d1e76a35d1fbdceb78bfb524bf', 1, 2, NULL, '[\"*\"]', 0, '2020-08-19 14:30:08', '2020-08-19 14:30:08', '2021-08-19 10:30:08'),
('1b21a92b1dc58a94a7e7f9fa1bd351dad316ab28b8805b5bf155508032b9a0e70a9ed26fa3f3af4f', 1, 2, NULL, '[]', 0, '2020-04-27 20:10:26', '2020-04-27 20:10:26', '2021-04-27 16:10:26'),
('1be9722310b36681eb692a32149cfe666d8760aa216f6bd1701b97a61b66d393b6f9fa8d4815f99a', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 19:15:24', '2020-03-28 19:15:24', '2021-03-28 20:15:24'),
('1bf4686fd3cfeb4dbb91125b563c36a73e359608b5313cd9bbdcf5ad1f516ac133a98d176367103a', 1, 2, NULL, '[]', 0, '2020-04-28 00:33:03', '2020-04-28 00:33:03', '2021-04-27 20:33:03'),
('1ca2f13e5e4cd5fcf32d709ec6b17c22d8a1ceeaa95dc2da259a2567b9f93afe0bd04d658f8b5d68', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 01:48:46', '2020-04-10 01:48:46', '2021-04-10 02:48:46'),
('1cdb726cfe8bc95859d2192d25b676cea0a1fca185d098a8cbc588a43f611837007a7d9dcafc9696', 85, 2, NULL, '[\"*\"]', 0, '2020-08-28 13:24:49', '2020-08-28 13:24:49', '2021-08-28 09:24:49'),
('1d77ff9235f8697fc265fa4f0538d73f2cc7e960d0ac5156fed61e087015781d5de0cc72bca51ca4', 2, 2, NULL, '[\"*\"]', 0, '2020-04-09 16:46:37', '2020-04-09 16:46:37', '2021-04-09 17:46:37'),
('1dfd68371ee87008002d28dd3fcf9da9d88a3caeee0ff8009a82ca254795c24520d5c66e50c981fa', 2, 2, NULL, '[]', 0, '2020-03-15 23:04:54', '2020-03-15 23:04:54', '2021-03-16 00:04:54'),
('1e44f17760bef1cca8120d5767912f76481fb2f9a6e57cd92f6e115562a59cdbb6b9ed4ccbd6dd32', 1, 2, NULL, '[]', 0, '2020-03-02 14:49:14', '2020-03-02 14:49:14', '2021-03-02 15:49:14'),
('1ef265569c156261d439b060662c6144f841bab58d5387e285e50847499716c45194ae38eb305c1e', 1, 2, NULL, '[]', 0, '2020-04-10 21:31:20', '2020-04-10 21:31:20', '2021-04-10 22:31:20'),
('1f6b6238e59746fd9a4ec220db3851b02a92eb84b649d4d8eb2da0f707ffda329805adcb0ea91c52', 85, 4, NULL, '[]', 0, '2020-08-22 12:32:03', '2020-08-22 12:32:03', '2021-08-22 08:32:03'),
('20795ac1bf52871af19a6d2abaa31c79a1de0fdb85e856ce2102725edc6e33fa42fd722504ad48be', 1, 4, NULL, '[]', 0, '2020-08-20 22:32:05', '2020-08-20 22:32:05', '2021-08-20 18:32:05'),
('20b707a5c9050e282192dee495873164056db9eea2a6a41efcb4a7b1815008c92c9631e9a2af0c70', 61, 2, NULL, '[\"*\"]', 0, '2020-07-19 17:35:57', '2020-07-19 17:35:57', '2021-07-19 13:35:57'),
('20c61868611e6432bca83aab41ebfd22da0f2ff25f38b0624351e1cb1c2f616d225808d3788f0422', 145, 4, NULL, '[]', 0, '2020-09-20 11:45:06', '2020-09-20 11:45:06', '2021-09-20 07:45:06'),
('2123edddce072f4197c70b75fe9f7c2d036bb6866d70fb5dcdc2ab4537aaca78d9c34ead2139d33a', 2, 2, NULL, '[]', 0, '2020-06-17 03:46:17', '2020-06-17 03:46:17', '2021-06-16 23:46:17'),
('21b2c3184cce67c0cd60e890701b9d0fd671688cc57e976837615ed896e00b7aa133871d9125f779', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 22:33:44', '2020-04-08 22:33:44', '2021-04-08 23:33:44'),
('21b6134fd4a3098f6dd4eb299e0247615e6b354d0ec5ad0640ab226bdf7db00e1fb9f7d9affd791f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 23:10:36', '2020-04-15 23:10:36', '2021-04-16 00:10:36'),
('21f79840e4f2482e3d4e0bf6a06679f2059dbcea55fc2ca24d2d71474ef4aaef8a7cc8cec6b97835', 2, 2, NULL, '[\"*\"]', 0, '2020-04-23 12:04:40', '2020-04-23 12:04:40', '2021-04-23 13:04:40'),
('2237a0c86943b69e0b1b98afa13927990bf96d62d8816457611b1faeb6b9e789b19be6604b44b112', 186, 4, NULL, '[]', 0, '2020-11-04 23:42:42', '2020-11-04 23:42:42', '2021-11-04 18:42:42'),
('23943238b1cff8adc0dff3b50739656468d73ce7ae882503d771263bda6ba34e47876cf0ff63d180', 1, 2, NULL, '[]', 0, '2020-04-05 15:00:45', '2020-04-05 15:00:45', '2021-04-05 16:00:45'),
('23b2710ae5435b855f1c546a1a15c042596109427992cd383f06965bf8238d13b277b571530b9aa0', 212, 2, NULL, '[\"*\"]', 0, '2020-11-06 23:21:33', '2020-11-06 23:21:33', '2021-11-06 18:21:33'),
('2423163f8776d90b6c74702d2d7046175f5590a8a0db20a26807920bbeb8bcd74686067df761d192', 16, 2, NULL, '[]', 0, '2020-06-17 10:08:28', '2020-06-17 10:08:28', '2021-06-17 06:08:28'),
('244687f458010feba5bc30cb979fc7846e7c2447d5a5125339175f95ae8aeb63f0d231cc7f55dd01', 78, 4, NULL, '[]', 0, '2020-08-16 00:26:18', '2020-08-16 00:26:18', '2021-08-15 20:26:18'),
('248d0f36b3fb86e7a9d240547fc7cca118470db64710a8dde1f6768643ad25362bc2a50cfba79296', 1, 2, NULL, '[]', 0, '2020-03-15 23:33:35', '2020-03-15 23:33:35', '2021-03-16 00:33:35'),
('2581c077b8eb48526cbdf55fa870e9adbe894ecdfab14d0223532a1316ec55b47903ee73092a284e', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:03:01', '2020-04-06 22:03:01', '2021-04-06 23:03:01'),
('25acfe64668f449a6a3e95e944ec96ab65f97c0c41d86e3a47f781f98f071d269ba5f37b812e7c7c', 166, 4, NULL, '[]', 0, '2020-11-06 23:08:44', '2020-11-06 23:08:44', '2021-11-06 18:08:44'),
('26f0db54d3a1c0896b5f5f2b6fe2c72e54978b26ac61204762c909513884765b4c5ba47db18397ac', 16, 2, NULL, '[]', 0, '2020-06-10 19:10:50', '2020-06-10 19:10:50', '2021-06-10 15:10:50'),
('2731bc195d35ba5111a96b165f9c3d539482fe82322c175cdc47f9c30e266a95f91b51702b3eaabd', 1, 2, NULL, '[]', 0, '2020-06-20 01:10:12', '2020-06-20 01:10:12', '2021-06-19 21:10:12'),
('281b307315fd15aef833bccf90477f51de47a895f09f52d9f5755c8be3166299482b521991599523', 16, 2, NULL, '[]', 0, '2020-05-14 19:21:28', '2020-05-14 19:21:28', '2021-05-14 15:21:28'),
('285560424f672c774009c2548b214bd99706a81d02afdc666017570e3d0ec36f9fc94524cd44c103', 85, 4, NULL, '[\"*\"]', 0, '2020-08-23 02:07:17', '2020-08-23 02:07:17', '2021-08-22 22:07:17'),
('286ddb434c33b92aa095fded348720c5bde0c64ba54e9939c2b63ebd5f892954fdc791e7d42245d0', 1, 2, NULL, '[]', 0, '2020-04-28 14:53:11', '2020-04-28 14:53:11', '2021-04-28 10:53:11'),
('298dd9d1044ebce4249da478f3d05c15984348e3aecca3f63f44f4f6cf46316a22d83cfb3cdb8356', 85, 2, NULL, '[\"*\"]', 0, '2020-09-01 15:48:24', '2020-09-01 15:48:24', '2021-09-01 11:48:24'),
('299441f6aa1e2bb4b19f358098db6efe1e904580686b7a237a9ab00346fbe7f84d0f5cde199c9d32', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 01:57:02', '2020-04-28 01:57:02', '2021-04-27 21:57:02'),
('29f4738417a1a4b00da58197bb7edf1233bc3b541c49519177f3fd432aaaee5f2057b0b94fcaba62', 1, 4, NULL, '[]', 0, '2020-08-06 22:13:13', '2020-08-06 22:13:13', '2021-08-06 23:13:13'),
('2a26ad16c2b458111c42b0337b07402b8a23a0ae47a8091a1d1182818abe4430075e1514c5cad556', 1, 2, NULL, '[]', 0, '2020-03-02 18:00:12', '2020-03-02 18:00:12', '2021-03-02 19:00:12'),
('2a8f64c4259ee285fd3f1175d6b43782a275a222ada8c0cc188bf4ddd292f413aa325ce7a508b8dd', 1, 2, NULL, '[]', 0, '2020-03-02 09:49:21', '2020-03-02 09:49:21', '2021-03-02 10:49:21'),
('2aa427b9bfa1091224c601ceb803fb4a8ead46cfc3993f1e26baa3b33966a37393c057f994127c77', 1, 4, NULL, '[]', 0, '2020-08-09 05:55:37', '2020-08-09 05:55:37', '2021-08-09 06:55:37'),
('2bb727ff4de943ffe0e3bfeef757b379ec64e8acce3bd81f08ac2b252277bb450be5d88dc1ae979e', 16, 2, NULL, '[]', 0, '2020-05-14 19:38:20', '2020-05-14 19:38:20', '2021-05-14 15:38:20'),
('2d15f8b8244eeca7f6e1cb0fd7919b6d1b2bc333608cb81ac57985ededcd23dd5adce48d0bfc374c', 148, 2, NULL, '[\"*\"]', 0, '2020-11-05 13:33:43', '2020-11-05 13:33:43', '2021-11-05 08:33:43'),
('2d7758d0f108865ba50a42e320d2334e5884181e978c65703cab91e3952e0d5bac9775e3ba35051b', 186, 4, NULL, '[]', 0, '2020-11-04 00:23:18', '2020-11-04 00:23:18', '2021-11-03 19:23:18'),
('2e2a4c05fa8b76096b880ec83f5b6e328dae551615f732ef099c6354dadbd8853807f122b5af8dd1', 1, 2, NULL, '[\"*\"]', 0, '2020-06-01 01:14:21', '2020-06-01 01:14:21', '2021-05-31 21:14:21'),
('2e3cfd661ee876ace58ff50999eca211ba3f1f68a8ee81f4d04d742bb910956a27b58fed99e98352', 166, 4, NULL, '[]', 0, '2020-10-30 23:22:51', '2020-10-30 23:22:51', '2021-10-30 19:22:51'),
('2ecfa2d16121ebb8bebbb393a48d309b3ae237a967feec17d947e4b8a3e788cf2339739f8590de44', 16, 2, NULL, '[]', 0, '2020-06-04 11:42:52', '2020-06-04 11:42:52', '2021-06-04 07:42:52'),
('2f0c34009dd98fc390cb7c0d3f468b162ba774f9581199e79487dba8129b2f68e0280d39963ee423', 1, 2, NULL, '[\"*\"]', 0, '2020-06-24 15:28:04', '2020-06-24 15:28:04', '2021-06-24 11:28:04'),
('2f3573c035a5cd75b990a770cae54103c9579f988e277ed540ec4f087e114d4ea43c12b58cf53c20', 1, 4, NULL, '[\"*\"]', 0, '2020-09-12 16:08:25', '2020-09-12 16:08:25', '2021-09-12 12:08:25'),
('2f94f5645f65572f254ae9284fb80db90558bb8405e10f4f65dd76424a9a5957f3cf455e10684c42', 162, 4, NULL, '[]', 0, '2020-11-06 16:45:22', '2020-11-06 16:45:22', '2021-11-06 11:45:22'),
('2f98aec349fabaa368368883241982379c50613438b997673d7823cae612b9fbce40df8c4d3fb1d8', 145, 2, NULL, '[\"*\"]', 0, '2020-09-20 12:17:28', '2020-09-20 12:17:28', '2021-09-20 08:17:28'),
('2fe3f7a073c196f1fb558ce260a84430c43aa6e0360c66c5c10ddee8753452c070de350ecd1306d7', 1, 2, NULL, '[]', 0, '2020-03-13 19:14:05', '2020-03-13 19:14:05', '2021-03-13 20:14:05'),
('313a8a06425ab1c98a6aa7a4ac3ed587d9a660d67bf18e2b779076a40f1f920b26cccaf029cd2060', 16, 2, NULL, '[]', 0, '2020-04-28 14:51:18', '2020-04-28 14:51:18', '2021-04-28 10:51:18'),
('31ea3da4217c231734764cd34793641826f40006befa388e523eee3755a3aa381fa8d91a6559e513', 85, 2, NULL, '[\"*\"]', 0, '2020-08-23 02:52:11', '2020-08-23 02:52:11', '2021-08-22 22:52:11'),
('322cdbc6033c929d732194c5a77e57874031ac166e81594e9ef7dfc74d6ba1f3ca1c6272ba601290', 166, 4, NULL, '[]', 0, '2020-11-01 17:23:33', '2020-11-01 17:23:33', '2021-11-01 12:23:33'),
('32feead6aa1e34c5d9188cd0f5d43243cc66ed9810f0b359fa0ceac3683869be0b0ffce516a65d5e', 1, 2, NULL, '[]', 0, '2020-04-16 21:59:31', '2020-04-16 21:59:31', '2021-04-16 22:59:31'),
('330f6411b5b5d8f04bed12b0788e9cc9fe3486f244adb5a31f8bee32c45620a709df1bf2ce85a632', 16, 2, NULL, '[\"*\"]', 0, '2020-06-25 04:27:41', '2020-06-25 04:27:41', '2021-06-25 00:27:41'),
('339687b32d31d5b7d3c9036d8bbb961c1f6b52132aa107afc4da3d7709324c7fff68a6db1f491bcc', 2, 2, NULL, '[\"*\"]', 0, '2020-08-22 12:12:40', '2020-08-22 12:12:40', '2021-08-22 08:12:40'),
('341eb58a37946f9500c2c2bd56f5f6114ad2f7276869b3abdbb6227713c3619f74597c2ff4926578', 16, 4, NULL, '[]', 0, '2020-08-20 22:32:29', '2020-08-20 22:32:29', '2021-08-20 18:32:29'),
('3461a2051c8d97c0c8d9c3362e84776c5d232b81ee0ef12cd6ac850a21617bbbd35c5b9b75039c67', 151, 2, NULL, '[\"*\"]', 0, '2020-10-29 13:30:54', '2020-10-29 13:30:54', '2021-10-29 09:30:54'),
('354defe66d557f1b6d99bb051173638d9769344ce2d1c8609b44e30827aa523bbb5282af982f287f', 1, 2, NULL, '[\"*\"]', 0, '2020-04-10 20:58:35', '2020-04-10 20:58:35', '2021-04-10 21:58:35'),
('35af6cbaab15a085fdb190777910e1b575153f1981447ffd23d1e4fd547739ce5b82d1c6e3bb6c66', 155, 2, NULL, '[\"*\"]', 0, '2020-10-29 14:45:08', '2020-10-29 14:45:08', '2021-10-29 10:45:08'),
('35c594ba1aeae5479968c104efb15b88a9701e005e9ab1b8b7feacd7c950a0f11c6720fd60912a7f', 2, 2, NULL, '[\"*\"]', 0, '2020-09-07 02:56:25', '2020-09-07 02:56:25', '2021-09-06 22:56:25'),
('3608ed137b9684fee3d67e76bae80db782635aca5a35766a07977e13eaf458742c8b44aca8b56e0c', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 01:49:47', '2020-04-10 01:49:47', '2021-04-10 02:49:47'),
('3617b3d9597948ca0076f5f33d081f96a5d0bfeae36fc5827615eb404d47e8d5fa0d070672e869a0', 60, 2, NULL, '[]', 0, '2020-07-19 17:20:17', '2020-07-19 17:20:17', '2021-07-19 13:20:17'),
('363add703d1574d51c9d3afc133d06f1034ce1496414d5f09d277a4a5f7276b1bf01fd2f17fe8071', 187, 4, NULL, '[]', 0, '2020-11-04 01:49:40', '2020-11-04 01:49:40', '2021-11-03 20:49:40'),
('38635aa7732488952b32e81ebaa685d6ac301fc4bee4541e7ab1470b3170a6c8bf4c0c59f35687f6', 16, 2, NULL, '[]', 0, '2020-04-28 14:57:28', '2020-04-28 14:57:28', '2021-04-28 10:57:28'),
('388984922bf3f7339d13493f15663d7cbdfa7ca51ccb70a8bdb6ba446d4f0a607847f6d685097ffd', 16, 4, NULL, '[]', 0, '2020-11-03 16:04:20', '2020-11-03 16:04:20', '2021-11-03 11:04:20'),
('38e41c6bc2b96f7b0f005390398a54ce5b65b9173c00d7382979b64509f1d205fd309b8728e2f200', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:14:39', '2020-04-06 22:14:39', '2021-04-06 23:14:39'),
('395f7e57b59586696c013df2ce2bdde4debe80704bee655fb5fc2be101a5b4ca2dd36448df513dac', 1, 2, NULL, '[]', 0, '2020-03-15 10:15:10', '2020-03-15 10:15:10', '2021-03-15 11:15:10'),
('3968f72ec7ab1e6ee84483e527d95c915dab716145b7dfc16ae8e3a2f39a55c74e6fe7532b016527', 2, 2, NULL, '[]', 0, '2020-04-16 22:17:06', '2020-04-16 22:17:06', '2021-04-16 23:17:06'),
('39898eb47810b55c128d7971a6e3b6d2d0d0eccfc916395a9cd74f12ff40b2d7098c8bcc3f1f320e', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 13:26:03', '2020-04-05 13:26:03', '2021-04-05 14:26:03'),
('3ae3fcdc93ce8e04cb6bbf50cf0d104ce0713f7e14b72722da63c24445b0ba6a083a1fdd2c64600d', 1, 2, NULL, '[]', 0, '2020-03-02 14:49:37', '2020-03-02 14:49:37', '2021-03-02 15:49:37'),
('3b0c117b680e1f2cd3809d10b1b8b4e98f3445b1c9ae38839e0fb5939e5617a3f0270632d1e9696e', 2, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:53:22', '2020-04-06 22:53:22', '2021-04-06 23:53:22'),
('3b7d6ed736450c323e3a040483bc6e61941cbd2ab77ab8ecb47c5d1026c703070215001542dbf2c7', 11, 2, NULL, '[\"*\"]', 0, '2020-04-15 14:50:08', '2020-04-15 14:50:08', '2021-04-15 15:50:08'),
('3c387784af0810c18f939ecad0f20e68c260e37e4f5baf83925635aea4f0833db87cf068db69ada3', 68, 4, NULL, '[]', 0, '2020-08-04 03:04:31', '2020-08-04 03:04:31', '2021-08-04 04:04:31'),
('3c6268971948b2c9dad90cbe7d199b2143f45045b64cd7a9eb05ce82372bb55360c61ccaa8252a99', 1, 2, NULL, '[]', 0, '2020-03-15 08:23:02', '2020-03-15 08:23:02', '2021-03-15 09:23:02'),
('3c892c61ae62e2d1700fb03a91679d44cae20ab849bc6edcd621b105b4c9c67fd405d5b5f8270a49', 1, 2, NULL, '[]', 0, '2020-03-14 22:27:01', '2020-03-14 22:27:01', '2021-03-14 23:27:01'),
('3e682e5664677258abad0a180c84b4097e083173d47b02897da3fdaf7f55a148c5f9d96311d5500c', 156, 2, NULL, '[\"*\"]', 0, '2020-10-29 14:43:01', '2020-10-29 14:43:01', '2021-10-29 10:43:01'),
('3fcd615e9cf42550e1656e9648114349b44b5a3b18d633b8a0963069121b23e804f35e47c0137366', 167, 4, NULL, '[]', 0, '2020-10-31 11:04:08', '2020-10-31 11:04:08', '2021-10-31 07:04:08'),
('40216d010e0ae6806db53ce2b96bc9a68a54f4c8ad22521df17ecc2fd76ea1e05d45673b62af1526', 2, 2, NULL, '[\"*\"]', 0, '2020-09-07 03:34:38', '2020-09-07 03:34:38', '2021-09-06 23:34:38'),
('4043f591a50fbf8cafb39c5ffb9dddf517d3b36eaf35a58c4c1380ace1e759a2059d80a8ff9e9c0d', 144, 2, NULL, '[\"*\"]', 0, '2020-10-04 12:59:40', '2020-10-04 12:59:40', '2021-10-04 08:59:40'),
('40a2f5f6059b52c36a8602fd54be6cd6a1b745121a383f8329f43178e883c6a2b09892daf91d0a67', 2, 4, NULL, '[]', 0, '2020-08-06 22:12:59', '2020-08-06 22:12:59', '2021-08-06 23:12:59'),
('4123f9d005efa37f3a22f11869d8340d217fcb9981cfa018970230d868c38ad8928f8bed3aa89e61', 16, 2, NULL, '[]', 0, '2020-06-20 11:37:37', '2020-06-20 11:37:37', '2021-06-20 07:37:37'),
('41dd649b8df79ba67ce63f79a2ab3a6a07977307a6905004993eafe8272a8f72fa23e3cee25ab840', 1, 4, NULL, '[]', 0, '2020-08-04 02:08:45', '2020-08-04 02:08:45', '2021-08-04 03:08:45'),
('428d44b833db0e87aec9299c463262bce8a868b7023e7715660d7104b30ce14bf9f69e1e17688d3e', 68, 4, NULL, '[\"*\"]', 0, '2020-08-04 03:13:09', '2020-08-04 03:13:09', '2021-08-04 04:13:09'),
('42f640ee0952faa5083877a74410743b0266186c5f19e96fef980094523a62a4c9e3182455fc948b', 85, 2, NULL, '[\"*\"]', 0, '2020-08-28 13:35:17', '2020-08-28 13:35:17', '2021-08-28 09:35:17'),
('431c75816e05a00f5036725222eb342f774cf4b4b5b6852a1bfd40fc8bfad9f8143825ca2ff38b87', 56, 2, NULL, '[\"*\"]', 0, '2020-07-04 14:10:00', '2020-07-04 14:10:00', '2021-07-04 10:10:00'),
('435517f7e34703b40ee637919f43ad27ae0e2a96892ad7afa184da4768a679617b5a26ac1278fa08', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:17:41', '2020-04-28 02:17:41', '2021-04-27 22:17:41'),
('439a6ca35ea4a098f7dcffc0a3e64473c5ab2aa3a6ad8c85e66292b7938e2b2e42c70797e7aad13b', 183, 4, NULL, '[]', 0, '2020-11-03 20:36:31', '2020-11-03 20:36:31', '2021-11-03 15:36:31'),
('43e7ee02ccc0bfd70e8addfb7eca47b576e4ad1aa818b2d705b1a7d8726a0eec70e2482b6c26ad10', 148, 2, NULL, '[\"*\"]', 0, '2020-11-07 02:10:58', '2020-11-07 02:10:58', '2021-11-06 21:10:58'),
('444440783ecf3300fa32b8afeba29df5aa6793154a39c1fbbf1389b3436d661990d5091033c11543', 56, 2, NULL, '[\"*\"]', 0, '2020-07-10 11:56:12', '2020-07-10 11:56:12', '2021-07-10 07:56:12'),
('45b91a660195292fe635368f64061d092f67d1b0c1046a0508be2c41ca7290e9b2224a0c1dd44f51', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 19:12:36', '2020-04-07 19:12:36', '2021-04-07 20:12:36'),
('45d7172d84035e0ff0ed117e665ff80c570f869e8982996b76eaeb684bd40469732927b20b0ffb7a', 44, 2, NULL, '[]', 0, '2020-06-19 04:07:56', '2020-06-19 04:07:56', '2021-06-19 00:07:56'),
('460f935030b75bc27bb19cd4d8b6134d19f845df0a4c2b69b73fbb3802897cc1848ba956ece5cd89', 17, 2, NULL, '[\"*\"]', 0, '2020-06-24 00:37:24', '2020-06-24 00:37:24', '2021-06-23 20:37:24'),
('46243471bbee38013033f138edca6c2897dd7fb5c4cd520ac58e771c034ac014929592abc1fedc0e', 85, 2, NULL, '[\"*\"]', 0, '2020-08-23 03:04:52', '2020-08-23 03:04:52', '2021-08-22 23:04:52'),
('4632b46274d92bd8a8732c86a34a5124f34e0e33a8ea7d80f5452312613ad91b1e055692ebdd9797', 1, 2, NULL, '[\"*\"]', 0, '2020-04-04 23:34:15', '2020-04-04 23:34:15', '2021-04-05 00:34:15'),
('46c71410c3e61729d7749b52a847d362add6dab6fcc24c22b8d57993a193f4a5dc09e76b95c6de08', 2, 2, NULL, '[\"*\"]', 0, '2020-04-20 16:53:27', '2020-04-20 16:53:27', '2021-04-20 17:53:27'),
('4766db98cc20d3f7a9f03e503609436354eaa917f4999be4e661e02340e0f184af002525dc9aaa10', 56, 2, NULL, '[\"*\"]', 0, '2020-07-10 12:08:48', '2020-07-10 12:08:48', '2021-07-10 08:08:48'),
('47721a4c2465f020092043bf0439456431a2c2e4252e15af60d1ddbd28b63a167cb1603bcf9ff19f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 19:28:08', '2020-04-15 19:28:08', '2021-04-15 20:28:08'),
('47c4757452dc1f9445fc29da328f3950852d973b1c317890bb7d5ba5fd200426cb8f42732a8e3fcd', 56, 2, NULL, '[\"*\"]', 0, '2020-07-04 10:47:46', '2020-07-04 10:47:46', '2021-07-04 06:47:46'),
('483980614604dd81c17f73683f5b14290fbac013ed45b222c37dee0c3adb4ab38e8a37e4ddc346d6', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 15:33:35', '2020-04-15 15:33:35', '2021-04-15 16:33:35'),
('48b4443604246306d2cd1b53c1b6c5a8f1c18a3284f0dd5643e7edb832642f4a036389ba02bb4606', 1, 2, NULL, '[\"*\"]', 0, '2020-07-13 00:58:36', '2020-07-13 00:58:36', '2021-07-12 20:58:36'),
('48e9ca8e9f7bbdec68514b69b2caa087a94be8c6262d5c97004fce7d0c78376551ecb5e5d6c9d789', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:31:54', '2020-04-28 02:31:54', '2021-04-27 22:31:54'),
('490e2c44e50636c94c77fa3f45e7a3e07727862c6fc399c038d487a21d9221c34b84cf499a67fb5b', 148, 4, NULL, '[]', 0, '2020-09-21 16:51:59', '2020-09-21 16:51:59', '2021-09-21 12:51:59'),
('4912b992fb79b31638f045b197210211dc1f9be1108fb43f5981539fae19ec1dbe4ae5615bd416d0', 2, 4, NULL, '[]', 0, '2020-08-05 08:37:51', '2020-08-05 08:37:51', '2021-08-05 09:37:51'),
('4978cb8226d084594a4444576ed7b455ebc4d63973df58121a6e5044e906338ace9ce4fb30900735', 35, 2, NULL, '[]', 0, '2020-06-12 17:06:59', '2020-06-12 17:06:59', '2021-06-12 13:06:59'),
('49c04fb47d552b717606827e5aa5157682ebfabdf3badf841d27b912509297540ec9e82a5083f5ae', 16, 2, NULL, '[\"*\"]', 0, '2020-06-24 14:35:35', '2020-06-24 14:35:35', '2021-06-24 10:35:35'),
('4aa0383e856eafd95e114188c338b8f73756630a01f1518a63f96635c2c879fd998832dbc89517f4', 49, 2, NULL, '[]', 0, '2020-06-23 23:45:17', '2020-06-23 23:45:17', '2021-06-23 19:45:17'),
('4aeb02bb8546228cef7d3736261c1c4fa7b3af7f880dd9c4487519b8476f6cb391e040a691c3cfa0', 56, 2, NULL, '[\"*\"]', 0, '2020-07-02 14:01:31', '2020-07-02 14:01:31', '2021-07-02 10:01:31'),
('4b590bb8d0b9bf5580c83f7b64fb879ced6bef09dc764687c67ddb6df66ed8dd1e0cf0e0ea75244d', 1, 2, NULL, '[\"*\"]', 0, '2020-03-02 14:30:12', '2020-03-02 14:30:12', '2021-03-02 15:30:12'),
('4d087a21e831cf29a927374a9ef6b989ad6d5188b81c39f9a50c153033374249e0b1f12d7e47db95', 65, 2, NULL, '[\"*\"]', 0, '2020-07-30 14:40:12', '2020-07-30 14:40:12', '2021-07-30 10:40:12'),
('4edc60660413a451801ddadb0a0bbe6e4ce80e212bd68525eed914816ca8d31d862ec90c32647397', 148, 2, NULL, '[\"*\"]', 0, '2020-11-04 10:57:34', '2020-11-04 10:57:34', '2021-11-04 05:57:34'),
('4ee2f0c2068e847acb1aefb65b4a40179fa704c72001e584b2048a266dae82fe69e9f11219ad05b1', 212, 2, NULL, '[\"*\"]', 0, '2020-11-07 01:46:20', '2020-11-07 01:46:20', '2021-11-06 20:46:20'),
('4f5e062f17e4a726a4b875f9a6ff2c78715a5886ee10db2c7cf3ccb4b72a888bb756ed195b098f25', 68, 4, NULL, '[]', 0, '2020-08-04 10:45:28', '2020-08-04 10:45:28', '2021-08-04 11:45:28'),
('503f107053e053c224ebe31576b07ea9dcc4f73b2ad0030663a3026ff22fcceb3943f7883a686603', 166, 4, NULL, '[]', 0, '2020-11-06 16:20:12', '2020-11-06 16:20:12', '2021-11-06 11:20:12'),
('50bc082da327225697f78d73ce720b368cc7d89c42b8091e3053a7404a97617d5c51366a9aa71a5e', 1, 2, NULL, '[]', 0, '2020-03-02 10:50:30', '2020-03-02 10:50:30', '2021-03-02 11:50:30'),
('526e9a393dee912349bb23cfe988d98f7a48833ebe4108f3643c984d83e1f5d3247a4fed3a3fd4ef', 68, 4, NULL, '[]', 0, '2020-08-09 06:09:20', '2020-08-09 06:09:20', '2021-08-09 07:09:20'),
('5295229d2acfee99b3da48969ebafd9b0491d8e84310af5452f621768ef150d9df63744f6a56303d', 1, 2, NULL, '[\"*\"]', 0, '2020-04-02 09:49:37', '2020-04-02 09:49:37', '2021-04-02 10:49:37'),
('52d79b58a1a2695793187b9fc219102f052cf09b50a6b3326dd16830d9b6b1933b6cd97212eb76af', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 20:04:20', '2020-04-07 20:04:20', '2021-04-07 21:04:20'),
('52f1f03bc47d220fdb81665f179fdd6fed5c407ea98660951d9470bc525c9dd270e3923ffcb4eff9', 1, 2, NULL, '[\"*\"]', 0, '2020-04-15 23:06:06', '2020-04-15 23:06:06', '2021-04-16 00:06:06'),
('52f31994d87e25286f0a16aa0126fb75868e9b80ac5d07d59db28a89f94581ba90129540b5c5cda2', 65, 2, NULL, '[\"*\"]', 0, '2020-07-30 15:05:22', '2020-07-30 15:05:22', '2021-07-30 11:05:22'),
('530bd87cbff0f44c833c1efe861a16037478782e26ebfccc24bf9ed074f756766a0db813bdfd6a48', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:19:51', '2020-04-06 22:19:51', '2021-04-06 23:19:51'),
('5318e91580bc55726beb7bd8b2fd3f984db890eaa0decc97bbfad5a20a40d3bfc1fcab141025cfa7', 85, 2, NULL, '[\"*\"]', 0, '2020-08-23 02:45:26', '2020-08-23 02:45:26', '2021-08-22 22:45:26'),
('5323cf75bb6fb899f5acfa1ebfad009e892d4f1539b5692e196e5347f2f436d51ec016f1c029e4ff', 55, 2, NULL, '[\"*\"]', 0, '2020-06-24 16:51:38', '2020-06-24 16:51:38', '2021-06-24 12:51:38'),
('5332b93786a3258dff28e5ba027f604d9a659970e65f0083802fd035d2c7bf5c8d4739e311b91044', 1, 2, NULL, '[]', 0, '2020-03-06 14:15:34', '2020-03-06 14:15:34', '2021-03-06 15:15:34'),
('535283019271804033f61b6716697b8cbfe37903c92ee3f6ec3a37439c904afa85b6953abe93b269', 186, 4, NULL, '[]', 0, '2020-11-04 00:23:31', '2020-11-04 00:23:31', '2021-11-03 19:23:31'),
('5375807605531d085c9d43481bc7802488e06a13375765e46b358f8d8982e1d17e95be18193fd815', 167, 4, NULL, '[]', 0, '2020-11-06 19:00:25', '2020-11-06 19:00:25', '2021-11-06 14:00:25'),
('53d98dcd86e109994cef8334cd0a4cece0069a0d800f887d18d0900919635a23fd418b78ad22cebb', 120, 4, NULL, '[]', 0, '2020-09-18 11:10:00', '2020-09-18 11:10:00', '2021-09-18 07:10:00'),
('5403e532ef17af0d672372472bc05cc55c99e65cf5dbdc29fb2de660731f855e174f75657a28b814', 213, 2, NULL, '[\"*\"]', 0, '2020-11-07 11:09:51', '2020-11-07 11:09:51', '2021-11-07 06:09:51'),
('5417aad37677d3476c645b3036959341d21c45ebe3ef1e1e6a6447963e8716162599fa170cb2b443', 1, 4, NULL, '[]', 0, '2020-10-27 09:11:11', '2020-10-27 09:11:11', '2021-10-27 05:11:11'),
('54975b894f04f47379d8d0af7c29daf0c27ee3d3e541ab62fc49c741920e4cac558675f08cd3a128', 148, 2, NULL, '[\"*\"]', 0, '2020-11-04 14:59:05', '2020-11-04 14:59:05', '2021-11-04 09:59:05'),
('54f49fbb838df04a3cb1dd78c0a20e523cfef158348ded5d17def6ab1ff41d50a56bda31f072a2be', 85, 4, NULL, '[]', 0, '2020-08-23 01:59:25', '2020-08-23 01:59:25', '2021-08-22 21:59:25'),
('559947817cdd679f4da87b7171b8fa365a97b3fa37af99ea8465adad3f473f9078b72ce4b0b4eb63', 148, 2, NULL, '[\"*\"]', 0, '2020-11-08 12:41:39', '2020-11-08 12:41:39', '2021-11-08 07:41:39'),
('561fe0ca3e20c9f44430bc778c28de37376c5355158be049feb81159a7c71169aef141baa4ffa585', 1, 2, NULL, '[]', 0, '2020-04-27 19:00:47', '2020-04-27 19:00:47', '2021-04-27 15:00:47'),
('56ba35f3ca675f9a22570c58b4b6afcfedfc9c9a9ede6c63380ca9c64684f51e503ccda93e4817c0', 142, 2, NULL, '[\"*\"]', 0, '2020-09-20 13:33:01', '2020-09-20 13:33:01', '2021-09-20 09:33:01'),
('571f2bae09d635682e0549fe815cfca447f4d7b57dd208dbb4d6f0ac610ecd4332793437dd293d76', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 03:51:26', '2020-04-28 03:51:26', '2021-04-27 23:51:26'),
('585e287e348d6bab90c0e08d9304fe4fd14ac01d9ed4c686cbd1ec89db4412834cec7bc261e816d7', 2, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:35:38', '2020-04-06 22:35:38', '2021-04-06 23:35:38'),
('58eb570d71252d714854cf6280894afa5d4f22f8d7d73c04937a117d6ced32977c55cc83c00be748', 68, 4, NULL, '[]', 0, '2020-08-11 09:08:17', '2020-08-11 09:08:17', '2021-08-11 10:08:17'),
('594386fdc2253a6c7d109759f104332c80d76774f6ffd92c885085bf28e97af4ff60ef62ab283ea1', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 21:22:49', '2020-04-07 21:22:49', '2021-04-07 22:22:49'),
('5a2c134db3e86b32e025e4b6ba6cb7184d3a740406dbe373ac2299e0933e62818bb75ce4ff509260', 193, 4, NULL, '[]', 0, '2020-11-05 13:50:01', '2020-11-05 13:50:01', '2021-11-05 08:50:01'),
('5a53ea1ff5934e7488fecdfcd6f59617ca48502bea317808a1a818bb8d1cbb434c9ab2e91af9a755', 16, 4, NULL, '[]', 0, '2020-10-12 17:33:51', '2020-10-12 17:33:51', '2021-10-12 13:33:51'),
('5af8b583e2ee181c9fb356639b0d4f181b2fcf88da8c0538ecbba6b33ef180d8ef968439429b3464', 16, 2, NULL, '[]', 0, '2020-06-17 10:16:35', '2020-06-17 10:16:35', '2021-06-17 06:16:35'),
('5b4c5fc93dc24a9a8008e5e95bc84d05fe6d01f66f94fe19690bdee1367a1ef14fe380802981c6ce', 1, 4, NULL, '[]', 0, '2020-08-06 23:02:16', '2020-08-06 23:02:16', '2021-08-07 00:02:16'),
('5c378437240292df401af31f69d5f980e2626c4ee6753706c8cb0f86b0dd88121f8306960f8f5484', 148, 4, NULL, '[]', 0, '2020-09-20 14:29:35', '2020-09-20 14:29:35', '2021-09-20 10:29:35'),
('5ce26e90f617096ddc16698130490dae17b6e6648b9965020227b38c11a96928f3cc2831fe2d410e', 148, 2, NULL, '[\"*\"]', 0, '2020-11-04 18:48:12', '2020-11-04 18:48:12', '2021-11-04 13:48:12'),
('5ce30af5d6e22be2be27727800eebf7e9e51c932edf3f99ce46cb36d5b155bcfa9a44bfb91cf97a0', 104, 4, NULL, '[]', 0, '2020-09-07 20:05:49', '2020-09-07 20:05:49', '2021-09-07 16:05:49'),
('5cfac16c4e78554a81145c90fbac6102afce3bf5f5d6914d52fde5085d86792d31328ad8fb13b17a', 85, 4, NULL, '[]', 0, '2020-08-22 12:40:03', '2020-08-22 12:40:03', '2021-08-22 08:40:03'),
('5db3c046b501d7b3c46931d9391942aefd35ec39d01afe91c85f0d744a3e8b482d4a8c2f4af3cd98', 180, 4, NULL, '[]', 0, '2020-11-03 18:03:23', '2020-11-03 18:03:23', '2021-11-03 13:03:23'),
('5dbb40c0d6f213d999b865821deddd24d6c37db693ee09fae70527362fb09eb6d39b52bf704df07c', 85, 2, NULL, '[\"*\"]', 0, '2020-09-01 16:14:48', '2020-09-01 16:14:48', '2021-09-01 12:14:48'),
('5f156863ff9df917acf0ef7232d1ab428257873a2b53caa16be75a0aab6e1f20a44443246204d947', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 13:11:49', '2020-04-05 13:11:49', '2021-04-05 14:11:49'),
('5f6a5404bba5554fe264a844fd0a0ab787a19821a582ef35608d72f550eaa442e841bac31271cdd5', 56, 2, NULL, '[\"*\"]', 0, '2020-07-03 22:24:29', '2020-07-03 22:24:29', '2021-07-03 18:24:29'),
('60a9ad58734cf20b4a4f9015054f0dbd91f8bb5c591f8377345eae9819a607ce6611e690e3c77a43', 1, 2, NULL, '[]', 0, '2020-03-06 12:43:12', '2020-03-06 12:43:12', '2021-03-06 13:43:12'),
('6108799e8fe251df9b0f4a89e0c3b679c05f84e2f4dc611a119e7cc49076299fa793bc76a11ae50d', 1, 2, NULL, '[]', 0, '2020-03-02 10:49:00', '2020-03-02 10:49:00', '2021-03-02 11:49:00'),
('6175d0e2af62c4e51aa8115529ab9e05dcc27c446b47bb4cfb6c1cb0636ce6afc7b156678a7785f5', 153, 2, NULL, '[\"*\"]', 0, '2020-10-29 13:56:38', '2020-10-29 13:56:38', '2021-10-29 09:56:38'),
('61e72decbacb7b30bae570faf87b10680d1b251d6b07051cd76081e7edb53750ad9369dd0eb0556b', 85, 2, NULL, '[\"*\"]', 0, '2020-09-07 03:45:36', '2020-09-07 03:45:36', '2021-09-06 23:45:36'),
('6218e46c1d84605fb3c1ead434841c034a7d8f69a754acad72c9080491e576007dcfe91decd9a0f5', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 17:18:24', '2020-04-10 17:18:24', '2021-04-10 18:18:24'),
('627ba587f34c454a570d7fe8cb5496f9e63cf00190da75fe1c95c301a3f231575a7ad0272d82c595', 16, 2, NULL, '[]', 0, '2020-05-21 12:02:33', '2020-05-21 12:02:33', '2021-05-21 08:02:33'),
('631160212996f9035c0388be653db1ec00ded5802f448a555e47e526380e73fcad452171dc65059e', 192, 4, NULL, '[]', 0, '2020-11-04 12:38:18', '2020-11-04 12:38:18', '2021-11-04 07:38:18'),
('6342b877f2407b09fcb3aed0e4f574f5468cd8570677e76d1f4e10b33375b4b2e3c714f830e171c3', 85, 4, NULL, '[]', 0, '2020-08-22 12:44:55', '2020-08-22 12:44:55', '2021-08-22 08:44:55'),
('636e5f92beedc01b0a477621ffb2d59db290a5eb26b70684f7e17eb18fc49bcc748d03d7d9294c30', 1, 2, NULL, '[]', 0, '2020-04-04 21:46:43', '2020-04-04 21:46:43', '2021-04-04 22:46:43'),
('63d373f15987ecd70797ed5cf54f4c72b8802e0afc80ce6ec7f5b39c7cf929295af512a5e4c42051', 1, 2, NULL, '[]', 0, '2020-07-07 01:10:57', '2020-07-07 01:10:57', '2021-07-06 21:10:57'),
('64d5d86f23a1931ae781f97d5f9569178db9bf17c57fbaa2df3b28c493db78e29705be5a6df2d275', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:30:22', '2020-04-28 02:30:22', '2021-04-27 22:30:22'),
('67b331013881fc1f41eb64356db8e7ae5a36d13b4e6481f193e0ac50b31e9988db39460bd6a269d8', 2, 2, NULL, '[]', 0, '2020-04-05 15:13:32', '2020-04-05 15:13:32', '2021-04-05 16:13:32'),
('6851cb607a23b81fd3cecee3cdc5496e0c2f6c791375d581350dfa5259261d434e88133e86ecf8b0', 1, 2, NULL, '[\"*\"]', 0, '2020-06-01 01:13:10', '2020-06-01 01:13:10', '2021-05-31 21:13:10'),
('6b8ef6977e9f4c9e9efa23c78eea0c505ec67bfbc112c7e84a60e21789f5f81db340d7a13f309c94', 85, 2, NULL, '[\"*\"]', 0, '2020-08-28 13:25:00', '2020-08-28 13:25:00', '2021-08-28 09:25:00'),
('6b9f71bfc7b3e38e6c66c86bacd5945a3da5395e936cce4130afff7c1f1f85dc8a3ea49155c304a3', 2, 2, NULL, '[\"*\"]', 0, '2020-04-23 12:08:24', '2020-04-23 12:08:24', '2021-04-23 13:08:24'),
('6c733068a9a2ea91311cd94fcbd8c9ddccfbd21279e4c9bd8c4b5b13785696386c3276733e6f0804', 174, 4, NULL, '[]', 0, '2020-11-02 23:31:17', '2020-11-02 23:31:17', '2021-11-02 18:31:17'),
('6d6aa72c10992db81689fd8b53087bc6dabb902320611d10576837b04b457b6c0235aacb07ae94e4', 186, 4, NULL, '[]', 0, '2020-11-04 00:21:39', '2020-11-04 00:21:39', '2021-11-03 19:21:39'),
('6dcf01c1146815c325f0ae5ad4caef0844b28ced5cf0f0f95dce7692b7698e3777c718caa909d48c', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 23:25:52', '2020-04-08 23:25:52', '2021-04-09 00:25:52'),
('6e27a29154818ea397648f8580d760514a78878e36b0ecdebea24bf14f8646580eeb2e80e62a51ea', 1, 2, NULL, '[]', 0, '2020-04-07 20:28:44', '2020-04-07 20:28:44', '2021-04-07 21:28:44'),
('6f2f9f3dfc363788f20ef4a25a97f289c567e5074e476a0f6d274d4ea9f8a0c856049fa0ceaa86b0', 1, 2, NULL, '[\"*\"]', 0, '2020-02-24 09:35:22', '2020-02-24 09:35:22', '2021-02-24 10:35:22'),
('6fc4a89c877cd6c94c77c14f99049005cc4b54ece4a7022b5b7dbb239dfb3b0983b955cd6b32b758', 1, 4, NULL, '[\"*\"]', 0, '2020-08-04 09:18:47', '2020-08-04 09:18:47', '2021-08-04 10:18:47'),
('7033d7e4416610652e9b24776e0a6c55ed4023a2f83154c8a75f28c2dd7bb1426d272333552a787a', 142, 2, NULL, '[\"*\"]', 0, '2020-10-17 20:03:17', '2020-10-17 20:03:17', '2021-10-17 16:03:17'),
('70b2357e0d99b8b6e950947fb61edbb649bfd0ae57e3b1aa9a030366c6b12cee425634b80b5dc386', 2, 4, NULL, '[]', 0, '2020-08-04 10:44:59', '2020-08-04 10:44:59', '2021-08-04 11:44:59'),
('70c58c826388305d728d4aae0ce8e307770660e49ef18b19b2aa9d9210262b41408f25db9d604d84', 16, 2, NULL, '[]', 0, '2020-07-26 17:35:35', '2020-07-26 17:35:35', '2021-07-26 13:35:35'),
('710b5d92b7f26efa47bb159a9f8eba5b40a4e0ef659293c146a1f5ebdb7c4d88bb7733586d6803cf', 11, 2, NULL, '[]', 0, '2020-03-15 22:45:09', '2020-03-15 22:45:09', '2021-03-15 23:45:09'),
('71aa0065c53062c45a4b1fcbab8bbdb71af58d1caff1693079092c897aa4019d52e8a4250a60e094', 176, 4, NULL, '[]', 0, '2020-11-03 11:44:02', '2020-11-03 11:44:02', '2021-11-03 06:44:02'),
('722992f5264d3ae731ceb5575524935e60192f4a4df76533bd367c78c60ff1c4a705b2c5f6506e23', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 21:43:56', '2020-04-05 21:43:56', '2021-04-05 22:43:56'),
('732c6393c66fd3dd7b78c5695d168af70b1c89313c952c9e8ac183506e27d7f26884317d814630d7', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:46:41', '2020-04-28 02:46:41', '2021-04-27 22:46:41'),
('739937fa5bddf0deff52f1cd7edf869d1a7fd343f821a9842be03d85abd615c0654992071f598b73', 68, 4, NULL, '[]', 0, '2020-08-05 08:39:05', '2020-08-05 08:39:05', '2021-08-05 09:39:05'),
('73c69140dad556d7c129cce7737cdb0939cc8810c9711f42a4ba09d1d9f7c1cdce6ad987e453812c', 69, 4, NULL, '[]', 0, '2020-08-06 23:03:25', '2020-08-06 23:03:25', '2021-08-07 00:03:25'),
('742b7a2876f2b55d663ca3c6ab2cfb7d124d74608ef9ad0bdb52e7dfd1508aa543a86dc66a535e91', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:35:19', '2020-04-28 02:35:19', '2021-04-27 22:35:19'),
('74d3b8774c84286691775ef3f5b0a10567ab7e21ba361411c5136397b50b2f15335fc51ee9252723', 2, 2, NULL, '[\"*\"]', 0, '2020-04-20 16:54:21', '2020-04-20 16:54:21', '2021-04-20 17:54:21'),
('7673bfa27a3c0d3601a88c838e00a469d553376feacec74bdb0936fb26a53d1bbae8d26debe7a69c', 1, 2, NULL, '[]', 0, '2020-03-02 10:43:35', '2020-03-02 10:43:35', '2021-03-02 11:43:35'),
('7698e8b1c71ab88c3e922ffdfc893010c43ab5660ffc53706efbda734af76d8faf12f72122a84cad', 71, 4, NULL, '[]', 0, '2020-08-11 08:21:31', '2020-08-11 08:21:31', '2021-08-11 09:21:31'),
('770ab9ae16ff322cc70a4de1843e98e28db98ea04057faa7fa566f0e77e4637debf24e84ed6af39d', 1, 2, NULL, '[\"*\"]', 0, '2020-04-10 17:24:24', '2020-04-10 17:24:24', '2021-04-10 18:24:24'),
('78fd55ca22b9dc1ee1072e7cb8c87b793b3435d6dda6f5f26719bd9002ca7d9abfac5c12d27e1990', 1, 2, NULL, '[\"*\"]', 0, '2020-04-10 17:27:37', '2020-04-10 17:27:37', '2021-04-10 18:27:37'),
('7905ad4982b06c1ae6c18d8313de3bb892161e27093171b920b10d29682d9400d6650aa7df3872a0', 69, 4, NULL, '[]', 0, '2020-08-06 22:59:49', '2020-08-06 22:59:49', '2021-08-06 23:59:49'),
('7a56b0efe949169db2b3a35faecaa9dcb165e8fe2f2c3ef2aea71b4b038d121c38f1752f1aec07b5', 17, 2, NULL, '[\"*\"]', 0, '2020-06-24 14:36:15', '2020-06-24 14:36:15', '2021-06-24 10:36:15'),
('7ac6a4e03957fa7235fa4b9d6d15995a042df93edeb4ebbe6925f4bee02dae9972f0d63aaf00ab8a', 16, 2, NULL, '[]', 0, '2020-05-23 22:16:46', '2020-05-23 22:16:46', '2021-05-23 18:16:46'),
('7b055563595560c9c2f5f0574af27dc2e97882f60d5dd95bc355e3ed3e8a4a87b2e0088943a77005', 1, 2, NULL, '[\"*\"]', 0, '2020-04-15 16:29:20', '2020-04-15 16:29:20', '2021-04-15 17:29:20'),
('7b1ef49f20598a930b31ece8674f734de68cd782dfd2fdc04ec15d30ec2e1a10810b18e429e0998e', 166, 4, NULL, '[]', 0, '2020-11-08 12:51:34', '2020-11-08 12:51:34', '2021-11-08 07:51:34'),
('7bab5d4cb68549f0cb2fcd321929966841f47c486728a7748f83e2a40a606135d9a281b6b50cf125', 212, 2, NULL, '[\"*\"]', 0, '2020-11-07 11:02:05', '2020-11-07 11:02:05', '2021-11-07 06:02:05'),
('7c55c11cbc5ed510c0b56250cf200ac184b3a98e6f22fd35211a8096a8f3f36b32cbdf513b1b6b25', 166, 4, NULL, '[]', 0, '2020-11-03 20:43:01', '2020-11-03 20:43:01', '2021-11-03 15:43:01'),
('7d2d81689c7e2f8233825b985050d6b31abcd03af22b677953dcbfc3287c0395afdf08463f0ece69', 1, 2, NULL, '[\"*\"]', 0, '2020-04-09 08:31:15', '2020-04-09 08:31:15', '2021-04-09 09:31:15'),
('7e54ceab7fb6e976bbc40762d59006885743b830ba15cbfd1da030231ebc5310fdcd2734df12116c', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:40:39', '2020-04-28 02:40:39', '2021-04-27 22:40:39'),
('7ed092860b6091dc37f36ff216b277df4cdf3c7d26d64d1554dd38f85545de711f9a3a96ed11d256', 1, 2, NULL, '[]', 0, '2020-03-02 14:13:40', '2020-03-02 14:13:40', '2021-03-02 15:13:40'),
('7f452d42e4bac355adbd1fd412b9da54a865b60c2f3662b6bae6d6a64fa5c8bc2dd429b3aa74ec01', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 13:13:43', '2020-04-05 13:13:43', '2021-04-05 14:13:43'),
('806c199079c75e3b97d52de3a985bc59849b0b7bdac0eeaf3314340624c2c72277e09520969e543f', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 12:53:14', '2020-04-05 12:53:14', '2021-04-05 13:53:14'),
('80ad27a2dd839d33a811035300a305aaff8f1fa6f26c2cb27b861e95112c2e3cd86d7cf15963db09', 166, 4, NULL, '[]', 0, '2020-11-08 03:22:30', '2020-11-08 03:22:30', '2021-11-07 22:22:30'),
('80e66db934e11ea82272f458f28b63ba90d7eae6e2f7dfbbf54159f6b709fa0acec798cd1ed0e018', 16, 2, NULL, '[]', 0, '2020-06-17 10:17:03', '2020-06-17 10:17:03', '2021-06-17 06:17:03'),
('81105766619e69110604442b259a2d17ff5956343232b99792073c38ec1f997239ef711d56d68cdf', 16, 2, NULL, '[\"*\"]', 0, '2020-07-26 17:11:14', '2020-07-26 17:11:14', '2021-07-26 13:11:14'),
('81135ff4bf20931d01cab04449dc255e5b4d3ea464e1b32898bbd4c3c93b64a48f3b9bead15ea5f9', 56, 2, NULL, '[\"*\"]', 0, '2020-07-05 12:13:16', '2020-07-05 12:13:16', '2021-07-05 08:13:16'),
('8128fc2fe625f26c9aee49823ca57d2091928bfd5b5752aa4d120a020d73feeb70bfbec0605d216b', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 01:32:35', '2020-04-10 01:32:35', '2021-04-10 02:32:35'),
('816b4095e797796a1a424b7e032924c76de91ffe9e17a98e031acdcf6b45d5cd70ae3bac8c12c924', 87, 2, NULL, '[\"*\"]', 0, '2020-08-23 11:25:14', '2020-08-23 11:25:14', '2021-08-23 07:25:14'),
('81e6b34becf23c90022b226883b52126ba1c8ffb2a90dc0392e4d75bfc67319c86f663b74a5eed6b', 186, 2, NULL, '[\"*\"]', 0, '2020-11-04 23:31:51', '2020-11-04 23:31:51', '2021-11-04 18:31:51'),
('822f538ce694cde0ea7875efb4296a1f98dde5e01dd23e7ed6d5ac95325b6361fbefcd19ececb56c', 16, 4, NULL, '[]', 0, '2020-08-22 12:40:41', '2020-08-22 12:40:41', '2021-08-22 08:40:41'),
('827b454361bff6dc3026fcc4a4c2fc33ac555ed79f53b14ab446424d621620fd6ad473de49e05aa1', 2, 2, NULL, '[]', 0, '2020-04-15 20:40:02', '2020-04-15 20:40:02', '2021-04-15 21:40:02'),
('82af5f2382dcc4bc82998579a5cccd3733a525b59fb494f236423b8d43e5c6c76721e6bb154854a3', 2, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:15:45', '2020-04-06 22:15:45', '2021-04-06 23:15:45'),
('82cb6415e59b639324a7f2eb37e8b740e3f6cff96cbd4979a7c451366b02812c61d754644534d5fd', 144, 2, NULL, '[\"*\"]', 0, '2020-11-05 19:16:44', '2020-11-05 19:16:44', '2021-11-05 14:16:44'),
('83e6c49967ce0adb1973bfc55d2ce38701cffd6deb42ba6fb12597d470394c2f0fb615cae5a5fd2d', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 23:00:12', '2020-04-08 23:00:12', '2021-04-09 00:00:12'),
('848735b4308b9564aa7ca9e538d0e14f44b42cf0963af221c7b48ca7f1593bf5d999271fcd1c7ddb', 16, 2, NULL, '[]', 0, '2020-04-28 00:34:05', '2020-04-28 00:34:05', '2021-04-27 20:34:05'),
('84cb33f111e02350e01ef8735d82b9b02d4d43bd266b14746feac58c082f61874d3ce6c5e5550c5f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 21:10:25', '2020-04-15 21:10:25', '2021-04-15 22:10:25'),
('85409d62e33d8578deff5c64cbee373f6021497b6787a21e82b94ccee7bc568c19dda622873e63d0', 166, 4, NULL, '[]', 0, '2020-11-02 20:14:57', '2020-11-02 20:14:57', '2021-11-02 15:14:57'),
('85e375a22d3c15be3c2e14f21f2b1571d2614d0bb8a3aed8db7403c0443a17b5b42ce860e1b42179', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 20:55:51', '2020-03-28 20:55:51', '2021-03-28 21:55:51'),
('85ee9ed27d440fe84ec4b5c3f2616e8cee66f33e4e0bc74892ceab49189f38bdacfe09097523b82e', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 03:30:18', '2020-04-28 03:30:18', '2021-04-27 23:30:18'),
('872eb438d99fd889fd09d97c6c6b0ab38b76bff6890e9e109bdefe4ac46576e954ad4be49e00e7ee', 16, 2, NULL, '[]', 0, '2020-05-30 15:31:53', '2020-05-30 15:31:53', '2021-05-30 11:31:53'),
('87a64eface40a5f35843cbb492110f8ec79b9ae3fb3aed61ee746bdb35c82d0b8875ac76fa6ec8a8', 1, 2, NULL, '[\"*\"]', 0, '2020-04-15 15:02:58', '2020-04-15 15:02:58', '2021-04-15 16:02:58'),
('880d35a4ece2de88fc14f753147689a3fd3a97523725757c0a2bd25d7eceb51e0db5dd56d7df1e7a', 16, 2, NULL, '[]', 0, '2020-05-30 21:26:40', '2020-05-30 21:26:40', '2021-05-30 17:26:40'),
('88757e6636c269dba47acd21b3c237ea5acd98c30488a2f3ffe9a38d0c02b9843c671e6119a31708', 16, 2, NULL, '[]', 0, '2020-07-19 17:23:35', '2020-07-19 17:23:35', '2021-07-19 13:23:35'),
('88837aa915ea03b3f452bb71d10e3402f273bb93fb21627d4e6b83007b69d3c8ded1afb3b405b751', 2, 4, NULL, '[]', 0, '2020-09-07 03:30:18', '2020-09-07 03:30:18', '2021-09-06 23:30:18'),
('88a9d3c20c31c40730d090aabf9f2de6958ad18dd134bacbaf80cafa2a6d5b15d86cf38a55e2510a', 124, 4, NULL, '[]', 0, '2020-09-18 14:17:16', '2020-09-18 14:17:16', '2021-09-18 10:17:16'),
('88c034de5321cfb5272d47218c8415fac49fb1c0924b6dbcbb7d143adaa3525baed66cd8d15417b2', 16, 2, NULL, '[]', 0, '2020-04-28 00:32:37', '2020-04-28 00:32:37', '2021-04-27 20:32:37'),
('894532d5dfe2983aa1f81820656d3174d739110e6ca202abc464e6b12c6fea59789ad3cb834409a7', 1, 4, NULL, '[]', 0, '2020-08-11 16:21:41', '2020-08-11 16:21:41', '2021-08-11 12:21:41'),
('89519570805951e4e7efc68b6ee6d7d50944633038b0433b26d14f498f5006337398697978cff6da', 1, 2, NULL, '[]', 0, '2020-05-21 12:15:08', '2020-05-21 12:15:08', '2021-05-21 08:15:08'),
('896dfc317129e8739027b6c325e3e9d67c9bb3156d992409010a54307fd32984a409b1f8512b4d30', 56, 2, NULL, '[\"*\"]', 0, '2020-07-03 22:07:06', '2020-07-03 22:07:06', '2021-07-03 18:07:06'),
('8986712b2664240d39912722984e674df7610f8fd3accd6fdf44337f14c35252e885ce838221d7b1', 148, 2, NULL, '[\"*\"]', 0, '2020-11-05 00:30:18', '2020-11-05 00:30:18', '2021-11-04 19:30:18'),
('8a6054392276530c88e82921d562d0ec4ddd408d7d6831ce9ef5b554b9cffbb53a102b9e61e522f6', 16, 2, NULL, '[]', 0, '2020-06-19 03:07:06', '2020-06-19 03:07:06', '2021-06-18 23:07:06'),
('8b96dfc918b8d5d8a7fd0c722a9ad93a3103b4444e5756a889b9a4e3b13eccdce02d5dde59165d47', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:19:40', '2020-04-28 02:19:40', '2021-04-27 22:19:40'),
('8bb51651c8e500a94119a1b4f89538166d5251f63ebfb6dbae02b50fbac7a41c0ac1553e2b769688', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 21:38:19', '2020-03-28 21:38:19', '2021-03-28 22:38:19');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('8bd286846363241bbfe2e242e2895c382f935e2f594249fd1f91ddd6ae02acc20c43f3f1777f54bb', 2, 2, NULL, '[\"*\"]', 0, '2020-04-09 14:59:40', '2020-04-09 14:59:40', '2021-04-09 15:59:40'),
('8c77b877c6da1b6857499c0c2f2dc26f22fae421e917762e5ea8f88e9a915f0d65c0c413ebbaa0dd', 166, 4, NULL, '[]', 0, '2020-10-31 09:53:33', '2020-10-31 09:53:33', '2021-10-31 05:53:33'),
('8d2796c2e2b145b8eec125d91c54e739992eb5ee09b9181d7ad266de479293ff427153b64b1c5b0b', 16, 2, NULL, '[]', 0, '2020-06-20 19:32:43', '2020-06-20 19:32:43', '2021-06-20 15:32:43'),
('8de937cc6deacc4d456a5c4df6bb88c0a11a540f0e8cc68569778d3e1f24afb993c39dc288e164b1', 151, 2, NULL, '[\"*\"]', 0, '2020-10-29 13:33:34', '2020-10-29 13:33:34', '2021-10-29 09:33:34'),
('8dec6e871d2e6ab05e74647bbffc4cc83ceddf623338f58bc5dd7697a7cc8ba96e3c4f0db6215932', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 23:01:02', '2020-04-08 23:01:02', '2021-04-09 00:01:02'),
('8e1edc4ca53a40226d6cd3278a6027b9e5ea13b41b5992f3338dce4624864472691a1eb515e341f8', 2, 2, NULL, '[\"*\"]', 0, '2020-04-23 19:28:18', '2020-04-23 19:28:18', '2021-04-23 20:28:18'),
('8e719387861cce09b9bb2501e2de36e3ae499f231e42fbe8ad89ac1f4e15290ff3493e0fed937097', 213, 2, NULL, '[\"*\"]', 0, '2020-11-07 11:06:57', '2020-11-07 11:06:57', '2021-11-07 06:06:57'),
('8fef46ff461806ccff99b6b2bb0e4d629b9439ea1e89c574dcd76cdb284fb0040020d3096def8f27', 1, 2, NULL, '[]', 0, '2020-06-17 04:31:00', '2020-06-17 04:31:00', '2021-06-17 00:31:00'),
('90a870a5112e5d0bdf9f7acda176450c3f119f3486860f6a1102eb7320578b7a8383151c2219f73f', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 21:50:00', '2020-03-28 21:50:00', '2021-03-28 22:50:00'),
('9106685debe23bfb3b441292dc5aa2f9c3afa8f45fb90d085c6048e4e97e56ac27eaab092310796d', 2, 2, NULL, '[]', 0, '2020-04-16 12:18:49', '2020-04-16 12:18:49', '2021-04-16 13:18:49'),
('915f331e24dd9d462b67343e7e6e9da41aa18df451f17687863edd17fd16bf8dba197d131ad04e15', 1, 2, NULL, '[\"*\"]', 0, '2020-06-23 16:46:44', '2020-06-23 16:46:44', '2021-06-23 12:46:44'),
('921d994e2d1948d829d960879e70ff9059af7e8491d08c517913e7d8612c4e4b5de3c56f0c3334a5', 145, 4, NULL, '[]', 0, '2020-11-05 19:19:01', '2020-11-05 19:19:01', '2021-11-05 14:19:01'),
('923bec54cfc0c59af55e0fe518a204972d885d82262e465f354e7bcbf778334538552ff3085117b5', 1, 2, NULL, '[]', 0, '2020-04-16 22:02:01', '2020-04-16 22:02:01', '2021-04-16 23:02:01'),
('936d9555c409d5d159220e52de509f1d6c9532a1147df3095dd98bd95507ebe399175a86900d143d', 1, 2, NULL, '[]', 0, '2020-03-03 15:19:06', '2020-03-03 15:19:06', '2021-03-03 16:19:06'),
('93ca8f5e875c179bd42916d9fbc503add5284f30fd10ccaa5a2fcde322bb63dc488d7a50c82441d1', 214, 4, NULL, '[]', 0, '2020-11-07 14:38:11', '2020-11-07 14:38:11', '2021-11-07 09:38:11'),
('94d2180883a7733689ec2b36272a975b9ca886eb95ec904290197cbe6c01279cc361009deb98d62a', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 19:15:06', '2020-03-28 19:15:06', '2021-03-28 20:15:06'),
('951310cd8447bd539f183fd9d588dc275f8398ce257324573321283fa3eb4a694f4e23a18697cfb7', 180, 4, NULL, '[]', 0, '2020-11-05 00:06:25', '2020-11-05 00:06:25', '2021-11-04 19:06:25'),
('9570604598f4258f534ecbe75436a8e6dc743803a954f88633257f8bf57ee0f73c9f1c74338e7d22', 16, 2, NULL, '[]', 0, '2020-04-28 14:54:10', '2020-04-28 14:54:10', '2021-04-28 10:54:10'),
('95c0303f70094eae2b65ca064cc8dd3a01e864af9358b095497406f7b510522ffe28212ef9fed7ae', 180, 2, NULL, '[\"*\"]', 0, '2020-11-03 22:41:01', '2020-11-03 22:41:01', '2021-11-03 17:41:01'),
('965910f83028bbfceff4f3a67d2b588005b1083206f428347313668ed23faffc770a19690bbf82c9', 212, 2, NULL, '[\"*\"]', 0, '2020-11-06 17:13:05', '2020-11-06 17:13:05', '2021-11-06 12:13:05'),
('96ced244af4e8119926a3d29b9395b7ef837e951001b9dda0784612c0a802af7356e9fac0bc40ab2', 2, 2, NULL, '[]', 0, '2020-06-17 04:32:59', '2020-06-17 04:32:59', '2021-06-17 00:32:59'),
('97e9e094407c2f4af65e02a16c2711a60f73e8d929ed314f3bac48872e814047e675b8eafa3febfd', 1, 2, NULL, '[]', 0, '2020-03-06 09:37:21', '2020-03-06 09:37:21', '2021-03-06 10:37:21'),
('9935e493276716c26dcbb3c3755866744168361788c2658a2ed0486c5cd12125506b1a2c5467c19d', 2, 2, NULL, '[\"*\"]', 0, '2020-04-23 19:40:26', '2020-04-23 19:40:26', '2021-04-23 20:40:26'),
('9bf68e07d9a67a948c0a871b2d9c1aeaec53e30a0d4fa49c82727067863cd80639801e73f1c8036e', 1, 2, NULL, '[]', 0, '2020-04-15 21:28:51', '2020-04-15 21:28:51', '2021-04-15 22:28:51'),
('9c89abca6fe574d99556174ebd7b06c0f23493ba83b770b131f809fa7df98ec3f2fe4b83e8e503c0', 56, 2, NULL, '[\"*\"]', 0, '2020-07-11 01:57:43', '2020-07-11 01:57:43', '2021-07-10 21:57:43'),
('9cdd2b0a03aae5386e23978b1308e2966b164769cdbaae390f030371a0ee3b43dd31348bfb9f8f1a', 1, 2, NULL, '[]', 0, '2020-04-03 10:33:25', '2020-04-03 10:33:25', '2021-04-03 11:33:25'),
('9d8a8055c15b6f0eb3897aa0150f946a43184ef1269c3bb3a10900e2b0495c67fd606f16ee579e3c', 85, 2, NULL, '[\"*\"]', 0, '2020-09-07 03:33:21', '2020-09-07 03:33:21', '2021-09-06 23:33:21'),
('9eaa619adf8fc9187b5fe4be993076c5f0f325c25d08129f99f336d795fa660124c1fd34faebba49', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 20:40:02', '2020-03-28 20:40:02', '2021-03-28 21:40:02'),
('9ffdf69983f1d18f1963038ec1f9278a4fa7ffd42da5d360adebdc40882beeecce5ffd02158d6b10', 146, 4, NULL, '[]', 0, '2020-09-20 12:20:13', '2020-09-20 12:20:13', '2021-09-20 08:20:13'),
('a0ced276f11165e9d1befef96c6782d1e030cc3ab9102328d07182bd4e1dd010239e20c392953577', 196, 4, NULL, '[]', 0, '2020-11-04 23:00:56', '2020-11-04 23:00:56', '2021-11-04 18:00:56'),
('a1dca887448c0ef702fce57f40d4cd8e725c46d16fc45cfea3fa4f9083721ca134ac44966a68e614', 85, 2, NULL, '[\"*\"]', 0, '2020-09-01 16:43:18', '2020-09-01 16:43:18', '2021-09-01 12:43:18'),
('a2786dc1c99307ce43392fd608d001d41c6b8cb18565363e51e8fac4bc78dbf25bd744024bad8647', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 17:31:06', '2020-04-15 17:31:06', '2021-04-15 18:31:06'),
('a2d5f0410ad8a2211abfddb3f1c416037402e11b99a6a28891a59fee6928020355baf58772d908dc', 2, 2, NULL, '[]', 0, '2020-04-07 20:35:32', '2020-04-07 20:35:32', '2021-04-07 21:35:32'),
('a3819af51172dd26ed7239ccff85394d5f1cdbccedff1fe45121f024b1d4827f6df5e1f111f3d8e1', 56, 2, NULL, '[\"*\"]', 0, '2020-07-03 22:16:17', '2020-07-03 22:16:17', '2021-07-03 18:16:17'),
('a42d5b1fa94524650d4e76a1b1343f774eafbaa9982ebfdac89c45b9ad34886ddf14f31b74d5b044', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 22:33:48', '2020-04-05 22:33:48', '2021-04-05 23:33:48'),
('a5958e5c0db8afc020f22db25fdf8a681e71e6c057fdf4112313b0077c703c371868d37841625f2b', 85, 4, NULL, '[]', 0, '2020-08-22 12:31:40', '2020-08-22 12:31:40', '2021-08-22 08:31:40'),
('a64301b6e0c48eea0c1dac3271326cf69704ed18507065d4b55f00c7091d4448cfca0c8db761379d', 1, 2, NULL, '[\"*\"]', 0, '2020-06-08 12:18:16', '2020-06-08 12:18:16', '2021-06-08 08:18:16'),
('a68cdd8bde442ebea6f4e3aad9d9e4c6dc085f5811c46fddc08664a45f789ca1698f9dfe8cbce91d', 1, 2, NULL, '[]', 0, '2020-03-02 14:37:06', '2020-03-02 14:37:06', '2021-03-02 15:37:06'),
('a6fd72271f8ab54897088c468b82d66778bf4b7c99e09c7a3ad98f102b7dabac2a67f697357614ef', 173, 4, NULL, '[]', 0, '2020-11-02 22:14:08', '2020-11-02 22:14:08', '2021-11-02 17:14:08'),
('a71f63144e776880c1f12de8b78a3e6a1f8c3ec7aefd12e812d4bfd1ba0e68b4fffdac76e27e3b3b', 56, 2, NULL, '[\"*\"]', 0, '2020-07-04 11:57:00', '2020-07-04 11:57:00', '2021-07-04 07:57:00'),
('a74d0fabe8e07e68945c07ea9c9c3643d501b37d73278a1fbf5c1891ca3710ef07924ab5cf4e62bf', 174, 4, NULL, '[]', 0, '2020-11-02 23:24:25', '2020-11-02 23:24:25', '2021-11-02 18:24:25'),
('a7973736f71ce5123dbcbc57f1200b46824539446fd650bdf7aaca9ad58dd6793a51f918dcaefb35', 1, 2, NULL, '[]', 0, '2020-04-16 22:00:38', '2020-04-16 22:00:38', '2021-04-16 23:00:38'),
('a9234782f2dbb213196495bfe558a32b86e6970eac5d3de95bf4567457d5f74342f4f564d2a7b623', 145, 4, NULL, '[]', 0, '2020-11-05 19:18:00', '2020-11-05 19:18:00', '2021-11-05 14:18:00'),
('ab35b6e133e52836975824a61a57844b6fa630652c82179b960a1a8c81d9d942793a4ed941e37648', 1, 2, NULL, '[]', 0, '2020-04-28 15:36:36', '2020-04-28 15:36:36', '2021-04-28 11:36:36'),
('ab45c476e166ed840c46d98c0e225de2644f7c2122235eaed9766f00e110bf2fd57d9895392f04dd', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 01:51:28', '2020-04-28 01:51:28', '2021-04-27 21:51:28'),
('ab7e27b8630566b3a2a4c07c7c694d6752d0268c781a719e32c90400ed409cbc1882c9a820481e90', 169, 4, NULL, '[]', 0, '2020-11-02 11:57:18', '2020-11-02 11:57:18', '2021-11-02 06:57:18'),
('abf4e04a303766fa62e0aba816554449638b21d5afa5cfbcad1bf4724dee4ef52fe4e00717f6b605', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 14:16:53', '2020-04-15 14:16:53', '2021-04-15 15:16:53'),
('ac9a967be7c34d93248afc523db08982a7cd4040a8d64adfde29f8e8f7b925c7ec0f923554e5c981', 16, 2, NULL, '[\"*\"]', 0, '2020-10-23 12:03:59', '2020-10-23 12:03:59', '2021-10-23 08:03:59'),
('aca242b5a0aa02db5dd2c9fb5af5b6603219e787025f940cf2088805a3e2ff06159f7843c0295377', 2, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:19:26', '2020-04-06 22:19:26', '2021-04-06 23:19:26'),
('ad66c81b4e03431e9aa99c1f364d2c260efcbfe3b9996ac30f1b05fc517fc0c8242341303e6295c9', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 21:48:18', '2020-04-05 21:48:18', '2021-04-05 22:48:18'),
('af8c9548e79e1b8712ec2dabfd49a1876a62ab83adee08d00190d962119b0ce8b5b0bfc5495ac163', 59, 2, NULL, '[\"*\"]', 0, '2020-07-15 21:36:34', '2020-07-15 21:36:34', '2021-07-15 17:36:34'),
('b0d7fe0f0f5249689198792a5cbbda4a3cdb6e699254adeb99600be812745bf959c0738ea6126dcd', 1, 2, NULL, '[]', 0, '2020-03-02 09:42:42', '2020-03-02 09:42:42', '2021-03-02 10:42:42'),
('b1e83ecc97a6154efb18f1bbcae52bc7cb4159150acf4b20b9dee91812ab2c16d655885d736a6ca3', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:44:53', '2020-04-28 02:44:53', '2021-04-27 22:44:53'),
('b1eed07a5ea83ae89bf093eda3e3a0943654481ca90b67861384edc079e5cf70ebe02835bbce9de7', 2, 2, NULL, '[\"*\"]', 0, '2020-04-17 11:09:16', '2020-04-17 11:09:16', '2021-04-17 12:09:16'),
('b27915ebb952fd439ed5d0a147d9b7d6c1c01ab3c4b4e160a721302970f64fa3f586e2b061a78a2d', 1, 2, NULL, '[]', 0, '2020-03-02 14:32:55', '2020-03-02 14:32:55', '2021-03-02 15:32:55'),
('b2f02877c1656861d37f1c6c7c19d2ce9a9b48358279ec0802099e80ef2cd56540efaf596fe93b75', 68, 4, NULL, '[]', 0, '2020-08-09 05:56:58', '2020-08-09 05:56:58', '2021-08-09 06:56:58'),
('b3675b860479e89e241913b78639ab10cbe308bd4958be6bd0f797dc96e5baa821ef7795d8891fc7', 155, 4, NULL, '[]', 0, '2020-10-29 14:41:24', '2020-10-29 14:41:24', '2021-10-29 10:41:24'),
('b442c71bc6de453532ef59f789e18e442f1200ce7aae1ba7740172098f5c910588ec50eb83e08fbc', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 21:22:46', '2020-04-06 21:22:46', '2021-04-06 22:22:46'),
('b44ca8040061bfb78d4abe2396eeb877c3958839b746b56839b297db5e08d1b1f891a1edc7ea4c7e', 199, 4, NULL, '[]', 0, '2020-11-05 12:24:51', '2020-11-05 12:24:51', '2021-11-05 07:24:51'),
('b4c940bd55fa6e76391a474d7b802edbbf465ae8abd17a549ec66b8401538c15d119250d28948e69', 2, 2, NULL, '[\"*\"]', 0, '2020-08-22 12:00:10', '2020-08-22 12:00:10', '2021-08-22 08:00:10'),
('b4f6071eaab7bd5efcada5c8b5615a30c4d13f43aae3da527240dd9312cd5c9edf50478b3d7eb670', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:27:20', '2020-04-28 02:27:20', '2021-04-27 22:27:20'),
('b59ee08fca69b88c2452f6a87c7a3780af4b0753f4faecbe4b6807cc0500beeb289d1095dd53419f', 56, 2, NULL, '[]', 0, '2020-07-05 12:27:53', '2020-07-05 12:27:53', '2021-07-05 08:27:53'),
('b5f3d23888abe974989bf1aff2c56feb47b0146c5d17a19ac6466c8006bdc3492e8c95dd1d13a92f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-09 08:33:32', '2020-04-09 08:33:32', '2021-04-09 09:33:32'),
('b74e35d0242f34713feee83348afaec688e8bd25b6f90c9a52bf7c8777d6cba250e097cba7399748', 16, 2, NULL, '[\"*\"]', 0, '2020-06-24 01:40:08', '2020-06-24 01:40:08', '2021-06-23 21:40:08'),
('b8e9fb69720ba02f5c3c70d11ce1501d6b19d29748288566c6d11d8a44f7f3698ff09c82fb7683f6', 85, 2, NULL, '[\"*\"]', 0, '2020-08-28 13:24:41', '2020-08-28 13:24:41', '2021-08-28 09:24:41'),
('b99463126caadbcd20c7e96fd48a7f44acab0c757586a5372a279ad9a019469476fc21bde366504d', 1, 2, NULL, '[]', 0, '2020-03-12 18:21:21', '2020-03-12 18:21:21', '2021-03-12 19:21:21'),
('b9aad71bfb0427408aa4f7591936e6d4f8a54136e7eaa4d1939619cd338adcefd358ebaa78c88707', 34, 2, NULL, '[]', 0, '2020-06-10 18:58:04', '2020-06-10 18:58:04', '2021-06-10 14:58:04'),
('ba6096755714fdedef1229145d6a2a2d783b99253b160b81df2f2f1af63fd77edb00d66b28dba183', 16, 4, NULL, '[]', 0, '2020-11-03 15:58:58', '2020-11-03 15:58:58', '2021-11-03 10:58:58'),
('bab6a8c629ef1d52338407b84da4f8923227fcfa85593d860eae6b301db310544039c66a2ae47a1c', 16, 2, NULL, '[]', 0, '2020-04-28 15:01:12', '2020-04-28 15:01:12', '2021-04-28 11:01:12'),
('bb3433afd3a928b59e134a25d61560274be4200388fe633fb58b8d853a09ced4c759daa75d6f13bc', 2, 4, NULL, '[]', 0, '2020-08-06 22:09:58', '2020-08-06 22:09:58', '2021-08-06 23:09:58'),
('bb4cb4be722aae8169a449b40b1a7a54055e6799fcd7a70b501934ebd670ac77ca4e76e70e649938', 1, 2, NULL, '[]', 0, '2020-06-17 05:27:30', '2020-06-17 05:27:30', '2021-06-17 01:27:30'),
('bbd788344b7624b4a200b9f73c8a22c15cec70ba5466ab50350944b4ffe54d4b9a218b449b9a69fc', 1, 2, NULL, '[]', 0, '2020-04-27 02:34:30', '2020-04-27 02:34:30', '2021-04-26 22:34:30'),
('bd47f7bbf5cdcc2f9351c349d07dd7ebf70b071ac6471239a507a00bf56f18bd5e435fa431e9747c', 2, 2, NULL, '[]', 0, '2020-03-15 23:35:06', '2020-03-15 23:35:06', '2021-03-16 00:35:06'),
('bd4db340de531ed9733791769655b9c4034a003f9ab9b5c1b765c578c933445bce1ac21186ce4003', 148, 2, NULL, '[\"*\"]', 0, '2020-11-09 19:56:14', '2020-11-09 19:56:14', '2021-11-09 14:56:14'),
('bdab93f0f583ee17fffbd4496411e4f8ff6aac4f378c331e9be7e16e1f726fb73ce3c7f85e85b095', 93, 2, NULL, '[\"*\"]', 0, '2020-11-02 22:31:45', '2020-11-02 22:31:45', '2021-11-02 17:31:45'),
('be29ea2267d6616035ee02597559c36374a8bd8bb2bdfcf300df58d3b4cc219a34ab231cff58bb1d', 59, 2, NULL, '[\"*\"]', 0, '2020-07-15 21:38:31', '2020-07-15 21:38:31', '2021-07-15 17:38:31'),
('be8a7ba9e5b387db25ba78ef557e2c3aeaaca0be6e086c1c25cbbd617360e536fc88dea464ff1cea', 1, 2, NULL, '[]', 0, '2020-04-07 20:34:32', '2020-04-07 20:34:32', '2021-04-07 21:34:32'),
('be9714264bcf8ea87a7a225ff18219fcb3e40714a85996bb74840ce5f59c5337ac725684bd5350b3', 22, 2, NULL, '[]', 0, '2020-05-30 21:22:54', '2020-05-30 21:22:54', '2021-05-30 17:22:54'),
('bf5d634543bf44ec0eb1121e6bc75da2ebcdc360cc21c4c2390524945461121ba418e4318ca78da7', 56, 2, NULL, '[\"*\"]', 0, '2020-07-10 11:30:58', '2020-07-10 11:30:58', '2021-07-10 07:30:58'),
('bf75225e89e717e9b2aa60ebd5f69bb8dcebe1bab430a0fda14a844e7d245d4347ebb8c75c978ed7', 16, 2, NULL, '[]', 0, '2020-04-28 14:59:01', '2020-04-28 14:59:01', '2021-04-28 10:59:01'),
('bf87dbc56d757903c08d67a9aeab8facb32e4acbd68122b570d41fb6b8abea57094986610a0a1a5a', 68, 4, NULL, '[]', 0, '2020-08-04 10:32:47', '2020-08-04 10:32:47', '2021-08-04 11:32:47'),
('bf89dd621c6938cce2c5f2902d71d28de4c74ace6c2b7217a4fbffcbfcc9a73b1c83c15ed8622a5d', 16, 2, NULL, '[]', 0, '2020-05-22 13:21:54', '2020-05-22 13:21:54', '2021-05-22 09:21:54'),
('bfabb7cb62397e355e3f02dccaccbd4b8a0613f300b6eb0b54292376f3b0330bcd8b8c5afe6fe2a6', 1, 2, NULL, '[]', 0, '2020-03-02 10:45:22', '2020-03-02 10:45:22', '2021-03-02 11:45:22'),
('c0018f4ea8955bdd72987270ab6baf8cff085ac50a5d4d8497cb1811047e7827a26c23376605e10c', 93, 4, NULL, '[]', 0, '2020-09-06 10:01:16', '2020-09-06 10:01:16', '2021-09-06 06:01:16'),
('c13f3ff7a9263061bb9fd92d3ba742569b6dc017584ae4aee4cbc513be8fe521ea8dbf859ad7f87e', 1, 2, NULL, '[\"*\"]', 0, '2020-04-04 22:47:50', '2020-04-04 22:47:50', '2021-04-04 23:47:50'),
('c1a8cf8142c9e3e77c36a9d398998e2facba185a8f549d85a0ab31a8a45c544fe6775f7ab80e5adf', 1, 2, NULL, '[]', 0, '2020-06-17 03:48:33', '2020-06-17 03:48:33', '2021-06-16 23:48:33'),
('c1cf7bad86c1d48f53338042e6c1d379f10398ba047bf4fdab28ce934cad897d38bd962e130bde62', 166, 4, NULL, '[]', 0, '2020-11-06 19:00:50', '2020-11-06 19:00:50', '2021-11-06 14:00:50'),
('c2c6591c6fcbc12bac4133c7f305cf5adc6c441e65bfd0351cb9e31dc46964c8a43d0f1cf8c1654a', 1, 2, NULL, '[]', 0, '2020-03-02 09:46:06', '2020-03-02 09:46:06', '2021-03-02 10:46:06'),
('c3092bee166cdd479d347e559cca94a74f9b9fa5c080ed483358c72aab5ae64a2748924b7ba15ce5', 64, 2, NULL, '[\"*\"]', 0, '2020-07-30 00:28:41', '2020-07-30 00:28:41', '2021-07-29 20:28:41'),
('c3842583639a31b274ea8031482941742d805aad59999ce900a323b8c8951ab7d653a84eeeb1032d', 16, 2, NULL, '[]', 0, '2020-06-16 18:28:04', '2020-06-16 18:28:04', '2021-06-16 14:28:04'),
('c45cb1527c44bf4778ef313df878ef015b279985e91a94f7532a6c8ebc96434030ab754cf29ecdd4', 166, 4, NULL, '[]', 0, '2020-11-06 18:22:56', '2020-11-06 18:22:56', '2021-11-06 13:22:56'),
('c4d1b5f6b4128ff0b6d24168dbadffd1c8cc31379ee06b45c7e29d1dcc401d625abe8b02d23e7d7f', 166, 4, NULL, '[]', 0, '2020-10-31 11:04:51', '2020-10-31 11:04:51', '2021-10-31 07:04:51'),
('c4f72498fefc9309359e922e01d4d98b864740ad41366ea5faace757025136d14f97d87a0e9dcc2b', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:07:36', '2020-04-28 02:07:36', '2021-04-27 22:07:36'),
('c512b88c01bb392bf5d735d035f690274660b12307e98a2107a3ae7eba10b6ff85ca431aac182663', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:29:44', '2020-04-28 02:29:44', '2021-04-27 22:29:44'),
('c5aa68e6ce3b47a0778a4fa0610fd6e3d2365a245fe0f3760c5bb7c1c1897c14c6e5a085114ac6bf', 166, 4, NULL, '[]', 0, '2020-11-06 01:19:53', '2020-11-06 01:19:53', '2021-11-05 20:19:53'),
('c85af333ffcc6c467e4cb201c83421d6aaf296df95140f559255521d6e858277bc438215dcdfd4c9', 1, 2, NULL, '[]', 0, '2020-04-27 14:44:37', '2020-04-27 14:44:37', '2021-04-27 10:44:37'),
('c8726ce8d7e7f60754fab098e8b2f3014a9bd4b2c7c596a35f9f9046d41233c54e3f80c6cad742c0', 2, 2, NULL, '[]', 0, '2020-03-15 23:33:06', '2020-03-15 23:33:06', '2021-03-16 00:33:06'),
('c8ef1adcdb047d72c36f655bfc9f6bf686e74baeac2ad503b5d99dc51d62b5c7a98de59e8986180f', 182, 4, NULL, '[]', 0, '2020-11-03 19:29:06', '2020-11-03 19:29:06', '2021-11-03 14:29:06'),
('c925a6b3672ef129a264aa8f11b335c649ca8045145b8267722813fff3f26fcc18aafe335069506f', 56, 2, NULL, '[\"*\"]', 0, '2020-07-04 00:29:48', '2020-07-04 00:29:48', '2021-07-03 20:29:48'),
('c969f238defd930b49f2a23ed08f6908f564ed37e308445a15cf5c8c68585969ee66db269a420806', 1, 2, NULL, '[\"*\"]', 0, '2020-04-09 08:33:03', '2020-04-09 08:33:03', '2021-04-09 09:33:03'),
('c992a1f7bba5f15171928f5e28a7d3ada891c524e01fb19031dd3c5963065706f3c133db38b5dad4', 68, 2, NULL, '[]', 0, '2020-08-04 01:59:48', '2020-08-04 01:59:48', '2021-08-04 02:59:48'),
('c9f2fd43c240c020aaf34aa916d228e2551321352628b7ad38b253b43762b6c7cb0e011edb6c8cb4', 212, 4, NULL, '[]', 0, '2020-11-06 17:14:20', '2020-11-06 17:14:20', '2021-11-06 12:14:20'),
('cb62db9e975db3a5bec6e9d85b5aeb61c9c7452de994702770b39c6830decb637122aef25690e9fc', 2, 2, NULL, '[]', 0, '2020-04-08 22:31:31', '2020-04-08 22:31:31', '2021-04-08 23:31:31'),
('cbdbc84ec509a398712273257435334b4191c35265d33359997589bc042a011cb9ebffd299ea69b7', 56, 2, NULL, '[\"*\"]', 0, '2020-07-03 22:21:01', '2020-07-03 22:21:01', '2021-07-03 18:21:01'),
('cd0835a309d000abb2035d3a35d11c0ec61bdb0e5aa7ec488a154dbd30cd77fab32fb66978798a9d', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 14:18:52', '2020-04-15 14:18:52', '2021-04-15 15:18:52'),
('cd2d1c80f02c55709331dbfdb342535e68ae682fdf801621ce3a42d98571fd0f5c5b3a15ca8fc502', 17, 2, NULL, '[]', 0, '2020-06-17 10:10:56', '2020-06-17 10:10:56', '2021-06-17 06:10:56'),
('cd736ec1329ea1fe46957f9f17ec8b01c312dd4b588aa05f21531b9c6085f2abf862fa35f4ac7056', 16, 2, NULL, '[\"*\"]', 0, '2020-06-24 00:40:06', '2020-06-24 00:40:06', '2021-06-23 20:40:06'),
('ce312641d5fd526e1e2becc3b6fe691e753794d7ea2e683f1cb61b67cbed7bed72c7e424b02c55be', 1, 4, NULL, '[]', 0, '2020-08-06 22:09:30', '2020-08-06 22:09:30', '2021-08-06 23:09:30'),
('ce38ed2ef5bf4678378c4a49bba6eeed6739cc293b9945694cdfd30b4e7ee742a9aa78ffa0007c31', 1, 2, NULL, '[]', 0, '2020-03-02 10:46:43', '2020-03-02 10:46:43', '2021-03-02 11:46:43'),
('cf215bad179678b6c04a4063380714361372d663170b9bad14bdba5b0f9465aa1f91989dfe7f51bd', 148, 4, NULL, '[]', 0, '2020-09-20 15:11:03', '2020-09-20 15:11:03', '2021-09-20 11:11:03'),
('cf25b4c0e4419e6f6e2fece0561d4f3dac2f9e3d728d4b1d6ce06c8ee81baa5b7bdef66da540a15d', 1, 4, NULL, '[]', 0, '2020-08-11 08:26:49', '2020-08-11 08:26:49', '2021-08-11 09:26:49'),
('d09fdf5f6832af29a5d740c83d701599580f0804f343ca741ee3e4203f36834c14550557caf9e186', 60, 2, NULL, '[]', 0, '2020-07-19 17:30:34', '2020-07-19 17:30:34', '2021-07-19 13:30:34'),
('d0d623fd38855f9850da73138b500a39bb0bb620dd23a12406975b26b6ff6382a2cbb012aaea5244', 166, 4, NULL, '[]', 0, '2020-11-02 14:21:18', '2020-11-02 14:21:18', '2021-11-02 09:21:18'),
('d20ae31a2887689ef7f9fd680394341dd7215fe78d97974f7560793a1d49ba399a71df196c290830', 183, 2, NULL, '[\"*\"]', 0, '2020-11-03 20:55:11', '2020-11-03 20:55:11', '2021-11-03 15:55:11'),
('d26d2f6270f55f5e7e1633bb50239a63b350d8c095cd1819804557d7dd312402dcd71304d496e74d', 85, 2, NULL, '[\"*\"]', 0, '2020-09-06 22:55:14', '2020-09-06 22:55:14', '2021-09-06 18:55:14'),
('d384dd0573008efad1154499a13a2778cab5668ed88c57781d79d16bfaeec1ca619d6fb6b1934867', 1, 2, NULL, '[\"*\"]', 0, '2020-07-02 13:44:11', '2020-07-02 13:44:11', '2021-07-02 09:44:11'),
('d51d7f781e250387eaa4869a6a68f3dc981c561248240a3bed789169be553025a7e65562c705dce8', 87, 2, NULL, '[\"*\"]', 0, '2020-11-04 01:42:02', '2020-11-04 01:42:02', '2021-11-03 20:42:02'),
('d646135d187e8dffb0ef7d85356e541e192e6b4618048ce17bd33f9cbaad9ca44b33cb8057a26d80', 2, 4, NULL, '[\"*\"]', 0, '2020-08-22 12:26:49', '2020-08-22 12:26:49', '2021-08-22 08:26:49'),
('d7102fad91ac9c828d5f78e4536adc4119655273d1eadd2c404d499a3b22d31cda7c05eb231a0925', 1, 4, NULL, '[]', 0, '2020-08-11 09:31:36', '2020-08-11 09:31:36', '2021-08-11 10:31:36'),
('d7d97a5b86638846c9769af802f53ef89da3f0b4a3f4496059c841e5fb3c534b70a25e736086e29e', 16, 2, NULL, '[\"*\"]', 0, '2020-08-11 17:07:17', '2020-08-11 17:07:17', '2021-08-11 13:07:17'),
('d7e777eef8d1992d266272d47415c2be3b12232a51578d219465377cde218674cdc7d95ff8a576c8', 43, 2, NULL, '[]', 0, '2020-06-19 03:00:58', '2020-06-19 03:00:58', '2021-06-18 23:00:58'),
('d7f53e7d9911c6374a8ec8dd1917821d008397108b87ad7506079081a45dc69f135ac255fc353c6d', 85, 2, NULL, '[\"*\"]', 0, '2020-09-07 01:35:14', '2020-09-07 01:35:14', '2021-09-06 21:35:14'),
('d8057c42b4d07442f5a3f1b9564944a399bdfc65127294a01bd0390c0f33619f1b27ec40c7c09b61', 1, 2, NULL, '[]', 0, '2020-03-15 23:02:59', '2020-03-15 23:02:59', '2021-03-16 00:02:59'),
('d80ac352c4504edca41da90ea2f3a8611951ecfbf86b865e9d516daef7a54db72f2d25316c1953d9', 1, 2, NULL, '[\"*\"]', 0, '2020-07-13 00:59:37', '2020-07-13 00:59:37', '2021-07-12 20:59:37'),
('d857c0fa88a773d7451463add51f27a8dc172efff067cd1a251d65c99ec485b9f4e0e2dfcabe8bc0', 2, 2, NULL, '[]', 0, '2020-04-25 03:24:06', '2020-04-25 03:24:06', '2021-04-24 23:24:06'),
('d890c477f56b3ad1c62e55b642b6d4285703750a07a30a4e0a47a747c3dc572ca7dd300ad36ae9c4', 1, 4, NULL, '[]', 0, '2020-08-11 08:34:13', '2020-08-11 08:34:13', '2021-08-11 09:34:13'),
('d8ac2c108cc29f4e6ad4b70a16a02b0554020535d969cce32399b41f5dbfa4f91c06a5824d0876b1', 148, 4, NULL, '[]', 0, '2020-11-06 18:52:27', '2020-11-06 18:52:27', '2021-11-06 13:52:27'),
('d8d583a126b4338735eac80de0b9191bf3256684b535258ed8bebbe9a047ea5dab766b45b209d1d9', 1, 4, NULL, '[]', 0, '2020-08-06 20:42:01', '2020-08-06 20:42:01', '2021-08-06 21:42:01'),
('d9792954930dd063ba5c0cf65b567fc4022048c89f188670ec044014221211c09672ec6fb75705dc', 16, 4, NULL, '[]', 0, '2020-08-20 13:52:42', '2020-08-20 13:52:42', '2021-08-20 09:52:42'),
('d9c222b6a3b1d0d1698ef26fe97d555df5205e2fe4d60bcd31511a6e092f73349a97e490e7bcde36', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 12:49:35', '2020-04-05 12:49:35', '2021-04-05 13:49:35'),
('db2e3305c8adc045a4f099ada8bdc8c485a960fabaa53b9b4f373f65593145ff3e31362c0fbec908', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:37:59', '2020-04-28 02:37:59', '2021-04-27 22:37:59'),
('db4943344576ca510866088e11f53f30bf438f378e2614d0e63d3997c6c777db27103210acb90d04', 142, 4, NULL, '[]', 0, '2020-09-20 11:28:26', '2020-09-20 11:28:26', '2021-09-20 07:28:26'),
('db7ea331b5c5ad494e13321ef660be6f645d534c9a4c5877713f6d19f2d851ceafe9ffae3a721b55', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 21:07:02', '2020-04-15 21:07:02', '2021-04-15 22:07:02'),
('db9d563485845e207d43b4217ad0e6832b6bab89fa211420a87aa4b814ac3bc9628af56b8c51dd77', 186, 4, NULL, '[]', 0, '2020-11-04 00:23:01', '2020-11-04 00:23:01', '2021-11-03 19:23:01'),
('dbd40fca2f99f0854b318e73140f0744cf0f89ba4ca25c7c40e013312627bddbd03b94c07367e4d9', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 01:50:48', '2020-04-28 01:50:48', '2021-04-27 21:50:48'),
('dbeede49da7c65a6b7d5b824f2ae68b90de49428db266fe98ada9eea23929f2857275009a262dd57', 144, 2, NULL, '[\"*\"]', 0, '2020-11-05 19:15:37', '2020-11-05 19:15:37', '2021-11-05 14:15:37'),
('dc63552adb99e3964050a7ddc988e1d49946ab80cc68de805a2a6cb1c56edd66b5409f7a1ae10d6a', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:14:10', '2020-04-28 02:14:10', '2021-04-27 22:14:10'),
('dc6f8b79952e51fe7eea3a96ba39515f2c26f981d28690b897e7928ff14947f10b3873c6704a3b0a', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 21:57:32', '2020-04-06 21:57:32', '2021-04-06 22:57:32'),
('de3f50b7ab7bf87fde691b74a911bb7804ed142cd61a2dc588841d2f83c16eab706dfd50594b7b54', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:48:57', '2020-04-28 02:48:57', '2021-04-27 22:48:57'),
('def32dc430ec72fb6eceae2d693e52a5425fe3a1613b3f27970af577fa725aa3f8e4b1d41ee8856b', 56, 2, NULL, '[\"*\"]', 0, '2020-07-15 00:02:09', '2020-07-15 00:02:09', '2021-07-14 20:02:09'),
('df1aa5241f5e8d6dd467fd239f8507c08e516b5589047ae05ec9adce6debac62a426f0630b8b8bc8', 2, 2, NULL, '[\"*\"]', 0, '2020-04-07 19:50:07', '2020-04-07 19:50:07', '2021-04-07 20:50:07'),
('df30ffe7821e285a89570883c65424a01bb2a8e10b7044d526c7d710213ea13aa024cdcae6e9a65a', 56, 2, NULL, '[\"*\"]', 0, '2020-07-11 01:52:16', '2020-07-11 01:52:16', '2021-07-10 21:52:16'),
('e03081326ffdf3d6f456235d724ab1969df1788d7d21fb2cc59cfe4ab94b171499f3bd617761f952', 2, 2, NULL, '[\"*\"]', 0, '2020-04-23 18:27:51', '2020-04-23 18:27:51', '2021-04-23 19:27:51'),
('e03acb154eed7d3e9b5bb4101313e3265d9bd674b3f0868f71fe2a8a80bb05d6ef040a2c5b66102a', 148, 2, NULL, '[\"*\"]', 0, '2020-11-09 20:06:07', '2020-11-09 20:06:07', '2021-11-09 15:06:07'),
('e1c3d227bbc91b89904b6f7cdfafce2d0c77f52193ba248fb18b9464faeec694b08b6e6c726edcd6', 148, 4, NULL, '[]', 0, '2020-09-20 14:47:40', '2020-09-20 14:47:40', '2021-09-20 10:47:40'),
('e1fb4abb42abb7ab9a8eb3ff5f965d8ebc6bd61fdcadd044b7023cb08a4e1087ca5c2ce14855e0f0', 1, 2, NULL, '[\"*\"]', 0, '2020-04-05 13:33:40', '2020-04-05 13:33:40', '2021-04-05 14:33:40'),
('e23fa709b8393d9400fbca54ab754cbfd77f63f7bef2f2ddd19b535e0d946533d81575491215f47d', 15, 2, NULL, '[]', 0, '2020-04-10 21:45:19', '2020-04-10 21:45:19', '2021-04-10 22:45:19'),
('e243ab9d62221f00f8e72cebee13838e30442dbebd85355df2b16b40f482b64594e1234218462f59', 59, 2, NULL, '[\"*\"]', 0, '2020-07-16 10:27:06', '2020-07-16 10:27:06', '2021-07-16 06:27:06'),
('e27d03eea2ed3bc2307e9c6d11bb8913f4188c84ab08f62daa999ce2db96bf861f8095305f6af387', 12, 2, NULL, '[]', 0, '2020-04-05 15:12:18', '2020-04-05 15:12:18', '2021-04-05 16:12:18'),
('e3d17bdfe2ae478168d3b1193e7959c722ba013d99dc1879d6c2c1914c7a04ff2562e7e58fcc9d94', 2, 2, NULL, '[\"*\"]', 0, '2020-04-15 23:11:35', '2020-04-15 23:11:35', '2021-04-16 00:11:35'),
('e3d86c5b9169ecf45aa5e738b76d1b2b7e22e661defde7908eade02718df3c2021a293406314859a', 16, 2, NULL, '[\"*\"]', 0, '2020-06-24 00:36:16', '2020-06-24 00:36:16', '2021-06-23 20:36:16'),
('e4a0245dd9105042496cdb9c0f9176000b6cf6cc296fb4bdabe7f1ace340070579244591a0623b66', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 22:50:02', '2020-04-08 22:50:02', '2021-04-08 23:50:02'),
('e4bfc04c36bdd4b70897246f53ba17e6cefb1ff24686daaf32679edaf3c3a4144b6026c02059813d', 1, 2, NULL, '[]', 0, '2020-03-02 14:06:53', '2020-03-02 14:06:53', '2021-03-02 15:06:53'),
('e4dd57c5ad442722b50d9374ad64d04dfa242a6406a3502ef87734036a357fa5f4d299003b2c2bc2', 16, 2, NULL, '[\"*\"]', 0, '2020-10-23 12:02:42', '2020-10-23 12:02:42', '2021-10-23 08:02:42'),
('e4f2d3ccb5a8a413015a92049150c6cc5cee1213a00b629f41cc8916472dc9990a6a2a9df3e632cc', 1, 2, NULL, '[]', 0, '2020-04-27 14:48:40', '2020-04-27 14:48:40', '2021-04-27 10:48:40'),
('e4fca360388666609132e87e647f050ab34b9f269b5fd7b606eb8728f557fad50b0ad6d7d43de95c', 166, 4, NULL, '[]', 0, '2020-11-05 13:57:42', '2020-11-05 13:57:42', '2021-11-05 08:57:42'),
('e526e2440fbbe13dcfce30b0d6b2ba4c55d57b411f923b71705edf4ab3d21f9e66483ffbf859566a', 145, 2, NULL, '[\"*\"]', 0, '2020-11-05 19:30:53', '2020-11-05 19:30:53', '2021-11-05 14:30:53'),
('e5af6450d03def79f6773fcc175ed4056a961675b9f04fc0c68d64a6d37afd74e2b1880a7eb130c2', 1, 2, NULL, '[]', 0, '2020-03-09 07:57:52', '2020-03-09 07:57:52', '2021-03-09 08:57:52'),
('e5e68bb5c1256e1333c4a168b4d938347f8fad2f9dd5e443932d0992617bea72ad7cb944d23d7ef9', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 01:43:56', '2020-04-10 01:43:56', '2021-04-10 02:43:56'),
('e76905eb37eda864160a646b40e9bb42872fa4d6a9875a1543d07c5c4637211407a71d1174a443c7', 1, 2, NULL, '[]', 0, '2020-03-03 15:17:48', '2020-03-03 15:17:48', '2021-03-03 16:17:48'),
('e78e0801dbcfcb300cdca740be3043a99b3cd088688e28cc719ede11851e215adf2b52fcedb246d2', 166, 4, NULL, '[]', 0, '2020-11-04 20:32:35', '2020-11-04 20:32:35', '2021-11-04 15:32:35'),
('e84d6cb8626c5c54ae771727a7cd0631e714d2813bb5a6766fa38bdde606c038beeb5fa83bf2db00', 16, 2, NULL, '[]', 0, '2020-07-19 17:24:57', '2020-07-19 17:24:57', '2021-07-19 13:24:57'),
('e87d45ca9dce4901e58a4df033213fa53eadb4a2cec7c88531059dd29d5487f6d4b70ac0a334ca1f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-17 10:46:24', '2020-04-17 10:46:24', '2021-04-17 11:46:24'),
('e95a472b08c37086a0ca18c24976d33cf9f12b3714d82958fc4b5db60fc25cf6c4880bc06b0a2ffa', 1, 2, NULL, '[]', 0, '2020-03-15 23:27:49', '2020-03-15 23:27:49', '2021-03-16 00:27:49'),
('e96c31071a76419c1343efab1f06649301bc1ce5e89e3496b996c6394f4d61ae5b2ffa2b0ac6424a', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 01:20:04', '2020-04-10 01:20:04', '2021-04-10 02:20:04'),
('e9d77df0ef673b0d2150d11fb1e124677e1a5561e41b7e311508840a6460372773cec2315bc5c005', 1, 2, NULL, '[\"*\"]', 0, '2020-04-06 22:53:02', '2020-04-06 22:53:02', '2021-04-06 23:53:02'),
('ea86c85ab5989abe627d1c51bdc6aa0ecd6150fcf927fc49c5a4a798d9fc31c4878b5070d89c9549', 85, 2, NULL, '[\"*\"]', 0, '2020-08-28 22:10:54', '2020-08-28 22:10:54', '2021-08-28 18:10:54'),
('ea91ff6a6b0fc5f8de0a34dc75f73a5363df03ef27805b107433f063dcdeb0d828cf54ae7014dbf4', 1, 2, NULL, '[]', 0, '2020-03-02 10:46:13', '2020-03-02 10:46:13', '2021-03-02 11:46:13'),
('eaa19c404af00e33937bf7dbb1f27f55ce407e41efef73a81ed0c9acad77f5d0357c1affbdfc39eb', 1, 4, NULL, '[]', 0, '2020-08-04 09:20:20', '2020-08-04 09:20:20', '2021-08-04 10:20:20'),
('eae58dc14ffa9c04dd501a7efcc200926e070541bd34d9fb7e56560271240bacc83c6056d65825d1', 1, 2, NULL, '[\"*\"]', 0, '2020-06-03 00:25:25', '2020-06-03 00:25:25', '2021-06-02 20:25:25'),
('eb8f79d269752311999fc6ef4b35e38822beedc626d4833726b228f063b85cac8a704342da29d347', 2, 4, NULL, '[]', 0, '2020-08-06 22:09:09', '2020-08-06 22:09:09', '2021-08-06 23:09:09'),
('ebfeaaeb76a38de9101676f4c79935ac16744949919e83ed33204eeab1f13413ca858f5623fd153f', 1, 2, NULL, '[\"*\"]', 0, '2020-04-04 22:45:28', '2020-04-04 22:45:28', '2021-04-04 23:45:28'),
('ec387fa7837fd8292bb48bf2655ffd8bbdc2a48c69bd3da43b55d71f945f866c1e6f58e0e3fa9ee7', 72, 4, NULL, '[]', 0, '2020-08-11 08:32:49', '2020-08-11 08:32:49', '2021-08-11 09:32:49'),
('ec5cdcc9399514d6c6e998238808c0588c7fb44590797e3caa4448e0fb1d4e00799b44ecb82120d3', 2, 2, NULL, '[]', 0, '2020-04-16 11:45:45', '2020-04-16 11:45:45', '2021-04-16 12:45:45'),
('ed2466da1fa329d76560cce7bdeb86afada79a7c44b3195f57eb85f01ad22f30767f5b20438c672e', 2, 2, NULL, '[\"*\"]', 0, '2020-04-08 22:46:55', '2020-04-08 22:46:55', '2021-04-08 23:46:55'),
('ed42d7633f6ab71b611fff96d8ce52eeadbe24088608cfa6de4dd080e53a955bec8ca92a960c07a9', 17, 2, NULL, '[]', 0, '2020-06-11 14:33:37', '2020-06-11 14:33:37', '2021-06-11 10:33:37'),
('edd794137547c202ed44d6d7e9a0569acfba028face19cf5ba4e96e9479d3bf3553b4b72cf28691c', 203, 4, NULL, '[]', 0, '2020-11-05 20:41:12', '2020-11-05 20:41:12', '2021-11-05 15:41:12'),
('ee52f938609e7376bd46d677aa3e21c46e8fbd574e452d3b5cf5f960c3c21bdd031d540d559be458', 11, 2, NULL, '[\"*\"]', 0, '2020-03-15 22:51:58', '2020-03-15 22:51:58', '2021-03-15 23:51:58'),
('eed19e905e43bbb8ad9a790cee7752be85dce7e307975250f558e72bc52ef5e573335aef9b96db30', 56, 2, NULL, '[\"*\"]', 0, '2020-07-04 16:00:48', '2020-07-04 16:00:48', '2021-07-04 12:00:48'),
('eff426a36aae178cdecb996ac01f3c7432b594e3575a6bd18cf3ec984b2a07c34caa97f2312a2392', 141, 4, NULL, '[]', 0, '2020-09-20 02:20:00', '2020-09-20 02:20:00', '2021-09-19 22:20:00'),
('f0bf5b5c632f38354b3e5d3f176b7e509918994254f5395206a9b0aac05918759ea8415ce36cc42a', 16, 4, NULL, '[]', 0, '2020-10-28 16:54:47', '2020-10-28 16:54:47', '2021-10-28 12:54:47'),
('f0e7446a0c09a3c0ef3488fa457819c3c3a027c0b799b960498f0331a76904d3cc73469d1c1cb297', 1, 4, NULL, '[\"*\"]', 0, '2020-08-04 02:48:19', '2020-08-04 02:48:19', '2021-08-04 03:48:19'),
('f1557fd98573d3cfe264c9fe62a21ffb25697ba2a4eec982b9cdd38647e5f84abeb05e2ea7d70f1d', 1, 2, NULL, '[]', 0, '2020-03-03 17:48:16', '2020-03-03 17:48:16', '2021-03-03 18:48:16'),
('f275dee64a2ea089726c92bab295bb5c04d97f914484168d6e1ee2a06022cb8832e36a7267a70d12', 16, 2, NULL, '[]', 0, '2020-04-28 15:10:08', '2020-04-28 15:10:08', '2021-04-28 11:10:08'),
('f2b6891d6428ffd5e6047439d67f90bd21c516ae7ec04ed785b745693f7de0703d1661f0d268e343', 193, 4, NULL, '[]', 0, '2020-11-04 13:55:25', '2020-11-04 13:55:25', '2021-11-04 08:55:25'),
('f2c7e71a22556ebb39909d2eb4c720e52a0c76b0418479c11293352ad989e9cf72aab1a228dd73e9', 16, 4, NULL, '[]', 0, '2020-08-23 01:54:48', '2020-08-23 01:54:48', '2021-08-22 21:54:48'),
('f3d21096290c04809884bf39dec8d9593d24575688827e23950088d701ee741a1987d9c65ce0ee98', 119, 4, NULL, '[]', 0, '2020-09-18 10:23:14', '2020-09-18 10:23:14', '2021-09-18 06:23:14'),
('f439d711eb6d32ca8ae3cdf3b5193c2bb396bc2f1ce61af01ed7661014b205cdd43dace66d4927b7', 211, 4, NULL, '[]', 0, '2020-11-06 15:30:05', '2020-11-06 15:30:05', '2021-11-06 10:30:05'),
('f4420a27b540755f2e5058b4a27b5d433321dac9b516a80503c1e3e8c8364c7dc0105f9eb33de6a0', 136, 2, NULL, '[\"*\"]', 0, '2020-09-19 09:36:47', '2020-09-19 09:36:47', '2021-09-19 05:36:47'),
('f4978b05f2b3d476eeb3deacb4cddd1b7970713cc5842425615503e2a3e9a47bfb49171f61e7d090', 154, 4, NULL, '[]', 0, '2020-10-29 14:30:15', '2020-10-29 14:30:15', '2021-10-29 10:30:15'),
('f6b4731c3c5a997934d932c2bb5e1cf4fc1e08c060540c1995a030eb5372f30322835a6b99720b38', 169, 4, NULL, '[]', 0, '2020-11-02 11:57:39', '2020-11-02 11:57:39', '2021-11-02 06:57:39'),
('f7261b10ce5d0cd2a569a65bccdb667eddedf5344bda9dedc615abf53c5c1013f0ec8a4c612fc7b1', 1, 2, NULL, '[]', 0, '2020-06-16 19:31:24', '2020-06-16 19:31:24', '2021-06-16 15:31:24'),
('f79fcbec3085103b8e85870830f442ea3d1a048c0e7e472bf2321e849a02b76d43c54005d8a83446', 184, 4, NULL, '[]', 0, '2020-11-03 23:54:34', '2020-11-03 23:54:34', '2021-11-03 18:54:34'),
('f814556b3de5663556c24a5e3814835215a7f95110f961a75452262dde1cb0406b00aca88004238d', 16, 4, NULL, '[]', 0, '2020-08-16 00:27:01', '2020-08-16 00:27:01', '2021-08-15 20:27:01'),
('f83614d3b566bca2f32b58a4b9ed9cefcc55691415ccbe3c7e53a0ae6f86234a8a2781e6ef2ae110', 142, 2, NULL, '[\"*\"]', 0, '2020-09-20 13:28:48', '2020-09-20 13:28:48', '2021-09-20 09:28:48'),
('f8497e8f1f9a2f16a081b4ea856cfe7d8c9c2a0d135201c248749fabb283509d50065a4acd2f18ef', 202, 4, NULL, '[]', 0, '2020-11-05 14:36:33', '2020-11-05 14:36:33', '2021-11-05 09:36:33'),
('f8b89f5628850a1bde21b51b614df3c15c0c0dbabf345862156a531ab8799f1cb0725b728bf06e4f', 16, 2, NULL, '[]', 0, '2020-04-28 14:49:55', '2020-04-28 14:49:55', '2021-04-28 10:49:55'),
('fa2a6d24d908e741f00987146d9bf4a2fc6c1ae618c21604a83934276d3f196ddfd020659280743a', 96, 4, NULL, '[]', 0, '2020-09-06 16:44:34', '2020-09-06 16:44:34', '2021-09-06 12:44:34'),
('fab31f7f12c1e9cd6e1d6ba7099926ff3d0e906ec7ce6edf741456d00091fddff265a2af9933263f', 183, 2, NULL, '[\"*\"]', 0, '2020-11-07 16:33:32', '2020-11-07 16:33:32', '2021-11-07 11:33:32'),
('fb58bb375bee83d9c35ed98eae37f8c74587e3a5d21c19b6f0f4f93da5f1a33cf4d7115f66445ece', 1, 2, NULL, '[]', 0, '2020-06-10 19:20:11', '2020-06-10 19:20:11', '2021-06-10 15:20:11'),
('fb8340feaa01bf0de75fd4b5085dcad583783e1ff12250f969502625334dddfe2d984522c573ea03', 68, 4, NULL, '[]', 0, '2020-08-04 03:12:10', '2020-08-04 03:12:10', '2021-08-04 04:12:10'),
('fbed94a0840ede4fb5e9446556b5c107ea370da46c96581fe7382fac1a519f38648bb94cf59f720c', 1, 2, NULL, '[]', 0, '2020-03-15 23:38:52', '2020-03-15 23:38:52', '2021-03-16 00:38:52'),
('fca5eebc59795223a88796c5509842d2c38ebd6f29ee3a6c3914185b3cad5113e2fcf078a5038030', 107, 4, NULL, '[]', 0, '2020-09-08 10:20:28', '2020-09-08 10:20:28', '2021-09-08 06:20:28'),
('fd12dd1f66cf6560742bde874d12fc306b019ef7b5353641e88bf7db175ea3b5b11f48916a8426e6', 17, 2, NULL, '[]', 0, '2020-06-10 18:40:46', '2020-06-10 18:40:46', '2021-06-10 14:40:46'),
('fd737651edf7d5b18429db041d3da848f7cd28cc8c90b7ece0f141f201118c60772dabb4492d8f83', 1, 2, NULL, '[\"*\"]', 0, '2020-03-28 20:57:39', '2020-03-28 20:57:39', '2021-03-28 21:57:39'),
('fead3b8dca9aeaae4d3fcdf5aa4383d87ce0c0fe70ea3bf6c87c992da7e53f18c68468b8198f2c9e', 1, 2, NULL, '[\"*\"]', 0, '2020-04-28 02:28:22', '2020-04-28 02:28:22', '2021-04-27 22:28:22'),
('ffd18795ade1e3f3c06c85f5c7c40b3d0e9bfe1433cef488b48132985d07828e4b6404945dfd2d1f', 2, 2, NULL, '[\"*\"]', 0, '2020-04-10 00:56:58', '2020-04-10 00:56:58', '2021-04-10 01:56:58'),
('ffe97fbed5e7af732d2eef2e9b6a9c89e820fba4e5467dc9bdfc3854af3e4b7fd9d9680044035ecb', 212, 2, NULL, '[\"*\"]', 0, '2020-11-06 19:53:20', '2020-11-06 19:53:20', '2021-11-06 14:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'NSTj7ZzfdxFGJcTpAgJcmbF0BRH96JNue3zYcwuC', 'http://localhost', 1, 0, 0, '2020-02-23 22:14:06', '2020-02-23 22:14:06'),
(2, NULL, 'Laravel Password Grant Client', 'GJxmN1xNVLcxvgJbiM4QZIyy8BaFi8kArcEGcJNP', 'http://localhost', 0, 1, 0, '2020-02-23 22:14:07', '2020-02-23 22:14:07'),
(3, NULL, 'Laravel Personal Access Client', '4TyYcGpB4yRQM9WwvqljPSGn5nLGKD6VXHnPlToJ', 'http://localhost', 1, 0, 0, '2020-08-04 01:59:02', '2020-08-04 01:59:02'),
(4, NULL, 'Laravel Password Grant Client', 'JODf1TSf4Wnc4qo2Clx2XMmNqH7vWxgSVHfpjGuH', 'http://localhost', 0, 1, 0, '2020-08-04 01:59:02', '2020-08-04 01:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-02-23 22:14:07', '2020-02-23 22:14:07'),
(2, 3, '2020-08-04 01:59:02', '2020-08-04 01:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('00b373498e0933a851bb1057673871538705b501460d3af03ef70618aa7b899300663e69628c9675', 'd8057c42b4d07442f5a3f1b9564944a399bdfc65127294a01bd0390c0f33619f1b27ec40c7c09b61', 0, '2021-03-16 00:03:00'),
('01a6d9ba292d4a0fb36b0412eec00212cf15afc3be4e83bc59300d63b0d0e04aacb2dbf8a37fb581', 'ad66c81b4e03431e9aa99c1f364d2c260efcbfe3b9996ac30f1b05fc517fc0c8242341303e6295c9', 0, '2021-04-05 22:48:18'),
('01b161703ef049252bb7b46c310f724fcfcfdfe5cf5ceda9b7e017f8f5d58acc69ea7ad1cf0d26b0', 'c8ef1adcdb047d72c36f655bfc9f6bf686e74baeac2ad503b5d99dc51d62b5c7a98de59e8986180f', 0, '2021-11-03 14:29:06'),
('01d14440fc1d79a20c90d7bcb8420a6c446a9499117ef1622437931fe281046222375b033e2d3535', '186e9cd85ab9f90e2081510ed24cc4b3b6568b878159e91010c2b809a5fda2f4efec9860b63abd52', 0, '2021-10-30 20:57:43'),
('01e7d4fc05b2fd00eea10982f575eb6e127301b30b6f8ec15af0da879e3dbe8de610e7c7b88610a2', 'db2e3305c8adc045a4f099ada8bdc8c485a960fabaa53b9b4f373f65593145ff3e31362c0fbec908', 0, '2021-04-27 22:37:59'),
('01fe910f46434227d95cc978212525b1cc08b9694274ba6ba06ba11e5baec84c86b619db65c468db', '23b2710ae5435b855f1c546a1a15c042596109427992cd383f06965bf8238d13b277b571530b9aa0', 0, '2021-11-06 18:21:33'),
('02280c859d6edcf330ee73a6354e92311e7a91bf77164c505b3f7ab1364a2fee5f30651320c5aa13', '1be9722310b36681eb692a32149cfe666d8760aa216f6bd1701b97a61b66d393b6f9fa8d4815f99a', 0, '2021-03-28 20:15:25'),
('0272298ec09fa7802db41fa6bd03bd2ee4f362a28ab3590d8c0fb55fa0bb277dc9fc74456aa0c95a', '48e9ca8e9f7bbdec68514b69b2caa087a94be8c6262d5c97004fce7d0c78376551ecb5e5d6c9d789', 0, '2021-04-27 22:31:54'),
('028f3ac34cefcdf908cf79ab5f820527a6773643804462fdb561173d76289ae778b697f9a599d7c3', 'd51d7f781e250387eaa4869a6a68f3dc981c561248240a3bed789169be553025a7e65562c705dce8', 0, '2021-11-03 20:42:02'),
('0365824fb240b511ec7f5cee7047d7a051da6bfdb4f2c783b1a83442d677f219a314175833efcdf6', '742b7a2876f2b55d663ca3c6ab2cfb7d124d74608ef9ad0bdb52e7dfd1508aa543a86dc66a535e91', 0, '2021-04-27 22:35:19'),
('03a8f07acfb24aa5232978b8d235bcc4d015a04b7d465b62b1d50f26bade391977f347a730eb945c', '7f452d42e4bac355adbd1fd412b9da54a865b60c2f3662b6bae6d6a64fa5c8bc2dd429b3aa74ec01', 0, '2021-04-05 14:13:43'),
('040911b7233bef9f2b757fcbc3542f00f9f4e156474b263979e521c46d7c0adea5afaea9bf2d48c2', '7bab5d4cb68549f0cb2fcd321929966841f47c486728a7748f83e2a40a606135d9a281b6b50cf125', 0, '2021-11-07 06:02:05'),
('0497548aa7d461f6e327ed3b651913ac94a5d4279a3a562b0e09a4f89eeb3c56a0ad0f7f3c617fb4', '8a6054392276530c88e82921d562d0ec4ddd408d7d6831ce9ef5b554b9cffbb53a102b9e61e522f6', 0, '2021-06-18 23:07:06'),
('049c284c670019d3be7a534ed1ce3c0649305ce429075cf674e97787a14313b106023454402ab124', '1dfd68371ee87008002d28dd3fcf9da9d88a3caeee0ff8009a82ca254795c24520d5c66e50c981fa', 0, '2021-03-16 00:04:54'),
('04b53e42af83315c675d2e9e1db5f13fb9b1c44dc299cf71463d96910adb2f92eda51b4878d4a493', 'c992a1f7bba5f15171928f5e28a7d3ada891c524e01fb19031dd3c5963065706f3c133db38b5dad4', 0, '2021-08-04 02:59:49'),
('04df1cc70634039c29ae9b6c0ea9a56efcf0b60ab197a0be78a31b78bde0110dadbb140baf31e361', 'd857c0fa88a773d7451463add51f27a8dc172efff067cd1a251d65c99ec485b9f4e0e2dfcabe8bc0', 0, '2021-04-24 23:24:06'),
('04e17b94ee783fc227ccf74243f8e936c648c5ec5890d530ee45c8b8cf01db20170f849e8e5c3221', 'f2c7e71a22556ebb39909d2eb4c720e52a0c76b0418479c11293352ad989e9cf72aab1a228dd73e9', 0, '2021-08-22 21:54:48'),
('057cf5aeb91ab8a1271b226a3761517b3b88499f91209152d70ddc03b27982ac439ecc5fe25bb445', '89519570805951e4e7efc68b6ee6d7d50944633038b0433b26d14f498f5006337398697978cff6da', 0, '2021-05-21 08:15:08'),
('0659b4c921d14f2e9d1942d57afced36be097a3690e4dcd06a246bff1dbbf098403fe3894e1330c5', '17b59c84ce54f56877e074b30f8010bf954420cc1de82d1a35dbe3703aadcdefb8360c7baee69221', 0, '2021-04-09 09:23:06'),
('06ba3cf19bbb70669c1909cd1e13a2de0cac9825e0d4859a13764d2905320962aff7402f8db26248', '1231e63004c97e74defcf13f7f7a1e66698d12f2bca7f3194fd53e9792335b79b3cd39bc268c26d3', 0, '2021-10-29 10:02:11'),
('078f6355d1e19bb6f476fbbc2961c2320a4dd8b03c04f131a590576773741b4214644855300284ce', '0d0f04a598b9a8fe321737e172151778e0e7560a0b9ebb7cf99387c27286aeee5af815c50bf6c55a', 0, '2021-08-09 07:17:32'),
('0844d8195547a7176864793cbd5825be51667a7ab90394cb65e22f84a1d49d98628193e96e1d5037', '5f6a5404bba5554fe264a844fd0a0ab787a19821a582ef35608d72f550eaa442e841bac31271cdd5', 0, '2021-07-03 18:24:29'),
('089185d3a09c27cd8e1e89713b60a5e8d6dfc8546f070c89aef6e3ff9545adced21fba9739b1cf6b', 'c1cf7bad86c1d48f53338042e6c1d379f10398ba047bf4fdab28ce934cad897d38bd962e130bde62', 0, '2021-11-06 14:00:50'),
('08e3c2c7d00d5b12896ad53e6cc443ed91d3b854c72c9d47db0f4f3639fb6e18fbeda78a5b7f55c4', '631160212996f9035c0388be653db1ec00ded5802f448a555e47e526380e73fcad452171dc65059e', 0, '2021-11-04 07:38:18'),
('08e57e363b5d256953de9981394b281e8f73ea7c94e8ded9cde039c0be469bfe6c686925b303879d', '48b4443604246306d2cd1b53c1b6c5a8f1c18a3284f0dd5643e7edb832642f4a036389ba02bb4606', 0, '2021-07-12 20:58:36'),
('09070f2012190293b9b1dc51dc335b7a78bd06a66b61810d3e33b32d2ed071e77ab52bcdcfeb9421', 'eaa19c404af00e33937bf7dbb1f27f55ce407e41efef73a81ed0c9acad77f5d0357c1affbdfc39eb', 0, '2021-08-04 10:20:20'),
('090b3727a411be0eb59ec7bf192f453d43d3a8c028b4b672d6fa1845f3968f76b6e8e64bb0ff15eb', 'd7102fad91ac9c828d5f78e4536adc4119655273d1eadd2c404d499a3b22d31cda7c05eb231a0925', 0, '2021-08-11 10:31:36'),
('093cb54474e8cdf1a0ff85a551598fcdf725e82defbd2b3ace34b4585eb16cf1ec41e1c5649d847b', '6fc4a89c877cd6c94c77c14f99049005cc4b54ece4a7022b5b7dbb239dfb3b0983b955cd6b32b758', 0, '2021-08-04 10:18:48'),
('0972a8b26a22184f17ac4397d362cca1d6f640f6b0a9f0cefffa18e5c2765819af5a51c0a1eb1eac', '444440783ecf3300fa32b8afeba29df5aa6793154a39c1fbbf1389b3436d661990d5091033c11543', 0, '2021-07-10 07:56:12'),
('09e68f5c795b96085c7a07eb28909cb7f45d5ad72059be23924c50a86c1b99ca6aed63ad3e695965', 'fb58bb375bee83d9c35ed98eae37f8c74587e3a5d21c19b6f0f4f93da5f1a33cf4d7115f66445ece', 0, '2021-06-10 15:20:11'),
('09fe67371c9a756d5fb4084a2cc9ad22f35197b26dcbb03b5bd144809e4c0b982eafb59c30c99a38', 'bd47f7bbf5cdcc2f9351c349d07dd7ebf70b071ac6471239a507a00bf56f18bd5e435fa431e9747c', 0, '2021-03-16 00:35:06'),
('0a257dc15ad924201c87878eff59be5c8672c27babf61a56bbcec79c7fdb333ea355eb160d145d0e', '322cdbc6033c929d732194c5a77e57874031ac166e81594e9ef7dfc74d6ba1f3ca1c6272ba601290', 0, '2021-11-01 12:23:33'),
('0b0cb427cb588a51c314c40ddaf158004a6220f5f48989433aec0ac401ea3d0f33b0197a2fccc240', 'e4fca360388666609132e87e647f050ab34b9f269b5fd7b606eb8728f557fad50b0ad6d7d43de95c', 0, '2021-11-05 08:57:42'),
('0b39ceb27b9975e4b2398b7d064cded59c91972018576f66d27f1af8111ba9c92c9011c600de1b22', '5cfac16c4e78554a81145c90fbac6102afce3bf5f5d6914d52fde5085d86792d31328ad8fb13b17a', 0, '2021-08-22 08:40:03'),
('0b461c184c155b9553812e21fe21dba1cacb94515ffcb6e71da817241a092cb10efdd110fb1bbf3d', 'e95a472b08c37086a0ca18c24976d33cf9f12b3714d82958fc4b5db60fc25cf6c4880bc06b0a2ffa', 0, '2021-03-16 00:27:49'),
('0b7d8427a2efb3ea836625a978025ad90e4c3f98d97502f8b3ace079abf1b9ed4ad8e48b4d1c7eed', '3968f72ec7ab1e6ee84483e527d95c915dab716145b7dfc16ae8e3a2f39a55c74e6fe7532b016527', 0, '2021-04-16 23:17:07'),
('0d29c454a8303f87beb987ebc5b42ff2b217e0cf7e4d0d56634447ee5f244cc6db3a98b3c3673aa4', '5af8b583e2ee181c9fb356639b0d4f181b2fcf88da8c0538ecbba6b33ef180d8ef968439429b3464', 0, '2021-06-17 06:16:35'),
('0ddf894fd2283bf9bb1a808506ce5f6e9b8bba3863df3a51b018243f7b5ef9b18e1c3c4cce28a99e', 'cf25b4c0e4419e6f6e2fece0561d4f3dac2f9e3d728d4b1d6ce06c8ee81baa5b7bdef66da540a15d', 0, '2021-08-11 09:26:50'),
('0e35fbf05de2f0632e41eb6e816d0c9e036a02620edea4a6140f842d94e68c85391fc38cbc7a735c', '88a9d3c20c31c40730d090aabf9f2de6958ad18dd134bacbaf80cafa2a6d5b15d86cf38a55e2510a', 0, '2021-09-18 10:17:16'),
('0e39c981c4ae3363a1d8f5242cd233ecebbc6d5250aeaeaafe3e3a819f82c027fb5171e539249cb6', '20c61868611e6432bca83aab41ebfd22da0f2ff25f38b0624351e1cb1c2f616d225808d3788f0422', 0, '2021-09-20 07:45:06'),
('107f2f4dcb432ddd18ca3136b29465db5d46197badd7459a2c049ee3ff6e9ab7833b5e7ba80ec187', 'c925a6b3672ef129a264aa8f11b335c649ca8045145b8267722813fff3f26fcc18aafe335069506f', 0, '2021-07-03 20:29:48'),
('10c7fca182571e1e00dda3e42809319dd0fc44811e15f6cfc765969248ac68246f2b1fc16dc49f87', '21b6134fd4a3098f6dd4eb299e0247615e6b354d0ec5ad0640ab226bdf7db00e1fb9f7d9affd791f', 0, '2021-04-16 00:10:36'),
('110e802c9e7c43b4edabf83f311ffea6297e6fc70bd3b456f4ae70d990779f3b298a4a9c90290b45', '2fe3f7a073c196f1fb558ce260a84430c43aa6e0360c66c5c10ddee8753452c070de350ecd1306d7', 0, '2021-03-13 20:14:05'),
('11a2c073b1553a54ada367c835c318a02c3104a54c47cd7424953e0d66e6b6521ff1b813f0d084a2', '58eb570d71252d714854cf6280894afa5d4f22f8d7d73c04937a117d6ced32977c55cc83c00be748', 0, '2021-08-11 10:08:17'),
('12a7502b8ac9dab398c6a38bc3df046536b341e289d96a596d43c70a140a43f8bf9b7233d2677068', '05e736c0bb3b064d65ffe11b9b065d707d40a28cc01784a0c3918a8b02b419875f4edcfefde5b768', 0, '2021-11-05 17:22:23'),
('12abe1f31ffd905a1ce12cd1b28ab78f6f9fafbef9ab3338d86b68f232ca1cc85dd833b875641022', '2f98aec349fabaa368368883241982379c50613438b997673d7823cae612b9fbce40df8c4d3fb1d8', 0, '2021-09-20 08:17:28'),
('13007acd22d4447d8b9a3cf8e1214fdf9f208a78e9b28c8c010662d1fd22b919e50ff255ef4ddd79', 'de3f50b7ab7bf87fde691b74a911bb7804ed142cd61a2dc588841d2f83c16eab706dfd50594b7b54', 0, '2021-04-27 22:48:57'),
('133aded68f031e0e9c9d226707ea3e0e8e2cf4d491057bb3d31743bc35fdb025ee605cd558a16b8f', '8128fc2fe625f26c9aee49823ca57d2091928bfd5b5752aa4d120a020d73feeb70bfbec0605d216b', 0, '2021-04-10 02:32:35'),
('13622249f8b4888a6a267c69020b93eed7b28bdb1c6073d08aa97bbbfedd78798a01c35fe201e9ad', 'f439d711eb6d32ca8ae3cdf3b5193c2bb396bc2f1ce61af01ed7661014b205cdd43dace66d4927b7', 0, '2021-11-06 10:30:05'),
('1379616b5a77d27bfd57dc94380157bb79dee7136e2661d6b880bc47d2e76548861ef084c383caa3', '921d994e2d1948d829d960879e70ff9059af7e8491d08c517913e7d8612c4e4b5de3c56f0c3334a5', 0, '2021-11-05 14:19:01'),
('150d52d2884a42f7756b13accc3a4ffa0cca34c26d5ed0dd35008f4012980d98b25d00995bf8dd24', '42f640ee0952faa5083877a74410743b0266186c5f19e96fef980094523a62a4c9e3182455fc948b', 0, '2021-08-28 09:35:17'),
('151d6ac51b3a0442fa41561eb4314a7bac23d862f1c0800b8d4f0f506663a67cbcbdfde291b3fbf1', '71aa0065c53062c45a4b1fcbab8bbdb71af58d1caff1693079092c897aa4019d52e8a4250a60e094', 0, '2021-11-03 06:44:02'),
('1582251d16a03d5b8e761455cfe9dbd354107c6afd585f1274f385b1a05a9f9d1c059190e9346a7f', '04233e78bb73f77c528b6c5bf2a0a6904a6aeaa01c426e91ef20289f50b4319ef5178e9b83853902', 0, '2021-06-02 20:42:48'),
('16537fa2d5e60d975ec5d6269d74771e8392387cb993576012d9bee179730b232edc8d5f50e1e074', '1a5bd0ef32e55473d63d94c781a0290fc10538d35d679630f5f6dc517de16220ac2e409ba0fda20a', 0, '2021-11-04 06:50:41'),
('165acaa3b97a19dd548055adcf6c2237fc366ffa0003ace411513d988427b4b359f82b66b1695e0e', 'b44ca8040061bfb78d4abe2396eeb877c3958839b746b56839b297db5e08d1b1f891a1edc7ea4c7e', 0, '2021-11-05 07:24:51'),
('16de7f37bcb10be756e4ba5973cd8bbe292c7d152968182203ba21b5f12dbdb0a9e4432138ced163', '7ac6a4e03957fa7235fa4b9d6d15995a042df93edeb4ebbe6925f4bee02dae9972f0d63aaf00ab8a', 0, '2021-05-23 18:16:46'),
('17495d4384f8902f623c2e682b576c058d9d05417613dfd0c98c4fd4cc0475b660acacf462c4ff66', '5c378437240292df401af31f69d5f980e2626c4ee6753706c8cb0f86b0dd88121f8306960f8f5484', 0, '2021-09-20 10:29:35'),
('174abeee5d9710c33a88afdf5447053795a575958cfe2fea6acb51a17d9282fdd3f95a7018871ee4', 'd09fdf5f6832af29a5d740c83d701599580f0804f343ca741ee3e4203f36834c14550557caf9e186', 0, '2021-07-19 13:30:34'),
('17d5b6ebb1dab6d8f831c8f11a762b964860fdf6bce768dcd06594d56002ca1d6a7e811506286fdc', 'd7d97a5b86638846c9769af802f53ef89da3f0b4a3f4496059c841e5fb3c534b70a25e736086e29e', 0, '2021-08-11 13:07:17'),
('181652cde31beb1fe17d205ba0c6554a8decb5caefa4831bfac80cd12fdd2a127f8759fe84da51b5', '67b331013881fc1f41eb64356db8e7ae5a36d13b4e6481f193e0ac50b31e9988db39460bd6a269d8', 0, '2021-04-05 16:13:32'),
('1915d0fe9d0b3afb14395e4efefae9088b8b79c7c097fd1cefc5a4c634035277c17860131fc82ddc', '1242967e126589a0501e777020d4e87f20cc09ff3fe180453dd36c500f657ea6f0cf0edb40c0443c', 0, '2021-04-25 21:33:11'),
('19c7bfd18580cc0960b37db9b27478f198ab03207b4e28338c4d93b19f3d20a0c2d3433b2419f90f', '354defe66d557f1b6d99bb051173638d9769344ce2d1c8609b44e30827aa523bbb5282af982f287f', 0, '2021-04-10 21:58:35'),
('19fbf766bcd3f8cf4d17125c48cc3a111a2e64ce15cb0a5fe2339db2309c33e40be0cc5fea2758e9', '3b7d6ed736450c323e3a040483bc6e61941cbd2ab77ab8ecb47c5d1026c703070215001542dbf2c7', 0, '2021-04-15 15:50:08'),
('1a13f6367261f3c76f2b1bc1d2368a9773ee85f0604abe018b8df46dce2cd26090bda4cadeaa18f7', '80e66db934e11ea82272f458f28b63ba90d7eae6e2f7dfbbf54159f6b709fa0acec798cd1ed0e018', 0, '2021-06-17 06:17:03'),
('1a6e2eb6aa9eaf37a7a554fb1c92176aea9b73bb27a1940abde2fd5a5be3f1f018cbf1a91e4cd6ed', '010972e34c82640ffc26b39a8e4f8438cf9a2df565065765504bed1053718b72f72cf60eb1675ce0', 0, '2021-04-07 21:13:29'),
('1aa4784021fd938ad5e0722303ac5888bea8a3f0ddcb80edbb3578384c7a6c12ba7a8022db2a4436', '82cb6415e59b639324a7f2eb37e8b740e3f6cff96cbd4979a7c451366b02812c61d754644534d5fd', 0, '2021-11-05 14:16:44'),
('1b0f1db6307808f484802f61aa2909262771dd0662848d59fe122b55acef099adb48e9246e2f5b4e', 'ac9a967be7c34d93248afc523db08982a7cd4040a8d64adfde29f8e8f7b925c7ec0f923554e5c981', 0, '2021-10-23 08:03:59'),
('1d1c77636d13d7d1211e4b116097ec2b26ea9c94affd212880bdf2a7da6cbf81f0abf98a24c5229d', '52d79b58a1a2695793187b9fc219102f052cf09b50a6b3326dd16830d9b6b1933b6cd97212eb76af', 0, '2021-04-07 21:04:20'),
('1d68734dea25b9f452436712c83a0a3ab9ffa2e48a4619489dac60db104fcaf45f12596a3c6db70e', 'a1dca887448c0ef702fce57f40d4cd8e725c46d16fc45cfea3fa4f9083721ca134ac44966a68e614', 0, '2021-09-01 12:43:18'),
('1d8f7e316677f303830caaf18aa8920531ba6f61d601f32e8b5905738bd363836429a0f2de57fc8d', '008cd3a9dcce04e9cb902209bcc073f88163b4bbed256b47a77b0ef97e3e7473517190af9bd65689', 0, '2021-04-15 22:52:17'),
('1dd84389f93fb060e4cf04d0b39f2e2bcf8378f98c768dd52814f8197398fb821ee9756d26a8b836', 'd646135d187e8dffb0ef7d85356e541e192e6b4618048ce17bd33f9cbaad9ca44b33cb8057a26d80', 0, '2021-08-22 08:26:49'),
('1dfe0972f9e4ab47a67804afe4982e07c2c2f07bd6f66b6b95c2bd18f9bb8d4eac14543ec51f6658', '5417aad37677d3476c645b3036959341d21c45ebe3ef1e1e6a6447963e8716162599fa170cb2b443', 0, '2021-10-27 05:11:11'),
('1e97d5fee1a690c51389b80f64e30251f578492f221753701da756110c6f82fc487355f1770ab8d5', '00b924d59fba3d1ab240aa29ef0352dbc1ebf4953181c4c8be366f321bcc8b322e38151f60c5cb9c', 0, '2021-04-15 17:46:57'),
('1ee488d9d953e58c1fa50fac939307d879b4534ec1ace82753b54d7c15c52b807122dbe07158ba0e', '439a6ca35ea4a098f7dcffc0a3e64473c5ab2aa3a6ad8c85e66292b7938e2b2e42c70797e7aad13b', 0, '2021-11-03 15:36:31'),
('204ae152be16a81e57750a2568f581492fa3812c4a81ab43332e9bf1781dd46050c5641087442e23', '936d9555c409d5d159220e52de509f1d6c9532a1147df3095dd98bd95507ebe399175a86900d143d', 0, '2021-03-03 16:19:06'),
('20a2bc8929a9824f12f43c9ea1fc4006e76fb0f152333f6eaaab608a3d64e4e77d92d55858bc0a4d', 'ffe97fbed5e7af732d2eef2e9b6a9c89e820fba4e5467dc9bdfc3854af3e4b7fd9d9680044035ecb', 0, '2021-11-06 14:53:20'),
('21ecd51cc3d791c56aa30645aa3180a42e138376944b66a1df89af68bc27bcdf14651162a0737c34', 'e3d17bdfe2ae478168d3b1193e7959c722ba013d99dc1879d6c2c1914c7a04ff2562e7e58fcc9d94', 0, '2021-04-16 00:11:36'),
('2228260e25961142c894151bde5610b19fdebd08e9a8383e38781078186563e63917734f8c995fdf', '732c6393c66fd3dd7b78c5695d168af70b1c89313c952c9e8ac183506e27d7f26884317d814630d7', 0, '2021-04-27 22:46:41'),
('22b6c514bc2f4dd6f3cafe2f6fa0fa88266b4caa0ee9ec5dffb3a056109258c575b83e9ba830ea3b', '894532d5dfe2983aa1f81820656d3174d739110e6ca202abc464e6b12c6fea59789ad3cb834409a7', 0, '2021-08-11 12:21:41'),
('22ed412fd643b668324cb523f5ef3b748a831828d54afd2ce2ba53b1113d744c1c5722f7dbc2364b', 'ea91ff6a6b0fc5f8de0a34dc75f73a5363df03ef27805b107433f063dcdeb0d828cf54ae7014dbf4', 0, '2021-03-02 11:46:13'),
('24250357804d08a2b089a2b77bb3ce178972edbd892d23bacec1c586b5e4cddc1308e2e2e16ce220', '4b590bb8d0b9bf5580c83f7b64fb879ced6bef09dc764687c67ddb6df66ed8dd1e0cf0e0ea75244d', 0, '2021-03-02 15:30:12'),
('244835395a863da3a500fbb5deb9378b4e0c6422e8ce0eaa351bfcbe2fd60b6714e78fcea0ab3a6b', '85e375a22d3c15be3c2e14f21f2b1571d2614d0bb8a3aed8db7403c0443a17b5b42ce860e1b42179', 0, '2021-03-28 21:55:51'),
('2536dea747bc8e52f622520f0f61dec538f1771575f59594bd471c28e6b1c9a6eb22c1329b2d6f7a', 'fab31f7f12c1e9cd6e1d6ba7099926ff3d0e906ec7ce6edf741456d00091fddff265a2af9933263f', 0, '2021-11-07 11:33:32'),
('25b47d63e2972e33983b74e66a7ce48d5d98937d1002fb8ba4401abc953d358a04ca236d49d14aa6', 'ea86c85ab5989abe627d1c51bdc6aa0ecd6150fcf927fc49c5a4a798d9fc31c4878b5070d89c9549', 0, '2021-08-28 18:10:54'),
('2661e948db7299e01033023a8b439445059d172d1f8d435625bfd07b1f4a0a30568322fa5eb74759', '806c199079c75e3b97d52de3a985bc59849b0b7bdac0eeaf3314340624c2c72277e09520969e543f', 0, '2021-04-05 13:53:14'),
('26e82398044728cb43a3346b956b0976cba5e5caaf83e14bb92f78624e3fcf4c6e8cbbad52727129', '7b055563595560c9c2f5f0574af27dc2e97882f60d5dd95bc355e3ed3e8a4a87b2e0088943a77005', 0, '2021-04-15 17:29:20'),
('271f837604cd7930716ea5ba3a3157190f8b3daba943f237ea3f390b672b3218b543c10d1fa70ff0', '4aeb02bb8546228cef7d3736261c1c4fa7b3af7f880dd9c4487519b8476f6cb391e040a691c3cfa0', 0, '2021-07-02 10:01:31'),
('275a93f0f98f219e4c36fe09153ef0c05e794a804f83c19851eb124d56721696624d1161d9e29b57', '5ce26e90f617096ddc16698130490dae17b6e6648b9965020227b38c11a96928f3cc2831fe2d410e', 0, '2021-11-04 13:48:12'),
('27df9acd936debc191facc289b9b0c987f61a10c69302ce02047823d50b7eb613fd707006e6c73fe', 'fd12dd1f66cf6560742bde874d12fc306b019ef7b5353641e88bf7db175ea3b5b11f48916a8426e6', 0, '2021-06-10 14:40:46'),
('283f29fd62c75abc3c0348b6945590c686b2b4cab5cbd4216c73036be84c439995353148b7390369', '503f107053e053c224ebe31576b07ea9dcc4f73b2ad0030663a3026ff22fcceb3943f7883a686603', 0, '2021-11-06 11:20:12'),
('288fd7211a4f7ff346a2fbc71bdeefdfa2c7c08b945cf90aa98667930b91da1eb5410f4dc6279367', '1507c4dbd3bafd001d1cecfa49e33ddef5ce1e0eb4ee2664c51e247cf05d72b20ebfc24cc744e6ac', 0, '2021-07-19 13:24:31'),
('28aba67c1e0045f822b5a1484b4c672d86b88ad23f7e9dbcda745fa830b29f04ecaae409866913c2', '5323cf75bb6fb899f5acfa1ebfad009e892d4f1539b5692e196e5347f2f436d51ec016f1c029e4ff', 0, '2021-06-24 12:51:38'),
('291cfc463ccf4112d2b28278ed1c15d58c4cb0d94f0e074d465e6bc25e15847472a25c1a57f2bdcf', '80ad27a2dd839d33a811035300a305aaff8f1fa6f26c2cb27b861e95112c2e3cd86d7cf15963db09', 0, '2021-11-07 22:22:30'),
('292d22511995eb3f1f974afadd4042c780dd396563e8fa50ce48a616d1fcc4ac69855eca4817ad87', '896dfc317129e8739027b6c325e3e9d67c9bb3156d992409010a54307fd32984a409b1f8512b4d30', 0, '2021-07-03 18:07:06'),
('29859058329610383aaa6a086d6740df16beb9dc8e45e6589e8df41dbc2ecfedb3f7b2536a7e7a88', 'aca242b5a0aa02db5dd2c9fb5af5b6603219e787025f940cf2088805a3e2ff06159f7843c0295377', 0, '2021-04-06 23:19:26'),
('29afa2348df8408d6c492e3f3c484ac6e6dd894efd01a462ae82b6218246da84ffc79007a01f93c3', '1a7b9236d2acc567d3c4970bcb3ce120569d00adb87aae3bb53479f399f4eccac35eaaeec82178dc', 0, '2021-08-17 19:12:37'),
('2a1f484391450950c7bf3260492f58f64bb18a177d29b84adaf48553392a6de98b79062153b4f6e2', 'b1e83ecc97a6154efb18f1bbcae52bc7cb4159150acf4b20b9dee91812ab2c16d655885d736a6ca3', 0, '2021-04-27 22:44:53'),
('2afb974e36f2ccb090522e995e3ea4dfffac611397ee1ba319fb695ba20162900784257252c84846', 'f79fcbec3085103b8e85870830f442ea3d1a048c0e7e472bf2321e849a02b76d43c54005d8a83446', 0, '2021-11-03 18:54:34'),
('2b6df28aae4b70d8160fc2e375c0d8446f958ab9d55a07d78619675d8f9c989040036644685bdf1a', '09a45dcf716f2b7b177c2fe2a568fcd43dc29f7c1df6406c91e275603688151404f104eeca5a7680', 0, '2021-04-28 09:37:39'),
('2bc631b6f8b37e49c7a2c1748c7b3b6eb82b1cee3358704e3b17de2218503c2aea60282f74425f4e', 'cd736ec1329ea1fe46957f9f17ec8b01c312dd4b588aa05f21531b9c6085f2abf862fa35f4ac7056', 0, '2021-06-23 20:40:06'),
('2c37215f7855ef576ea167b8543265a462aa0d257cc971b4af77a8f6141e4cc6ca0016e8469abea9', 'd8ac2c108cc29f4e6ad4b70a16a02b0554020535d969cce32399b41f5dbfa4f91c06a5824d0876b1', 0, '2021-11-06 13:52:27'),
('2c4b1881a908c18a0d6206107c83be066a00faae4ee3ff0757e129d9659e5123e76a0b44d54f43e6', 'a0ced276f11165e9d1befef96c6782d1e030cc3ab9102328d07182bd4e1dd010239e20c392953577', 0, '2021-11-04 18:00:57'),
('2c5c7c56436248a57d6fbe1da896145da1b3154ac0914544bba15ce0036eb92dc44512d6e64292c6', '97e9e094407c2f4af65e02a16c2711a60f73e8d929ed314f3bac48872e814047e675b8eafa3febfd', 0, '2021-03-06 10:37:21'),
('2df887f21592bf3292aed018c0a6c3df4b2c4bfcefbb6a3d7a147398ca4d5781bbebb14689ce0742', 'f1557fd98573d3cfe264c9fe62a21ffb25697ba2a4eec982b9cdd38647e5f84abeb05e2ea7d70f1d', 0, '2021-03-03 18:48:17'),
('2e5e372477807c2c13d56880dd76b2a94fcee644081f7ed679d0f6fba490862630035225b1e711d3', 'bd4db340de531ed9733791769655b9c4034a003f9ab9b5c1b765c578c933445bce1ac21186ce4003', 0, '2021-11-09 14:56:14'),
('2ed50697df13ed3545b22599dfc6330d16c001300ebe25bf4fff9ddff8078e57c0e3fa7ff4c8522c', 'c8726ce8d7e7f60754fab098e8b2f3014a9bd4b2c7c596a35f9f9046d41233c54e3f80c6cad742c0', 0, '2021-03-16 00:33:06'),
('30c3fa6d2f9dccd80966ae08e691825e2d39149adc819b6e9a782d9c457f90b99c7055761fc94aa9', '0113c194ed3cf58e7e3398bec311648e3c65eab89afef7700d2d134b5c112148bdc01d159d6290f3', 0, '2021-08-22 08:38:14'),
('31661fcb713c5329a0ff27504d059c37b54020eb26e835b0af6d5558f553621492804cc70965c68f', '8dec6e871d2e6ab05e74647bbffc4cc83ceddf623338f58bc5dd7697a7cc8ba96e3c4f0db6215932', 0, '2021-04-09 00:01:02'),
('319dddd73dc18b766218a12beb70195521d32334bc12b31bf4c84a1304a4b4c0882b326fbbf36115', '8e1edc4ca53a40226d6cd3278a6027b9e5ea13b41b5992f3338dce4624864472691a1eb515e341f8', 0, '2021-04-23 20:28:18'),
('32f174fb5db905ed7127d8b2e08a6ed0fdac7ff492cc5fd82345fb54996dd362755642e381925824', '1b21a92b1dc58a94a7e7f9fa1bd351dad316ab28b8805b5bf155508032b9a0e70a9ed26fa3f3af4f', 0, '2021-04-27 16:10:26'),
('33126d926350e85d7f3bbe739e5ad71044c8ce7d26b92b477015bf1ae1119e7b363b392865789f17', 'f6b4731c3c5a997934d932c2bb5e1cf4fc1e08c060540c1995a030eb5372f30322835a6b99720b38', 0, '2021-11-02 06:57:39'),
('3409b97682c4c0a216c1e06f4eec4112c155dbaae9b2aba05d17f7f38bffbf8bf3db94f8653f9016', 'cbdbc84ec509a398712273257435334b4191c35265d33359997589bc042a011cb9ebffd299ea69b7', 0, '2021-07-03 18:21:01'),
('3474504c598e85e0f3f46589123e7bc25f4c7c03555a8b72134ec9ddb8649120857cea64818befd7', '330f6411b5b5d8f04bed12b0788e9cc9fe3486f244adb5a31f8bee32c45620a709df1bf2ce85a632', 0, '2021-06-25 00:27:41'),
('34f2b41f7be942184f991abba89b9cca1d0aeb836548c79708dadc636c3ca284e3c91720ef3297b2', '95c0303f70094eae2b65ca064cc8dd3a01e864af9358b095497406f7b510522ffe28212ef9fed7ae', 0, '2021-11-03 17:41:01'),
('358a2717a8cb4ba7f9f4f80383b0fb4877871e771a52fdea5dcfd0676f58a86a0f1e94c4bb4e66ed', '281b307315fd15aef833bccf90477f51de47a895f09f52d9f5755c8be3166299482b521991599523', 0, '2021-05-14 15:21:28'),
('35dae936c53bb91915dee5a58ab8c3cfbc5f5b7fb9dc3a3a6c0f6b1cf56083f40c6fd3296690f4a7', '70c58c826388305d728d4aae0ce8e307770660e49ef18b19b2aa9d9210262b41408f25db9d604d84', 0, '2021-07-26 13:35:35'),
('369a2be1f627bb67ba14c1bb650567a487f8a6fa59f3cabc5e2dd765df0a083d220645cc3d3a6e25', 'b27915ebb952fd439ed5d0a147d9b7d6c1c01ab3c4b4e160a721302970f64fa3f586e2b061a78a2d', 0, '2021-03-02 15:32:55'),
('36f0ddfec44e3a478d2ca91790467888960442a9ca974b200a965bcc0c6d047d524e8e1a7b967db0', 'c85af333ffcc6c467e4cb201c83421d6aaf296df95140f559255521d6e858277bc438215dcdfd4c9', 0, '2021-04-27 10:44:37'),
('384553ffd534a293ae3ea3218088c8f095712d93e82654a4af9cb63153087a88e2033a17ecfde95b', 'a3819af51172dd26ed7239ccff85394d5f1cdbccedff1fe45121f024b1d4827f6df5e1f111f3d8e1', 0, '2021-07-03 18:16:17'),
('39d03737c50d8344164be99bd0dd795d69072fae916982909903ac682a24496f6c80f7e150e2541a', 'bbd788344b7624b4a200b9f73c8a22c15cec70ba5466ab50350944b4ffe54d4b9a218b449b9a69fc', 0, '2021-04-26 22:34:30'),
('39d36ab06ea9e06c8546f41447e7a2eb5cdbdfdce2a6d150fe895de843219ada8e9af3c9ac2f89ef', 'a6fd72271f8ab54897088c468b82d66778bf4b7c99e09c7a3ad98f102b7dabac2a67f697357614ef', 0, '2021-11-02 17:14:08'),
('3a2d0fe6c031291fc361415c3daa1a7bd1ce2b805c27bf9e6e2aab04cd6e5836028f8ca4469a6a39', 'd9c222b6a3b1d0d1698ef26fe97d555df5205e2fe4d60bcd31511a6e092f73349a97e490e7bcde36', 0, '2021-04-05 13:49:36'),
('3a35842d3bb12c4609e532a1a061c2f20e349a4b771e470fbd5f876fe10fb833433372e0bda22956', 'dc6f8b79952e51fe7eea3a96ba39515f2c26f981d28690b897e7928ff14947f10b3873c6704a3b0a', 0, '2021-04-06 22:57:32'),
('3aa95f19d1dab3cd0dcbb35e78383fbf4c86e20c991dec2c078facd48cc7ca8ab34986968765db6c', '2d15f8b8244eeca7f6e1cb0fd7919b6d1b2bc333608cb81ac57985ededcd23dd5adce48d0bfc374c', 0, '2021-11-05 08:33:43'),
('3b50c2c0e3459481423744ac2931c41b52d6cb6d417835d6d30b7d780b6dc2546a329f9f55892508', 'eb8f79d269752311999fc6ef4b35e38822beedc626d4833726b228f063b85cac8a704342da29d347', 0, '2021-08-06 23:09:09'),
('3b877aa158bf5c658eec521c2cff0cc30216c1ca3e9b57006ec4ff3479dec73f8340ea6986b2a1f2', '6d6aa72c10992db81689fd8b53087bc6dabb902320611d10576837b04b457b6c0235aacb07ae94e4', 0, '2021-11-03 19:21:39'),
('3c25758c1c5ffd9895825082557235ff809e0f957cd23654a852a0398dd0ec232a4d8f45e82339e7', '0ba83fb6266ac94aff65e3eb24906bcb67008e6e4f7d63fb155d9ad8f9f35fc9b7660b4f143b414a', 0, '2021-11-03 20:21:00'),
('3dfe57373166583cb7ddb7433048cb6664620ae592b7a9a1c47a498c9179d85393c5d6f00a2771da', '63d373f15987ecd70797ed5cf54f4c72b8802e0afc80ce6ec7f5b39c7cf929295af512a5e4c42051', 0, '2021-07-06 21:10:57'),
('3e0297a17511902e2f19ea36856270b70443b888b76ffbe18117ebd5b7c1f2e43f8fcfc32e88e63a', '3c6268971948b2c9dad90cbe7d199b2143f45045b64cd7a9eb05ce82372bb55360c61ccaa8252a99', 0, '2021-03-15 09:23:02'),
('3e447e41ef51c16425b80d71adbbdf3f1d292772ad45d6fa65cfc8793df2a2e58cbc01922db3950e', '636e5f92beedc01b0a477621ffb2d59db290a5eb26b70684f7e17eb18fc49bcc748d03d7d9294c30', 0, '2021-04-04 22:46:43'),
('3e824323223b5df2c3440e73c2d80f6d84796d745a10e9c356ac03ab01d43dc8c87a6c25b3bbf024', 'bf87dbc56d757903c08d67a9aeab8facb32e4acbd68122b570d41fb6b8abea57094986610a0a1a5a', 0, '2021-08-04 11:32:47'),
('404bc871e49fdcea86f4ef27bcb061a3a81cfe67ce8eea2d413ce5d5afa3a5244bba43d62fa6e87d', 'd0d623fd38855f9850da73138b500a39bb0bb620dd23a12406975b26b6ff6382a2cbb012aaea5244', 0, '2021-11-02 09:21:18'),
('40629964fea68dba9a9dba92102b268017c0d0dc71bfeaca8de2b41ea076d51b3a4655ec581f7fb7', '020a0f55c4a9171a44cd9737d0ce9d7b7a55e4ad3dc408a55590d94120ca95935feb6fd5fb8ad74e', 0, '2021-04-10 18:32:42'),
('4072fb1a2b7f631e0cae4eff6d7c7e3d22c759f477d70aa90bac889663b0355759837de09dc2ae67', '0f63cf19e594c83a62a493f3d96df069e45642b4f03dc4513fd3a573af5ca0f065bd15b7978d80a7', 0, '2021-11-06 09:05:22'),
('40810e27838a04deeebd4db9c78d2f2ac444e1a5342022d27a215997a7d1b735cfdd3a0a5050790b', 'ab45c476e166ed840c46d98c0e225de2644f7c2122235eaed9766f00e110bf2fd57d9895392f04dd', 0, '2021-04-27 21:51:28'),
('415bb7b7eabbe8f1dd0f0dfa5daacf25a592a3bf2abc2fa0b6608076cd5a4853d9db2ebfa29df1fa', 'a64301b6e0c48eea0c1dac3271326cf69704ed18507065d4b55f00c7091d4448cfca0c8db761379d', 0, '2021-06-08 08:18:16'),
('41786f5c1a1403373ab8f9a54fd6fde4b911d42d31ca1ee2eec90e6fcff11bc3514b53fd6ea78409', '4632b46274d92bd8a8732c86a34a5124f34e0e33a8ea7d80f5452312613ad91b1e055692ebdd9797', 0, '2021-04-05 00:34:15'),
('41a40a9e2362ed9b7eaf0b2d34d415d86a77617a522afc1835292cc02ac6e282ec18bdd54edd45be', 'fb8340feaa01bf0de75fd4b5085dcad583783e1ff12250f969502625334dddfe2d984522c573ea03', 0, '2021-08-04 04:12:11'),
('41e9ddd75cc73a1f2b4dd1ba83c28b959843aaa009fe024ad42ade84292b4d99aedf5b8ebfe0a83a', '4912b992fb79b31638f045b197210211dc1f9be1108fb43f5981539fae19ec1dbe4ae5615bd416d0', 0, '2021-08-05 09:37:51'),
('421b43c7dd3603ec99c533f77e1823acea3341e9208d506bfd739c1550b4cb8107c566070c526a8e', '1757b6e725cf2bd7393c24f08c9b7fcd5dff9db2df3b8ad2fb0f7574862785502e2439e59beb54b7', 0, '2021-11-06 21:46:21'),
('434edb589294614c66d696ddffe02bde7ac6da392bc2d1b1e1aa6434b71f0510dad43551fd8eac27', '3617b3d9597948ca0076f5f33d081f96a5d0bfeae36fc5827615eb404d47e8d5fa0d070672e869a0', 0, '2021-07-19 13:20:17'),
('4352dd66dc30e403db37605be10bbe94d075ada5e8b7f3806aadf735af3552c6121594246e29cb30', '45b91a660195292fe635368f64061d092f67d1b0c1046a0508be2c41ca7290e9b2224a0c1dd44f51', 0, '2021-04-07 20:12:36'),
('444473abca69bd7fafe2e44347a644ffab29bdb6a3cc165654c7700575166f03f68520a956b445da', '96ced244af4e8119926a3d29b9395b7ef837e951001b9dda0784612c0a802af7356e9fac0bc40ab2', 0, '2021-06-17 00:32:59'),
('44a2cd442f398767d2245d6e262150ee9b5d7d664ee728b325a46dbd904af803808e75e89cd56cda', 'e78e0801dbcfcb300cdca740be3043a99b3cd088688e28cc719ede11851e215adf2b52fcedb246d2', 0, '2021-11-04 15:32:35'),
('44dcbcae305464ecdd6708358961b76cd166d889e3fba24fd5c60345af9240cf2d35fe5079938781', '363add703d1574d51c9d3afc133d06f1034ce1496414d5f09d277a4a5f7276b1bf01fd2f17fe8071', 0, '2021-11-03 20:49:40'),
('44e3099cc69243398560eb971ed412e58a19e69615e5ef85be6d824f3cbd11feaaecdbc81da6ea78', 'f8497e8f1f9a2f16a081b4ea856cfe7d8c9c2a0d135201c248749fabb283509d50065a4acd2f18ef', 0, '2021-11-05 09:36:33'),
('457a0b532ce4414fcd487c7340b3feaf215481ec95c6ff792d754a1202a87bbec21843a11d28782c', 'ab35b6e133e52836975824a61a57844b6fa630652c82179b960a1a8c81d9d942793a4ed941e37648', 0, '2021-04-28 11:36:36'),
('45870cb45b822d368e31289d6f6f0b83da76332e8f1795bd9b031a96333dec872852652b4260a3b2', 'b4f6071eaab7bd5efcada5c8b5615a30c4d13f43aae3da527240dd9312cd5c9edf50478b3d7eb670', 0, '2021-04-27 22:27:20'),
('45c5379f6acce608a23971121a9ae582ac5160ae452fed311b5da9ed5e9c6923b1727716f6e3f06d', 'ce312641d5fd526e1e2becc3b6fe691e753794d7ea2e683f1cb61b67cbed7bed72c7e424b02c55be', 0, '2021-08-06 23:09:30'),
('45c64ff88c19eaa79650bb633cf124e60cd71284c8ba2eaa1ee4c13b098560a05c043b4d953a8e64', '1f6b6238e59746fd9a4ec220db3851b02a92eb84b649d4d8eb2da0f707ffda329805adcb0ea91c52', 0, '2021-08-22 08:32:03'),
('465be65a85b0467f21cb93195a5f7610c7cbd0dfab782d118a7f6ae600f61e95fd5130e220a35963', '40a2f5f6059b52c36a8602fd54be6cd6a1b745121a383f8329f43178e883c6a2b09892daf91d0a67', 0, '2021-08-06 23:13:00'),
('4777132751002b9e52d86cb2621694f16bc56d912b097fa70fade602527773365ec6bcf7b47c0649', 'b4c940bd55fa6e76391a474d7b802edbbf465ae8abd17a549ec66b8401538c15d119250d28948e69', 0, '2021-08-22 08:00:10'),
('47e347af00dfe044b455d8a64ec97f8f2131967fb6b4ca8789b685523ff1c48135271d24e4cefe79', '46c71410c3e61729d7749b52a847d362add6dab6fcc24c22b8d57993a193f4a5dc09e76b95c6de08', 0, '2021-04-20 17:53:28'),
('48b07ee22a44f723014115529a101c8c408321b34c78374e2cc73ddf64f6681b9a5fce8682206543', '2f3573c035a5cd75b990a770cae54103c9579f988e277ed540ec4f087e114d4ea43c12b58cf53c20', 0, '2021-09-12 12:08:25'),
('493d51927383cdb4911585e4d1335bbfb78c28ac5676f94fe6a5311f0888810d0cc2481eb0578994', '4123f9d005efa37f3a22f11869d8340d217fcb9981cfa018970230d868c38ad8928f8bed3aa89e61', 0, '2021-06-20 07:37:37'),
('4952213a031c071c033b7802087ca219a85aa4cfb31d45c3131253d79f4c24ff71b561377970973f', '35af6cbaab15a085fdb190777910e1b575153f1981447ffd23d1e4fd547739ce5b82d1c6e3bb6c66', 0, '2021-10-29 10:45:08'),
('4a1e791412643a4be8898c3d24eba46a4db2d099019b5e9f17df31c4597028397727abd13b34c326', 'bdab93f0f583ee17fffbd4496411e4f8ff6aac4f378c331e9be7e16e1f726fb73ce3c7f85e85b095', 0, '2021-11-02 17:31:45'),
('4a65bd6d82d8040a3e1fd61a76f133dfcf507c7a192eaf8a9b66e602339234c0f4cf08a121816223', '73c69140dad556d7c129cce7737cdb0939cc8810c9711f42a4ba09d1d9f7c1cdce6ad987e453812c', 0, '2021-08-07 00:03:25'),
('4acedbf7f8728fe6013ec94b8454a382209564a626d12f549b161458a7cd909036f1d2da5f39fb73', 'bf75225e89e717e9b2aa60ebd5f69bb8dcebe1bab430a0fda14a844e7d245d4347ebb8c75c978ed7', 0, '2021-04-28 10:59:01'),
('4ae8b61870c6ab55f095287185db389ad672934d31fe2b72bf6ccc5f74915575b7f4e098cda77d12', 'f7261b10ce5d0cd2a569a65bccdb667eddedf5344bda9dedc615abf53c5c1013f0ec8a4c612fc7b1', 0, '2021-06-16 15:31:24'),
('4c5d853172c405a4010ff75fa6eeee1a62b6378e9d69a94692cec308c03666ac3c9ac376ff47c4fb', '7d2d81689c7e2f8233825b985050d6b31abcd03af22b677953dcbfc3287c0395afdf08463f0ece69', 0, '2021-04-09 09:31:15'),
('4cd223658f041e58c87f38aaf134beb22e9818afa2a2efe59b52dc88490948779530a57834f2c539', 'cf215bad179678b6c04a4063380714361372d663170b9bad14bdba5b0f9465aa1f91989dfe7f51bd', 0, '2021-09-20 11:11:03'),
('4d5504ecce3f03d992ac1b1b73235153536173975cac52649c938bd6c8f7385a15d7a9e4ad0a449a', '0b452f49f979aef99c6bfba6c4a2466f3293f9786644cd66c6f741985dfc9889b9bd9611352eb217', 0, '2021-11-03 04:41:18'),
('4e981aebd23c4399d8a7aae3d81918b74db5b1523c032d7e77b26aa2ca3dba1e2802c1b28c0ac9e1', '8d2796c2e2b145b8eec125d91c54e739992eb5ee09b9181d7ad266de479293ff427153b64b1c5b0b', 0, '2021-06-20 15:32:43'),
('4ea8c676e93965d74fa540286b5fc02ec2aeefbcaca335141903df4ed780447c91a6e52b69784b88', '6218e46c1d84605fb3c1ead434841c034a7d8f69a754acad72c9080491e576007dcfe91decd9a0f5', 0, '2021-04-10 18:18:24'),
('4fea8df71f34a27c4a0a0fa44ef9735d44d6fb0da7bfa3612814be299328831b43265b4fd81bb4c5', 'bfabb7cb62397e355e3f02dccaccbd4b8a0613f300b6eb0b54292376f3b0330bcd8b8c5afe6fe2a6', 0, '2021-03-02 11:45:22'),
('4ff7df24ab949b47f46d85ede2a5c59e2c9b59ce7fc3cc44182cccbd71c6d969f61d76153b214f47', '6c733068a9a2ea91311cd94fcbd8c9ddccfbd21279e4c9bd8c4b5b13785696386c3276733e6f0804', 0, '2021-11-02 18:31:17'),
('502e34f63b05852330a7f41aacf068e093235d03a0d8a7d2d02144e5b0139c3c982859a8607b2519', '848735b4308b9564aa7ca9e538d0e14f44b42cf0963af221c7b48ca7f1593bf5d999271fcd1c7ddb', 0, '2021-04-27 20:34:05'),
('50b1330c465bfe5e5f9a033ee6f9f20de2ba63cb74d7019f3d4711d4b6ca20d1edbee54bcd8341c0', 'ec387fa7837fd8292bb48bf2655ffd8bbdc2a48c69bd3da43b55d71f945f866c1e6f58e0e3fa9ee7', 0, '2021-08-11 09:32:49'),
('50bd41bbbb2b2cc99e2278e7dee830c496a9100e8442a4786331bfea5d37d592c09e13f670409552', '0244df89f638e96882ea062b1d957d945f9eb8718844919dbc530b71699f5665fadf8453247a2330', 0, '2021-06-16 15:30:25'),
('5167543741857c9ee104f9867009cd96975005c9f999b599343887af8904c1a639fd9d2d15da08a6', '2aa427b9bfa1091224c601ceb803fb4a8ead46cfc3993f1e26baa3b33966a37393c057f994127c77', 0, '2021-08-09 06:55:38'),
('517597e9e579f65704f1a011be5ff37767ed5b31a0d099442880b0e5ed7619d52ef1fcfb788c54a7', 'b99463126caadbcd20c7e96fd48a7f44acab0c757586a5372a279ad9a019469476fc21bde366504d', 0, '2021-03-12 19:21:21'),
('51865964574d98f4f8dc3e0864bdf17cf749a6fb569416ebbbdd3b56dc02cd17f86635bfbc44be71', '7ed092860b6091dc37f36ff216b277df4cdf3c7d26d64d1554dd38f85545de711f9a3a96ed11d256', 0, '2021-03-02 15:13:40'),
('518f8bf6efc4434754420c1c7a1bf47d1816ab4e0f5efd8d812110f2bc24c1404346a9bfbe7a9ef5', '585e287e348d6bab90c0e08d9304fe4fd14ac01d9ed4c686cbd1ec89db4412834cec7bc261e816d7', 0, '2021-04-06 23:35:38'),
('51f537fbbfab46688166b9c04ef60b274818575b282c8079de949f48774b309fde7c5546e0866132', 'f0bf5b5c632f38354b3e5d3f176b7e509918994254f5395206a9b0aac05918759ea8415ce36cc42a', 0, '2021-10-28 12:54:47'),
('53821d6cdba6501192b541bd61b78f7d324ee082b2c445781c7ee29b6f161e9e55aa59baea5bdf95', 'ebfeaaeb76a38de9101676f4c79935ac16744949919e83ed33204eeab1f13413ca858f5623fd153f', 0, '2021-04-04 23:45:28'),
('539c492336263dd754cd322c32b6af4764e0cd306582e07329ffccb03f656e0acbceb213aed25e81', 'e4f2d3ccb5a8a413015a92049150c6cc5cee1213a00b629f41cc8916472dc9990a6a2a9df3e632cc', 0, '2021-04-27 10:48:40'),
('53c80582cde0c1c0122d4b451544321e0750e86589cccb332d6997e3b33b0ae3a94db9df11d18949', 'b442c71bc6de453532ef59f789e18e442f1200ce7aae1ba7740172098f5c910588ec50eb83e08fbc', 0, '2021-04-06 22:22:46'),
('53cda1aae4c6f4a6ebfc6f0decf0cd046ce1767003ac9e22f12bf972ea4a847b99404aee10b291d2', 'a2d5f0410ad8a2211abfddb3f1c416037402e11b99a6a28891a59fee6928020355baf58772d908dc', 0, '2021-04-07 21:35:32'),
('547789b70d24fb5260a8b86a5b0774ae1bb29f0440d218b18127e65e8e02c3bc8560b3329d371423', '872eb438d99fd889fd09d97c6c6b0ab38b76bff6890e9e109bdefe4ac46576e954ad4be49e00e7ee', 0, '2021-05-30 11:31:53'),
('55d54cf86d5f673a222666834b7739c06f0cc189d837b64eedc67376bf00a8b1fbd028c434611bc6', '90a870a5112e5d0bdf9f7acda176450c3f119f3486860f6a1102eb7320578b7a8383151c2219f73f', 0, '2021-03-28 22:50:00'),
('560e683394a56a2928aca2bc9fd1b66826c6d511d1fa988ac225a03177d91f11dc0611022e329722', 'dc63552adb99e3964050a7ddc988e1d49946ab80cc68de805a2a6cb1c56edd66b5409f7a1ae10d6a', 0, '2021-04-27 22:14:10'),
('562dab57f57b0a91d61651e966279febda55d4448c798a2eb4d60e7fcc117ff34e5fceccfb0a0819', '3608ed137b9684fee3d67e76bae80db782635aca5a35766a07977e13eaf458742c8b44aca8b56e0c', 0, '2021-04-10 02:49:47'),
('564222efefa985030804bde8744b6ec78f8ca54f890395ae329cd27a1f0902d7f4fd3b14c7df7e0e', '4f5e062f17e4a726a4b875f9a6ff2c78715a5886ee10db2c7cf3ccb4b72a888bb756ed195b098f25', 0, '2021-08-04 11:45:28'),
('56473f53101dae433ae027bef0100b7df34a05333dea64fd55795887e81f61b38da8159d7750d5b6', 'f83614d3b566bca2f32b58a4b9ed9cefcc55691415ccbe3c7e53a0ae6f86234a8a2781e6ef2ae110', 0, '2021-09-20 09:28:48'),
('567227528053ccfd345a944cd73164452a988f5e9fa0a045df464736fa98d5fb84a911fd8cc41ed2', 'a9234782f2dbb213196495bfe558a32b86e6970eac5d3de95bf4567457d5f74342f4f564d2a7b623', 0, '2021-11-05 14:18:00'),
('568e9748abe0b0d2219fa027ecb37ae9a68dbf75d6ee72d54165408425a6963f86a88fa72a851eff', '5db3c046b501d7b3c46931d9391942aefd35ec39d01afe91c85f0d744a3e8b482d4a8c2f4af3cd98', 0, '2021-11-03 13:03:23'),
('56fd0c8cd169c3abaa4deadd94bc2ec075d346e04aef81336c1334d9ac51d49c98269936313581eb', '38e41c6bc2b96f7b0f005390398a54ce5b65b9173c00d7382979b64509f1d205fd309b8728e2f200', 0, '2021-04-06 23:14:40'),
('58d0fa233245ff24af8e7c9fe97a13009caf32528d9574129f700cbd85ba8984935b0b64c0cbd4ee', '099f052c89fb6feca9b3f6403f977f2167ae3db7cfbd7c9463c9d00a6ab7ccd92d8ba2c40052b43c', 0, '2021-08-11 09:25:07'),
('5a856b2eab583b4a218eb2f1f937055a2bdd7f4dc6f1f23c4b4008053ad6fbaf910dd099d428d557', '17a5c5324cf4b5b64780769fac763deb0c5f4f674ea05bc7c16b475fb2cb297e7ed0c2bcccf14ed2', 0, '2021-10-27 05:10:44'),
('5a8c675db0db3bc1561a890b696d5b6eb1daa45fd9d800bb1f020ebcbe7948eb6dee9198a3a1d1c1', 'c13f3ff7a9263061bb9fd92d3ba742569b6dc017584ae4aee4cbc513be8fe521ea8dbf859ad7f87e', 0, '2021-04-04 23:47:50'),
('5bf277359e8a31d2b845b888d8bf9475e5c361e2d1feb73f18dd1fa277b8566a5714ac7bc396d492', '9eaa619adf8fc9187b5fe4be993076c5f0f325c25d08129f99f336d795fa660124c1fd34faebba49', 0, '2021-03-28 21:40:03'),
('5d2a401c40ddcf821985c188a3add9f4c1caa8ff8fc3153f78a2553092b6f4abb14c77be4fb150be', '2a26ad16c2b458111c42b0337b07402b8a23a0ae47a8091a1d1182818abe4430075e1514c5cad556', 0, '2021-03-02 19:00:12'),
('5e746ec61a86fbabdd88c318337d0a3d0bc445dfcb1e96401afc1d0589c1991199a8962a71133433', '6dcf01c1146815c325f0ae5ad4caef0844b28ced5cf0f0f95dce7692b7698e3777c718caa909d48c', 0, '2021-04-09 00:25:52'),
('5e8367774b3a18a8ae2459d9b45048fd67edb4fafc774d6188893823aa62b9266bb18ebbf7449591', 'dbd40fca2f99f0854b318e73140f0744cf0f89ba4ca25c7c40e013312627bddbd03b94c07367e4d9', 0, '2021-04-27 21:50:48'),
('5ecf6c75d52bdc541296de27f25712685bce7068d2daa2bfb4e65508e92ca9327b30208e191d989f', '5b4c5fc93dc24a9a8008e5e95bc84d05fe6d01f66f94fe19690bdee1367a1ef14fe380802981c6ce', 0, '2021-08-07 00:02:16'),
('5ed3da24cfaba68bbac97bee8128dc98b9f5afee3c51468a3fc0407dc847aa0a07b51a4f1f2ec608', '10e9519b9be586d04396077f99617ef64a245e4fcba85cb56e5b9edf09c75b0c1a32ad15501a3ca4', 0, '2021-04-15 21:04:54'),
('6084fcbb914856ee96712af73f380afe63093493c9ecc197c89c2843b5f4bc4f433f531bee8719f7', '1bf4686fd3cfeb4dbb91125b563c36a73e359608b5313cd9bbdcf5ad1f516ac133a98d176367103a', 0, '2021-04-27 20:33:03'),
('60c4dfb1cf8dba2abc3a86900d6d7b682e21dced0e55f663b6987309d530fccb1e397d3482f28b07', 'bf89dd621c6938cce2c5f2902d71d28de4c74ace6c2b7217a4fbffcbfcc9a73b1c83c15ed8622a5d', 0, '2021-05-22 09:21:54'),
('60e2f49ab340fca18342279c891e002d5d8ffbfab2eeec2b0fedc39f14b42b4e42fd5c7c1bde7a0c', '03ebdb29f8ccaaa4a88823d6719de0772d25c5aafc5bce0f83bec40a1cc37d7a67299d249e9a3fad', 0, '2021-03-06 15:15:18'),
('614d2c43e4a66e9b4789ce808d7984e456c069598055c241978f1a443b1580059f99312ea9d82408', 'd7f53e7d9911c6374a8ec8dd1917821d008397108b87ad7506079081a45dc69f135ac255fc353c6d', 0, '2021-09-06 21:35:14'),
('624f40c71329e13e8ea4adcddd0db8aa553799434bda477907fb11c23e5e56323ed1cb511ffadbd9', 'fca5eebc59795223a88796c5509842d2c38ebd6f29ee3a6c3914185b3cad5113e2fcf078a5038030', 0, '2021-09-08 06:20:28'),
('62b5dd105c8292fae95dd8f902642daf1ffda36d292275e7dce1d88e2d6cf9a743bac20fa90f9a1d', 'e3d86c5b9169ecf45aa5e738b76d1b2b7e22e661defde7908eade02718df3c2021a293406314859a', 0, '2021-06-23 20:36:16'),
('638c7cd3899cf0dd647280c0b5798ebc270c28d8bf1d8fa78609d621dc331a4d7bdd8f88242fefbc', 'eae58dc14ffa9c04dd501a7efcc200926e070541bd34d9fb7e56560271240bacc83c6056d65825d1', 0, '2021-06-02 20:25:25'),
('645e4aa617fefde4357b8a056121c8c5c0b586cb4f6d0961d81ed98727e6cf8a47faabc6659a98c4', '0ef99444e4e91fe2fda488173704fc682e6e1d2abcd790f546eaead2d72ea31c9b8fa81a4bf89348', 0, '2021-04-28 10:50:35'),
('6511691af062843e5d13b2e0b471bfd268db8bb350341cb156b5bd27b5f64bf8bdd9e04972578ae2', '8c77b877c6da1b6857499c0c2f2dc26f22fae421e917762e5ea8f88e9a915f0d65c0c413ebbaa0dd', 0, '2021-10-31 05:53:33'),
('6524b9a5ef8f5cea3ee1fb21171e48d2384abd2df01f2efefb374f3c4f2b4bd698d2382ebe5a8080', '923bec54cfc0c59af55e0fe518a204972d885d82262e465f354e7bcbf778334538552ff3085117b5', 0, '2021-04-16 23:02:02'),
('65a52c14af1bf5e0afaadd13ec14570ee4bd20af7ce58ee5ef0796d5431747f13df50c96b10e683e', '1a862bb77eac3adfdbef1e9ac796e0cee30f47ed1e1e3fc761abbbab5846fc47534a0577cd645c66', 0, '2021-11-06 11:49:16'),
('65d21accf7d26f3b6df2fbf097f2a60fca3962a85873d0866105807108f2d77a3bc654dd7a8e72fd', '52f31994d87e25286f0a16aa0126fb75868e9b80ac5d07d59db28a89f94581ba90129540b5c5cda2', 0, '2021-07-30 11:05:22'),
('66ce59071df2e238fc9e312f249c5165acb0234234c6fcc86908db72478f9d4762826e162d4f1e68', '0cea37b0508c0afe86410625a23d0cce6b4bd75d6193c7c7acfe794c5a3a001dcf480333d93095b6', 0, '2021-04-07 21:36:30'),
('66e969c427dbaf6f5996e0acc6b51797d4a10d1f440acec2257abfa6257c09fc656a6b4cd74fec63', '8986712b2664240d39912722984e674df7610f8fd3accd6fdf44337f14c35252e885ce838221d7b1', 0, '2021-11-04 19:30:18'),
('682a5bb4fb0706dfeb886a4d4d7a4c4fcf5d91fc534e8e389632bb567c42ed3adb4084d767a2aebf', '1d77ff9235f8697fc265fa4f0538d73f2cc7e960d0ac5156fed61e087015781d5de0cc72bca51ca4', 0, '2021-04-09 17:46:37'),
('69eeaf703fdc21781c8a9bc58ef5352409a0bde3787bf54a0d4944d7fbb5a4135146af088a7d6142', '2f0c34009dd98fc390cb7c0d3f468b162ba774f9581199e79487dba8129b2f68e0280d39963ee423', 0, '2021-06-24 11:28:04'),
('6a72b16c09fcece98f0a3a07ed0388fde45a8a06828a509dc71fa00ad34e9d94177793521160093b', '3ae3fcdc93ce8e04cb6bbf50cf0d104ce0713f7e14b72722da63c24445b0ba6a083a1fdd2c64600d', 0, '2021-03-02 15:49:37'),
('6ab59cc9b79d85615253ea4729774217f61d11d119c2eedf1af4d85ad043517a5f31deccc79edc2e', '710b5d92b7f26efa47bb159a9f8eba5b40a4e0ef659293c146a1f5ebdb7c4d88bb7733586d6803cf', 0, '2021-03-15 23:45:09'),
('6ab7e7cde9e7fc21d745e1680be5a27c5e0a9c637ae1218086d264600e5966e6bb102889115b50f7', '9d8a8055c15b6f0eb3897aa0150f946a43184ef1269c3bb3a10900e2b0495c67fd606f16ee579e3c', 0, '2021-09-06 23:33:21'),
('6ad44a9ed50b71c522e1d3de8842a8bc36007d3c0626e72b89c079191caf60fbfd477804ebbd6f5b', 'cd2d1c80f02c55709331dbfdb342535e68ae682fdf801621ce3a42d98571fd0f5c5b3a15ca8fc502', 0, '2021-06-17 06:10:56'),
('6bab240c88665c8906e128d74c6a44a1009108f63ca73cf5ffa84c038efdb25d82d574bb31dbda8b', '17755625be89e8b8dc68452de0d5b1fb06c61b3170266b71fef5cdab9e1c11b8c96286befcdec48c', 0, '2021-04-06 23:17:55'),
('6cead6f8e1efb3c399110a180db02ea73f7be5ff22cf11872c2db0181208ca96dbf83e00f0b183b5', 'e4bfc04c36bdd4b70897246f53ba17e6cefb1ff24686daaf32679edaf3c3a4144b6026c02059813d', 0, '2021-03-02 15:06:53'),
('6f6a0457f9d97fa94b5fd7ff572d249b9c6e8359a9969bc78d901d31f271d477084caeee78a1a095', 'db7ea331b5c5ad494e13321ef660be6f645d534c9a4c5877713f6d19f2d851ceafe9ffae3a721b55', 0, '2021-04-15 22:07:02'),
('6fd73013535fbb397172f618c3e93e7b6feac5af97db92f8273e1f5f52ed696d00807b1ab6ceff36', 'eff426a36aae178cdecb996ac01f3c7432b594e3575a6bd18cf3ec984b2a07c34caa97f2312a2392', 0, '2021-09-19 22:20:00'),
('7147cd70f4e01f83686b67515efd3ca813a166fb6580cef818f5cf1ccb7bc4320cd2dc9d7a7eeffb', '915f331e24dd9d462b67343e7e6e9da41aa18df451f17687863edd17fd16bf8dba197d131ad04e15', 0, '2021-06-23 12:46:44'),
('715e3dae0c87d7fcd77752f3aec0c2405f4597bc517906aed31076df3dd59fac1cf99288a66eb832', 'edd794137547c202ed44d6d7e9a0569acfba028face19cf5ba4e96e9479d3bf3553b4b72cf28691c', 0, '2021-11-05 15:41:12'),
('7244986c7e303832a33664e89f510f386df11d027a62f205f9cfbbe2000c5f8e2a043b78021d4334', '02fb294012a19fb60c0d177720f9ab708b5805060e568bde49dae0e577dabc7db12fe92bf8647688', 0, '2021-04-27 14:59:36'),
('7279b1b0889f84e2efe7bdd792b04447fb5a097fc530fb305582d9b891bc7987113f39f7e20dede7', 'f4420a27b540755f2e5058b4a27b5d433321dac9b516a80503c1e3e8c8364c7dc0105f9eb33de6a0', 0, '2021-09-19 05:36:47'),
('7291d095c6e6b4e117df65f35a73d99be1d4ad7a56ae12dfa75763232b6d98c0feec7ecaf82db759', '31ea3da4217c231734764cd34793641826f40006befa388e523eee3755a3aa381fa8d91a6559e513', 0, '2021-08-22 22:52:11'),
('750226e1ec7e1ed2b995ad99fa26b3cbbc307af40d9b04d41944e4dd059ab9631848a1cd5df37bd8', '6175d0e2af62c4e51aa8115529ab9e05dcc27c446b47bb4cfb6c1cb0636ce6afc7b156678a7785f5', 0, '2021-10-29 09:56:38'),
('753a887a2f246ccf94bbd5d23524ef1aad6b8c56a492c32a6963ef5e296b49da9d4d42f05433d530', '32feead6aa1e34c5d9188cd0f5d43243cc66ed9810f0b359fa0ceac3683869be0b0ffce516a65d5e', 0, '2021-04-16 22:59:32'),
('7574fadb6547e9b18215da138919b2f60ab2ad5553a70d0eb21a50bfddbb7399d6b23b4d032795f2', '60a9ad58734cf20b4a4f9015054f0dbd91f8bb5c591f8377345eae9819a607ce6611e690e3c77a43', 0, '2021-03-06 13:43:13'),
('75778a4d9899bece6aa72d1266476aa4107382134fc5d28696b3fe50753abb6c972eaff931c37a3b', '286ddb434c33b92aa095fded348720c5bde0c64ba54e9939c2b63ebd5f892954fdc791e7d42245d0', 0, '2021-04-28 10:53:11'),
('762c66863723ed1efc09775be2a07912f0fcdbd4fa4c3e792ee1707065817d9c575f8a9ade1c58c3', 'd26d2f6270f55f5e7e1633bb50239a63b350d8c095cd1819804557d7dd312402dcd71304d496e74d', 0, '2021-09-06 18:55:14'),
('768355ac871c65bbe3bdee50eb060eb0da46795953c86429a66b4f8a61c52dec334c0574c92f23d6', '9cdd2b0a03aae5386e23978b1308e2966b164769cdbaae390f030371a0ee3b43dd31348bfb9f8f1a', 0, '2021-04-03 11:33:26'),
('76b8346522e4c04aff356b694cc7f6763bb3a3b88cfa757dc3d94aab13ede5366e3efbca7459fa76', 'c4f72498fefc9309359e922e01d4d98b864740ad41366ea5faace757025136d14f97d87a0e9dcc2b', 0, '2021-04-27 22:07:36'),
('76b8e99d268a46db4e7e6564c4b1a3bff119f4bfb4610a84c45f9e52b3c4857bf7aacbe0e313094a', '3e682e5664677258abad0a180c84b4097e083173d47b02897da3fdaf7f55a148c5f9d96311d5500c', 0, '2021-10-29 10:43:01'),
('76baa76ff577186b1902e7afc9d613d05ebb2fb48b8d77adeebd130129e8c465c74463d41d28945e', 'e96c31071a76419c1343efab1f06649301bc1ce5e89e3496b996c6394f4d61ae5b2ffa2b0ac6424a', 0, '2021-04-10 02:20:04'),
('7703bd017a41a382d06c7ee1366ab3375b46c9889cfd75492489479d74551942009e9174b3438987', '816b4095e797796a1a424b7e032924c76de91ffe9e17a98e031acdcf6b45d5cd70ae3bac8c12c924', 0, '2021-08-23 07:25:14'),
('7847edabcce62b2086d00f2be4f352c44467a45b5592ad4e706098642ad13b3ac7b94bee56c7b799', '3fcd615e9cf42550e1656e9648114349b44b5a3b18d633b8a0963069121b23e804f35e47c0137366', 0, '2021-10-31 07:04:08'),
('79051bf5e8e9ee03a7619aea20733cb0e5e62a20f99d1305b3279b335c9f6a317fb9b696898f39c4', 'c512b88c01bb392bf5d735d035f690274660b12307e98a2107a3ae7eba10b6ff85ca431aac182663', 0, '2021-04-27 22:29:44'),
('793ec462bbf2bd627e94a39380c8cfa7b37c5aa1b67b667cca7b4b56147c860abf286ec2c2350cd0', 'ffd18795ade1e3f3c06c85f5c7c40b3d0e9bfe1433cef488b48132985d07828e4b6404945dfd2d1f', 0, '2021-04-10 01:56:58'),
('7943c8437fe6dabb92965199e60d9468b10255e5de14b8f7fa42674d44028c1d36b77bade518948e', '6f2f9f3dfc363788f20ef4a25a97f289c567e5074e476a0f6d274d4ea9f8a0c856049fa0ceaa86b0', 0, '2021-02-24 10:35:23'),
('798f1094ad42f9c150067618117e3b680285c2259a067662f49348808970ee220151ec80e27077ae', 'c3842583639a31b274ea8031482941742d805aad59999ce900a323b8c8951ab7d653a84eeeb1032d', 0, '2021-06-16 14:28:04'),
('7a66ee2936855689e282e7cb7ac7dc8dc53257516eaed88d5f0dd3d22593ae7f2ea69db40f8bbc3e', 'be8a7ba9e5b387db25ba78ef557e2c3aeaaca0be6e086c1c25cbbd617360e536fc88dea464ff1cea', 0, '2021-04-07 21:34:33'),
('7a7cd35b5dfd2a7fb53f0f53bebfef36a5a11e930756464218c842830333f8c3ce99641d68a18e83', '83e6c49967ce0adb1973bfc55d2ce38701cffd6deb42ba6fb12597d470394c2f0fb615cae5a5fd2d', 0, '2021-04-09 00:00:12'),
('7abdc693b6f648f2af78d00e43efc6c3d300a4eaa5af22216464833b7f38ae2c2d0496265e1b0b98', 'f8b89f5628850a1bde21b51b614df3c15c0c0dbabf345862156a531ab8799f1cb0725b728bf06e4f', 0, '2021-04-28 10:49:55'),
('7b5c36521cb4fab9597d5d9a7d2cc8ee21b8cf1764e43216f8eebf2ee0debd72b4e08c0e68342a11', '880d35a4ece2de88fc14f753147689a3fd3a97523725757c0a2bd25d7eceb51e0db5dd56d7df1e7a', 0, '2021-05-30 17:26:40'),
('7b60da3b906d4aa8068aa2e367f4cbadfedf6e0b823a980559be261e701f6c3f916cfc7b0f39256b', '5a53ea1ff5934e7488fecdfcd6f59617ca48502bea317808a1a818bb8d1cbb434c9ab2e91af9a755', 0, '2021-10-12 13:33:51'),
('7bd3ae16b1daf60e7e7a4db3992c9ccbb00c6b6762dd3af6ad507adcaa853079943b860605e91e87', 'e27d03eea2ed3bc2307e9c6d11bb8913f4188c84ab08f62daa999ce2db96bf861f8095305f6af387', 0, '2021-04-05 16:12:19'),
('7c4c8b35d8f3a63ca8571cd8b34ade48c5080b215b1dedec5f2e3dcce8f021e557efe8240fa995b5', '594386fdc2253a6c7d109759f104332c80d76774f6ffd92c885085bf28e97af4ff60ef62ab283ea1', 0, '2021-04-07 22:22:50'),
('7c806a6bfdf96f5ed1bdfd84f20d89a9a86c284068c3740d17e636b8f1129d49c07ececaf6b390b4', '81e6b34becf23c90022b226883b52126ba1c8ffb2a90dc0392e4d75bfc67319c86f663b74a5eed6b', 0, '2021-11-04 18:31:51'),
('7eb8ba7e4cf6b6e8fb9514a0cea9def89fa15199d390e6ba31684cb80eb6d98414257b99ff8285d6', '7905ad4982b06c1ae6c18d8313de3bb892161e27093171b920b10d29682d9400d6650aa7df3872a0', 0, '2021-08-06 23:59:49'),
('7f402caedc30da57803bd56483527394a3e968c14b2153fd34a5d826d2aeece624295c5c7cb7e460', '0cd857cfbe584eb381f8ecdc2a09222e65bdce0292a25492021abc0b3188c4db5fc44dae9f796305', 0, '2021-04-27 10:12:29'),
('7f62a19957a6b9edcaedd7ebdf24eddda8a2ee9d8bbcb7bb040db3af499e4046f5889a290a885961', '074a2a237deaa40d77b4141475e3cf949e85768a6b11f59aeee11e27136929772c4d4fb12e700013', 0, '2021-06-16 15:19:58'),
('80236546c871b828f850518220ba9ff2fb6f6922884561e866e0d465ee574417eac289a51a612fba', 'c4d1b5f6b4128ff0b6d24168dbadffd1c8cc31379ee06b45c7e29d1dcc401d625abe8b02d23e7d7f', 0, '2021-10-31 07:04:51'),
('80fddad9be280f5dc93f5bf23d3b9878bd396ec557cba8b0a393785e522a9f0cac294d8eebdd0f5d', '285560424f672c774009c2548b214bd99706a81d02afdc666017570e3d0ec36f9fc94524cd44c103', 0, '2021-08-22 22:07:17'),
('82cb609e5220615eabab177fa838a32b6b1592a2f3b0a266928045e64aa4d3ca0f4a40779e0fcf0b', '8fef46ff461806ccff99b6b2bb0e4d629b9439ea1e89c574dcd76cdb284fb0040020d3096def8f27', 0, '2021-06-17 00:31:00'),
('838395a71002df59f207b327bcbf4e75235250df3f554789a299e6bc8e57cf69581aeb133155c768', '8bb51651c8e500a94119a1b4f89538166d5251f63ebfb6dbae02b50fbac7a41c0ac1553e2b769688', 0, '2021-03-28 22:38:19'),
('848cf64039615ee8eb023b20673b2337159ddbf5c14ab7d9d70ca3b40298c731af9ec5dce41a153f', 'bb4cb4be722aae8169a449b40b1a7a54055e6799fcd7a70b501934ebd670ac77ca4e76e70e649938', 0, '2021-06-17 01:27:30');
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('84b2ec44e41a0d4b86672eee5aa469b91a341b40cd5d83e6fd5906690f8c7018a7efb36223c6c4c3', 'd384dd0573008efad1154499a13a2778cab5668ed88c57781d79d16bfaeec1ca619d6fb6b1934867', 0, '2021-07-02 09:44:11'),
('84c9bce9e07960983b5b92644f5891b68b4a16c9d39c39badff80ba8f641028ffb93889de280b42e', 'd9792954930dd063ba5c0cf65b567fc4022048c89f188670ec044014221211c09672ec6fb75705dc', 0, '2021-08-20 09:52:42'),
('84ecfd5fd0fe52c4b48e57b2a0affdcbeddbe1b50af5b0d7ffd24ea59d72b27c6264727ad04206de', '0168bd3aeaae9fcd02f8ab741af33e2ddad9b7c024fdd4fcae720b8fb44b6785f04304f22eb3ce34', 0, '2021-06-16 15:28:23'),
('86297fdbe1f97c2324110e0e6c930c82c6f0dcfe068add951da4ba0cdfdb203bc859b834c27400a9', 'c5aa68e6ce3b47a0778a4fa0610fd6e3d2365a245fe0f3760c5bb7c1c1897c14c6e5a085114ac6bf', 0, '2021-11-05 20:19:54'),
('8642f1fca479782532651fdb7e2f5df8c45a56a34bb9378a64300946ac14845ed001075e3340b397', '84cb33f111e02350e01ef8735d82b9b02d4d43bd266b14746feac58c082f61874d3ce6c5e5550c5f', 0, '2021-04-15 22:10:25'),
('865efed2da9c566ed034cb8eedf2658378111a6ec174ded51c3a5fe5c5f2851dfb98c5b42fe51657', '1ca2f13e5e4cd5fcf32d709ec6b17c22d8a1ceeaa95dc2da259a2567b9f93afe0bd04d658f8b5d68', 0, '2021-04-10 02:48:47'),
('86789b1b3989cf779f5a3512fb307a3df7c85816e319dc641118f55ab85cf78697c3220984d86bc6', '8b96dfc918b8d5d8a7fd0c722a9ad93a3103b4444e5756a889b9a4e3b13eccdce02d5dde59165d47', 0, '2021-04-27 22:19:40'),
('86c71b2a0665f4d1f73f1884f06e0fe1060cde6f61376108b4a6e063a750ed00f9be7f1ff505136d', '7698e8b1c71ab88c3e922ffdfc893010c43ab5660ffc53706efbda734af76d8faf12f72122a84cad', 0, '2021-08-11 09:21:31'),
('86c97f578c252a105339f5819367fc2edb15538a3ec0d8d97d99570417ca21c28daf74a9f2a2aeac', '38635aa7732488952b32e81ebaa685d6ac301fc4bee4541e7ab1470b3170a6c8bf4c0c59f35687f6', 0, '2021-04-28 10:57:28'),
('87ded78af32b45ef2cc29aaceb9365fe42bd9d42a50ac18702823d67892fb995b70720cd83b85144', 'ee52f938609e7376bd46d677aa3e21c46e8fbd574e452d3b5cf5f960c3c21bdd031d540d559be458', 0, '2021-03-15 23:51:58'),
('8963cca8a9922f3b2574349fa9f36a6915fd3b1092025c5a4ed6a421659d8fb35fb75073bbee3395', 'e526e2440fbbe13dcfce30b0d6b2ba4c55d57b411f923b71705edf4ab3d21f9e66483ffbf859566a', 0, '2021-11-05 14:30:53'),
('89839de923188a840d4d4f7b55d3529c779dc34182fab641a0a6efffda4c47f3895f18055a1bb06c', 'df30ffe7821e285a89570883c65424a01bb2a8e10b7044d526c7d710213ea13aa024cdcae6e9a65a', 0, '2021-07-10 21:52:16'),
('89fe3b64cda00681d3c39bdbc9d6a21f7961c617d82f401e27e3ad3e6ecb91fed0d0fe0be416c7bc', 'd8d583a126b4338735eac80de0b9191bf3256684b535258ed8bebbe9a047ea5dab766b45b209d1d9', 0, '2021-08-06 21:42:01'),
('8a33821556fadc78662ff9deb5d937d3dc83d139f4fb8108453ad81adbe94c2b176fc75fe821db27', '2bb727ff4de943ffe0e3bfeef757b379ec64e8acce3bd81f08ac2b252277bb450be5d88dc1ae979e', 0, '2021-05-14 15:38:20'),
('8a86de4d7672308a7298ab137a0316d01b4d2c95ee5a98029ec4e5575857de0e2902f4d3874a58b0', '04c7d04f0e7e9f6d5023577d4ebe3473a3b3dd58cd27c441377afee2b33425e7f7f1df2ca4c80e41', 0, '2021-04-27 22:38:54'),
('8ab81260fcab7aed6581a42ac7b922a3e17091010a8bcef60cf4935b92ca15a83266918d513d5592', '5295229d2acfee99b3da48969ebafd9b0491d8e84310af5452f621768ef150d9df63744f6a56303d', 0, '2021-04-02 10:49:38'),
('8ac7eaf04c4ed47fc0ece163770b21c3d138f8fd856fcadafae1bb4c0bd167e361324c4264811eb3', '7e54ceab7fb6e976bbc40762d59006885743b830ba15cbfd1da030231ebc5310fdcd2734df12116c', 0, '2021-04-27 22:40:39'),
('8af3ce66ea66d2f55d67e8323a6312b5350cb97220f2daa6451d4f03aa7b74313fd71c618656c451', 'be9714264bcf8ea87a7a225ff18219fcb3e40714a85996bb74840ce5f59c5337ac725684bd5350b3', 0, '2021-05-30 17:22:54'),
('8b55647b14cc0d3140691b849b9d8de2cc3baec2e1c19473af1a20e006f73f8201d897f7cc49c903', 'c3092bee166cdd479d347e559cca94a74f9b9fa5c080ed483358c72aab5ae64a2748924b7ba15ce5', 0, '2021-07-29 20:28:41'),
('8b68867bd538e62b96bd3921be62d7603cf1682412166cbd4f1d0a402b62c56deaf8e750c0aaa948', 'e03081326ffdf3d6f456235d724ab1969df1788d7d21fb2cc59cfe4ab94b171499f3bd617761f952', 0, '2021-04-23 19:27:51'),
('8ba30196992676d20f71ef91f91951af33d2f1c6ae32d35c2e0fd9503bc395e91a260eede49247c8', '2423163f8776d90b6c74702d2d7046175f5590a8a0db20a26807920bbeb8bcd74686067df761d192', 0, '2021-06-17 06:08:28'),
('8ce02e9eca1ebf258d29d7d80d2bdffa23cd021b4093ded9991675c4a56a390fb5f0704dfa001731', '248d0f36b3fb86e7a9d240547fc7cca118470db64710a8dde1f6768643ad25362bc2a50cfba79296', 0, '2021-03-16 00:33:35'),
('8daf8dcf9d01aab3c569ef66d397d155c174688357772ddd587f54ea856771994a2918e4dcb43c28', 'a2786dc1c99307ce43392fd608d001d41c6b8cb18565363e51e8fac4bc78dbf25bd744024bad8647', 0, '2021-04-15 18:31:07'),
('90b0998b957e12432738467c9c0f0fb36e82967c8cc21c752630a8e4e64ea23f3aa3c2e06203b386', '39898eb47810b55c128d7971a6e3b6d2d0d0eccfc916395a9cd74f12ff40b2d7098c8bcc3f1f320e', 0, '2021-04-05 14:26:03'),
('90f2046046e9857e5eb82bc6b3f734f695303d89ae25b17671d68db5b35c9927e6e4ec6e65174891', '2581c077b8eb48526cbdf55fa870e9adbe894ecdfab14d0223532a1316ec55b47903ee73092a284e', 0, '2021-04-06 23:03:01'),
('91ed54e153b62400cfc48622add7c63dc93d89ba92f97585c1ba51b07f5f4a3ab2394605333c79b5', '8bd286846363241bbfe2e242e2895c382f935e2f594249fd1f91ddd6ae02acc20c43f3f1777f54bb', 0, '2021-04-09 15:59:41'),
('91ff39f87f20e5b4037146a859fef42dba2753f1ae6cafe2179851776c2ea92e35004af5203ea027', '068f850a35a67991871cd79d2c8f9a90d1093becf499d9f2d976dbd9f35da4dd5dd927ce9360ca44', 0, '2021-08-23 07:09:08'),
('927356d438798b2c8f62ce5f7d4d11cd3b5bcbf9d5c63ce97629e52bdc093bda305a5e44c285a0bd', '298dd9d1044ebce4249da478f3d05c15984348e3aecca3f63f44f4f6cf46316a22d83cfb3cdb8356', 0, '2021-09-01 11:48:24'),
('93547d5248d57d212af255f4bdb27f3e17489a7c8eab070c237a462c3af92f4317f7b6a451a7cdc9', 'a68cdd8bde442ebea6f4e3aad9d9e4c6dc085f5811c46fddc08664a45f789ca1698f9dfe8cbce91d', 0, '2021-03-02 15:37:06'),
('93a9904a94af3153014dadab2a7ef4c5c64e77ef803a502334644b32525dc3c5f4ff29bb06d5c2cd', '82af5f2382dcc4bc82998579a5cccd3733a525b59fb494f236423b8d43e5c6c76721e6bb154854a3', 0, '2021-04-06 23:15:45'),
('944f47650036ae1d1d04c23962ca4fd08cd93667d507c2f07539b5dae977cac863a058a1d54fd878', '2e3cfd661ee876ace58ff50999eca211ba3f1f68a8ee81f4d04d742bb910956a27b58fed99e98352', 0, '2021-10-30 19:22:51'),
('956c5335f0e93b268866654691b1e3856c76765d794d9f8145f21f27b6f2f588dec54a9d0a0971ab', '1b1e1003318ee69c6183733031472b431aa5f8590b3eb54a5a3225d1e76a35d1fbdceb78bfb524bf', 0, '2021-08-19 10:30:08'),
('96362b2ae444ba236ca92d6bec1dbeffa24669821c18b0ef89b95a0ca57c7a69881891db81e47bf8', 'a74d0fabe8e07e68945c07ea9c9c3643d501b37d73278a1fbf5c1891ca3710ef07924ab5cf4e62bf', 0, '2021-11-02 18:24:25'),
('966eb3f41860a62fe0da8caca5204f6ee2f01e6e9f96c4c91c66bdd172f500be71952c8898e559be', '50bc082da327225697f78d73ce720b368cc7d89c42b8091e3053a7404a97617d5c51366a9aa71a5e', 0, '2021-03-02 11:50:30'),
('970d54deaea60b937f39645db5686df4500ce6cf509d4779e49b2a80d9b24f56c9f50d46dd52b00d', 'cb62db9e975db3a5bec6e9d85b5aeb61c9c7452de994702770b39c6830decb637122aef25690e9fc', 0, '2021-04-08 23:31:33'),
('97609531e47bdbee7f427c1d88627c8927302aebfd7dd42035fb2e1afb49d8f608c5341688e7ec9a', '3c892c61ae62e2d1700fb03a91679d44cae20ab849bc6edcd621b105b4c9c67fd405d5b5f8270a49', 0, '2021-03-14 23:27:01'),
('976a4a713cfd9c8e7192673c842e9e52908d30ba7fd428d2911a3477673fbf8d9a52aaa8cd49ff81', '9c89abca6fe574d99556174ebd7b06c0f23493ba83b770b131f809fa7df98ec3f2fe4b83e8e503c0', 0, '2021-07-10 21:57:43'),
('985887a5ebb9bfa59592d047d9b493bf08c65d969737fc192ea9b85a697c4fe11f195ce0b59edb07', 'd80ac352c4504edca41da90ea2f3a8611951ecfbf86b865e9d516daef7a54db72f2d25316c1953d9', 0, '2021-07-12 20:59:37'),
('9877424abf8c5ad0dc733e2633748c4baa3b9bfd7d6f660a7b9c6822a5f86af29a837206b53fd259', 'f814556b3de5663556c24a5e3814835215a7f95110f961a75452262dde1cb0406b00aca88004238d', 0, '2021-08-15 20:27:01'),
('989e84e4dcfa5ba81eb6352702e50a3e934e082e5b7c4953ed0121567d21e431052c38decc4ad1a6', '2237a0c86943b69e0b1b98afa13927990bf96d62d8816457611b1faeb6b9e789b19be6604b44b112', 0, '2021-11-04 18:42:42'),
('991d9001c77cca4caa3f3a25e9cf9f4e84f3438b5e90b9c6fcc259d6e79e104fd539c8e46f52bb2b', '571f2bae09d635682e0549fe815cfca447f4d7b57dd208dbb4d6f0ac610ecd4332793437dd293d76', 0, '2021-04-27 23:51:26'),
('992b28f93f205819601f704af08747bed3e180f3ab833cd3e9dbf36e7181ade750f0d8cb4918a2ed', '951310cd8447bd539f183fd9d588dc275f8398ce257324573321283fa3eb4a694f4e23a18697cfb7', 0, '2021-11-04 19:06:25'),
('9969969e41d755d9515498db98b3e5bdcd3bf9c62b692a3f685900b0d0c372d3fca0c0d9dfecc30e', 'e23fa709b8393d9400fbca54ab754cbfd77f63f7bef2f2ddd19b535e0d946533d81575491215f47d', 0, '2021-04-10 22:45:19'),
('99fca220c9d996ce385eadf715775f8b09412f5da81369f25b478608ddff35d903a69f083281e26f', 'b5f3d23888abe974989bf1aff2c56feb47b0146c5d17a19ac6466c8006bdc3492e8c95dd1d13a92f', 0, '2021-04-09 09:33:32'),
('9b7d554961765d6cd28c9d11f9fbd5a2679c4fd6c7aa83ba57317bf8c6d0e88f4544b6dde9b613b4', '3b0c117b680e1f2cd3809d10b1b8b4e98f3445b1c9ae38839e0fb5939e5617a3f0270632d1e9696e', 0, '2021-04-06 23:53:23'),
('9b835289e4f3ca37b7114616310106d21b478520a3ff7dd9e6f1b1206ee060521d5fcd4f1041448f', '93ca8f5e875c179bd42916d9fbc503add5284f30fd10ccaa5a2fcde322bb63dc488d7a50c82441d1', 0, '2021-11-07 09:38:11'),
('9bbf858b9f7a1cff51109f1f706d1fab36690a5cf5e2f5c8d9e7e9c4dbac8ebd8a77ad0c9190c91f', 'eed19e905e43bbb8ad9a790cee7752be85dce7e307975250f558e72bc52ef5e573335aef9b96db30', 0, '2021-07-04 12:00:48'),
('9c448901a5e9fa66d881a35749abbc4777420e87d5fd7521703ff32c760cee38a34a122c40e71c5d', '031d9fdcfe474bb73398f932f1c6ebe0d7458b741d6f345b2159502a240f3b58dc9a1c36189b81d3', 0, '2021-06-10 14:43:59'),
('9cf029e410b8f38033269c2f1fcfb19dd9099fab33767b049ecc38f762d6353d89b0d67b254732ad', '2123edddce072f4197c70b75fe9f7c2d036bb6866d70fb5dcdc2ab4537aaca78d9c34ead2139d33a', 0, '2021-06-16 23:46:17'),
('9d481e85f5cfe61c422e8451b17b6f5fe97894f5cb407b1b892077d1f807f565e6373d00b08d27fd', '20795ac1bf52871af19a6d2abaa31c79a1de0fdb85e856ce2102725edc6e33fa42fd722504ad48be', 0, '2021-08-20 18:32:05'),
('9df7cb1124461058d8a4e4bf2a892f8bea9aa41106c8f8f40a85a31d0db5c90388be19bc73fcac0f', '559947817cdd679f4da87b7171b8fa365a97b3fa37af99ea8465adad3f473f9078b72ce4b0b4eb63', 0, '2021-11-08 07:41:39'),
('9e5425716226d96e7ea648fd837b89f3040186257cc5c798344543280d933c3abe4da6098111c6f8', '395f7e57b59586696c013df2ce2bdde4debe80704bee655fb5fc2be101a5b4ca2dd36448df513dac', 0, '2021-03-15 11:15:11'),
('9e7d144083693f3ec25cf6a4cd43319f7ab4c400b4c79183555eb13e3f976614949324e2b0b9f08f', '26f0db54d3a1c0896b5f5f2b6fe2c72e54978b26ac61204762c909513884765b4c5ba47db18397ac', 0, '2021-06-10 15:10:50'),
('9ee9dee321cd669724754c235f4ac24912608c21abedc8eb7a98a87a1f827e3881d60348165ec53e', '7c55c11cbc5ed510c0b56250cf200ac184b3a98e6f22fd35211a8096a8f3f36b32cbdf513b1b6b25', 0, '2021-11-03 15:43:01'),
('9f19592dbb662a6d21c0984116aeb1c9aad7ce9238b5f16db3b5a27c829eb580dd3cc0577849ad84', 'e9d77df0ef673b0d2150d11fb1e124677e1a5561e41b7e311508840a6460372773cec2315bc5c005', 0, '2021-04-06 23:53:03'),
('a059a68689ddc8ed3f563afde1ed1c0ddb1133e3daadadf4e15951ee4a71c199a2afa65f93f085b5', '56ba35f3ca675f9a22570c58b4b6afcfedfc9c9a9ede6c63380ca9c64684f51e503ccda93e4817c0', 0, '2021-09-20 09:33:01'),
('a05f0095ef9de5bf88f9aa8f139e821d6817dcaf6c0f534697dc1ac89910dd9faa9bc6866ee4d233', '341eb58a37946f9500c2c2bd56f5f6114ad2f7276869b3abdbb6227713c3619f74597c2ff4926578', 0, '2021-08-20 18:32:29'),
('a12e42d0c9766701d41006ae508c6cf62bf10ab87f9eae2986ac7277aa6a11d11ffe880f1eb639d6', '561fe0ca3e20c9f44430bc778c28de37376c5355158be049feb81159a7c71169aef141baa4ffa585', 0, '2021-04-27 15:00:47'),
('a2848220e3a51da5f0cc44c094e92e9325115842b6d490335fca1fd460097a1a795c0ca5ee31cb45', 'e5af6450d03def79f6773fcc175ed4056a961675b9f04fc0c68d64a6d37afd74e2b1880a7eb130c2', 0, '2021-03-09 08:57:52'),
('a2ad69da21a9861f60930af51d0ccc89da09b463ba58bf6b04148bdc024b1d0ceb79edb9cb4d7fed', 'e84d6cb8626c5c54ae771727a7cd0631e714d2813bb5a6766fa38bdde606c038beeb5fa83bf2db00', 0, '2021-07-19 13:24:57'),
('a2addb67ea22ff8a255d3bb9dc5991e45f1984d69ef3ee5dc79baf4678f040f8356fe46adce29de8', 'be29ea2267d6616035ee02597559c36374a8bd8bb2bdfcf300df58d3b4cc219a34ab231cff58bb1d', 0, '2021-07-15 17:38:31'),
('a3f4fc680fa902ace96b370bc74d97048843f2e02fe8b50cf6928c93b1bfd9d8f9e972b5b3a923e6', '87a64eface40a5f35843cbb492110f8ec79b9ae3fb3aed61ee746bdb35c82d0b8875ac76fa6ec8a8', 0, '2021-04-15 16:02:58'),
('a4ce98422afea587f44fd47ea2f6e6469b803bd326c4715d10e08231999bf01cf57a9ab2ef290d19', '490e2c44e50636c94c77fa3f45e7a3e07727862c6fc399c038d487a21d9221c34b84cf499a67fb5b', 0, '2021-09-21 12:51:59'),
('a76ad8a7e7fc06ccbb647b974e9fdd326b0a9c8263d76166f6759fd7d0db7fa64f700afa2e7530e0', '2d7758d0f108865ba50a42e320d2334e5884181e978c65703cab91e3952e0d5bac9775e3ba35051b', 0, '2021-11-03 19:23:18'),
('a7821e869dfe462b9a5b32666c87573f8a0370dada116e8248ad68f11e6473572cded3c8d8d45e1c', 'd890c477f56b3ad1c62e55b642b6d4285703750a07a30a4e0a47a747c3dc572ca7dd300ad36ae9c4', 0, '2021-08-11 09:34:13'),
('a7b359fcfabb4368eb37d80a0a54a5bdbda9527fe6fcbaae2dd6ec35049c45e24439d5a82ebfbe1b', '4978cb8226d084594a4444576ed7b455ebc4d63973df58121a6e5044e906338ace9ce4fb30900735', 0, '2021-06-12 13:06:59'),
('a85e674e2f9cf08c7b7b0304d822e746b9676013ce599122b249f6a92c20f4f5bfadac3803078bf1', '012acb2243beadbb16e841d2032b2ff7ff2ae8369bcf1fdbdacab53fa8b6082d3937bd50a8a15550', 0, '2021-04-06 23:01:26'),
('a8990254cea8ec068091168289787d37a60c0fb29f760f47b172283077c64b5781cb0f366e9320b7', 'af8c9548e79e1b8712ec2dabfd49a1876a62ab83adee08d00190d962119b0ce8b5b0bfc5495ac163', 0, '2021-07-15 17:36:34'),
('a89ff4c1b35cb9381a93b5eee4907979277c75c1b07f4b5af18fe0fac7a4ba8a04f7db8f343c09a6', '18f57720f87820762168ce81df93b18d3fdf5f94248f9349fbf50f7a25ed3f26962a17a593a1ada4', 0, '2021-03-28 21:57:41'),
('a8cdf5c971b550584c47be0e036795cd6b6f85f043ee4b2119837b7e65b6bd2b255dcce0f962f30a', '09e03ffe5d12edc4976f01383b81a9197b0c4b20ac335a761e5b392597d6868b964f0379405c4fb4', 0, '2021-04-05 15:13:13'),
('a9689eb83df4f81039d5181c17492187f1364d129ab5f57d7eeef2cda4124edb79007b2c687a14fc', '7a56b0efe949169db2b3a35faecaa9dcb165e8fe2f2c3ef2aea71b4b038d121c38f1752f1aec07b5', 0, '2021-06-24 10:36:15'),
('a99457b0f38b65609703c961cf191a5c2512a196dea1fd6fe4c8ca47f78e646862750e584f734e4f', '4766db98cc20d3f7a9f03e503609436354eaa917f4999be4e661e02340e0f184af002525dc9aaa10', 0, '2021-07-10 08:08:48'),
('a9c013200d0828607e6cfcfd06d181bf005be8b337d29d661604fd9cf3817359e4a45414f770c7a3', 'fbed94a0840ede4fb5e9446556b5c107ea370da46c96581fe7382fac1a519f38648bb94cf59f720c', 0, '2021-03-16 00:38:52'),
('a9c4b587a04ebe9c192204bdaaf386add26aa4e0d531ca3bc3976c0778f7e4fcb350010d5ea06994', 'bf5d634543bf44ec0eb1121e6bc75da2ebcdc360cc21c4c2390524945461121ba418e4318ca78da7', 0, '2021-07-10 07:30:58'),
('aa07fe6dc9e2a6e69b184fb588971a50356b91bece62f412db61571822d33444295db2b127c0a850', '1049f1e9ae04f614c31e83fcf61a0200a3b976bdac9bb4257b86d71a5a08beb433c2f570e103d18e', 0, '2021-09-04 21:18:11'),
('ab47e94186ff81c1e6244246e495f591116cd4fe884bedebf84899df569e98b0492c4fcaadca667e', '014bab181c80264f14f0651888f8bb19c460f3e2439b689f4a4cad59d7b05541ca0be43aa6bd92e3', 0, '2021-08-15 20:28:38'),
('ab7b3b8fb0c3e30b75631a2f74dfb764d1efb57b4d7b3f23625e98316bf1e3d514188d3e4e1cc359', 'c2c6591c6fcbc12bac4133c7f305cf5adc6c441e65bfd0351cb9e31dc46964c8a43d0f1cf8c1654a', 0, '2021-03-02 10:46:06'),
('ac2f8c9ec95171de25f0a37fe8fdfb35bf2b93e8e9c57aeb6b1a0a8cc844e41677865e7ea6c860a4', 'def32dc430ec72fb6eceae2d693e52a5425fe3a1613b3f27970af577fa725aa3f8e4b1d41ee8856b', 0, '2021-07-14 20:02:09'),
('ac7f6a3e5a340f0c04e75a199f62f4663e2517e499afa44910b8bf93c23bf837ef39e29484e7c4f4', '23943238b1cff8adc0dff3b50739656468d73ce7ae882503d771263bda6ba34e47876cf0ff63d180', 0, '2021-04-05 16:00:46'),
('acbea2c1b714a31a3c7511b110263dbfef0ae73b4ee9a82c65c925d352589fd5721de3154532eaea', '4ee2f0c2068e847acb1aefb65b4a40179fa704c72001e584b2048a266dae82fe69e9f11219ad05b1', 0, '2021-11-06 20:46:20'),
('acc2a5170c7c5d0859f9837e97460591639cef8654220c17b87b00bdcc03d71d85745c596f74ade0', '4d087a21e831cf29a927374a9ef6b989ad6d5188b81c39f9a50c153033374249e0b1f12d7e47db95', 0, '2021-07-30 10:40:12'),
('ad8dbb1886c8b7f6c6a2287f62ff2b53f1fa0b9670f946b25b0d8843f6a7841e5a6f91b58080995b', 'b2f02877c1656861d37f1c6c7c19d2ce9a9b48358279ec0802099e80ef2cd56540efaf596fe93b75', 0, '2021-08-09 06:56:59'),
('adb0879b7deb1366914ac1eac8cc6649c0e10e41290277d269e27ac90206215c2d0a62f8baf1726b', '4edc60660413a451801ddadb0a0bbe6e4ce80e212bd68525eed914816ca8d31d862ec90c32647397', 0, '2021-11-04 05:57:34'),
('adb64a2385a634bc7f8205d0cc1ed8586e394022ae54ac0a197e2bba2935928ae077d290ddf8cdf0', '1cdb726cfe8bc95859d2192d25b676cea0a1fca185d098a8cbc588a43f611837007a7d9dcafc9696', 0, '2021-08-28 09:24:49'),
('add08400ce480c42e6c75b0d9ad429aad199b0634bf49c6143f8c155deff11c5e3b709f12bb59ef5', '722992f5264d3ae731ceb5575524935e60192f4a4df76533bd367c78c60ff1c4a705b2c5f6506e23', 0, '2021-04-05 22:43:57'),
('ae63c087504232b78cbaa4848ecc7583a32131cf699b1474c3561c207797c5f1bfd63bcffc4e3570', '47c4757452dc1f9445fc29da328f3950852d973b1c317890bb7d5ba5fd200426cb8f42732a8e3fcd', 0, '2021-07-04 06:47:46'),
('af074eaa671cdec3e18f9450e1121b3826cc176c611209354cb5788facf172363c4294a0089ae4eb', '2a8f64c4259ee285fd3f1175d6b43782a275a222ada8c0cc188bf4ddd292f413aa325ce7a508b8dd', 0, '2021-03-02 10:49:21'),
('af0c286af1224fe364e819d4e4f5db5ec37ef45f1c0cfce7c915df7d68447857edee89f9a317f0b7', '6b8ef6977e9f4c9e9efa23c78eea0c505ec67bfbc112c7e84a60e21789f5f81db340d7a13f309c94', 0, '2021-08-28 09:25:00'),
('b007cdf8cacc971ac07fa6be7d906269edf617d799d173a322103c4ff47cb4391df04b8f6936ddaa', 'dbeede49da7c65a6b7d5b824f2ae68b90de49428db266fe98ada9eea23929f2857275009a262dd57', 0, '2021-11-05 14:15:37'),
('b066fdfe38de31b0ebd8b8d4bd6670c95573854a6287cc5cf9852b969a5f05c6f0251d5f62c30153', '5ce30af5d6e22be2be27727800eebf7e9e51c932edf3f99ce46cb36d5b155bcfa9a44bfb91cf97a0', 0, '2021-09-07 16:05:49'),
('b08cb8c43e6c5ade53d5ee5f7407f58c52f9c2e518df552043310d9fb22813603d00b8e20f2d28d0', 'b3675b860479e89e241913b78639ab10cbe308bd4958be6bd0f797dc96e5baa821ef7795d8891fc7', 0, '2021-10-29 10:41:24'),
('b18d923b768d8c14b0256560d541f597e5c425f8a2e44048ac0b5d4c32691b3390c65b10b9a0fd2e', '08e183ac74d8f848e927618ed4be3dd354b68c21378c2bf15cded17b755c6456abbd6594ce5188c9', 0, '2021-07-19 13:32:52'),
('b3b7ee170ad7f7988716d2a4545583243ec636a9f07c58f5467687de8cdaf12cd84962276ae9663a', 'c969f238defd930b49f2a23ed08f6908f564ed37e308445a15cf5c8c68585969ee66db269a420806', 0, '2021-04-09 09:33:03'),
('b4c6322f58a2f7ed3ee1e2e11eea7f2a35e48d710d8cffea0db72a979fb42e8a485f6daa21cf5979', '106ba4e09b02ed2e5fd7fd79b23f279ec9edec7f05bbf7cb8a3e5d0e1249be43e17dedbdd801c2a9', 0, '2021-06-16 15:29:59'),
('b529dde8c0d880ef7bea7ea7c57497fdd4c2cee45b8b0220468a36d2dcdcc794865a5830d3bfe0e4', 'b8e9fb69720ba02f5c3c70d11ce1501d6b19d29748288566c6d11d8a44f7f3698ff09c82fb7683f6', 0, '2021-08-28 09:24:41'),
('b54985e5838fa422ef59a221555c3a8b5c98eb1c195b4934d390e508f2c465d7af1f688be27d0a73', 'c1a8cf8142c9e3e77c36a9d398998e2facba185a8f549d85a0ab31a8a45c544fe6775f7ab80e5adf', 0, '2021-06-16 23:48:33'),
('b5d3d145a9577c009a0c1243986a3e7360ac0027a9dd2de96d8e2525920f41bf500c7a593afcf17c', '7b1ef49f20598a930b31ece8674f734de68cd782dfd2fdc04ec15d30ec2e1a10810b18e429e0998e', 0, '2021-11-08 07:51:34'),
('b6e2b02d931c4090e75c8791c65124528ba59508245f47d4b9ce44c91abe25b88252654269e1c674', 'b74e35d0242f34713feee83348afaec688e8bd25b6f90c9a52bf7c8777d6cba250e097cba7399748', 0, '2021-06-23 21:40:08'),
('b747a33cc6b3d3b59fedf80bef9939013e93155e161e50d33676080444ea91e6c38ac65937d23e18', '9bf68e07d9a67a948c0a871b2d9c1aeaec53e30a0d4fa49c82727067863cd80639801e73f1c8036e', 0, '2021-04-15 22:28:51'),
('b809b3911090a21ebbffd99f78236c346e7bdf452a212c10da48275e004cb3c76d4523e3c59e6e68', '9570604598f4258f534ecbe75436a8e6dc743803a954f88633257f8bf57ee0f73c9f1c74338e7d22', 0, '2021-04-28 10:54:10'),
('b80a845ba1ffb07eb8ef4b92bd89d32839b94b3ccc4a773961884cb74fe47174c97c8980b7a650b3', '88c034de5321cfb5272d47218c8415fac49fb1c0924b6dbcbb7d143adaa3525baed66cd8d15417b2', 0, '2021-04-27 20:32:37'),
('b8190c26f2ea250dbd622434b14842e24262cf3a67fa93f10b75a3bf8607622ce1e5ccb1bb031c98', '535283019271804033f61b6716697b8cbfe37903c92ee3f6ec3a37439c904afa85b6953abe93b269', 0, '2021-11-03 19:23:31'),
('b9915a460a23a68b99c42897270968b394b0893b770e82a5c61544782d3632e7e51da51830f71f24', '2ecfa2d16121ebb8bebbb393a48d309b3ae237a967feec17d947e4b8a3e788cf2339739f8590de44', 0, '2021-06-04 07:42:52'),
('b9b8f804da509231cd3b9f9439c1204378b3033bef228159063d97b65bc0fbd0b582dad5320b198a', '7673bfa27a3c0d3601a88c838e00a469d553376feacec74bdb0936fb26a53d1bbae8d26debe7a69c', 0, '2021-03-02 11:43:35'),
('b9e407016cf886337b9506b06ea282f29cfa632841fc2b82d998d2f6a15ec40b991bf377baf10bce', 'e4dd57c5ad442722b50d9374ad64d04dfa242a6406a3502ef87734036a357fa5f4d299003b2c2bc2', 0, '2021-10-23 08:02:42'),
('bb2cc1ba119e9c94bbf3aeb5c797e243fa70ec21454463786b431cf490f6f3fc319dfc8dbaee988e', 'ec5cdcc9399514d6c6e998238808c0588c7fb44590797e3caa4448e0fb1d4e00799b44ecb82120d3', 0, '2021-04-16 12:45:45'),
('bbda61bdd43ee455e959b0ccea62bc6bff818ec18984b3373b4af2f82dbfa53db9b48c5d3f67009a', 'bab6a8c629ef1d52338407b84da4f8923227fcfa85593d860eae6b301db310544039c66a2ae47a1c', 0, '2021-04-28 11:01:12'),
('bca2e5d41a753dc5f399cecbfbb7364bfb7dcfd745f31efcfa861918132a03018bf20f0c4abf070d', '85ee9ed27d440fe84ec4b5c3f2616e8cee66f33e4e0bc74892ceab49189f38bdacfe09097523b82e', 0, '2021-04-27 23:30:18'),
('bd1533dd6bc5e025feedc0dcd4c02d808573335378aaf2f63ee62000078eb56ff45c57793a9c7dcb', '08ba4b54f579f0ccd3c23a6add712f939752d938865707c9531cbed53630ad3c949402d6e2feaf5c', 0, '2021-04-28 11:07:05'),
('bdcdd384f9391ccbd0956d70c5e534ed9dc9fef17afc87889e2687323a2bd2a1df48cfdd61bec317', '52f1f03bc47d220fdb81665f179fdd6fed5c407ea98660951d9470bc525c9dd270e3923ffcb4eff9', 0, '2021-04-16 00:06:07'),
('bfc17f326d5fb1191ac736289e3549cd455c261b12caca158a0f2325d3311b4ca7daa27eecab28f0', '3c387784af0810c18f939ecad0f20e68c260e37e4f5baf83925635aea4f0833db87cf068db69ada3', 0, '2021-08-04 04:04:31'),
('c0e718d846aed72597673ae515cd6644bfd020f8dc54c34af3267664369634a3383b49eb0399586e', '06dd7ed2b2057a0ff73d574e73c706df042987afde0dd0bb4ef8db3d88933380874ae709f3867e42', 0, '2021-05-30 15:20:41'),
('c1a397c41786520006089bfdb872f701cc323679e518a587d3e2f9ca207142bf334885f134f49545', '20b707a5c9050e282192dee495873164056db9eea2a6a41efcb4a7b1815008c92c9631e9a2af0c70', 0, '2021-07-19 13:35:57'),
('c1d117d8c38f37de8408859d9a17d6bdef9a8981c7094569ad081a2bb2f8a981aea5f9cbfbb3613e', '7033d7e4416610652e9b24776e0a6c55ed4023a2f83154c8a75f28c2dd7bb1426d272333552a787a', 0, '2021-10-17 16:03:18'),
('c27e37b8fd8035dba73761c4df2b44e6e7900fd5475ccc7a380a9c00a204723b9eec29d4d4f96bef', '627ba587f34c454a570d7fe8cb5496f9e63cf00190da75fe1c95c301a3f231575a7ad0272d82c595', 0, '2021-05-21 08:02:33'),
('c31dd4e9f3d1f207a364bbf2656a253ed64cbfce1df987cb18c26e4a6a0697a17a3b31252bd312a0', 'db9d563485845e207d43b4217ad0e6832b6bab89fa211420a87aa4b814ac3bc9628af56b8c51dd77', 0, '2021-11-03 19:23:01'),
('c535d35c8306d7ad8912a7533becd07316905d74a0a1943b9c830223cb2724ff6ab57183ab55f607', 'a7973736f71ce5123dbcbc57f1200b46824539446fd650bdf7aaca9ad58dd6793a51f918dcaefb35', 0, '2021-04-16 23:00:38'),
('c6408d2fdc531ff24dabb06a3679afaf240da2c11267570c4405e7d2da947dce524589e5a03ce155', 'e4a0245dd9105042496cdb9c0f9176000b6cf6cc296fb4bdabe7f1ace340070579244591a0623b66', 0, '2021-04-08 23:50:02'),
('c731da6e7eef13ca23fbeebbfa2b2a8642041c8d5ec27a77c8e61af05d1a87977d2d476f32eec938', '8de937cc6deacc4d456a5c4df6bb88c0a11a540f0e8cc68569778d3e1f24afb993c39dc288e164b1', 0, '2021-10-29 09:33:34'),
('c74d58967a628ddaf43991e98567b392304f7fc3b7179debf7d96c6c61778051a9a083d3beb1a23e', '64d5d86f23a1931ae781f97d5f9569178db9bf17c57fbaa2df3b28c493db78e29705be5a6df2d275', 0, '2021-04-27 22:30:22'),
('c7c3f02658dfaffe49c4c306df3ca7de1dd704ef5968bbb8b477be2143d12a06b5ea5e5eb463a9fa', 'ce38ed2ef5bf4678378c4a49bba6eeed6739cc293b9945694cdfd30b4e7ee742a9aa78ffa0007c31', 0, '2021-03-02 11:46:43'),
('c81eed0b2d711ee9f75ec8abd197d960c6f0fc90be20f00842e6da1a1dc3aab17d5070138a53f1b1', '78fd55ca22b9dc1ee1072e7cb8c87b793b3435d6dda6f5f26719bd9002ca7d9abfac5c12d27e1990', 0, '2021-04-10 18:27:38'),
('c892136160a213aa5aa02b5df63780cd01ebbb0573ee44d039a54e97377bd44e840470150e2d0615', '2f94f5645f65572f254ae9284fb80db90558bb8405e10f4f65dd76424a9a5957f3cf455e10684c42', 0, '2021-11-06 11:45:22'),
('c8cfaedd4a60c39165c9801f73b824f62fa8885bad4fea800fb722373d368893cfeaa14e836ab727', '54975b894f04f47379d8d0af7c29daf0c27ee3d3e541ab62fc49c741920e4cac558675f08cd3a128', 0, '2021-11-04 09:59:05'),
('c972e39e5ab821da6c4dad98311730e0ee4594abd135299217d5f5bf470da4184e543fc7f917f2e9', '6342b877f2407b09fcb3aed0e4f574f5468cd8570677e76d1f4e10b33375b4b2e3c714f830e171c3', 0, '2021-08-22 08:44:55'),
('c98c80470474f9444a3437964a74a2e2b3be564f172b2631b09f0e41f65a87e6558574fd320726f0', '5318e91580bc55726beb7bd8b2fd3f984db890eaa0decc97bbfad5a20a40d3bfc1fcab141025cfa7', 0, '2021-08-22 22:45:26'),
('c99f65f57739a8ca3926e37aed82a5c62fb6f7c269c9ed7daa2ec80c875d0cf77bdf02ba283434d2', '483980614604dd81c17f73683f5b14290fbac013ed45b222c37dee0c3adb4ab38e8a37e4ddc346d6', 0, '2021-04-15 16:33:35'),
('c9b8e9c4c36ba0ebb55938fe337b652f94ec32e8702a8e9a178afa0efa647b6ff3a1a0bea9843c61', '0b39f9b307e3a4a2bd1bd736fd52746565fb9c91021a91081270bdbfc8f94ff526e7f11ef02ea9b8', 0, '2021-09-20 12:49:27'),
('ca376aba1bb76bb3e96f5f1778a1aae34ce22a60146eaa0de8606d5da8983376f21ca2d9fc41cbb7', '88837aa915ea03b3f452bb71d10e3402f273bb93fb21627d4e6b83007b69d3c8ded1afb3b405b751', 0, '2021-09-06 23:30:18'),
('ca491ab2652101261a73827bee982f1af9ae1c3dae9758e243e4dda79080afbf75acb7fb60ffdbd6', 'fa2a6d24d908e741f00987146d9bf4a2fc6c1ae618c21604a83934276d3f196ddfd020659280743a', 0, '2021-09-06 12:44:34'),
('ca91dee31b188a0c631c218a0c83f419dfa2de67f01901fa28bc1565e5a8ca86a6ac085e5fa00818', '53d98dcd86e109994cef8334cd0a4cece0069a0d800f887d18d0900919635a23fd418b78ad22cebb', 0, '2021-09-18 07:10:00'),
('cb0e900c007d993f52cf0b1a95eb44f9a8362a0ec8cd9bfd8611cb68cc9413ee56fdc5164c39582a', '1ef265569c156261d439b060662c6144f841bab58d5387e285e50847499716c45194ae38eb305c1e', 0, '2021-04-10 22:31:20'),
('cb833bb15e543c9d45a7edf1eb9360f32fb79077a7977d827e1e7d729d0a69e501263388dc1f1105', '74d3b8774c84286691775ef3f5b0a10567ab7e21ba361411c5136397b50b2f15335fc51ee9252723', 0, '2021-04-20 17:54:21'),
('cbaa0d739b3a38781955e7b23d3c5d6a2db6c2f1e10cd5d4b1640079c048fa3873c0d60787ef09ef', 'c45cb1527c44bf4778ef313df878ef015b279985e91a94f7532a6c8ebc96434030ab754cf29ecdd4', 0, '2021-11-06 13:22:56'),
('cc2b78af79456c99e49dca84f4af79fa91b1ee616282d61f05188612ebfcd57e6963b547eaada976', '822f538ce694cde0ea7875efb4296a1f98dde5e01dd23e7ed6d5ac95325b6361fbefcd19ececb56c', 0, '2021-08-22 08:40:41'),
('cd35c2ce61fc65b57fbbd21027a1c6711d20fe618debc30d2b42d3c337af636e19b7fae6c52a6ad4', '9ffdf69983f1d18f1963038ec1f9278a4fa7ffd42da5d360adebdc40882beeecce5ffd02158d6b10', 0, '2021-09-20 08:20:13'),
('cd5cffa8b19cfb96631f18ed83d861d6c914bacf5003af544174a38b33ddbce5cf5d6e4d7f0f9be4', '54f49fbb838df04a3cb1dd78c0a20e523cfef158348ded5d17def6ab1ff41d50a56bda31f072a2be', 0, '2021-08-22 21:59:25'),
('cdabb21e0b70abb80a758df291cd58a41e4a577e565390196e0cc70f9a1c7d5c3fc6b2d8b50f28d8', '25acfe64668f449a6a3e95e944ec96ab65f97c0c41d86e3a47f781f98f071d269ba5f37b812e7c7c', 0, '2021-11-06 18:08:44'),
('ce08317396ea16a018e08f94f90e8c47976ba2578c334dc2613cc2cebf8c70402fc41e7a79d5727a', 'e1fb4abb42abb7ab9a8eb3ff5f965d8ebc6bd61fdcadd044b7023cb08a4e1087ca5c2ce14855e0f0', 0, '2021-04-05 14:33:40'),
('ce0dcf8b70d197e8db56a096178742be1ddf85217743d290af85eaaef40e2bab8982e745442a8bdb', '46243471bbee38013033f138edca6c2897dd7fb5c4cd520ac58e771c034ac014929592abc1fedc0e', 0, '2021-08-22 23:04:52'),
('ce95064abf5cd1f0611c11e3c1d8b7b9f1976a3d37293e66222144c61eb1df3ab64ac485d89487b6', '21f79840e4f2482e3d4e0bf6a06679f2059dbcea55fc2ca24d2d71474ef4aaef8a7cc8cec6b97835', 0, '2021-04-23 13:04:41'),
('ceadf9038eb66c8041d5353de0d9c1650469470b1f1828eaa7f706627680237e39b8546f028e6fba', '49c04fb47d552b717606827e5aa5157682ebfabdf3badf841d27b912509297540ec9e82a5083f5ae', 0, '2021-06-24 10:35:35'),
('cf0a76fe30dec61cfae07b4a0c56a5995c5ba3c09fbd88ac1ec682420f20b4f9ae9d3b994b7985f6', '313a8a06425ab1c98a6aa7a4ac3ed587d9a660d67bf18e2b779076a40f1f920b26cccaf029cd2060', 0, '2021-04-28 10:51:18'),
('cf65b7bff5d669643ae3a0befeee324e1246292aa03aa054b3923b76ad5c82232498488e15077d0e', 'e76905eb37eda864160a646b40e9bb42872fa4d6a9875a1543d07c5c4637211407a71d1174a443c7', 0, '2021-03-03 16:17:48'),
('cf6ce66837906e86e10de40322591ba75a9cbc6895744a066e6ac769602c0f9450e731a11940ae9d', '40216d010e0ae6806db53ce2b96bc9a68a54f4c8ad22521df17ecc2fd76ea1e05d45673b62af1526', 0, '2021-09-06 23:34:38'),
('cf97e52a107cad1f13cc9c4c610b92cdad8b7c925e2adede45cd6de661d77aa361d7b4379fe34200', '4aa0383e856eafd95e114188c338b8f73756630a01f1518a63f96635c2c879fd998832dbc89517f4', 0, '2021-06-23 19:45:17'),
('cfa306542bf1a0f65643b17a001fbe414789245d4fe471c16e1083d59524a68b0eeadadc9135b941', '4043f591a50fbf8cafb39c5ffb9dddf517d3b36eaf35a58c4c1380ace1e759a2059d80a8ff9e9c0d', 0, '2021-10-04 08:59:40'),
('cfd2901f7f83682c78c7fc5fffb127a429c19975f3aa8f9437e00be8b10c17640e79de655a029efa', '435517f7e34703b40ee637919f43ad27ae0e2a96892ad7afa184da4768a679617b5a26ac1278fa08', 0, '2021-04-27 22:17:41'),
('d00ea34294402326f084fc70661d63d88951bd1e13c56859c04ee4ccd75e7dd190fc717c6d8b30ba', '5403e532ef17af0d672372472bc05cc55c99e65cf5dbdc29fb2de660731f855e174f75657a28b814', 0, '2021-11-07 06:09:51'),
('d0631d21cfaecb384cb1fa4eb8747abd7c5df95fd6e8783fbfceeb36047c5c0bb2fe4d74d623c70f', 'f2b6891d6428ffd5e6047439d67f90bd21c516ae7ec04ed785b745693f7de0703d1661f0d268e343', 0, '2021-11-04 08:55:25'),
('d06fad26178dd6813ba7e6c6c04db53e7e30e26fca4879108c5efc14d5404a3450526b47c8be60e1', '21b2c3184cce67c0cd60e890701b9d0fd671688cc57e976837615ed896e00b7aa133871d9125f779', 0, '2021-04-08 23:33:44'),
('d18f74beee228697190eba1668c7ab66f3efe512f7674fb2f0be65edb5fde7b7b5a6cbf13baae240', '6b9f71bfc7b3e38e6c66c86bacd5945a3da5395e936cce4130afff7c1f1f85dc8a3ea49155c304a3', 0, '2021-04-23 13:08:24'),
('d1a21fc30ac512dc2e1ed61c27b56baad873d07f9b23576fcbb93d195986f167222e40190d1dca42', '61e72decbacb7b30bae570faf87b10680d1b251d6b07051cd76081e7edb53750ad9369dd0eb0556b', 0, '2021-09-06 23:45:36'),
('d1b406a73b221ae11369b68143dba567e2e1a791a59f4175b22bfc3aec79144b77b7651dcb56bed0', 'f275dee64a2ea089726c92bab295bb5c04d97f914484168d6e1ee2a06022cb8832e36a7267a70d12', 0, '2021-04-28 11:10:08'),
('d1f8f4c3cd99eee716fd997a68bb43df04540c12bb4c6ff3d4e6673ba08c8b73e5d796ce64ebe021', 'abf4e04a303766fa62e0aba816554449638b21d5afa5cfbcad1bf4724dee4ef52fe4e00717f6b605', 0, '2021-04-15 15:16:55'),
('d3a991bd72a603405a7f2fed2b7d73e7e0900a5579b212eb2c324c9921b5eb1403667b6fadec17be', 'a71f63144e776880c1f12de8b78a3e6a1f8c3ec7aefd12e812d4bfd1ba0e68b4fffdac76e27e3b3b', 0, '2021-07-04 07:57:00'),
('d3db5b957ede342155afb1f454d3da6b3ce3fdbea6920a835123a29b91cb3b7102b8e699170397ef', '07bb128c97ef1d02b1e944cbf3d42c93c0fc6ee8494183fa775046e94d8564978187cab640e7b20f', 0, '2021-11-03 20:28:00'),
('d4c867ff32c5536ba3abdbbc20b3b77a1d7bd79e11c7ff64cfe4bd162d98261d80a3e461e13db1fd', '2e2a4c05fa8b76096b880ec83f5b6e328dae551615f732ef099c6354dadbd8853807f122b5af8dd1', 0, '2021-05-31 21:14:21'),
('d50a720c0d64859782de4cc8e356ecd89ae9de97351fdb1ecd422ef010e27b71d700cebecfa99633', '6e27a29154818ea397648f8580d760514a78878e36b0ecdebea24bf14f8646580eeb2e80e62a51ea', 0, '2021-04-07 21:28:44'),
('d640da786ae9a4711660661a38245feb83daf09ac3d85f4101143aa76f9bca06bb9506d32fa7b3b2', '41dd649b8df79ba67ce63f79a2ab3a6a07977307a6905004993eafe8272a8f72fa23e3cee25ab840', 0, '2021-08-04 03:08:45'),
('d6a40bba810e82defcbb4be7441fc680f23b68195f8b49806ed5a904388fb129e8e335cfa4ea254f', 'bb3433afd3a928b59e134a25d61560274be4200388fe633fb58b8d853a09ced4c759daa75d6f13bc', 0, '2021-08-06 23:09:58'),
('d6efc550a7cd9f888f3714eeb7c5e1f1ea1335a0bc2613e6d65b0d5567863409263ba4b14bf4a20e', '244687f458010feba5bc30cb979fc7846e7c2447d5a5125339175f95ae8aeb63f0d231cc7f55dd01', 0, '2021-08-15 20:26:18'),
('d7cd38732256dc0d5d42946551eb4d4482659da06ce56b4646878fc58ca994bc6f625c76c3005b31', '2731bc195d35ba5111a96b165f9c3d539482fe82322c175cdc47f9c30e266a95f91b51702b3eaabd', 0, '2021-06-19 21:10:12'),
('d7fb73b158e30a9f39a8ede80f42d75fdcd1425e8de32cc91009c6e714ebb9b52c90cfdca7ba7d29', 'a42d5b1fa94524650d4e76a1b1343f774eafbaa9982ebfdac89c45b9ad34886ddf14f31b74d5b044', 0, '2021-04-05 23:33:48'),
('d80048c134719ef7aea73517f295363aeb2b685e1bdb767478f61b0886a07e0c7a911ef862ed52a5', 'e5e68bb5c1256e1333c4a168b4d938347f8fad2f9dd5e443932d0992617bea72ad7cb944d23d7ef9', 0, '2021-04-10 02:43:57'),
('d8b8314ea5a691be08e12ef4a93dd2ddac8162cfeffb0d157037fff8597e8feb613292a507b96605', '94d2180883a7733689ec2b36272a975b9ca886eb95ec904290197cbe6c01279cc361009deb98d62a', 0, '2021-03-28 20:15:08'),
('da36138ddbba08332aac4607884620b1b4ed84fd6f065eb97fd7458222f6c4707c5f9cb0a3ed6b85', '85409d62e33d8578deff5c64cbee373f6021497b6787a21e82b94ccee7bc568c19dda622873e63d0', 0, '2021-11-02 15:14:57'),
('da6db6c2124e7303e0b40ddc7e09e4c17cd78a6926acc331dd4bb080ad58c7124d5fb816890fafcd', '739937fa5bddf0deff52f1cd7edf869d1a7fd343f821a9842be03d85abd615c0654992071f598b73', 0, '2021-08-05 09:39:05'),
('da9b128d145275c53b6e5b79336d4672254feb035890702c4b5fb8e9029c3c72f94db3e2989127c2', '530bd87cbff0f44c833c1efe861a16037478782e26ebfccc24bf9ed074f756766a0db813bdfd6a48', 0, '2021-04-06 23:19:51'),
('db95da7f789a37e74a8f85c99af4ed61ebca804d7300747ddbec82be120e4dfc680a8abc6e10dee1', '5f156863ff9df917acf0ef7232d1ab428257873a2b53caa16be75a0aab6e1f20a44443246204d947', 0, '2021-04-05 14:11:49'),
('dc2d5c91ce5209e06342278866e872d74da18826d7628e57948c0d7dad05a3bf0f97938f36445410', '9106685debe23bfb3b441292dc5aa2f9c3afa8f45fb90d085c6048e4e97e56ac27eaab092310796d', 0, '2021-04-16 13:18:49'),
('de7a926b453ea9f6fee3f38cd267030f1992d8d13fff9ebb2076b7ffb3b7ea8ccc3f99ab9d30fdd9', '526e9a393dee912349bb23cfe988d98f7a48833ebe4108f3643c984d83e1f5d3247a4fed3a3fd4ef', 0, '2021-08-09 07:09:21'),
('deb2394964ced7dfb934fe1ee2045de2b61d4c57833de4ad0e7ecaefc532c8a68ceba64639842a7e', '8e719387861cce09b9bb2501e2de36e3ae499f231e42fbe8ad89ac1f4e15290ff3493e0fed937097', 0, '2021-11-07 06:06:57'),
('df49bc481268666f92eace218614e342590fbf5e48a3521895cc21770dad59d01edd135f9660a577', '5332b93786a3258dff28e5ba027f604d9a659970e65f0083802fd035d2c7bf5c8d4739e311b91044', 0, '2021-03-06 15:15:34'),
('dff39ab63049118a3341f96b7bace7e91c77933911129c06ffaed28b6c3cb136ac0f972708958360', '388984922bf3f7339d13493f15663d7cbdfa7ca51ccb70a8bdb6ba446d4f0a607847f6d685097ffd', 0, '2021-11-03 11:04:20'),
('e0283df0eb556761d04dbb2a2eba35d8807f49463fbb6b8f79a1e33da15b57a633e510a1e5c1c12c', '47721a4c2465f020092043bf0439456431a2c2e4252e15af60d1ddbd28b63a167cb1603bcf9ff19f', 0, '2021-04-15 20:28:08'),
('e12eb4659f82e07adb1ab68ebc7e80aa5d9378c6a87f3333beccc558d4feb36640ad8766555c1146', 'fead3b8dca9aeaae4d3fcdf5aa4383d87ce0c0fe70ea3bf6c87c992da7e53f18c68468b8198f2c9e', 0, '2021-04-27 22:28:22'),
('e220bc0305db5e628b8480ad605bdbbab3c1816f2986d04a390722a9598ac4dfaa71b81e2ea5d63b', '5dbb40c0d6f213d999b865821deddd24d6c37db693ee09fae70527362fb09eb6d39b52bf704df07c', 0, '2021-09-01 12:14:48'),
('e23e2448bbb29d67f3039e6bda81132ca737d874eeea98a7c253e0971e9bc71897cda5ab81873742', 'c0018f4ea8955bdd72987270ab6baf8cff085ac50a5d4d8497cb1811047e7827a26c23376605e10c', 0, '2021-09-06 06:01:16'),
('e2cb8f292a002a9399abc1830a0702c2765eee94e9b305c63efe96c13d0b0d4e495b2e3bc58c403f', '1e44f17760bef1cca8120d5767912f76481fb2f9a6e57cd92f6e115562a59cdbb6b9ed4ccbd6dd32', 0, '2021-03-02 15:49:14'),
('e32d964a33c500ccd5b13143d3310a5315e5ecf493d50da0ba8629c1c7154f51a306e55699e66805', '29f4738417a1a4b00da58197bb7edf1233bc3b541c49519177f3fd432aaaee5f2057b0b94fcaba62', 0, '2021-08-06 23:13:13'),
('e349d6c4c2039559177d95e510fbeb6d93f49984743aa8a22d378badc4f0b20ba3ac8adf183efd86', '35c594ba1aeae5479968c104efb15b88a9701e005e9ab1b8b7feacd7c950a0f11c6720fd60912a7f', 0, '2021-09-06 22:56:25'),
('e449323cf15e374cefc48cc9d37ef75b9dfa7a67d70e9567c60948986f164970103ef2a662316170', '5375807605531d085c9d43481bc7802488e06a13375765e46b358f8d8982e1d17e95be18193fd815', 0, '2021-11-06 14:00:25'),
('e4989b1e0b9bd1be70c5e2eea5089663841455a2621612552649b60adb90520b40f950edcd84d9d6', 'db4943344576ca510866088e11f53f30bf438f378e2614d0e63d3997c6c777db27103210acb90d04', 0, '2021-09-20 07:28:26'),
('e4b0e45f54f535deb37b740c04653e4081ae8b660065604f294edd12745b5c2ad36f8e3d13ee9824', '81105766619e69110604442b259a2d17ff5956343232b99792073c38ec1f997239ef711d56d68cdf', 0, '2021-07-26 13:11:14'),
('e522d5fb99a2d53a05e3f36a63f1624c7cbdcbb168af542b9fce5400b5225a841bf0105f1e06f8e6', 'df1aa5241f5e8d6dd467fd239f8507c08e516b5589047ae05ec9adce6debac62a426f0630b8b8bc8', 0, '2021-04-07 20:50:08'),
('e56a85f719229be3306831bb74c18cacb6c8f917b0220ef835a7e2f90a345e714196ba5f0ce50448', 'e03acb154eed7d3e9b5bb4101313e3265d9bd674b3f0868f71fe2a8a80bb05d6ef040a2c5b66102a', 0, '2021-11-09 15:06:07'),
('e620055277c0464ba164722f136b370f2a24b4162cc063f62091b9c49d90f057039664fd8f0d9e30', '43e7ee02ccc0bfd70e8addfb7eca47b576e4ad1aa818b2d705b1a7d8726a0eec70e2482b6c26ad10', 0, '2021-11-06 21:10:58'),
('e6e369c4d338a70b40316db1cbfb9580f052be09079dbb4eb123e56a706c0500667b694d0f6a15f9', 'b59ee08fca69b88c2452f6a87c7a3780af4b0753f4faecbe4b6807cc0500beeb289d1095dd53419f', 0, '2021-07-05 08:27:53'),
('e7814eb4a5cd401dddc491cc9ab61c326414fd314293261bc5f41476907f020c5425f2df100fa28c', '45d7172d84035e0ff0ed117e665ff80c570f869e8982996b76eaeb684bd40469732927b20b0ffb7a', 0, '2021-06-19 00:07:56'),
('e7af6ba2fc4f838c802a3da91f5bb58c49b69270388bea6e4eb4479d3c7d4d13c6cb2affcb00851d', '770ab9ae16ff322cc70a4de1843e98e28db98ea04057faa7fa566f0e77e4637debf24e84ed6af39d', 0, '2021-04-10 18:24:24'),
('e7ec3b3501495444c8b4bdef0ee497a141d5e2f58857657fb32601bb2544459d2a2498ac96cf613a', 'd7e777eef8d1992d266272d47415c2be3b12232a51578d219465377cde218674cdc7d95ff8a576c8', 0, '2021-06-18 23:00:58'),
('e80e371b49d3d81bbe78cfd1eb9dc9341f74767c5fb61ec45382ea4f10cdef3db5835c65b142a640', 'ab7e27b8630566b3a2a4c07c7c694d6752d0268c781a719e32c90400ed409cbc1882c9a820481e90', 0, '2021-11-02 06:57:18'),
('e84d7f3bf16e6adc90cabc25d12ddcd1b9f3fae8680fb9089959e2478e1ee4ec65fd5ff3e58d1947', '0e4c7971a50b34b7ef9c4ee671e6ff1bab7d36058c9ad8e6324c40cf42ff34358a48f5afd20237c6', 0, '2021-06-17 00:34:46'),
('e85390746766ab2e33b39a36246f2a29343316589be67d109909096995c2e31b153d4e026c14061f', 'ed42d7633f6ab71b611fff96d8ce52eeadbe24088608cfa6de4dd080e53a955bec8ca92a960c07a9', 0, '2021-06-11 10:33:37'),
('e8b7ae81ae8d14b491f9bfdd6c660166b75fd72587c3591fc66f21d1ad11ec06bb309defd8210336', 'b0d7fe0f0f5249689198792a5cbbda4a3cdb6e699254adeb99600be812745bf959c0738ea6126dcd', 0, '2021-03-02 10:42:42'),
('e8eea5944847ba5acdf5c5a7ac5cfb181dda98d4003d07a94dd18d5bc3d868a5a4307b80da2b28f5', 'e1c3d227bbc91b89904b6f7cdfafce2d0c77f52193ba248fb18b9464faeec694b08b6e6c726edcd6', 0, '2021-09-20 10:47:40'),
('e8ff990ca72166e5ffe70a530b254fcda8f8a8c340293bf68f7bd63a3359849dd3e17b30ad2eee4c', '965910f83028bbfceff4f3a67d2b588005b1083206f428347313668ed23faffc770a19690bbf82c9', 0, '2021-11-06 12:13:05'),
('e97688a5b21f0e5538319dd2601943237d07035804ad0231ee92c2837d7a04334296dbf0803e74ab', 'b9aad71bfb0427408aa4f7591936e6d4f8a54136e7eaa4d1939619cd338adcefd358ebaa78c88707', 0, '2021-06-10 14:58:04'),
('eaf3319ed99a886dd8e8833632c1b83a109ce9833b9cef55ca912077b3353d9d03216779eccd1c02', '70b2357e0d99b8b6e950947fb61edbb649bfd0ae57e3b1aa9a030366c6b12cee425634b80b5dc386', 0, '2021-08-04 11:44:59'),
('eb33178e8807cfe520973f4d4cb40c132a0bfb24d983683d333022a44748255f53654f63279e5cee', 'ba6096755714fdedef1229145d6a2a2d783b99253b160b81df2f2f1af63fd77edb00d66b28dba183', 0, '2021-11-03 10:58:58'),
('eba325d94a32a3af94f6c8b375be4f40f93a82872f2a74585ebcd7dd8b0dacd3146286e4363ad25e', 'b1eed07a5ea83ae89bf093eda3e3a0943654481ca90b67861384edc079e5cf70ebe02835bbce9de7', 0, '2021-04-17 12:09:16'),
('ec6d72996a1c93f7e70ce703585582c49a475fb436c6b6d39aeaa03f06f50a300229541eabd38fad', '6108799e8fe251df9b0f4a89e0c3b679c05f84e2f4dc611a119e7cc49076299fa793bc76a11ae50d', 0, '2021-03-02 11:49:00'),
('edd7844485d56c5f13b31fa82ad66b3c6e1d1d95bd09e03f509153f8803d310bf8ca6e730b4b5e49', '16e5ded995bc2e9a88f5a269ec8692eccd4f1bd8b30fa2ed6ea741b3626ecf90349aabc8170c071e', 0, '2021-06-16 14:51:05'),
('ee2e3495836203f95d056e275ecc5caa8d9067e55821ce54686cb46bcf03a2b096fa9430dfeca32b', '5a2c134db3e86b32e025e4b6ba6cb7184d3a740406dbe373ac2299e0933e62818bb75ce4ff509260', 0, '2021-11-05 08:50:01'),
('eefc894c50faf8f1eb2b224262e498ecd0aedb55014dbeaf262ba1d341faa0ac1d699efa9a035950', 'f0e7446a0c09a3c0ef3488fa457819c3c3a027c0b799b960498f0331a76904d3cc73469d1c1cb297', 0, '2021-08-04 03:48:20'),
('ef01e1d34e0dccb3b020b014be60e46c1f1e3a087eb9ff3f3120f4de686cf12f639b9c004df48752', '9935e493276716c26dcbb3c3755866744168361788c2658a2ed0486c5cd12125506b1a2c5467c19d', 0, '2021-04-23 20:40:26'),
('ef191a85dd4040898919fd64a36a2d3a86b7dffa457d525f7c7b66017bb0c7d30eee3945d3d71903', '428d44b833db0e87aec9299c463262bce8a868b7023e7715660d7104b30ce14bf9f69e1e17688d3e', 0, '2021-08-04 04:13:09'),
('ef372ac0ecc7223adb07eb71a34d03d1da6548418fa9d0600150be5fd4a4e37fe8821101eb63ec68', 'f4978b05f2b3d476eeb3deacb4cddd1b7970713cc5842425615503e2a3e9a47bfb49171f61e7d090', 0, '2021-10-29 10:30:15'),
('ef9d3518114cdd6adc6b35f6497b1eb7dc0d3849f869d83077134f90e57e797ad5c664fa837c6300', '460f935030b75bc27bb19cd4d8b6134d19f845df0a4c2b69b73fbb3802897cc1848ba956ece5cd89', 0, '2021-06-23 20:37:24'),
('f00acbcf3e35b6f220239d046b74193b52ab2426ff0aaff10805581346af065fa7eb1bab8adcc92e', 'a5958e5c0db8afc020f22db25fdf8a681e71e6c057fdf4112313b0077c703c371868d37841625f2b', 0, '2021-08-22 08:31:40'),
('f1270a5c9fc5a3913c9bf8e0296ed11b358e9ad7af45747811e518b91ca2e571b77f3d7c7b317d41', '0e9fb6750c1deefc940d4b4a64a3bd6e8cb7588170b8a40f2c799e14211e83de7981206f691587ee', 0, '2021-09-18 05:33:09'),
('f15171da550ac590c6df58689a639da5180bb2c321febc6901b60dd5e26bb4050d36701de44ede4e', '6851cb607a23b81fd3cecee3cdc5496e0c2f6c791375d581350dfa5259261d434e88133e86ecf8b0', 0, '2021-05-31 21:13:10'),
('f1cd226ea554077fa191dc4a0f2c2b9a9d73d947d709d250015152db1481401762dec424022a670c', '0e70fcb9d9b89c5500a0b9e2aa437b0a41a197b52247882f9d8a77718e88d5989c13cdf7015b9324', 0, '2021-08-28 09:36:05'),
('f420dc920a1eb432376649677dc9c1583b4da500eedb387c1ea45954f623bac0db909bd91266a69c', 'd20ae31a2887689ef7f9fd680394341dd7215fe78d97974f7560793a1d49ba399a71df196c290830', 0, '2021-11-03 15:55:11'),
('f4b674ac2ddd03fb707bda318d4259edb005ce51a066f461594aa584de9cc0ac9b7e12ca620de0bf', '339687b32d31d5b7d3c9036d8bbb961c1f6b52132aa107afc4da3d7709324c7fff68a6db1f491bcc', 0, '2021-08-22 08:12:40'),
('f58cf4920756f49f93198068f9d7d37436ab1283a90c41afa12ba12bca39797440aa0502799c87be', '005206c6e52f2a339139f4067d4923f33754d6662b284e141fcc2a15adc561b9d952282a2dcd4e2f', 0, '2021-07-03 18:19:43'),
('f59c407c25e9d20a6f02734c04bbf60b2c2fce2a827010c36eff8629164384e4bcb55c78ad305575', 'ed2466da1fa329d76560cce7bdeb86afada79a7c44b3195f57eb85f01ad22f30767f5b20438c672e', 0, '2021-04-08 23:46:55'),
('f61da7bef4444f0d964eaef9dd5eda0cbae97602377139dcea92060e90bedd4d7bda1b1842205c97', 'fd737651edf7d5b18429db041d3da848f7cd28cc8c90b7ece0f141f201118c60772dabb4492d8f83', 0, '2021-03-28 21:57:39'),
('f678ff5539421416c082378bccfa809c2c6b16e709dedef1904915f17a5d81795a797d8a1be857d1', 'cd0835a309d000abb2035d3a35d11c0ec61bdb0e5aa7ec488a154dbd30cd77fab32fb66978798a9d', 0, '2021-04-15 15:18:52'),
('f756d8086a270ffee7b41d4a425bc2474e2002419a923ce8f8605ac2f13b9a8a520751c53b531ad2', 'e87d45ca9dce4901e58a4df033213fa53eadb4a2cec7c88531059dd29d5487f6d4b70ac0a334ca1f', 0, '2021-04-17 11:46:26'),
('f86b77c7b404c68b7db90cbbc58395310a2eae8d96b7a15e7a3f10a3509cb46f947fe980bc750976', '299441f6aa1e2bb4b19f358098db6efe1e904580686b7a237a9ab00346fbe7f84d0f5cde199c9d32', 0, '2021-04-27 21:57:02'),
('f8b51c11d500041fe213c605fd663cc333ceb1733a345726b82f6fba45881c7ba465f1883e7aebca', 'c9f2fd43c240c020aaf34aa916d228e2551321352628b7ad38b253b43762b6c7cb0e011edb6c8cb4', 0, '2021-11-06 12:14:20'),
('fa2208c1e9cf8d28795d4a89a24caaffa5b202d26f856a9173e2c87157e087c820685b5cabd06cb1', '81135ff4bf20931d01cab04449dc255e5b4d3ea464e1b32898bbd4c3c93b64a48f3b9bead15ea5f9', 0, '2021-07-05 08:13:16'),
('fa6ea6d621b872da87463602ab4dea06f22d21bac03e6dc58fad570197b7e1706e3cb9f142de55e4', '3461a2051c8d97c0c8d9c3362e84776c5d232b81ee0ef12cd6ac850a21617bbbd35c5b9b75039c67', 0, '2021-10-29 09:30:54'),
('fa804ae1e11dd040a52bfa8351e5425ef74573dbd6636346606efb2964305a8dda3aa7571d7613de', '431c75816e05a00f5036725222eb342f774cf4b4b5b6852a1bfd40fc8bfad9f8143825ca2ff38b87', 0, '2021-07-04 10:10:00'),
('fb313a62abbf9c4f74fe082b2360c65962866f955e212ea8a69cb84d0c5c419e9d289d6a18826c0e', '004b22b272c905508ceeaa85ab84bed3279202b20c97a66713c0ab6bf361b6624a45f767e8a9fc29', 0, '2021-04-27 22:43:04'),
('fc6c72e0cc4b128042530176c64880214039cdf1de3b6406c84edbd945cb01a5137f993b42ace5a2', 'e243ab9d62221f00f8e72cebee13838e30442dbebd85355df2b16b40f482b64594e1234218462f59', 0, '2021-07-16 06:27:06'),
('fdd4a0e62ea713678f4b9f565a2555f163891cca22d9fd51e80479c40a3e329bd261c763a4e870f5', '88757e6636c269dba47acd21b3c237ea5acd98c30488a2f3ffe9a38d0c02b9843c671e6119a31708', 0, '2021-07-19 13:23:35'),
('fecdf2b7a35e5db9c97fdb45f4d643828de85f92306a48c418894a4677612de5657b2e5c7f941c58', '827b454361bff6dc3026fcc4a4c2fc33ac555ed79f53b14ab446424d621620fd6ad473de49e05aa1', 0, '2021-04-15 21:40:03'),
('fef5b87b5e57b5bb5df1d1af2d2de09d0b2eff137e81eb4d6948b4725d090681aaecc27db36b46f7', 'f3d21096290c04809884bf39dec8d9593d24575688827e23950088d701ee741a1987d9c65ce0ee98', 0, '2021-09-18 06:23:14'),
('ff60211b143a4fbd71df7efac85121099aca2f151400be1a818b156f26eb6e33a220e8f3285b99cf', '01a4de3415da8906a15f4e595d35c0686ddc96eb7434a9e2d96e59d99c85c3ad3c3383d99203bffa', 0, '2021-04-16 12:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `item_id`, `quantity`, `amount`, `status`, `message`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 17, 1, 1000000, 'pending', 'i need the house', '18', '2020-05-23 22:15:39', '2020-05-23 22:15:39'),
(5, 19, 1, 200000, 'pending', 'the land is it still avaliabe?', '37', '2020-06-12 18:00:54', '2020-06-12 18:00:54'),
(6, 17, 1, 1000000, 'pending', 'Checking', '17', '2020-06-16 18:27:25', '2020-06-16 18:27:25'),
(7, 18, 1, 70000000, 'pending', 'xefw fwr', '2', '2020-06-16 19:22:47', '2020-06-16 19:22:47'),
(8, 18, 1, 70000000, 'pending', 'xefw fwr', '2', '2020-06-16 19:23:00', '2020-06-16 19:23:00'),
(9, 19, 1, 200000, 'pending', 'r32r23', '1', '2020-06-16 19:28:07', '2020-06-16 19:28:07'),
(10, 17, 1, 1000000, 'pending', 'nice', '2', '2020-06-17 03:44:56', '2020-06-17 03:44:56'),
(11, 20, 1, 100000, 'pending', 'ddced', '2', '2020-06-17 04:44:44', '2020-06-17 04:44:44'),
(12, 20, 1, 100000, 'pending', 'can i get a painter', '42', '2020-06-17 10:14:16', '2020-06-17 10:14:16'),
(13, 20, 1, 100000, 'pending', 'need the house', '45', '2020-06-19 04:11:45', '2020-06-19 04:11:45'),
(14, 20, 1, 100000, 'pending', 'tniec', '1', '2020-08-11 07:05:38', '2020-08-11 07:05:38'),
(15, 20, 1, 100000, 'pending', 'tniec', '1', '2020-08-11 07:10:03', '2020-08-11 07:10:03'),
(16, 20, 1, 100000, 'pending', 'tniec', '70', '2020-08-11 07:11:23', '2020-08-11 07:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proofs`
--

CREATE TABLE `proofs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jobmerge_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `proofs`
--

INSERT INTO `proofs` (`id`, `name`, `jobmerge_id`, `created_at`, `updated_at`) VALUES
(14, 'proof_1587382577.jpg', 11, '2020-04-20 10:36:18', '2020-04-20 10:36:18'),
(15, 'proof_1587670751.jpg', 12, '2020-04-23 18:39:14', '2020-04-23 18:39:14'),
(16, 'proof_1587670928.jpg', 12, '2020-04-23 18:42:08', '2020-04-23 18:42:08'),
(17, 'proof_1587674160.jpg', 12, '2020-04-23 19:36:01', '2020-04-23 19:36:01'),
(18, 'proof_1587674300.jpg', 12, '2020-04-23 19:38:20', '2020-04-23 19:38:20'),
(19, 'proof_1587675092.jpg', 12, '2020-04-23 19:51:32', '2020-04-23 19:51:32'),
(20, 'proof_1587675145.jpg', 12, '2020-04-23 19:52:25', '2020-04-23 19:52:25'),
(21, 'proof_1587675227.jpg', 11, '2020-04-23 19:53:47', '2020-04-23 19:53:47'),
(22, 'proof_1587675306.jpg', 12, '2020-04-23 19:55:06', '2020-04-23 19:55:06'),
(23, 'proof_1587675444.jpg', 12, '2020-04-23 19:57:24', '2020-04-23 19:57:24'),
(24, 'proof_1587675523.jpg', 12, '2020-04-23 19:58:43', '2020-04-23 19:58:43'),
(25, 'proof_1587675531.jpg', 12, '2020-04-23 19:58:51', '2020-04-23 19:58:51'),
(26, 'proof_1587675576.jpg', 12, '2020-04-23 19:59:36', '2020-04-23 19:59:36'),
(27, 'proof_1595165534.png', 23, '2020-07-19 17:32:14', '2020-07-19 17:32:14');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Architect', 'profile_1584314788.jpg', 1, '2020-03-15 22:26:28', '2020-03-15 22:26:28'),
(5, 'Civil Engineer', 'profile_1584314841.jpg', 1, '2020-03-15 22:27:21', '2020-03-15 22:27:21'),
(6, 'SURVEYOR', 'profile_1584314880.jpg', 1, '2020-03-15 22:28:00', '2020-03-15 22:28:00'),
(7, 'Contractor', 'profile_1584314925.jpg', 1, '2020-03-15 22:28:45', '2020-03-15 22:28:45'),
(8, 'Carpenter', 'profile_1584314964.jpg', 1, '2020-03-15 22:29:24', '2020-03-15 22:29:24'),
(9, 'Electrician', 'profile_1584315003.jpg', 1, '2020-03-15 22:30:03', '2020-03-15 22:30:03'),
(10, 'Plumber', 'profile_1584315029.jpg', 1, '2020-03-15 22:30:29', '2020-03-15 22:30:29'),
(11, 'Tiler', 'profile_1584315091.jpg', 1, '2020-03-15 22:31:31', '2020-03-15 22:31:31'),
(12, 'Roofers', 'profile_1584315125.jpg', 1, '2020-03-15 22:32:05', '2020-03-15 22:32:05'),
(13, 'Plasterer', 'profile_1584315176.jpg', 1, '2020-03-15 22:32:56', '2020-03-15 22:32:56'),
(14, 'Painter', 'profile_1584315220.jpg', 1, '2020-03-15 22:33:40', '2020-03-15 22:33:40'),
(15, 'Wallpaper/wallpanel installer', 'profile_1584315270.jpg', 1, '2020-03-15 22:34:30', '2020-03-15 22:34:30'),
(16, 'Interior Designer', 'profile_1584315322.jpeg', 1, '2020-03-15 22:35:22', '2020-03-15 22:35:22'),
(17, 'Epoxy wall/ floor makers', 'profile_1584315364.jpg', 1, '2020-03-15 22:36:04', '2020-03-15 22:36:04'),
(25, 'Plaster Ceiling Maker (POP)', 'profile_1592354511.jpg', 1, '2020-06-17 04:41:51', '2020-06-17 04:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Abia State', 1, '2020-06-04 11:44:33', '2020-06-04 11:44:33'),
(7, 'Adamawa State', 1, '2020-06-04 11:45:35', '2020-06-04 11:45:35'),
(8, 'Akwa Ibom State', 1, '2020-06-04 11:45:47', '2020-06-04 11:45:47'),
(9, 'Bauchi State', 1, '2020-06-04 11:47:06', '2020-06-04 11:47:06'),
(10, 'Anambra State', 1, '2020-06-04 11:46:13', '2020-06-04 11:46:13'),
(12, 'Bayelsa State', 1, '2020-06-04 11:47:18', '2020-06-04 11:47:18'),
(13, 'Benue State', 1, '2020-06-04 11:47:29', '2020-06-04 11:47:29'),
(14, 'Borno State', 1, '2020-06-04 11:47:39', '2020-06-04 11:47:39'),
(15, 'Cross River State', 1, '2020-06-04 11:47:50', '2020-06-04 11:47:50'),
(16, 'Delta State', 1, '2020-06-04 11:47:59', '2020-06-04 11:47:59'),
(17, 'Ebonyi State', 1, '2020-06-04 11:48:08', '2020-06-04 11:48:08'),
(18, 'Edo State', 1, '2020-06-04 11:48:19', '2020-06-04 11:48:19'),
(19, 'Ekiti State', 1, '2020-06-04 11:51:13', '2020-06-04 11:51:13'),
(20, 'Enugu State', 1, '2020-06-04 11:55:15', '2020-06-04 11:55:15'),
(21, 'Federal Capital Territory', 1, '2020-06-04 11:55:27', '2020-06-04 11:55:27'),
(22, 'Gombe State', 1, '2020-06-04 11:55:39', '2020-06-04 11:55:39'),
(23, 'Imo State', 1, '2020-06-04 11:57:20', '2020-06-04 11:57:20'),
(24, 'Jigawa State', 1, '2020-06-04 11:57:33', '2020-06-04 11:57:33'),
(25, 'Kaduna State', 1, '2020-06-04 11:58:17', '2020-06-04 11:58:17'),
(26, 'Kano State', 1, '2020-06-04 11:58:29', '2020-06-04 11:58:29'),
(27, 'Katsina State', 1, '2020-06-04 11:58:39', '2020-06-04 11:58:39'),
(28, 'Kebbi State', 1, '2020-06-04 11:59:56', '2020-06-04 11:59:56'),
(29, 'Kogi State', 1, '2020-06-04 12:00:05', '2020-06-04 12:00:05'),
(30, 'Kwara State', 1, '2020-06-04 12:00:14', '2020-06-04 12:00:14'),
(31, 'Lagos State', 1, '2020-06-04 12:00:23', '2020-06-04 12:00:23'),
(32, 'Nasarawa State', 1, '2020-06-04 12:00:30', '2020-06-04 12:00:30'),
(33, 'Niger State', 1, '2020-06-04 12:00:38', '2020-06-04 12:00:38'),
(34, 'Ogun State', 1, '2020-06-04 12:00:48', '2020-06-04 12:00:48'),
(35, 'Ondo State', 1, '2020-06-04 12:00:55', '2020-06-04 12:00:55'),
(36, 'Osun State', 1, '2020-06-04 12:01:22', '2020-06-04 12:01:22'),
(37, 'Oyo State', 1, '2020-06-04 12:01:31', '2020-06-04 12:01:31'),
(38, 'Plateau State', 1, '2020-06-04 12:01:40', '2020-06-04 12:01:40'),
(39, 'Rivers State', 1, '2020-06-04 12:01:49', '2020-06-04 12:01:49'),
(40, 'Sokoto State', 1, '2020-06-04 12:01:57', '2020-06-04 12:01:57'),
(41, 'Taraba State', 1, '2020-06-04 12:02:06', '2020-06-04 12:02:06'),
(42, 'Yobe State', 1, '2020-06-04 12:02:28', '2020-06-04 12:02:28'),
(43, 'Zamfara State', 1, '2020-06-04 12:02:40', '2020-06-04 12:02:40');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supervisor_data`
--

CREATE TABLE `supervisor_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantor_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `state_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supervisor_data`
--

INSERT INTO `supervisor_data` (`id`, `user_id`, `gender`, `phone_number`, `account_number`, `account_name`, `guarantor`, `guarantor_number`, `profile_image`, `address`, `area_id`, `state_id`, `state_name`, `area_name`, `date_of_birth`, `id_type`, `id_number`, `id_image`, `created_at`, `updated_at`) VALUES
(1, 0, 'Male', '0', '0', '0', '0', '0', '0', '0', 0, '0', '', '', '2020-08-04 04:58:28', NULL, NULL, NULL, NULL, NULL),
(2, 68, 'Female', '08796789879', '055544544', '0etyty', '4dfggdf', '0656543455', 'profile_1596518459.png', 'sauka', 122, '24', 'Jigawa State', 'Gumel', '2020-08-11 10:27:34', 'Drivers licence', 'e2341243e23', 'profile_1597141653.png', '2020-08-04 04:03:10', '2020-08-11 09:27:34'),
(3, 71, 'Male', '003939333', '03354545245', 'GTB', 'vgtrevrgw', '054253534', 'profile_1597137762.png', '0sasad', 62, '21', 'Federal Capital Territory', 'Kado', '2020-08-11 09:22:42', NULL, NULL, NULL, '2020-08-11 08:22:18', '2020-08-11 08:22:42'),
(4, 85, 'Female', '07676765', '7645656652', 'gh gdfj', '0bbgfbhgf', '056565465', 'profile_1598085457.jpg', '0sasad', 27, '14', 'Borno State', 'Dikwa', '2020-09-06 22:46:09', 'Pvc', '85675643', 'profile_1599432369.jpg', '2020-08-22 12:36:55', '2020-09-07 02:46:09'),
(5, 141, 'Male', '07031911853', '2176468466', 'Zenith Bank', 'Engr.Joseph Akpabio', '07052873551', 'profile_1600554512.JPG', '35, Osongama estate', 16, '8', 'Akwa Ibom State', 'Uyo', '2020-09-19 22:28:32', NULL, NULL, NULL, '2020-09-20 02:27:25', '2020-09-20 02:28:32'),
(6, 145, 'Female', '08120007693', '0', 'Etim Ekemini', 'Peterson umoh', '0803085610', 'profile_1604586240.jpg', 'Duste makaranta , bwari area council', 16, '8', 'Akwa Ibom State', 'Uyo', '2020-11-05 14:29:10', 'National ID', '47935810354', 'profile_1604586550.jpg', '2020-09-20 11:49:27', '2020-11-05 19:29:10'),
(7, 148, 'Male', '08037174717', '2034520710', 'UBA', 'Miss Ezenwa Augusta Onyinye', '08036572466', 'profile_1600598361.jpg', 'House 107, Behind ECWA Church', 57, '21', 'Federal Capital Territory', 'Durumi', '1988-09-16 04:00:00', 'Pvc', 'A21818739', 'profile_1600599020.jpg', '2020-09-20 14:36:54', '2020-09-20 14:52:57'),
(8, 153, 'Male', '08169401926', '044040040', '654456', '54645', '08169401926', 'profile_1603965807.PNG', '0sasad', 119, '23', 'Imo State', 'Owerri', '2020-10-29 10:03:53', 'Drivers licence', '06066060', 'profile_1603965833.jpg', '2020-10-29 14:02:59', '2020-10-29 14:03:53'),
(9, 166, 'Male', '07036994580', '0170495924', 'GTBANK', 'Hajia Jummai Abubakar', '07082224896', 'profile_1604090863.jpg', 'Borehole lane behind St Anthony\'s Catholic Church Kabusa Lokogoma', 135, '29', 'Kogi State', 'Idah', '1985-07-05 04:00:00', 'International passport', 'A08914373', 'profile_1604091252.jpg', '2020-10-31 00:46:17', '2020-10-31 09:58:47'),
(10, 173, 'Male', '08027330996', '1771736032', 'Polaris Bank', 'Hon. Dachung Musa Bagos', '08036205122', 'profile_1604338041.jpg', 'Plot 115, Area One Opposite Dam, Karu L.G.A, Nasarawa State', 49, '21', 'Federal Capital Territory', 'Central Area', '2020-11-02 05:00:00', 'National ID', '41928054391', 'profile_1604338054.jpg', '2020-11-02 22:20:44', '2020-11-02 22:28:05'),
(11, 186, 'Male', '08141179616', '6017183858', 'Omoge Kolawole samson', 'Adedokun Emmanuel', '09033493900', 'profile_1604515472.jpg', 'no 7 isokan estate Sanyo Ibadan Oyo state', 180, '37', 'Oyo State', 'Ibadan', '2020-11-04 18:44:32', 'National ID', '33057635263', 'profile_1604436072.jpg', '2020-11-04 01:39:06', '2020-11-04 23:44:32'),
(12, 187, 'Male', '09079449867', '0177449263', 'GTBank', 'Aisha Momoh', '09073795172', 'profile_1604643523.jpg', 'No 2 GWK Gwagwalada Abuja', 49, '21', 'Federal Capital Territory', 'Central Area', '1986-06-06 04:00:00', NULL, NULL, NULL, '2020-11-04 01:59:20', '2020-11-06 17:48:37'),
(13, 191, 'Male', '07015321066', '0039812172', 'Guarantee Trust Bank', 'Precious Gad', '07065572091', 'profile_1604475391.JPG', '#3, Nwigbalor Compound Old Bori Road Eteo-Eleme Eleme LGA Rivers State', 192, '39', 'Rivers State', 'Port Harcourt', '2020-11-04 07:36:31', 'International passport', 'A10366956', 'profile_1604475314.jpg', '2020-11-04 11:54:54', '2020-11-04 12:36:31'),
(14, 193, 'Male', '07030372209', '00005', 'Ecobank', 'Mr. Ukpor Augustine', '08148830875', NULL, 'Mpape , Abuja', 114, '21', 'Federal Capital Territory', 'Mpape', '2020-08-04 04:00:00', NULL, NULL, NULL, '2020-11-05 13:54:43', '2020-11-05 13:54:43'),
(15, 202, 'Male', '07069324371', '0128557821', 'Guaranty Trust Bank', 'Orimoloye Kehinde', '09073018658', 'profile_1604569738.jpg', 'No 2, Olayinka Sanusi street, Arab Road, Kubwa Abuja', 112, '21', 'Federal Capital Territory', 'Kubwa', '1993-08-13 04:00:00', 'National ID', '2750807566', 'profile_1604570011.jpg', '2020-11-05 14:46:30', '2020-11-05 14:55:17'),
(16, 205, 'Male', '08152907067', '0165765748', 'Gtbank', 'Akinsete kehinde', '08164234466', 'profile_1604597396.jpg', 'Plot 11 lane 3 ifeoluwa estate off orita challenge ibadan', 180, '37', 'Oyo State', 'Ibadan', '2020-11-05 17:29:56', NULL, NULL, NULL, '2020-11-05 22:26:46', '2020-11-05 22:29:56'),
(17, 214, 'Male', '08134671333', '0010341538', 'GT Bank', 'Pastor Adebisi James', '07033250077', 'profile_1604742196.JPG', '3, Oluwakemi Street, Mowe, Ogun state', 161, '34', 'Ogun State', 'Abeokuta', '2020-11-07 09:44:58', 'Drivers licence', 'FKJ31700AB13', 'profile_1604742298.JPG', '2020-11-07 14:42:37', '2020-11-07 14:44:58');

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE `tips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id`, `title`, `body`, `status`, `created_at`, `updated_at`) VALUES
(5, 'OUR AIM IS TO HELP YOU EARN MORE CONTRACTS', 'Join us, get more contracts, make more money and live good life. with us; your full contracts payment is assured. lets help get the contract you ever dream off.', 1, '2020-10-29 16:26:47', '2020-10-29 16:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `status`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'samueleke', 'samueleke71@gmail.com', 2, 0, NULL, '$2y$10$OazxwhL.5/NsnowOTBdp2eMIhLSaruWM04buj7gsbEtEA3RV1zVYW', NULL, '2020-02-24 09:34:31', '2020-06-16 18:39:07'),
(2, 'samueleke', 'samueleke712@gmail.com', 3, 1, NULL, '$2y$10$MAbM50CxoBm6ON6PeJsxfuJuIEH4s5zgALf76A.0YhcN/vPhAww.m', NULL, '2020-02-24 10:00:51', '2020-08-06 22:09:45'),
(3, 'samueleke', 'samueleke732@gmail.com', 3, 0, NULL, '$2y$10$JNFwE0S8J4Ux0Fev2zapPuAG46KGXFg/NeNDKgb/4nu3dW8J32NDi', NULL, '2020-02-24 10:01:22', '2020-03-15 23:04:19'),
(4, 'samueleke', 'samueleke7321@gmail.com', 3, 0, NULL, '$2y$10$6eCvwwj4A9kGPS/oHrmrruA9dK5GVHM7RcO8cBZ/b6JO1J6CGt2jq', NULL, '2020-02-24 10:02:00', '2020-03-15 23:04:32'),
(5, 'samueleke', 'samueleke7341@gmail.com', 1, 0, NULL, '$2y$10$iLWcYJBf69zuL6w4k1hPo.G8nOGSw/HVKqZoehogdVualXgNfFCo6', NULL, '2020-02-24 10:02:19', '2020-02-24 10:02:19'),
(6, 'samueleke', 'samueleke7841@gmail.com', 1, 0, NULL, '$2y$10$xJ3TfjNNHSWURUwq.V9ZXeTqBL90a6XYaesNA82fKIKSaYn5X7YxC', NULL, '2020-02-24 10:03:58', '2020-02-24 10:03:58'),
(7, 'samueleke', 'samueleke78451@gmail.com', 1, 0, NULL, '$2y$10$vn.0NrKElCeGDC2jdruIHuwikS/5E89m7PsuUt/gmPJt.FPUkgeRK', NULL, '2020-02-24 10:06:39', '2020-02-24 10:06:39'),
(8, 'samuel eke', 'samueleke1@gmail.com', 2, 0, NULL, '$2y$10$oR7O1BAhDNjR4TcCs/McbuZKc5ptWRS77XgdhGaHB9pJVu1NvkPSO', NULL, '2020-02-27 11:46:00', '2020-03-09 10:44:29'),
(9, 'james kantoma', 'jamesatomw@gmail.com', 1, 0, NULL, '$2y$10$QVc5Naban0xBoDBU6tGub.7FrWwzT1DP2YGDHEJN1Ae/W6ZGBbnde', NULL, '2020-03-12 22:20:13', '2020-03-12 22:20:13'),
(10, 'fdh r', 'teghrt2@GMS', 1, 0, NULL, '$2y$10$42HnrSTLehvcA3d.v6GfvOORm/NIMkJiGcOS.j/phA/t/Dk/qXqDG', NULL, '2020-03-13 20:12:45', '2020-03-13 20:12:45'),
(11, 'iconet', 'testuser@gmail.com', 1, 0, NULL, '$2y$10$cFCQJt9PbA/VdkGNS820VOpHGDTFT98QHQTdiEAtmgDrraTGWPAqe', NULL, '2020-03-15 22:43:39', '2020-03-15 22:43:39'),
(12, 'go9v8uhfdion', 'samueleke@gmail.com', 1, 0, NULL, '$2y$10$xdjK6jbuVMtuQYBlGcNRt.PBenWMffdzHh.IjfhX/gr9f3Z9jeXj.', NULL, '2020-04-03 11:09:45', '2020-04-03 11:09:45'),
(13, 'sfpisjiods', 'samue@gm.do', 1, 0, NULL, '$2y$10$Dk/nV9etvj9ilqYVOP11LOjCsV4/d9RjpOs0iGZipnpU.08DlQIiC', NULL, '2020-04-03 12:05:44', '2020-04-03 12:05:44'),
(14, 'eruofh[sbdsj', 'samueleke@hmal.ci', 1, 0, NULL, '$2y$10$imKFP3vkUeHcCVPSRJ3R6u.9SfS7/Ra0axyOdyYpmB4tZZ6Gtsk3G', NULL, '2020-04-03 12:11:08', '2020-06-19 03:56:20'),
(15, 'james  artisan', 'artisan1@gmail.com', 3, 1, NULL, '$2y$10$HYPdcwwFeXaMopwlmIc5aOQaFGRB9h0XPYw1lDQY.3rg.qfNMge/y', NULL, '2020-04-10 21:39:14', '2020-04-28 15:40:30'),
(16, 'Admin Admin', 'admin@gmail.com', 2, 1, NULL, '$2y$10$8Nv7QIGTYch1wGUJe59kk.3EiUZLNMKhrxqfkHlUsUudTgBXqNlNq', NULL, '2020-04-26 23:19:31', '2020-04-28 00:33:45'),
(17, 'adus michael', 'adusmichael@gmail.com', 3, 0, NULL, '$2y$10$U98ag6Nh9gS78.oxVONBiexhcOzZbvxsVFOCiXQJKFCHOI/5HM/K.', NULL, '2020-04-28 14:54:38', '2020-04-28 14:54:38'),
(18, 'Kelvin Chibuike', 'eyakelvinchibuike@gmail.com', 3, 0, NULL, '$2y$10$Revru1BqsS/AyeNmgNLuw.yI/pR..zlbBoe/5fS/DiopBIZAT4kZi', NULL, '2020-05-22 14:04:49', '2020-05-22 14:04:49'),
(19, 'Kelvin Chibuike', 'eyaklvn@gmail.com', 3, 0, NULL, '$2y$10$YyLvUYXtk9EURc8mCsKYmebP84RzTSkVVKJCaHbliyBnYY.XvUikm', NULL, '2020-05-22 14:06:03', '2020-05-22 14:06:03'),
(20, 'Buka Chibuike', 'makeklassic@gmail.com', 3, 0, NULL, '$2y$10$VfLIzwjz37R677TxIv5nUesHiZqwXBGOLRKWqHE93pDS.Owl2bKeS', NULL, '2020-05-22 19:48:51', '2020-05-22 19:48:51'),
(21, 'nedu aboki', 'nedu@gmail.com', 3, 1, NULL, '$2y$10$KSNizYyF4m4XGetcBqmQquUcVTGD9b.D..u7PJrZx4B1jKooQNvOu', NULL, '2020-05-30 19:19:35', '2020-05-30 19:33:06'),
(22, 'kelvin bu', 'bu@gmail.com', 1, 1, NULL, '$2y$10$B9.eZ86pXceB/V3Kb7I/veyRAdfKo1p3XCOkCaVNPdEN3WXHd9sGO', NULL, '2020-05-30 21:21:12', '2020-11-05 19:21:51'),
(23, 'jay', 'jay@gmail.com', 1, 1, NULL, '$2y$10$JLK0DzWsZRHhxZdsXoIUDOzd64lHfpNLUPtItIJpCzbS65x8CNrGK', NULL, '2020-05-31 12:37:26', '2020-05-31 12:41:09'),
(24, 'samuel sa', 'adminw@gmail.com', 3, 0, NULL, '$2y$10$zaiz.iS/hoSUSHiiu9FBOOzspMMqUKI9NdMY85yVxnpE64Mcm.G9G', NULL, '2020-06-01 22:21:12', '2020-06-01 22:21:12'),
(25, 'samuel sa', 'adminw3@gmail.com', 3, 0, NULL, '$2y$10$y7y/D5at3hV0a01y2pnVoukc/GLYt6gq3w1.Usm27aoe4vY.iz5Mq', NULL, '2020-06-01 22:21:37', '2020-06-01 22:21:37'),
(26, 'B', 'd@gmail.com', 1, 0, NULL, '$2y$10$eIFd1XLwGT/mJ0d8mB8RJuJS/12XsCX8Ggqi2YbPnqzw7s6EC5gKq', NULL, '2020-06-03 16:17:59', '2020-06-03 16:17:59'),
(27, 'G K', 'k@gmail.com', 3, 0, NULL, '$2y$10$z4Y0ltjpizauEhBdf1QbWOXVi3lKoMYVdbfbGull0Gn/rIH3rRr6O', NULL, '2020-06-03 16:19:27', '2020-06-03 16:19:27'),
(28, 'Mr Emmanuel', 'e@gmail.com', 1, 0, NULL, '$2y$10$aF206S23hQCDhEa9d7uFb.fnBFTTzlaA4xDVIlpvshPy8bqDo.ovK', NULL, '2020-06-03 17:16:10', '2020-06-03 17:16:10'),
(29, 'Kelvin Chibuike', 'ke33@gmail.com', 3, 0, NULL, '$2y$10$E0G5cxKGVegG9./a9CRMdO6Nx8a2TKbFFWlrqssx6gE9HczZPVzna', NULL, '2020-06-04 18:28:19', '2020-06-04 18:28:19'),
(30, 'Buka Chibuike', 'kl@gmail.com', 3, 0, NULL, '$2y$10$LDg2e7M/wAjMU0ysS.61UuWAudwKLbkZpGKfomf65J7iMHl2M7jmm', NULL, '2020-06-06 22:13:39', '2020-06-06 22:13:39'),
(31, 'Buka Chibuike', 'dd@gmail.com', 3, 0, NULL, '$2y$10$T1dBp86azoVQHy01SlMpOO9/BxQMfTPckCeQLKTfcbA9b3lBBbkke', NULL, '2020-06-10 10:29:00', '2020-06-10 10:29:00'),
(32, 'samuel eke', 'samueleke70@gmail.com', 3, 0, NULL, '$2y$10$5vwqZreiJru34CQ6fnqB4uO8j39JNWQOlfMPD7MGzQ8EiY0tRQPfW', NULL, '2020-06-10 17:10:54', '2020-06-10 17:10:54'),
(33, 'Buka G', 'kokkka@gmail.com', 3, 0, NULL, '$2y$10$HN1fkCuUngK1jRCfqpHfC.7aZi5qPLNaOFv4lRhv3upG2bjV6D5sa', NULL, '2020-06-10 18:36:35', '2020-06-10 18:36:35'),
(34, 'H C', 'ch@gmail.com', 3, 0, NULL, '$2y$10$yRk.7K6GsrFA9QUpOjBpD.0XeN6Yz..wcVkKsHEJ8RKCYR9CvbKSy', NULL, '2020-06-10 18:38:52', '2020-06-10 18:38:52'),
(35, 'go o', 'o@gmail.com', 3, 1, NULL, '$2y$10$Wkj7foibPkPKY88RnnVCAeTllTgJ4EM/4gje7zdNnKkIlLHNcPJdq', NULL, '2020-06-12 17:05:53', '2020-06-12 17:15:57'),
(36, 'zoro', 'zoro@gmail.com', 1, 0, NULL, '$2y$10$9P36T6nvckiqCmQ6Fas5WOsu6tNZEhRoDCpDIS7XOGc8DxqfkRP7O', NULL, '2020-06-12 17:21:30', '2020-06-12 17:21:30'),
(37, 'me', 'me@gmail.com', 1, 0, NULL, '$2y$10$7dpQqIZ0VyivLxYid8CBTeahpaNvi.qw7cx3MI6X/yxf5Y5/zCWTO', NULL, '2020-06-12 17:57:47', '2020-06-12 17:57:47'),
(38, 'samuel eke', 'artisantest@gmail.com', 3, 0, NULL, '$2y$10$eNbm/JB/2oV/YwQSLuq8N.n8RJf8AlP0VhAzmeCIQZxzhB30IIcj6', NULL, '2020-06-17 03:42:01', '2020-06-17 03:42:01'),
(39, 'samuel eke', 'artisantest1@gmail.com', 3, 0, NULL, '$2y$10$V9YbDilv/PucB3eRbyUmDOb2cO3g3w6hCOlQ1zd1PQAmIiVKQmG4O', NULL, '2020-06-17 03:43:55', '2020-06-17 03:43:55'),
(40, 'Admin eke', 'artisantest2@gmail.com', 3, 0, NULL, '$2y$10$sgbKA.UWC91Ix6/79XXRpuh4B8.6tniH5cW29KIWe0nmriTyQSeae', NULL, '2020-06-17 03:44:24', '2020-06-17 03:44:24'),
(41, 'samuel err', 'samueleke7891@gmail.com', 1, 0, NULL, '$2y$10$f6YMae5ofMzov6kxzoO90.d5vvDg/PuV6Ndk0CqLr1AZ9t2pmRlc6', NULL, '2020-06-17 05:26:21', '2020-11-05 19:56:24'),
(42, 'mike john', 'philrobertsofficial@gmail.com', 1, 0, NULL, '$2y$10$l3FRtzh6swEUo.uh30CblecMXcIfXQ2aaj8LEzPU7j/pXdvoyt0ae', NULL, '2020-06-17 10:14:16', '2020-06-17 10:14:16'),
(43, 'ben ben', 'ben@gmail.com', 3, 1, NULL, '$2y$10$Bpa./tivTrubdg6auNl3XubzRiVsESdnibdR8LddHhsXbEGVzBCVy', NULL, '2020-06-19 02:59:30', '2020-06-19 03:46:34'),
(44, 'emma', 'emma@gmail.com', 1, 0, NULL, '$2y$10$a8Ed2pk6FeLNnRFT5DHSkeNCYCTesp5XRcrDGOWD41Za3l1JIq0F.', NULL, '2020-06-19 03:37:48', '2020-06-19 03:37:48'),
(45, 'na me', 'name@gmail.com', 1, 0, NULL, '$2y$10$tzCi31wp5fZIYfcjWM2Yt.glnHDy4mVRJhur8/o7v4eOiNq.mD38.', NULL, '2020-06-19 04:11:45', '2020-06-19 04:11:45'),
(46, 'kia chop', 'chop@gmail.com', 3, 0, NULL, '$2y$10$bhjzUSt2Kn8RMTBQl9c5eeQt99PMplJKRBaf.yrZdhtVxOmoP62MS', NULL, '2020-06-19 04:22:30', '2020-06-19 04:22:30'),
(47, 'H C', 'c@gamil.com', 3, 0, NULL, '$2y$10$fib7KLX0g5FV1iUnBl8sxexLoP4ZWSgUprPDUGXLNuNnJ6cG1v6Oa', NULL, '2020-06-22 11:32:55', '2020-06-22 11:32:55'),
(48, 'Mr Emmanuel', 'kc@gmail.com', 1, 0, NULL, '$2y$10$VyinsI/Tj.uaba4cRJAoy.cwWCA5uCA.sYQhGPK0X./dyC89NbH6S', NULL, '2020-06-22 11:35:23', '2020-06-22 11:35:23'),
(49, 'dano dano', 'dano@gmail.com', 3, 1, NULL, '$2y$10$6y8FaHWCCKY9TsP1FPKwL.nSKTzKSDKXtP1b/HoQYT0VKCoocqN3.', NULL, '2020-06-23 23:44:27', '2020-06-23 23:48:32'),
(50, 'mike', 'mike@gmail.com', 1, 0, NULL, '$2y$10$g9wKiOxd/R./RxWkBBaJH.qArepm/qlbfsuc8Nof7xR5hS1IdvALa', NULL, '2020-06-23 23:51:59', '2020-06-23 23:51:59'),
(51, 'john Mike', 'vcggg@gmail.com', 1, 0, NULL, '$2y$10$hho8ou8bjYBeoRmQBY1AB.BsgeyO8mCx2MH5EDg3X4lgmv2lRsgUS', NULL, '2020-06-24 14:39:40', '2020-06-24 14:39:40'),
(52, 'Samuel eke', 'samueke71@gmail.com', 3, 0, NULL, '$2y$10$JmqhwjHfSTMlW6JGQp5haOCPh8rbTlGaogHJdQYCDs7EwekrKUoRG', NULL, '2020-06-24 15:53:59', '2020-06-24 15:53:59'),
(53, 'samuel eke', 'samuelekei8767@gmail.com', 3, 0, NULL, '$2y$10$XZpic2jLT3MH1AwVb3CjUui07RWUVr.8X8k2b9k8qMrQr9ccvhtHS', NULL, '2020-06-24 16:03:04', '2020-06-24 16:03:04'),
(54, 'Samuel amin', 'samuel@gmail.com', 3, 0, NULL, '$2y$10$6nbpCRPOLcMBSCQxQT1C3eB8pSwTzvYIq3ZtUbc67AZCFgA3OGxcS', NULL, '2020-06-24 16:49:17', '2020-06-24 16:49:17'),
(55, 'samuel', 'samueleke7001@gmail.com', 3, 0, NULL, '$2y$10$yLdpTaj8.o2MPX8Gp0jla.au9gvYuEYUSpANogAdwZrsWcvqoOo8a', NULL, '2020-06-24 16:51:05', '2020-06-24 16:51:05'),
(56, 'samuel', 'samueleke74@gmail.com', 3, 0, NULL, '$2y$10$LMUWU3yJp0sqIsgmPdxbMuKO52NCGY1dvAmITMOTUVjQlFCIZ1fGy', NULL, '2020-07-02 13:42:46', '2020-07-02 13:42:46'),
(57, 'izu', 'izu@gmail.com', 1, 0, NULL, '$2y$10$1SK.VgdnsolhteYeTzTMn.9iljOO1c4UWU5awgDQ2Ylemg4psdIUm', NULL, '2020-07-08 06:19:04', '2020-07-08 06:19:04'),
(58, 'james', 'Samname@gmail.com', 3, 0, NULL, '$2y$10$q9biQ9VNsyHTcQiuUfjKzuFLu4VJHJvUUetah/otUVpphhbfUnEie', NULL, '2020-07-15 00:01:15', '2020-07-15 00:01:15'),
(59, 'EYA Kelvin Chibuike', 'madek@gmail.com', 3, 0, NULL, '$2y$10$gadYLj.3.HpBeeeAim9rzePBvyp/89O1w98f9Fv81cRCXCOVcJjXu', NULL, '2020-07-15 21:35:09', '2020-07-15 21:35:09'),
(60, 'james sala', 'jamessala@gmail.com', 3, 1, NULL, '$2y$10$/Ygywjn4e9Av1oM/zJdIK./743Y/jLpe3/OHsbE.Ol8I4hbX2GSOK', NULL, '2020-07-19 17:19:53', '2020-07-19 17:24:15'),
(61, 'kel', 'kel@gmail.com', 3, 0, NULL, '$2y$10$Comra5Xafw8ffP8.9XuE9OW1hsyL59yz..pjsP2rt0TQcBM1j4OvC', NULL, '2020-07-19 17:35:26', '2020-07-19 17:35:26'),
(62, 'Gbolahan Yasline', 'yasline200@gmail.com', 3, 0, NULL, '$2y$10$2yEE/zikd80VGMwDV./uFuw4E15bu4O.L1jOGXSvj.SQu3EsF0QES', NULL, '2020-07-22 17:16:24', '2020-07-22 17:16:24'),
(63, 'vv cc', 'vv@gmail.com', 3, 0, NULL, '$2y$10$GT9Z6DxucO3khrDVAucDsOmzV/YUA.e7pW53EDTg53/iOYF.JCE4a', NULL, '2020-07-30 00:08:51', '2020-07-30 00:08:51'),
(64, 'ken', 'ken@gmail.com', 3, 0, NULL, '$2y$10$Dkc6aeoI4id93EfYrGjRjuz5933XquQP8AxQAOlWMi0wwtNbw3LFO', NULL, '2020-07-30 00:28:06', '2020-07-30 00:28:06'),
(65, 'chu', 'chuchu@gmail.com', 3, 0, NULL, '$2y$10$C219KFAgSHp9nbIqOgn.WeSdminlXAOowXf8H/YhtKqRPIyYkvBZS', NULL, '2020-07-30 14:39:35', '2020-07-30 14:39:35'),
(66, 'Admin eke', 'samueleke7187@gmail.com', 3, 0, NULL, '$2y$10$Jzuef89rSwo13AU3xOl6vuqBL/Xcw6NXky9vMprZfX0YTOmumylVW', NULL, '2020-08-04 01:41:47', '2020-08-04 01:41:47'),
(67, 'sa saas', 'samuelekew71@gmail.com', 4, 0, NULL, '$2y$10$SJhIpY3VP6D81CxBbX6ZteBLcUZ5PkBeMbuuIFuo.NXYiWrKQPGVK', NULL, '2020-08-04 01:42:48', '2020-08-04 01:42:48'),
(68, 'rrtet saas', 'samueleke11@gmail.com', 4, 0, NULL, '$2y$10$vvEy0FEx1xVBIBo2nX0EguDrCce/FbFXHCNlu8qapM9DV2MNA6kL6', NULL, '2020-08-04 01:57:32', '2020-08-04 02:02:55'),
(69, 'samuel qqq', 'samueleke12@gmail.com', 3, 1, NULL, '$2y$10$wWHE3tR0JVT55vXwfMnCqOCKBT46PekhwORpXkTYCDWOlTMQYFy12', NULL, '2020-08-06 22:59:33', '2020-08-06 23:02:34'),
(70, 'samuel eke', 'samueleke722@gmail.com', 1, 0, NULL, '$2y$10$Gxy0RGogD6rif5YMHHYFYupCg0K.RPITbpJH5kwUAIWTLKNe0OanC', NULL, '2020-08-11 07:11:21', '2020-08-11 07:11:21'),
(71, 'sammy  lie', 'samueleke81@gmail.com', 4, 1, NULL, '$2y$10$g30y93A9qkrgUAEcSptXGet5km2nyiYwPYyWVwyWTnRp55Yw5/SzS', NULL, '2020-08-11 08:20:50', '2020-08-11 08:29:01'),
(72, 'samuel eke', 'samuel@buypower.ng', 3, 1, NULL, '$2y$10$HiT1p2F0kvMznl.mGymAq.W1sb94TThAKxHSSWYcU8TMLhL4Q4XoO', NULL, '2020-08-11 08:31:08', '2020-08-11 08:34:37'),
(73, 'samuel eke', 'samueleke791@gmail.com', 3, 0, NULL, '$2y$10$ekJfsJFJHkhX7PIYbQnm4eHyDAY7FrXXRdFsonJ4vhKppYvxCvSom', NULL, '2020-08-11 08:45:23', '2020-08-11 08:45:23'),
(74, 'sammy  saas', 'samueleke71000@gmail.com', 4, 0, NULL, '$2y$10$iWk9xLgfTaBd8ND/5tdudeeVzbwvJo8FiT9UbV13E0dxSqSMExydS', NULL, '2020-08-11 08:47:34', '2020-08-11 08:47:34'),
(75, 'rrtet qwS', 'samueleke7221@gmail.com', 4, 0, NULL, '$2y$10$qSkLF.8xk51I9QCgA2s0zuYlpLWsLgPeCnzcDd2LgNAKku1QIUus.', NULL, '2020-08-11 10:09:41', '2020-08-11 10:09:41'),
(76, 'Kel vin', 'vin@gmail.com', 4, 0, NULL, '$2y$10$fmMFmyEuT.KAmFGhYQuHQeJ48pLRMlyemmBGBK8LZ2m5BgZ1U4P..', NULL, '2020-08-11 21:05:08', '2020-08-11 21:05:08'),
(77, 'Chi bu', 'cj@gmail.com', 4, 0, NULL, '$2y$10$6oYNJ7rpKqBBRWyji7a3.eCIUaOzSy4oj5.iK8Vn6DMiSZ9HoT0Jm', NULL, '2020-08-12 12:13:40', '2020-08-12 12:13:40'),
(78, 'Ben zen', 'zen@gmail.com', 4, 1, NULL, '$2y$10$XE9oI3drflUSbPAxesCpgOx6SBRdHP3GCzvjIadPcPtXCOGjmv4AK', NULL, '2020-08-16 00:24:55', '2020-08-16 00:27:48'),
(79, 'samuel@gmail.com', 'samueleke7111@gmail.com', 4, 0, NULL, '$2y$10$4xRo21phYSeQxvhfLL//huC2xRIbCYapXhseSxbiSspO4cKsj9w2i', NULL, '2020-08-19 16:25:50', '2020-08-19 16:25:50'),
(80, 'samuel@gmail.com', 'samueleke7113@gmail.com', 4, 0, NULL, '$2y$10$VJ.aVBE9FVAtDUA7Gho0j.YUrDp7SBRQcg4YxVqWcHPctui4rScYS', NULL, '2020-08-19 16:28:58', '2020-08-19 16:28:58'),
(81, 'samu', 'samueleke7141@gmail.comt', 4, 0, NULL, '$2y$10$oPz9zkSytzfsAjqxxypIZu8knhPy/sgHUk2q/r7F8aHOdHE9jD/oq', NULL, '2020-08-19 16:30:24', '2020-08-19 16:30:24'),
(82, 'erfd', 'samueleke7813@gmail.com', 4, 0, NULL, '$2y$10$HebLcj7kmLo/fLLLwe2fsOAwz5hHOb7AJ3g1Mj4Ipdxv5QFrKlGFW', NULL, '2020-08-19 16:33:31', '2020-08-19 16:33:31'),
(83, 'Mark Nmaju', 'marknmaju20166@hotmail.com', 3, 0, NULL, '$2y$10$ZooKNHwuAqlgjHDN8QDXbupr0OiohO.P9aIB4BjOOfhibTRr42qRu', NULL, '2020-08-21 23:58:14', '2020-08-21 23:58:14'),
(84, 'Michael Edogamhe', 'michaeledogamhe2020@gmail.com', 3, 0, NULL, '$2y$10$Wo9MuO8ANEAU.Kqgqza6fudlzJH///B7XuQGJUWwBfFMFWsw0S57.', NULL, '2020-08-22 00:43:39', '2020-08-22 00:43:39'),
(85, 'sa si', 'supervisor@gmail.com', 4, 1, NULL, '$2y$10$5dY4owtBw/pa7KLszGZC5ebhr98K93vVXPhWrA0P0zo/5kTKs5UDe', NULL, '2020-08-22 12:31:18', '2020-08-23 01:59:51'),
(86, 'Damilola Ademoye', 'ademoyedamilola@yahoo.com', 3, 0, NULL, '$2y$10$uRYYVzi6y/CaHm43kYvc..pFSEC2xTa99aoG1wAA2LyHnW00docmC', NULL, '2020-08-22 13:43:24', '2020-08-22 13:43:24'),
(87, 'Galadima Elijah', 'chetazomidan565@gmail.com', 3, 0, NULL, '$2y$10$a0Gva4K4W90ASc5AAmHGOebb6cxehEnN1QNB8iVf9CW3Jt0i1J8cy', NULL, '2020-08-23 11:08:09', '2020-08-23 11:08:09'),
(88, 'Godwin Edet', 'donkerrywisedydx@gmail.com', 3, 0, NULL, '$2y$10$e6yjELY1ANUOodc2Hlh44e2AZKe8r4iQX64Edjvn8pwNMvN4GKY4y', NULL, '2020-08-23 13:13:05', '2020-08-23 13:13:05'),
(89, 'Peter Ukachukwu', 'peteruka49@gmail.com', 3, 0, NULL, '$2y$10$MOaAJBGPSxG7FdR6/LAxr.lIEETB3rt20AaizUXibqfxJa8L9BEbG', NULL, '2020-08-25 03:03:08', '2020-08-25 03:03:08'),
(90, 'Ibidapo Cyril', 'dapocyril@yahoo.com', 3, 0, NULL, '$2y$10$jjCG.ie5YsMzS6Je1BmPx.Mg4dt9hZQML.nkTTVEUqIQ7jjH257uq', NULL, '2020-08-25 14:41:16', '2020-08-25 14:41:16'),
(91, 'Okeoghene Igoroko', 'fidelitycrewcomedy@gmail.com', 4, 0, NULL, '$2y$10$c5skEw4pBX80j.IlwN6yievYTnCL3iDkRVE/WGiVeaVIzie7zY5VC', NULL, '2020-09-06 09:42:42', '2020-09-06 09:42:42'),
(92, 'Okeoghene Triumphant', 'okeoghenetriumph@gmail.com', 3, 0, NULL, '$2y$10$h6TF9MFEgYyFHN8ISoalpONF4uUbrOYu6MDS4yUEjur5TI3hUQhEW', NULL, '2020-09-06 09:45:37', '2020-09-06 09:45:37'),
(93, 'Fabian Chinedu IYIDA', 'iyidafabian@gmail.com', 3, 0, NULL, '$2y$10$hIynO1SD1Y0syEGXIJEzIe6fRjgLgS2oIm9jFahgTL5xp/MwsLLFq', NULL, '2020-09-06 09:56:54', '2020-09-06 09:56:54'),
(94, 'Fabian Chinedu Iyida', 'iyidafabian@outlook.com', 4, 0, NULL, '$2y$10$0KaIiW1BrJSP3wNOKjAkzuJ7qSc9vNtuiCENVehOwhi/dw8kQ9PdK', NULL, '2020-09-06 09:58:31', '2020-09-06 09:58:31'),
(95, 'Isaac Akwanga', 'isaacakwanga@gmail.com', 3, 0, NULL, '$2y$10$.DHnQKd3vaW73F9Kvq64qOHL1AQMqxykppnN7pPLy4rS4uiiJhF5G', NULL, '2020-09-06 10:28:45', '2020-09-06 10:28:45'),
(96, 'Babajide Temitope', 'topcornerstone020@gmail.com', 3, 0, NULL, '$2y$10$xcE.cGWKmSk9CzUXqpC0x.d63IufRe8Uu3ES3Aa8dLKktY1LcbLpm', NULL, '2020-09-06 16:42:34', '2020-09-06 16:42:34'),
(97, 'Temitope Francis', 'frantechelectrical@gmail.com', 3, 0, NULL, '$2y$10$hIZpWrxc0HHYWN5OyyBjeO/3yLeyHA/eLPF4vMG/WbpHReEhC7cv.', NULL, '2020-09-06 19:41:54', '2020-09-06 19:41:54'),
(98, 'Godwin Francis', 'kimgyfrancis01@yahoo.com', 1, 0, NULL, '$2y$10$sFTm3y/jiHnkZ/3Q6h0COO3XvLbuz4RffWg5f12C2yYdLCNUhThNq', NULL, '2020-09-06 19:56:27', '2020-09-06 19:56:27'),
(99, 'Sylvester Ofemile', 'sylvesterofemile@gmail.com', 3, 0, NULL, '$2y$10$VW3sZL4FBHFuxKFsfcrvd.4ySTgMAwh45sNU2o7oNIhYXGUwGy84m', NULL, '2020-09-07 00:09:05', '2020-09-07 00:09:05'),
(100, 'Thankgod Ejikeme', 'eejikeme4@gmail.com', 3, 0, NULL, '$2y$10$pCBgKXmpxEqv/29OfI.TGeFXhnc.KhdaFnpYQxcLyGoQVl9J3oM4S', NULL, '2020-09-07 03:01:27', '2020-09-07 03:01:27'),
(101, 'Ibibia Horsfall', 'ibibia42@gmail.com', 3, 0, NULL, '$2y$10$R34aRomfUwa/WG4gdhuOeetxDcdXsrYd6slorQLaeUsnpTmv4Ctku', NULL, '2020-09-07 10:07:21', '2020-09-07 10:07:21'),
(102, 'Jacob', 'jacobelkanah91@gmail.com', 1, 0, NULL, '$2y$10$tBnTQKRtM4FpzjCFK6csY.vkXGkTCM2uid3KlshA.jBzvTEEfZaO.', NULL, '2020-09-07 11:43:32', '2020-09-07 11:43:32'),
(103, 'Adewale Yusuf', 'yusufadewale872@gmail.com', 4, 0, NULL, '$2y$10$wzMQgYnX1UWpPM8IbXZUDeXVXq3id2tdTuWAMS2pdrJfQLE4UfgXS', NULL, '2020-09-07 15:28:12', '2020-09-07 15:28:12'),
(104, 'Boniface Ariku', 'arikuboniface@gmail.com', 3, 0, NULL, '$2y$10$Y6zaRBCAvY7.zIVu/HM4hudQxmEtTkiHggp3aEd7lIn8OzrRNVnoS', NULL, '2020-09-07 20:03:37', '2020-09-07 20:03:37'),
(105, 'Alameen Umar  farouk', 'ameenuu61@gmail.com', 3, 0, NULL, '$2y$10$aIRWGjFDO3CeW7LxCWrH4e61VL0wYc6tDmlHANxzCSdyIzIqhadF.', NULL, '2020-09-07 22:22:53', '2020-09-07 22:22:53'),
(106, 'Igue Shedrach', 'shedrachgroup@gmail.com', 3, 0, NULL, '$2y$10$0wMYg7skMk.ajoLS5s2PkuumLrfZND4UeWI0l.MUwiMEI6b.MF0YS', NULL, '2020-09-08 10:16:15', '2020-09-08 10:16:15'),
(107, 'Igue Shedrach', 'igueorhiiga@gmail.com', 4, 0, NULL, '$2y$10$k9dHa4zWiIeYhTPpi9GL/eIvxjW96N1T/3BnKRBxJ8dtMD/6By5wK', NULL, '2020-09-08 10:19:13', '2020-09-08 10:19:13'),
(108, 'Kingsley Danladi', 'kingsleydanladi46@gmail.com', 3, 0, NULL, '$2y$10$ePpue2c9HA8c7SrIHjPvUeoskj/5YjavTLta7FOiVYL3JCjWYl9om', NULL, '2020-09-08 11:26:10', '2020-09-08 11:26:10'),
(109, 'Mathew Afolayan', 'dejimathew05@gmail.com', 3, 0, NULL, '$2y$10$xiJco9m0cw2YdTr4N7JFW.VXvRBsuaeGV4AZM.6E3d2L9AxSYP/Tm', NULL, '2020-09-08 13:05:23', '2020-09-08 13:05:23'),
(110, 'Aminu Umar', 'alameen99.au@gmail.com', 3, 0, NULL, '$2y$10$aaxg8LhWY5NVh69DfhldjuLv37b9kXMDnUUcJKAaTPLPNE0UicCrS', NULL, '2020-09-08 13:59:59', '2020-09-08 13:59:59'),
(111, 'Olupitan Babatunde', 'tundetiler2011@gmail.com', 3, 0, NULL, '$2y$10$.FG2XzFICCMnddFikL3qOOB8vUWNeLq0zDh9nEzFRessWKgb0O9Qi', NULL, '2020-09-08 20:49:25', '2020-09-08 20:49:25'),
(112, 'Ebuka emma', 'Ebuka@gmail.com', 3, 0, NULL, '$2y$10$rEL8zEQxIFU6hDCheJIAtOk47eKduAJNRTx5YXJkhi7aFmu43G7P2', NULL, '2020-09-16 23:16:05', '2020-09-16 23:16:05'),
(113, 'Orotoma  Frank obogheneruru', 'orotoma@gmail.com', 3, 0, NULL, '$2y$10$vOrA8e6mHP/lyb1bhUTEYeYVqL/1eiNI10xdrtdNbfo4MG9YIYoRu', NULL, '2020-09-18 09:18:41', '2020-09-18 09:18:41'),
(114, 'David Ocheje', 'dd4gold2019@gmail.com', 3, 0, NULL, '$2y$10$tte1UN8hYORbGbgOyRC1hu21K2U9HVO/MjV2FZYqxvGELxerfE4EW', NULL, '2020-09-18 09:30:07', '2020-09-18 09:30:07'),
(115, 'Oloyede Ismail', 'oloyedeismail6@gmail.com', 3, 0, NULL, '$2y$10$aWojfGQmuwp2LzlPPwjEZu6n2hyVTN12SxE0G/JFwSy78ER7NotIG', NULL, '2020-09-18 09:30:20', '2020-09-18 09:30:20'),
(116, 'Decency  Nze', 'nzedecencychukwunyere@yahoo.com', 3, 0, NULL, '$2y$10$L7Z.mesNwX9Wsf2tf2qVPeRH42URCjlHJN.f9FRHmY5QQtRUSMri.', NULL, '2020-09-18 10:19:39', '2020-09-18 10:19:39'),
(117, 'ThankGod Ben', 'denubaribari@gmail.com', 3, 0, NULL, '$2y$10$NpyOxpyhYdgt/Z4PKWiEh.U1g/Z2aX.4Xs9W/prAwGCZcAAdy9BJK', NULL, '2020-09-18 10:19:48', '2020-09-18 10:19:48'),
(118, 'Okonkwo Kennedy  Obinna', 'donkenny943@gmail.com', 3, 0, NULL, '$2y$10$hFV5lIVYU5g84tOwpOAEy.M17l9c0gTTADa9lKgVLGSdxuEVX2cMi', NULL, '2020-09-18 10:20:30', '2020-09-18 10:20:30'),
(119, 'Okonkwo Kennedy  Obinna', 'odenttemisaac69@gmail.com', 3, 0, NULL, '$2y$10$baDWpYnPVhHcBEdjq6UYkejtj7VX/CZh3yho2LQufTuvDYGYNSgqK', NULL, '2020-09-18 10:22:17', '2020-09-18 10:22:17'),
(120, 'Bonet Daniel', 'bonetdaniel345@gmail.com', 4, 0, NULL, '$2y$10$Vl3cQP4NCNZukwj.hEzTAOkub9w0Vd5Ed6zPyXmxPsEs4mhrMHlgW', NULL, '2020-09-18 11:08:45', '2020-09-18 11:08:45'),
(121, 'Bright  Uzoma', 'destandardtechnicalltd@gmail.com', 3, 0, NULL, '$2y$10$Xw4MqApM7cRVi4TR6EmQ4OlbYckZzVtzpD93mOSGQTWSF8WADvk/O', NULL, '2020-09-18 12:39:16', '2020-09-18 12:39:16'),
(122, 'Femi Omojola', 'omojola.femi@yahoo.com', 3, 0, NULL, '$2y$10$7DRJNb9ZTrz/uEbndp..meJrXuxM3G/5tdQI5rtTiwdn2kCvXS3EG', NULL, '2020-09-18 13:02:51', '2020-09-18 13:02:51'),
(123, 'Ebili Jonathan chijioke', 'ebilijonathanchijioke@gmail.com', 1, 0, NULL, '$2y$10$OjO/4RWWpv6Wp2r.Q59.beN5GI7o6NfdhV0k.vueu0FCTvZP4G9n6', NULL, '2020-09-18 14:14:57', '2020-11-05 19:57:40'),
(124, 'Ebili Jonathan chijioke', 'ebilijonathanchijoke@gmail.com', 4, 0, NULL, '$2y$10$qc6j1GYtF0f6bFQN5xDFTOMPvvQb92iKv8QjemD5D7/T5RSwxBqBu', NULL, '2020-09-18 14:16:37', '2020-09-18 14:16:37'),
(125, 'Ebili Jonathan chijioke', 'ebilijonathan@gmail.com', 4, 0, NULL, '$2y$10$4DLBOVHOR5hjKdqtDpTtv..zYxgldYPZpQdh3Gd8hkdZ4TJE8ufiG', NULL, '2020-09-18 14:16:51', '2020-09-18 14:16:51'),
(126, 'Jerome Jerome', 'doublejworkz@gmail.com', 3, 0, NULL, '$2y$10$YLPYSGikRKWD.c.HB6tXsuejloNk7oRo/yTluNbtVypE2lA5Msafe', NULL, '2020-09-18 14:24:39', '2020-09-18 14:24:39'),
(127, 'Abdulsalam Usman', 'peacelandempirecrete@gmail.com', 3, 0, NULL, '$2y$10$YcRQsS.SeSwnSJS0pVj7u.xrxYyhz5VoWGA0N5yZwBL2v/BYzisaC', NULL, '2020-09-18 18:24:28', '2020-09-18 18:24:28'),
(128, 'Daniel  Ohiri', 'ohiri.daniel@yahoo.com', 4, 0, NULL, '$2y$10$sarIHqCRKqGTbSM5CTCX4u1OsHC4SBqoCNbQRaG4SiJ2iFBC13Rqq', NULL, '2020-09-18 18:25:29', '2020-09-18 18:25:29'),
(129, 'Isaac Okpokiri', 'waxco4all@gmail.com', 4, 0, NULL, '$2y$10$SHzBVXC/9pzbKJcWCLALXuThFnA5JxG0L8cBong6jH4HxjQRhdTZO', NULL, '2020-09-18 20:01:52', '2020-09-18 20:01:52'),
(130, 'Isaac Okpokiri', 'info.mcbrick@gmail.com', 4, 0, NULL, '$2y$10$oGD4uui75htEJOLb6HnXpOeVd.DjBFySgdOiBNOwT.cpPKyk4QvRK', NULL, '2020-09-18 20:02:28', '2020-09-18 20:02:28'),
(131, 'Miracle  Chinaza', 'fluxdecors@gmail.com', 3, 0, NULL, '$2y$10$QblIMMISUKcxxrv5IjB4Sua.rmlVrIUYMnPqhBat9OHhUfjoeo.F.', NULL, '2020-09-18 20:29:16', '2020-09-18 20:29:16'),
(132, 'Justus  Aluya', 'kess21@yahoo.com', 3, 0, NULL, '$2y$10$TgjfFi4Ngrvi4DS0vOc.aOT9JS9wztx2MaRwUi65NCT1P/mjxpw42', NULL, '2020-09-18 22:48:16', '2020-09-18 22:48:16'),
(133, 'Moses Agbodukuru', 'moscodonsace@gmail.comm', 1, 0, NULL, '$2y$10$zwI8MY6zDBaJvldpmdC0r.JQhvWOAn1GxGiat6d4KpWsUS/eMxkvm', NULL, '2020-09-18 22:49:18', '2020-11-05 19:18:22'),
(134, 'Usulor Friday Gabriel', 'usulorg@gmail.com', 1, 0, NULL, '$2y$10$1v4pyW9MI2SFIc14ntFbFuBKlgCU/oc/bsK7Rj7M4Uet6BDcVA03C', NULL, '2020-09-18 23:57:04', '2020-11-05 19:18:41'),
(135, 'Richard Dominic', 'Imoheffiong301@gmail.com', 1, 0, NULL, '$2y$10$IMhFVNbMEELHRCvO9E5HO.gUBvtfboTHcqtx/dwGGeMAINntAmgfO', NULL, '2020-09-19 09:22:33', '2020-11-05 19:19:03'),
(136, 'eya', 'you@gmail.com', 1, 0, NULL, '$2y$10$Tb8rYfQ0PeYXnAgSHILLguVaT8bLqqmcrVQKwZpe6NldOOT.1Qeb6', NULL, '2020-09-19 09:35:56', '2020-11-05 19:22:08'),
(137, 'Godswill  Mark', 'etenduk.gmark@yahoo.com', 1, 0, NULL, '$2y$10$c7mxKOoR7/GElAKCOv4/GeufS2/DGRH3mCUvazGbGLDr985Ut/c3y', NULL, '2020-09-19 12:43:16', '2020-11-05 19:58:05'),
(138, 'Khalid  Sani', 'khaleed.scapo@gmail.com', 3, 0, NULL, '$2y$10$B6BBQFgjrh7O0jSSYv54IeAlj9dM5DLQHUcHoRsBW.UiDPloe0kuS', NULL, '2020-09-19 16:22:17', '2020-09-19 16:22:17'),
(139, 'Yusuf Bhadmus', 'saidkaubhadlinks@outlook.com', 3, 0, NULL, '$2y$10$Uu8VBfpMUdwc0oaHXXG3FucZ2PWPCW7D4aKrrgfndaxZoOCVvLt7q', NULL, '2020-09-19 18:22:19', '2020-09-19 18:22:19'),
(140, 'Gift Adolphus', 'giftnseyo@gmail.com', 1, 0, NULL, '$2y$10$eC4psVQ..r7WAkv7TLCK6O0u4gbWzIeyJuqOtd3Z/kKG.tlkCGW5y', NULL, '2020-09-20 01:16:29', '2020-11-05 19:22:43'),
(141, 'Gift Adolphus', 'bar.gift@hotmail.com', 4, 0, NULL, '$2y$10$.HVxxlCi6GyCZVv/h3RigOCAgad.vcBMMubFQhqzGkt8SetCssAoS', NULL, '2020-09-20 01:17:38', '2020-09-20 01:17:38'),
(142, 'Akosile adewumi', 'akoshpeace19@gmail.com', 3, 0, NULL, '$2y$10$vWQ.XVIxco7Wq3M826cD/O47USyJ7e81V1QQEYXZycoINhLYTE9fe', NULL, '2020-09-20 11:27:09', '2020-09-20 11:27:09'),
(143, 'Sedi Shafihi', 'mcgrin1@gmail.com', 1, 0, NULL, '$2y$10$4z5EiDweLFJX6xy6W.29NudUnlKA26qUjLe4Ajb0xgEVZvDV8zASC', NULL, '2020-09-20 11:31:08', '2020-11-05 18:58:49'),
(144, 'Ekemini Etim', 'kreampieswt@gmail.com', 1, 0, NULL, '$2y$10$wlK6bLZeTlO6uHspuQUtLOUUjLPiS5ReG/qBciDS..du5.MBxK1S6', NULL, '2020-09-20 11:41:47', '2020-11-05 19:15:11'),
(145, 'El-kairos Etim', 'kairosetim@gmail.com', 4, 0, NULL, '$2y$10$e0ILn0eriPNSIFU6OYc6SuSXwgBTqNLiDoflfyCVAUKl0uhGnLVp2', NULL, '2020-09-20 11:43:38', '2020-09-20 11:43:38'),
(146, 'Majid Ibrahim', 'abdulmajid753@gmail.com', 1, 0, NULL, '$2y$10$B88CZW/dz0un5wtlxqJ/cu3Wppm0XvkA7mBxLceFXEekfRTJf1VvS', NULL, '2020-09-20 12:18:25', '2020-11-05 19:15:41'),
(147, 'Kins  Achas', 'stkingsachara008@gmail.com', 1, 0, NULL, '$2y$10$g2xjiBJNVKcJ/HRPjmU2zOliefUoaDInw2.PvICcglFtYH4yT3JYy', NULL, '2020-09-20 13:13:52', '2020-11-05 19:16:05'),
(148, 'Godfrey  Ezenwa', 'ezenwagodfrey@gmail.com', 4, 1, NULL, '$2y$10$EvDbD19IVenziWxFiz7qYejB/ciOVYud7JuVeT0stkr1B4okcw8WS', NULL, '2020-09-20 14:27:44', '2020-11-06 20:30:07'),
(149, 'Williams Imoh', 'imohwill@gmail.com', 1, 0, NULL, '$2y$10$gZFn.oLUnot5dJSiXJ8nT.DVNy3.8WfScYzRkc.dN8rtHG6fF7kk.', NULL, '2020-09-20 16:55:46', '2020-11-05 19:16:33'),
(150, 'Samuel Solomon', 'caboysky4all@yahoo.com', 1, 0, NULL, '$2y$10$soRAqcuBL43EVwBw7y53T.Bkkf2FW7YNUIroGgaYsKtFhMnQR.lZy', NULL, '2020-09-20 17:27:53', '2020-11-05 19:17:48'),
(151, 'Adejoh Christiana', 'adejohchristianauyo@gmail.com', 1, 0, NULL, '$2y$10$iCXqFZMG6CgHG1k.kzqGAeojvdhX..ZZ7H1tiDRUgzhP73fTz9liu', NULL, '2020-10-29 13:26:33', '2020-11-05 19:17:34'),
(152, 'Adejoh Christiana uyo', 'adejochristiana@yahoo.com', 1, 0, NULL, '$2y$10$tch09/FcwQY4npgiptOLLeHWR5x/fqwI6C1c5HVrO79xXMdf8wl0W', NULL, '2020-10-29 13:29:12', '2020-11-05 19:17:17'),
(153, 'samuii', 'samuelekeb71@gmail.com', 1, 0, NULL, '$2y$10$LhqBvmO5NlQ4It2HqIc7/eRC9iaIA5nmERf6z7iJtRytEcCF6Z8i.', NULL, '2020-10-29 13:55:58', '2020-11-05 18:46:30'),
(154, 'empire me', 'empire@gmail.com', 1, 0, NULL, '$2y$10$owq6DpQE1lSOMEwi1IJQBOLq0BUjRjXRAkGNYFGwv0eP7FrKYxEL6', NULL, '2020-10-29 14:27:08', '2020-11-05 18:49:57'),
(155, 'ma due', 'due@gmail.com', 1, 0, NULL, '$2y$10$60Cub9IOLg48vCza7TDLT.ttPFrpYqhOxOr/Cl/Q8KBwIgk1/wtmq', NULL, '2020-10-29 14:37:08', '2020-11-05 18:50:38'),
(156, 'klein c', 'klein@gmail.com', 1, 0, NULL, '$2y$10$T4Mmfm349Zs4enm9XZ6Yj.oPf9OVbfmNWetfXJsBoJVVLKxSfr8gW', NULL, '2020-10-29 14:39:28', '2020-11-05 18:49:40'),
(157, 'sgawtqwe', 'sasas@gmail.com', 1, 0, NULL, '$2y$10$wA4hpu2X0ELhL5Fc9V9rTutCS7PC.6OX3oG/rzd28hrkqBUbOzede', NULL, '2020-10-29 15:26:09', '2020-11-05 18:50:12'),
(158, 'sa saas', 'samuelekee71@gmail.com', 1, 0, NULL, '$2y$10$PfKXTP0L8v.Qsvr9mzgpfOp0.xz/plqbM9FRJFc1S7P/XuVU27XX6', NULL, '2020-10-29 15:28:13', '2020-11-05 18:50:50'),
(159, 'sa saas', 'samuelek44ee71@gmail.com', 1, 0, NULL, '$2y$10$5sKipESeIMtVm9LdmtZgNOTdEYWKCHDxHAn.q8/ffWMwWQAWKItDu', NULL, '2020-10-29 15:32:32', '2020-11-05 18:58:31'),
(160, 'sa saas', 'samuelek4433e1@gmail.com', 1, 0, NULL, '$2y$10$gtefN/z9OVBYctPTaJbp9uYSCaLwxIYDTc.TeGAPHiMuYGp7d3bWi', NULL, '2020-10-29 15:36:27', '2020-11-05 18:51:01'),
(161, 'hey hot', 'hot@gmail.com', 1, 0, NULL, '$2y$10$ks5GvliI1T/AgBHQFCWS1./dYpC7ZlvkiANeTFdDnQZiibIR1Jb06', NULL, '2020-10-29 15:47:22', '2020-11-05 18:58:03'),
(162, 'hey hot', 'hey@gmail.com', 1, 0, NULL, '$2y$10$FsuGkiZvWIFtt3SVrpZ00.tAx4UgqKOfgo5iSGGBvSWbSqqDAOrs.', NULL, '2020-10-29 15:54:30', '2020-11-05 18:46:10'),
(163, 'juda', 'juda@gmail.com', 1, 0, NULL, '$2y$10$vTojU/oCgo.8LdO4DbNT5u78Sg4rK0NHHU969HSl9Z.fLVIpHRVuW', NULL, '2020-10-29 15:56:49', '2020-10-29 15:56:49'),
(164, 'wada', 'wada@gmail.com', 1, 0, NULL, '$2y$10$xU3ggrqK8SCoA3w8M73gYuFypTeqc.o86g2yKfOXd8DmT2UOTGPBy', NULL, '2020-10-29 15:59:54', '2020-10-29 15:59:54'),
(165, 'lada', 'lada@gmail.com', 1, 0, NULL, '$2y$10$epXhFI599Rh0vY2fO19fGe7tn3eYVi/aVoBqXUoxUWzHVavmYo1Vu', NULL, '2020-10-29 16:01:24', '2020-10-29 16:01:24'),
(166, 'Adams Abubakar', 'abubakaradams73@gmail.com', 4, 1, NULL, '$2y$10$tUAw9sPRtLl5dp2h3KONO.mNw73E5swlxWcukYnaqZAB7MVD/GCWq', NULL, '2020-10-30 19:42:16', '2020-11-06 20:29:38'),
(167, 'Adams Abubakar', 'abubadams73@gmail.com', 3, 1, NULL, '$2y$10$puBP2GzG2VDU5U9tiKALx.xFuLZxOhkLoOcTL2V0XbCzp88.1DVf.', NULL, '2020-10-31 11:03:04', '2020-11-06 20:29:30'),
(168, 'Akaninyene James', 'akanjames2016@gmail.com', 1, 0, NULL, '$2y$10$3rovRXCdYV28j6V5vrL0ZOvxwCEanQc4rc4kA4fKaVAEmawaOtEJe', NULL, '2020-11-02 11:54:20', '2020-11-05 18:37:46'),
(169, 'Akaninyene James', 'jwaynebanks@yahoo.com', 1, 0, NULL, '$2y$10$UV/yk/.Le3rZW9G8/dkGH.PPLWBRsH8r4g/kqI0H9aSqvr6Bj3a3S', NULL, '2020-11-02 11:56:55', '2020-11-05 18:38:17'),
(170, 'OGUNTADE OLADIPUPO', 'oguntade0316@gmail.com', 3, 0, NULL, '$2y$10$z/AVF/LlhuNPUSthGcRQmun/PIj74QtSmP5hvMm8BvMkRblkQyJYa', NULL, '2020-11-02 12:07:55', '2020-11-02 12:07:55'),
(171, 'Olugbogi Barnabas', 'bolugbogi@gmail.com', 3, 0, NULL, '$2y$10$QM1iIPME.8YcogekVPuzD.pxAqcHLjxny0dommsxzStjMZF7WJ1Hm', NULL, '2020-11-02 13:04:38', '2020-11-02 13:04:38'),
(172, 'Joshua  Oluwaseyi', 'oluwasheyioyeniyi@gmail.com', 3, 0, NULL, '$2y$10$491TYKBB1/sYndbC7vtWeuCop9B8DADepw9YOCTABoiYGT5VBOCYe', NULL, '2020-11-02 13:23:55', '2020-11-02 13:23:55'),
(173, 'Machief  Ayuba Mallo', 'machief88@gmail.com', 4, 0, NULL, '$2y$10$E4JjRKy4ehF98S/rFVDSLuaVHP12QTvlHTpbBySAzQ/TYMpJ7n8tG', NULL, '2020-11-02 22:13:32', '2020-11-02 22:13:32'),
(174, 'Olawale Obafemi', 'mr.femoo@gmail.com', 1, 0, NULL, '$2y$10$63r/45EvupKOCDT3etpD7O3l904z6CgpHFgvKJ0OuNHC0tI95iOHO', NULL, '2020-11-02 23:21:50', '2020-11-05 18:34:51'),
(175, 'Solomon  John', 'Solomonokoh1@gmail.com', 1, 0, NULL, '$2y$10$4mHbUQiOsGdTtSMJBbRc0uXdZZAiFv584sln5ODH297fnBLOu2hdG', NULL, '2020-11-03 09:40:19', '2020-11-05 18:35:19'),
(176, 'NNAEMEKA  UGWUOGOR', 'ugwuogornnaemeka.205@gmail.com', 1, 0, NULL, '$2y$10$7isBCYPZ9bFmgxjqU5OT0eShKZ6CNRyZCpninfqVydy2An6CvYIkG', NULL, '2020-11-03 11:43:17', '2020-11-05 18:35:37'),
(177, 'Edwin Ozoemenam', 'edwin.ozoemenam@yahoo.com', 1, 0, NULL, '$2y$10$TPUScKeuRM8LxILJ3LU.Me2Zu6h05vrQc2yRtqfNcPnDQaLl3Ex7i', NULL, '2020-11-03 15:20:50', '2020-11-05 18:36:05'),
(178, 'Adebayo Busari Sunday', 'sundayadebayo990@gmail.com', 1, 0, NULL, '$2y$10$0xBVV4Zxq1xYUYPbOcs3HufUYXExMyTcEEe/DJpE3NnDY7aJA.lcu', NULL, '2020-11-03 15:31:49', '2020-11-05 18:36:43'),
(179, 'Chima Asawalam', 'chimaasawalam@yahoo.com', 3, 0, NULL, '$2y$10$SlgIizZA770qfSbWPQj7POn46u.Z5d/wVlMfKEjHtyoYUzTU5kt6q', NULL, '2020-11-03 15:43:31', '2020-11-03 15:43:31'),
(180, 'Micklum Aruwat Nigeria Limited', 'micklum_aruwat@yahoo.com', 1, 0, NULL, '$2y$10$4BqJoe6vQhPAyrYF7owme.ZmRxqGPie2Mf94uQAED6odKQ07lnPla', NULL, '2020-11-03 17:31:17', '2020-11-03 18:04:09'),
(181, 'Edidiong  Udofa', 'edidiongudofa@gmail.com', 1, 0, NULL, '$2y$10$qEV/elo.mHp1GfiMfQZEl.dt44B2.blrod/3xlgp7CCsY6qN0KZqW', NULL, '2020-11-03 18:41:26', '2020-11-05 18:24:45'),
(182, 'Paul Osasumwen', 'paulosasmwen42@gmail.com', 1, 0, NULL, '$2y$10$JEmWGSVuk3id2ih3FucWd.498vqJlGPPUZ8OUESWJ2MsIbC2onGFu', NULL, '2020-11-03 19:28:11', '2020-11-05 18:24:18'),
(183, 'OLUBAYO OLUMIDE', 'motilade1989@gmail.com', 3, 0, NULL, '$2y$10$g5XXxfM/pqNbpmoU6T0vTeZsKC9ndHBh9qcKGB37XkJKy/7XA9vP2', NULL, '2020-11-03 20:35:15', '2020-11-03 20:35:15'),
(184, 'Cyriacus  Onwumere', 'earthquake4real@gmail.com', 3, 0, NULL, '$2y$10$pBg5GFWpOgJg8mWg.OZx9uEvpI8Dv.qjYbnqucuCine9cktouDxMC', NULL, '2020-11-03 23:46:28', '2020-11-03 23:46:28'),
(185, 'Cyriacus  Onwumere', 'marycyriacus@yahoo.com', 3, 0, NULL, '$2y$10$prwJReZ/C6hMqFHTc9HUzOI1Zjs3ptFyXDXwjfmStadHWRl4WCGB6', NULL, '2020-11-03 23:52:43', '2020-11-03 23:52:43'),
(186, 'Kolawole omoge', 'kolawole7460@gmail.com', 4, 0, NULL, '$2y$10$XIfGqvocWW54hbvgRoqBYuNJ8BaI5rx06ioRFLzq.lReSI.IrcXG6', NULL, '2020-11-04 00:20:38', '2020-11-04 00:20:38'),
(187, 'Momoh Jimoh Abdullahi', 'mjconsult@yahoo.com', 4, 0, NULL, '$2y$10$iupU8/.haH7QVKEaTojwle/nA6yxbBSvTAHBzLLEN5.hOwH8FNn2a', NULL, '2020-11-04 01:49:08', '2020-11-04 01:49:08'),
(188, 'Kolawole samson', 'kolawole7474@gmail.com', 3, 0, NULL, '$2y$10$H2ICzwybJY6DfeoXZHB9QuVDSKJGNqWHdLK1Gvb8AiOKiUHn0pQ0.', NULL, '2020-11-04 01:50:52', '2020-11-04 01:50:52'),
(189, 'Momoh Abdullahi', 'danbaba4success@yahoo.com', 1, 0, NULL, '$2y$10$FYq4/rRlwX/nuQ0x2xa8UOPeX.TiKNEbmirN7YrYJqrb1aErB/wAm', NULL, '2020-11-04 02:01:08', '2020-11-05 17:53:59'),
(190, 'Victor Okeh', 'vickyvibestech@gmail.ciom', 1, 0, NULL, '$2y$10$x77Ui9CyOygkLFgXDYbKJ..Qd4Pw0clGu0vY29eYWxvYdnE2paRmy', NULL, '2020-11-04 07:39:22', '2020-11-05 17:53:27'),
(191, 'Gad Nwigbalor', 'gideongadson@gmail.com', 4, 0, NULL, '$2y$10$8iT1z0BPb3KslI26GuMKLeCeDDeVEGFexQNMCRNBVNH9ILzVwTvK6', NULL, '2020-11-04 11:50:01', '2020-11-04 11:50:01'),
(192, 'Ezeugo  Pascal moschino', 'moschino419@gmail.com', 1, 0, NULL, '$2y$10$sBnX7H1XtWuqFCPxqMk1Ou6cqNf952fha/c7uyDz7ZRm7Gc9Rh9Hi', NULL, '2020-11-04 12:37:41', '2020-11-05 17:40:45'),
(193, 'Ukpor Ikechukwu F.', 'ukporemmanuel@gmail.com', 1, 0, NULL, '$2y$10$m5V5BBP6UeMN2MI/kIqFkesYTmYT2fx5uyvIM4fe6TRTKZNn.yvi2', NULL, '2020-11-04 13:53:36', '2020-11-05 17:41:21'),
(194, 'Damilare Owonle', 'owonledamilare@gmail.com', 1, 0, NULL, '$2y$10$FqkNethKTP2oKwXMH1Gp1.d9nTFykMkSgcbhB1ITgJTXNYMHpGr5a', NULL, '2020-11-04 18:08:19', '2020-11-05 17:34:40'),
(195, 'IYANIMIGHA RICHMAN', 'felixrichman259@gmail.com', 1, 0, NULL, '$2y$10$x3LHUjkLeLbk4971Duqr6umoEO/hGeH1JsyEXxPsYv.GSAV/0zpMu', NULL, '2020-11-04 19:38:39', '2020-11-05 17:35:18'),
(196, 'Adetunji Okeyemi', 'tunix4prince@gmail.com', 1, 0, NULL, '$2y$10$ErQrMHQ7AhyOfPStksifw.no.pf2ELlDeYEoS8w3/P/7hAAKu3XWC', NULL, '2020-11-04 22:59:51', '2020-11-05 17:37:47'),
(197, 'Emmanuel Etukudo', 'emmaetukudo81@gmail.com', 1, 0, NULL, '$2y$10$6SRKl/ZgkKeC3fNVbcLw7.79x8dUAqAyXJltofl16Z5ZjR/l1TnEq', NULL, '2020-11-04 23:36:29', '2020-11-05 17:38:00'),
(198, 'Rejoice Samuel', 'Samuelrejoice135@gmail.com', 1, 0, NULL, '$2y$10$ZBvv7lZg/ptAt2MCqIp29eWXW6g8A0Z50EnNLj8xq4vw/AVwlR18y', NULL, '2020-11-05 00:26:03', '2020-11-05 17:38:16'),
(199, 'Sesugh Songo', 'songosesugh04@gmail.com', 1, 0, NULL, '$2y$10$LpOz/0fbKkLZbY9uDX7cGeg6OPF7n5RYUdgfPCf53thRljscueLL2', NULL, '2020-11-05 12:24:12', '2020-11-05 17:38:28'),
(200, 'Juventus Offorjindu', 'justjuventus222@gmail.com', 1, 0, NULL, '$2y$10$5t3fTrrxM.eelc2VWBz8NedUpKKHyZdyc4k6t2iqOxzIPlK45cN5e', NULL, '2020-11-05 13:06:41', '2020-11-05 17:38:39'),
(201, 'Emmanuel Ebubeogu', 'ebubeoguemmanuel@gmail.com', 3, 0, NULL, '$2y$10$HyAs3FzWMJxSuoC3kf3L9Olw6ovCnk7bfaJSHCateoYA94APnSnYm', NULL, '2020-11-05 13:35:52', '2020-11-06 17:09:02'),
(202, 'Barnabas  Oladejo', 'oladejobarnabas@gmail.com', 4, 0, NULL, '$2y$10$ftjzoXc7Ovkibmk13Yum2.uVHLCFqcNhMIDeaas110TWIGHRK6xbO', NULL, '2020-11-05 14:33:13', '2020-11-05 14:33:13'),
(203, 'Eyong Williams', 'eyongwilliams69@gmail.com', 3, 0, NULL, '$2y$10$crN6LcYHBFoh7064btCT6uR/VsTegK8OuhXClCv62tp.3PsCqjKSq', NULL, '2020-11-05 20:36:32', '2020-11-05 20:36:32'),
(204, 'Clever Okesiri', 'cleverokesirimacpherson@yahoo.com', 3, 0, NULL, '$2y$10$pJwtb3M82vM/4QiPwUhl4O67i6csqlwmo3iOv6pwBQ6JdCFI7L55e', NULL, '2020-11-05 21:38:09', '2020-11-05 21:38:09'),
(205, 'Omoniyi Taiwo', 'omoniyitaiwo447@gmail.com', 4, 0, NULL, '$2y$10$LsUZCevE8NmHsRLwLx.A0.JyaDGPQhEHLWR8RMCC7G4DLhq6.SmJC', NULL, '2020-11-05 22:21:20', '2020-11-05 22:21:20'),
(206, 'Olugboyega Enoch', 'olugboyegaenoch@gmail.com', 3, 0, NULL, '$2y$10$JZCL60AzKgtbybjAz6w2IuOvOLjuQHb3QsCNN8dDbIC1GAa7MLtsO', NULL, '2020-11-05 23:39:03', '2020-11-05 23:39:03'),
(207, 'Ishola Ademola', 'molaishola@gmail.com', 3, 0, NULL, '$2y$10$wME7cCSdfRFcbrulbKkCiOVVExDwpa8Vt8z/XsMcrMELWbuKKxjxK', NULL, '2020-11-05 23:39:11', '2020-11-05 23:39:11'),
(208, 'Eboigbe Efe davies', 'daslinkiest@yahoo.com', 3, 0, NULL, '$2y$10$HfjrhG1nlon5zWqvv9gxFurskYaGBjU6xQfXWEUM82a/KEMfgt9C.', NULL, '2020-11-06 00:44:50', '2020-11-06 00:44:50'),
(209, 'McSaben Silas', 'drsabensilas@gmail.com', 3, 0, NULL, '$2y$10$wb7qSNQMrMUnaqB7gtOUH.l6bx5aETK08eJY.U2J0l11DGHqBcjai', NULL, '2020-11-06 14:22:57', '2020-11-06 14:22:57'),
(210, 'Lukman Sodiq', 'lukmansodiq069@gmail.com', 3, 0, NULL, '$2y$10$UzXEYXrNkJsaw1PRgLE5fuGyskHj3g7TWfoSKc7Ndveyn/mMP4rcC', NULL, '2020-11-06 15:26:40', '2020-11-06 15:26:40'),
(211, 'Lukman Sodiq', 'yusuphmosun12@gmail.com', 4, 0, NULL, '$2y$10$Laowanq3jPYMRapA5IOcHeFJDsCq0.yxmx3SkazM8gEkN/yQ/U1Lq', NULL, '2020-11-06 15:28:37', '2020-11-06 15:28:37'),
(212, 'Him Him', 'him@gmail.com', 3, 0, NULL, '$2y$10$r7ytKVgou6HbVWpWxtNN1.8l1bi5BQJjcVC4kyBUmoCIS0McvyEtW', NULL, '2020-11-06 16:47:06', '2020-11-07 01:52:19'),
(213, 'she', 'she@gmail.com', 3, 0, NULL, '$2y$10$Nbc1Loy2pv9v3uty5sz7nev7o6oLHdsUhI.E7j8xuaQv5Guju5bLS', NULL, '2020-11-07 11:06:09', '2020-11-07 11:06:09'),
(214, 'Ajayi Aribire Oluwaseun', 'aojwater@gmail.com', 4, 0, NULL, '$2y$10$8UYowyaozaR/Q4c80woT..JsUBLFnELr2ZL2eAGEVYHY3hw9IHz0y', NULL, '2020-11-07 14:35:46', '2020-11-07 14:35:46'),
(215, 'Ibrahim Adedamola', 'adedamola193@gmail.com', 3, 0, NULL, '$2y$10$c1q8eKV0KPnIvHwJPN313uNDv.zZIWr2zDHCu9yhLemHgTmQXL4yi', NULL, '2020-11-07 20:07:34', '2020-11-07 20:07:34'),
(216, 'FRED OMOIGBERAI', 'omoigberai_fred@yahoo.com', 3, 0, NULL, '$2y$10$187.79X8j8fypKq8bmjkoeDAxlEFArtZNhtV.GL7/WL3xaOWzxS4K', NULL, '2020-11-08 02:54:25', '2020-11-08 02:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `user_datas`
--

CREATE TABLE `user_datas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artisans`
--
ALTER TABLE `artisans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_uses`
--
ALTER TABLE `contact_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itemcategories`
--
ALTER TABLE `itemcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_images`
--
ALTER TABLE `item_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobmerges`
--
ALTER TABLE `jobmerges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `proofs`
--
ALTER TABLE `proofs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supervisor_data`
--
ALTER TABLE `supervisor_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_datas`
--
ALTER TABLE `user_datas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `artisans`
--
ALTER TABLE `artisans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `contact_uses`
--
ALTER TABLE `contact_uses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemcategories`
--
ALTER TABLE `itemcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `item_images`
--
ALTER TABLE `item_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobmerges`
--
ALTER TABLE `jobmerges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `proofs`
--
ALTER TABLE `proofs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supervisor_data`
--
ALTER TABLE `supervisor_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `user_datas`
--
ALTER TABLE `user_datas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
