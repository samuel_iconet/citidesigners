<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sms', 'frontendcontroller@testSms');
Route::get('/', 'frontendcontroller@home');
Route::get('/recover/{email}/{token}', 'frontendcontroller@setnewpassword');
Route::get('/itemdetail/{id}', 'frontendcontroller@getItem');
Route::post('/makeorder', 'frontendcontroller@makeOrder');
Route::post('/recoverpassword/save', 'frontendcontroller@savePassword');
Route::post('/contactus', 'frontendcontroller@contactus');
Route::get('/register', 'frontendcontroller@register');
Route::get('/register/ref/{ref_code}', 'frontendcontroller@registerRef');

Route::get('/marketplace','frontendcontroller@market');
Route::get('/marketplace/sort','frontendcontroller@sortarea');
Route::get('/marketplace/category/{id}','frontendcontroller@categoryset');
Route::get('/marketplace/subcategory/{id}','frontendcontroller@subcategoryset');
Route::get('/artisans','frontendcontroller@skills');
Route::get('/requestartisan','frontendcontroller@requestartisan');
Route::get('/about', function () {
    return view('about');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/privacy', function () {
    return view('privacy');
});
Route::get('/terms', function () {
    return view('terms');
});
Route::get('/login', function () {
    return view('login');
});

Route::get('/notfound', function () {
    return view('notfound');
});


Route::get('{path}',"HomeController@index")->where( 'path', '([A-z\d\-\/_.]+)?' );
