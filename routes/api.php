<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// admin market categories controller
Route::get('/category', 'categoryContoller@getcategories');
Route::post('/category/create', 'categoryContoller@createCategory');
Route::post('/category/edit', 'categoryContoller@editCategory');
Route::get('/category/flipState/{id}', 'categoryContoller@flipState');
// admin market sub categories controller
Route::get('/subcategory', 'subCategoryController@getsubcategories');
Route::post('/subcategory/create', 'subCategoryController@createsubCategory');
Route::post('/subcategory/edit', 'subCategoryController@editsubCategory');
Route::get('/subcategory/flipState/{id}', 'subCategoryController@flipState');

Route::get('/category/delete/{category_id}', 'categoryContoller@deleteCategory');
Route::get('/subcategory/delete/{subcategory_id}', 'categoryContoller@deleteSubcategory');
// user product management;
Route::get('/admin/product', 'adminAdController@getProducts');
Route::post('/admin/product/approveAd', 'adminAdController@approveAd');
Route::post('/admin/product/rejectAd', 'adminAdController@rejectAd');

// user product management;
Route::get('/createproduct/properties', 'authProductController@getProperties');
Route::get('/products', 'authProductController@getProducts');
Route::post('/product/create', 'authProductController@createProduct');
Route::get('/product/chat/{msg}/{product_id}', 'authProductController@startChat');
Route::post('/product/chat/reply', 'authProductController@replychat');
Route::get('/product/chat', 'authProductController@getchats');


//properties
Route::get('/subcategory/properties', 'categoryPropertiesController@getProperties');
Route::post('/property/create', 'categoryPropertiesController@addProperty');
Route::post('/property/edit', 'categoryPropertiesController@editProperty');
Route::post('/option/add', 'categoryPropertiesController@addoption');
Route::get('/property/delete/{property_id}', 'categoryPropertiesController@deleteproperty');
Route::get('/option/delete/{option_id}', 'categoryPropertiesController@deleteoption');



// Authentcation route
Route::post('/testcredit', 'frontendcontroller@testcredit');

Route::post('/register', 'apiAuthController@register');
Route::post('/register/artisan', 'apiAuthController@registerArtisan');
Route::post('/register/user', 'apiAuthController@registerUser');
Route::post('/register/supervisor', 'apiAuthController@registerSupervisor');
Route::post('/recover/password', 'apiAuthController@recover');
Route::get('/gettips', 'apiAuthController@tips');
Route::get('/adminauth/error','apiAuthController@adminAuthError');
Route::post('/recoverpassword/save', 'apiAuthController@savePassword');
// personel USer Modification Route
Route::post('/updatepassword',  'usercontroller@updatepassword');

// Route::get('/user','usercontroller@getUser' );
Route::post('/user',  'usercontroller@editUser');
//payment gate way 
Route::post('/adPayment/card', 'paymentController@cardadPayment');
Route::post('/reqpayment/card', 'paymentController@cardrequestPayment');
Route::post('/reqpayment/dbt', 'paymentController@dbtrequestPayment');
Route::post('/regpayment/card', 'paymentController@cardregistrationPayment');
Route::post('/regpayment/dbt', 'paymentController@dbtregistrationPayment');
Route::get('/regpayment/reject/{proof_id}', 'paymentController@rejectRegPayment');
Route::get('/regpayment/comfirm/{proof_id}', 'paymentController@acceptRegPayment');
Route::get('/reqpayment/reject/{proof_id}', 'paymentController@rejectReqPayment');
Route::get('/reqpayment/comfirm/{proof_id}', 'paymentController@acceptReqPayment');
//supervisor data
Route::get('/supervisor/data','supervisordatacontroller@getData' );
Route::post('/supervisor/addQuote','supervisorjobcontroller@addQuote');
Route::get('/supervisor/validate', 'supervisordatacontroller@validateSupervisor');
Route::get('/supervisor/getrefferals', 'supervisordatacontroller@getrefferals');

Route::post('/supervisor/updatesupervisor', 'supervisordatacontroller@updatesupervisor');
Route::post('/supervisor/setdata', 'supervisordatacontroller@setSupervisorData');
Route::post('/supervisor/uploadprofile', 'supervisordatacontroller@uploadprofile');
Route::post('/supervisor/uploadid', 'supervisordatacontroller@uploadid');
Route::get('/supervisor/getrequests','supervisorjobcontroller@getNewRequest');
Route::get('/supervisor/accept_request/{id}' , 'supervisorjobcontroller@acceptRequest');
Route::get('/supervisor/reject_request/{id}' , 'supervisorjobcontroller@rejectRequest');
Route::get('/supervisor/getmergeRequest' , 'supervisorjobcontroller@getmergeRequest');
Route::post('/supervisor/getproofs','supervisorjobcontroller@getproofs');
Route::post('/supervisor/comfirmrequest','supervisorjobcontroller@comfirmrequest');

//Artisan data ROute
Route::get('/artisan','artisandatacontroller@getData' );
Route::post('/setdata', 'artisandatacontroller@setArtisanData');
Route::post('/uploadprofile', 'artisandatacontroller@uploadprofile');
Route::post('/artisan/updatedata', 'artisandatacontroller@updateArtisanData');
Route::post('/artisan/updateArtisan', 'artisandatacontroller@updateArtisan');

Route::get('/artisan/verify', 'artisandatacontroller@verify');
Route::get('/artisan/validate', 'artisandatacontroller@validateArtisan');

Route::get('/artisan/{id}','adminArtisanController@getartisanData' );
Route::get('/supervisor/{id}','adminArtisanController@getsupervisorData' );

// user account route
Route::get('/getAccount','accountcontroller@getAccount' );
Route::post('/withdrawal/cancel','accountcontroller@cancelwithdrawalrequest' );
Route::post('/withdrawalrequest','accountcontroller@withdrawalrequest' );
//admin account
Route::get('/company/getAccount','adminaccountcontroller@getAccount' );
Route::post('/withdrawal/confirm','adminaccountcontroller@confirmWithdrawal' );

//order

Route::post('/makeorder', 'ordercontroller@makeOrder');


// job 
Route::get('/getstates', 'jobcontroller@getstatesData');
// Route::post('/makeJobrequest', 'jobcontroller@makeJobrequest');

//artisan job handle
Route::get('/getrequests','artisanjobcontroller@getNewRequest');
//request artisan 
Route::get('/userRequestData','authjobcontroller@userRequestData');
Route::post('/makeJobrequest','authjobcontroller@makeJobrequest');
Route::post('/cancelRequest','authjobcontroller@cancelRequest');
Route::post('/confirmrequest','authjobcontroller@confirmrequest');

//
//job requests
Route::get('/getmyrequest','authjobcontroller@getallRequest');
Route::get('/getmergeRequest','artisanjobcontroller@getmergeRequest');
Route::post('/uploadproof','artisanjobcontroller@uploadproof');
Route::post('/app/uploadproof','artisanjobcontroller@appUploadproof');
Route::post('/addQuote','artisanjobcontroller@addQuote');
Route::post('/getproofs','artisanjobcontroller@getproofs');
Route::get('/accept_request/{id}' , 'artisanjobcontroller@acceptRequest');
Route::get('/reject_request/{id}' , 'artisanjobcontroller@rejectRequest');

Route::get('/admin/artisans','adminArtisanController@getArtisans');

//loginin attempt
Route::get('/attemptlogin', 'usercontroller@attemptlogin');
Route::get('/logout', 'usercontroller@logout');
Route::get('/validateSession', 'usercontroller@validateSession');
// review
Route::post('/review', 'frontendcontroller@review');

// auth support ticket 
Route::get('/support/ticket', 'authTicketController@getTickets');
Route::post('/support/ticket/create', 'authTicketController@createTickets');
Route::post('/support/ticket/reply', 'authTicketController@replyTickets');
Route::get('/support/ticket/markread/{ticket_id}', 'authTicketController@markRead');
// admin support ticket 
Route::post('/admin/ticket/reply', 'adminTicketController@replyTickets');
Route::get('/admin/ticket/close/{ticket_id}', 'adminTicketController@closeTicket');
Route::get('/admin/ticket', 'adminTicketController@getTickets');

//admin user
Route::get('/admin/home', 'adminhomecontroller@gethome');
Route::post('/admin/saveSetting', 'adminhomecontroller@saveSetting');
Route::get('/admin/getuserContact/{role}', 'adminhomecontroller@getuserContact');
Route::get('/admin/getuserEmail/{role}', 'adminhomecontroller@getuserEmail');
Route::get('/admin/getcontactus', 'adminhomecontroller@getcontactus');
Route::get('/admin/user', 'adminusercontroller@getusers');
Route::get('/admin/supervisors', 'adminusercontroller@getsupervisors');
Route::get('/admin/artisans', 'adminusercontroller@getartisans');
Route::post('/admin/flipuser', 'adminusercontroller@flipuser');
Route::post('/admin/saveuser', 'adminusercontroller@saveuser');
Route::post('/admin/rejectUser', 'adminusercontroller@rejectUser');
Route::post('/acceptuser', 'adminusercontroller@approveUser');
Route::post('/admin/deleteUser', 'adminusercontroller@deleteUser');

//admin jobs mergejob
Route::get('/admin/getjobs', 'adminjobcontroller@getJobs');
Route::get('/admin/automerge', 'adminjobcontroller@automergeAll');
Route::post('/admin/mergejobrepairs', 'adminjobcontroller@mergeJobRepairs');
Route::post('/admin/mergejoball', 'adminjobcontroller@mergejobComplete');
Route::post('/admin/remergejob', 'adminjobcontroller@remergejob');
Route::post('/admin/cancelmerge', 'adminjobcontroller@cancelMerge'); 

//admin product management 
Route::get('/admin/getitems', 'adminitemcontroller@getitems'); 
Route::post('/admin/addproduct', 'adminitemcontroller@addproduct'); 
Route::post('/admin/deleteproduct', 'adminitemcontroller@deleteproduct'); 
Route::post('/admin/flipitem', 'adminitemcontroller@flipitem'); 
Route::post('/admin/addimage', 'adminitemcontroller@addimage'); 
Route::post('/admin/deleteimage', 'adminitemcontroller@deleteimage'); 
Route::post('/admin/setProfile', 'adminitemcontroller@setProfile'); 
Route::get('/admin/storerequest', 'adminitemcontroller@storerequest'); 

//admin artisan skills management 
Route::get('/admin/skills', 'adminArtisanController@getskills'); 
Route::post('/admin/addstate', 'adminArtisanController@addstate'); 
Route::post('/admin/addarea', 'adminArtisanController@addarea'); 
Route::post('/admin/addskill', 'adminArtisanController@addskill'); 
Route::post('/admin/fliparea', 'adminArtisanController@fliparea'); 
Route::post('/admin/flipstate', 'adminArtisanController@flipstate'); 
Route::post('/admin/flipskill', 'adminArtisanController@flipskill'); 
Route::post('/admin/deletearea', 'adminArtisanController@deletearea'); 
Route::post('/admin/deletestate', 'adminArtisanController@deletestate'); 
Route::post('/admin/deleteskill', 'adminArtisanController@deleteskill'); 

//admin tips management
Route::post('/admin/addtips', 'adminusercontroller@addtip'); 
Route::get('/admin/gettips', 'adminusercontroller@gettips'); 
Route::post('/admin/fliptip', 'adminusercontroller@fliptip'); 
Route::post('/admin/deletetip', 'adminusercontroller@deletetip'); 


Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);
 
    if (!File::exists($path)) {
        abort(404);
    }
 
    $file = File::get($path);
    $type = File::mimeType($path);
 
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
 
    return $response;
});

