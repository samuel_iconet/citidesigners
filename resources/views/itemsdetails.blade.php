@extends('layouts.front')

@section('content')
<div class="container" >



        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
            <div class="carousel-inner">
            
            
            <div class="carousel-item active">
              <img class="d-block w-100" src="/storage/store/{{$item->profile_image}}" alt="Second slide">
              </div>
              @if(isset($images))
                @foreach ($images as $image)
                <div class="carousel-item">
                  <img class="d-block w-100" src="/storage/store/{{$image->name}}" alt="Second slide">
                  </div>
                @endforeach
                  @endif
           
                                  
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <hr class="m-0" />
          <section class="bg-light pt-10" >
              <div class="container">
                 
                  <div class="row z-1">
                      <div class="col-lg-7" data-aos="fade-up" data-aos-delay="100">
                          <div class="card pricing h-100">
                              <div class="card-body p-5">
                                  <div class="text-center">
                                      <div class="badge badge-light badge-pill badge-marketing badge-sm">{{$item->name}}</div>
                                  <div class="pricing-price"><h3 class="amount">{{$item->amount}}</h3></div>
                                  </div>
                                  <hr>
                                <p>{{$item->description}}</p>
                                 
                              </div>
                          </div>
                      </div>
                   
                      <div class="col-lg-5 mb-lg-n10" data-aos="fade-up" data-aos-delay="100">
                          <div class="card pricing h-100">
                              <div class="card-body p-5">
                                  <div class="text-center">
                                      <div class="badge badge-light badge-pill badge-marketing badge-sm">For Purchase and Enquiry</div>
                                      <p class="card-text py-1">Contact Seller!!!</p>
                                  </div>
                                  <hr class="m-0" />
                                  <div id="sellerDetail" style="display:none">
                                  <h5>Seller Name: {{$item->seller_name}}</h5>
                                  <h5>Seller Phone Number: {{$item->seller_contact}}</h5>
                                  </div>
                                  <button class="btn btn-primary btn-marketing btn-block rounded-pill mt-4" type="submit" id="getdetails">Get Sellers Contact</button>
                                 
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
   
          </section>
            </div>


    @endsection


    

    @section('script')
<script type="text/javascript">

     $( document ).ready(function() {
      event.preventDefault();

        $('#getdetails').click(function (event) {
            $('#sellerDetail').show()
        });
        $( "#target" ).submit(function( event ) {
       
        event.preventDefault();
        $('#loader').show();
        $('#submit').attr('disabled','disabled');
        let email = $('#email').val();
        let username =  $('#fname').val() + " " + $('#lname').val();
        let id = $('#id').val();
        let message = $('#resptext').val();

        console.log(email);
        console.log(username);
        console.log(id);
        console.log(message);

        $.ajaxSetup({
                headers: { }
            });
$.post('/api/makeorder',   // url
       {        name: username, 
                email: email,
                item_id: id,
                message: message
                
       }, 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            $('#loader').hide();
           $('#submit').removeAttr('disabled');
           $('#message').show(); 
           $('#message').text("Order Message Have Been Recieved, you would be contacted as soon as possible." );
        }else{
            $('#error').show(); 
           $('#error').text(data.email[0]);
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
        }


        }).fail(function(jqxhr, settings, ex) {
           $('#err').show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
           alert("An Error Occured on the Server.")
         });




        });

     });
   </script>
@endsection


