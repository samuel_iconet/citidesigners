<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.lionode.com/focus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Nov 2020 11:00:46 GMT -->
<head>
<title>Citi Designers</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="e-commerce site well design with responsive view." />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="/front/images/looogo.ico">

<link href="/market/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="/market/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href="/market/css/stylesheet.css" rel="stylesheet">
<link href="/market/css/responsive.css" rel="stylesheet">
<link href="/market/javascript/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
<link href="/market/javascript/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />
<script src="/market/javascript/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="/market/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/market/javascript/template_js/jstree.min.js"></script>
<script type="text/javascript" src="/market/javascript/template_js/template.js"></script>
<script src="/market/javascript/common.js" type="text/javascript"></script>
<script src="/market/javascript/global.js" type="text/javascript"></script>
<script src="/market/javascript/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
</head>
<body>
<div class="preloader loader" style="display: block;"> <img src="/market/image/loader.gif"  alt="#"/></div>

<nav id="menu" class="navbar">
    <div class="nav-inner container">
        <div class="navbar-header"><span id="category" class="visible-xs">Categories</span>
            <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
        </div>
        <div class="navbar-collapse">
            <ul class="main-navigation">
                <li><a href="/"   class="parent"  >Home</a> </li>
                @foreach($categories as $category)
                <li><a href="/marketplace/category/{{$category->id}}" class="active parent">{{$category->name}} ({{$category->count}})</a>
                    <ul>
                        @foreach($category->subcategories as $subcategory)
                        <li><a href="/marketplace/subcategory/{{$subcategory->id}}">{{$subcategory->name}} ({{$subcategory->count}})</a></li>
                      
                        @endforeach
                    </ul>
                </li>
                @endforeach
                <a href="/register"   class="btn btn-success"  >SELL FOR FREE</a>
               
            </ul>
           
        </div>
    </div>
</nav>
<div class="container col-2">
    <div class="row">
        <div class="col-md-12">
            <div id="main-banner" class="owl-carousel home-slider">
                <div class="item"> <a href="#"><img  src="/front/images/barner1.jpg" alt="main-banner1" class="img-responsive" /></a> </div>
                <div class="item"> <a href="#"><img src="/front/images/barner2.jpg" alt="main-banner2" class="img-responsive" /></a> </div>
                <div class="item"> <a href="#"><img src="/front/images/barner5.jpg" alt="main-banner3" class="img-responsive" /></a> </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="cms_banner">
            <div class="col-md-4 cms-banner-left"> <a href="#"><img alt="#" src="/market/image/banners/subbanner1.jpg"></a> </div>
            <div class="col-md-4 cms-banner-middle"> <a href="#"><img alt="#" src="/market/image/banners/subbanner2.jpg"></a> </div>
            <div class="col-md-4 cms-banner-right"> <a href="#"><img alt="#" src="/market/image/banners/subbanner3.jpg"></a> </div>
        </div>
    </div>
    <div class="row">
        <div id="column-left" class="col-sm-3 hidden-xs column-left">
            <div class="column-block">
                <div class="columnblock-title">Categories</div>
                <div class="category_block">
                    <ul class="box-category treeview-list treeview">

                       @foreach($categories as $category) <li >
                       <a href="/marketplace/category/{{$category->id}}" class="activSub">{{$category->name}} ({{$category->count}})</a>
                            <ul>
                            @foreach($category->subcategories as $subcategory)
                                <li><a href="/marketplace/subcategory/{{$subcategory->id}}">{{$subcategory->name}} ({{$subcategory->count}})</a></li>
                                @endforeach
                            </ul>
                        </li>
                       @endforeach
                    </ul>
                </div>
            </div>
        
            <h3 class="productblock-title">Latest</h3>
            <div class="row latest-grid product-grid">
                @foreach($latest3 as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"><a href="/itemdetail/{{$product->id}}"><img  style="height: 100px; width: 60px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" /></a>
                            <!-- <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                            </div> -->
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                            <p class="price product-price" style="color:green">₦ {{number_format($product->price, 0, '.', ',')}}</p>
                            
                            <br><p class="price product-price" >{{$product->statedata->name}}  >> {{$product['areadata']->name}}</p>
                        </div>
                        <!-- <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                        </div> -->
                    </div>
                </div>
              @endforeach
            </div>
            <h3 class="productblock-title">Specials</h3>
            <div class="row special-grid product-grid">
                @foreach($special3 as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"><a href="/itemdetail/{{$product->id}}"><img style="height: 100px; width: 60px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" /></a>
                            <!-- <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                            </div> -->
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                            <p class="price product-price" style="color:green">₦ {{number_format($product->price, 0, '.', ',')}}</p>
                            
                            <br><p class="price product-price" >{{$product->statedata->name}}  >> {{$product['areadata']->name}}</p>
                        </div>
                        <!-- <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                        </div> -->
                    </div>
                </div>
              @endforeach
         
            </div>
        </div>
        <div id="content" class="col-sm-9">
            <div class="customtab">
                <div id="tabs" class="customtab-wrapper">
                    <ul class='customtab-inner'>
                        <li class='tab'><a href="#tab-latest">Latest</a></li>
                        <li class='tab'><a href="#tab-special">Special</a></li>
                        <li class='tab'><a href="#tab-bestseller">Bestseller</a></li>
                    </ul>
                </div>
                <div id="tab-latest" class="tab-content">
                    <div class="box">
                        <div id="latest-slidertab" class="row owl-carousel product-slider">
                            @foreach($latestProducts as $product)
                            <div class="item">
                                <div class="product-thumb transition">
                                    <div class="image product-imageblock"> <a href="/itemdetail/{{$product->id}}"><img style="height: 294px; width: 220px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}"  class="img-responsive" /> </a>
                                        <!-- <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                        </div> -->
                                    </div>
                                    <div class="caption product-detail">
                                        <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                                        <p class="price product-price" style="color:green" >₦ {{number_format($product->price, 0, '.', ',')}}</p>
                                        
                                        <br><p class="price product-price" >{{$product->statedata->name}}  >> {{$product['areadata']->name}}</p>
                                    </div>
                                    <!-- <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" >Add to Cart</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div> -->
                                </div>
                            </div>
                         
                           @endforeach
                            
                        </div>
                    </div>
                </div>
                <!-- tab-latest-->
                <div id="tab-special" class="tab-content">
                    <div class="box">
                        <div id="special-slidertab" class="row owl-carousel product-slider">
                            @foreach($specialProducts as $product)
                            <div class="item">
                                <div class="product-thumb transition">
                                    <div class="image product-imageblock"> <a href="/itemdetail/{{$product->id}}"><img style="height: 294px; width: 220px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}"  class="img-responsive" /> </a>
                                        <!-- <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                        </div> -->
                                    </div>
                                    <div class="caption product-detail">
                                        <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                                        <p class="price product-price" style="color:green" >₦ {{number_format($product->price, 0, '.', ',')}}</p>
                                        
                                        <br><p class="price product-price" >{{$product->statedata->name}}  >> {{$product['areadata']->name}}</p>
                                    </div>
                                    <!-- <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" >Add to Cart</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div> -->
                                </div>
                            </div>
                         
                           @endforeach
                     
                        </div>
                    </div>
                </div>
                <!-- tab-special-->
                <div id="tab-bestseller" class="tab-content">
                    <div class="box">
                        <div id="bestseller-slidertab" class="row owl-carousel product-slider">
                            @foreach($premiumProducts as $product)
                            <div class="item">
                                <div class="product-thumb transition">
                                    <div class="image product-imageblock"> <a href="/itemdetail/{{$product->id}}"><img style="height: 294px; width: 220px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}"  class="img-responsive" /> </a>
                                        <!-- <div class="button-group">
                                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                        </div> -->
                                    </div>
                                    <div class="caption product-detail">
                                        <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                                        <p class="price product-price" style="color:green" >₦ {{number_format($product->price, 0, '.', ',')}}</p>
                                        
                                        <br><p class="price product-price" >{{$product['statedata']->name}}  >> {{$product['areadata']->name}}</p>
                                    </div>
                                    <!-- <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" >Add to Cart</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div> -->
                                </div>
                            </div>
                         
                           @endforeach
                           
                        </div>
                    </div>
                </div>
              
                <h3 class="productblock-title">Featured</h3>
                <div class="box">
                    <div id="feature-slider" class="row owl-carousel product-slider">
                        @foreach($premiumProducts as $product)
                        <div class="item product-slider-item">
                            <div class="product-thumb transition">
                                <div class="image product-imageblock"> <a href="/itemdetail/{{$product->id}}"><img style="height: 294px; width: 220px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}"  class="img-responsive" /> </a>
                                    <!-- <div class="button-group">
                                        <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                        <button type="button" class="addtocart-btn" >Add to Cart</button>
                                        <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                    </div> -->
                                </div>
                                <div class="caption product-detail">
                                    <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                                    <p class="price product-price" style="color:green" >₦ {{number_format($product->price, 0, '.', ',')}}</p>
                                    <br><p class="price product-price" >{{$product['statedata']->name}}  >> {{$product['areadata']->name}}</p>
                                    
                                </div>
                                <!-- <div class="button-group">
                                    <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                                    <button type="button" class="addtocart-btn" >Add to Cart</button>
                                    <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                                </div> -->
                            </div>
                        </div>
                     
                       @endforeach
                        
                    </div>
                </div>
            
            </div>
            <!-- <div id="brand_carouse" class="owl-carousel brand-logo">
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand1.png" alt="Disney" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand2.png" alt="Dell" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand3.png" alt="Harley" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand4.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand6.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand7.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand8.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand9.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="/market/image/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
            </div> -->
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="container">
        <div class="copyright">Powered By<a class="yourstore" href="http://www.lionode.com/">Citi Designers &copy; 2020 </a> </div>
        <div class="footer-bottom-cms">
            <div class="footer-payment">
                <ul>
                    <li class="mastero"><a href="#"><img alt="" src="/market/image/payment/mastero.jpg"></a></li>
                    <li class="visa"><a href="#"><img alt="" src="/market/image/payment/visa.jpg"></a></li>
                    <li class="currus"><a href="#"><img alt="" src="/market/image/payment/currus.jpg"></a></li>
                    <li class="discover"><a href="#"><img alt="" src="/market/image/payment/discover.jpg"></a></li>
                    <li class="bank"><a href="#"><img alt="" src="/market/image/payment/bank.jpg"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>

<!-- Mirrored from html.lionode.com/focus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Nov 2020 11:03:04 GMT -->
</html>
