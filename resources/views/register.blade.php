@extends('layouts.front')
@section('style')

@endsection
@section('content')
<section class="section-grey" id="team" style="margin-buttom:40px">
<div class="container">
            <div class="row align-items-center" >
                <div class="col-lg-1">
   
                </div>
                <div class="col-lg-10">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-info active " id="user" >Register to Make Request.</button>
                            <button type="button" class="btn btn-info " id="artisan">Become an Agent</button>
                            <button type="button" class="btn btn-info" id="supervisor">Become a Supervisor</button>
                            <a href="/login" type="button" class="btn btn-info" >login</a>
                            </div>
                            </div>
                        <div class="card-body">

                            <div id="userReg">
                            <h6 style="color: green">Register as a User</h6>
                            <br>

                            <form class="row" id="userForm" method="post">
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="form-control" required="" style="margin-buttom: 50px" name="username" placeholder="Your Name*" type="text">
                                <br>
                                <label for="">Phone Number(08099299292)</label>
                                    <input class="form-control" required="" name="phone" placeholder="Phone Number*" id="userphone" type="text">
                                    <br>
                                    <label for="">Password</label>
                                    <input class="form-control" required=""  name="password" placeholder="password*" type="password" minlength="6">
                                
                                </div>
                                <!--end col-md-6-->
        
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="form-control" required="" name="email" placeholder="Email Adress*" type="email">
                                    <br>
                                    <textarea class="form-control" required="" name="address" placeholder="Address*" type="text"></textarea>

                                    
                                
                                </div>
                                
                                <div class="col-md-12"><hr class="alert-warning"></div>
                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="small text-gray-600" for="leadCapEmail">State</label>
                                    <select required name="state" class="form-control rounded-pill" id="stateselect">
                                        
                                      </select>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="area" id="areaselect"  class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                </div>
                                
                                <!--end col-md-6-->
        
                                <!--begin col-md-12-->
                                <div class="col-md-4 offset-4">

                                      <br><br>          
                                    <button type="submit" class="btn btn-info" id="submit2" ><i id="loader2" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i>Register</button>
                                
                                </div>
                                <!--end col-md-12-->
                
                            </form></div>

                            <div id="artisanReg" style="display:none">
                            <h6 style="color: green">Register as an Artisan</h6>
                            <br>
                                
                            <form class="row"  id="artisanForm" enctype="multipart/form-data"  method="post">
                                    <!--begin col-md-6-->
                                    <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>User Data</h6>
                                 <hr class="alert-warning">
                                </div>
                                <div class="col-md-6">

                                    <input class="form-control" required="" style="margin-buttom: 50px" name="username" placeholder="Your Name*" type="text">
                                <br>
                                <label for="">Phone Number(08099299292)</label>
                                    <input class="form-control" required="" id="artisanphone" name="phone" placeholder="Phone Number*" type="text">
                                <br> 
                                    <label for="">Profile Picture*</label>
                                    <input type="file" name="file_profile" required class="form-control" placeholder="">
                                    <br>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Skills</label>
                                                <select required    name="skill_id" class="form-control rounded-pill" readonly="">
                                                 @foreach($skills as $skill)
                                                   <option value="{{$skill->id}}"> {{$skill->name}} </option>     
                                                @endforeach
                                                </select>
                                </div>
                                 <hr class="alert-warning">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password" minlength="6" required>
                                </div>
                                <!--end col-md-6-->
        
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="form-control" required="" name="email" placeholder="Email Adress*" type="email">
                                    <br>
                                    <textarea class="form-control" required="" name="address" placeholder="Address*" type="text"></textarea>
                                     <br>   
                                    <div class="form-group">
                                                 <label class="small text-gray-600" for="leadCapEmail">Gender</label>
                                                <select   required   name="gender" class="form-control rounded-pill">
                                                   <option value="Male">Male </option>     
                                                   <option value="Female" > Female</option>     
                                                     
                                                </select>
                                
                                </div>
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <input type="text" id="datepicker" name="date_of_birth" class="form-control" required placeholder="Choose">
                                </div>
                                <div class="form-group">
                                    <label>years of Experience*</label>
                                    <input type="number"  name="years_of_experience" class="form-control" required placeholder="Years of Experience">
                                </div>
                                </div>
                                
                                <div class="col-md-12"><hr class="alert-warning"></div>
                                
                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="small text-gray-600" for="leadCapEmail">State of Origin</label>
                                    <select required name="state_origin" class="form-control rounded-pill" id="stateselectartisanorigin">
                                        
                                      </select>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group" style="display: none"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="area_local"  id="areaselectartisanorigin"   class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                </div>       

                                <div class="col-md-12"><hr class="alert-warning"></div>
                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="small text-gray-600" for="leadCapEmail">State of Residence</label>
                                    <select required name="state" class="form-control rounded-pill" id="stateselectartisan">
                                        
                                      </select>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="area" id="areaselectartisan"  class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                </div>
                                
                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>Bank Details</h6>
                                 <hr class="alert-warning">
                                </div>

                                 <div class="col-md-6">
                                 <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Bank Name*</label>
                                 <select required name="bank_name" id="bank1"  class="form-control rounded-pill">
                                      
                                      </select>
                                            </div>
                                            
                                 </div>               
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Account Number*</label>
                                            <input required type="number" id="artisanaccount" class="form-control" name="account_number"   >
                                </div>
                                </div>

                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>Garantor Details</h6>
                                 <hr class="alert-warning">
                                </div>
                                 <div class="col-md-6">
                                 <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Name*</label>
                                    <input  required type="text" class="form-control" name="guarantor_name" >
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Profile Picture*</label>
                                    <input  required type="file" class="form-control" name="file_guarantor_profile" >
                                    </div>
                                 </div>  
                                 <div class="col-md-6">
                                 <label for="">Guarantor Phone Number(08099299292)</label>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Phone Number*</label>
                                    <input  required type="number" id="artisangphone" class="form-control" name="guarantor_number" >
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Valid ID*</label>
                                    <input  required type="file" name="file_guarantor_id" class="form-control"  >
                                    </div>
                                 </div> 
                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>User Valid ID Details</h6>
                                 <hr class="alert-warning">
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">ID Type</label>
                              <select   required name="id_type"    class="form-control rounded-pill">
                                <option   value="Drivers licence">Drivers licence</option>    
                                <option   value="Pvc">Pvc</option>     
                                <option   value="National ID"> National ID</option>     
                                <option   value="International passport">International passport</option>     
                               </select>
                               </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">ID Number</label>
                                            <input  required type="text" name="id_number" class="form-control"  >
                                   </div>    
                                    <label for="">Select the image</label>
                                    <input required  type="file"  name="file_id" />
                                    <hr class="alert-danger">
                                    <label for="">Referral Code</label>
                                    @if(isset($ref_code))
                                    <input required class="form-control" type="text" name="ref_code" value="{{$ref_code}}" />
                                    @else
                                    <input required class="form-control" type="text" name="ref_code" value="none" />
                                    @endif
                                    <hr class="alert-danger">
                              </div>
                            <div class="col-md-12">
                            <hr class="alert-danger">
                            <br>
                            <div id="bodydesc" style="margin-left:auto; margin-right:auto; width:460px; height: 200px; overflow-y: scroll;">

                                    <p id="desc" >
                                    Special Terms and conditions agreement Between Citi design and our Esteem Artisans:
                                    </p>
                                    <ol>
                                        <li>1. Desist from unlawful act that might leads to your profile termination</li>
                                        <li>2. Neath work and straightforwardness with client's. </li>
                                        <li>3. work professionally. Avoid illegal negotiation or embezzlement of client's properties, the offense is punishable and such can not be penalise but a jail term. Your Experts work is auction to meets up with client's best interests.</li>
                                    </ol>
                                    
                                                                  

                         
                                    <p class="alert-warning">
                                    NOTE:CITI design takes 23.5%of every contract awarded and Every contracts Transaction is expected to be under the company's account. Your Withdraw payment to be allowed only when the work is Completed and confirmed         
                                    </p>

                            </div>
                            </div>
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                            <label class="form-check-label" for="defaultCheck1">
                                I accept the terms and condition.
                            </label>
                            </div>       
                                <!--end col-md-6-->
        
                                <!--begin col-md-12-->
                                <div class="col-md-4 offset-4">

                                      <br><br>  
                                    <button type="submit" class="btn btn-info" id="submit1" ><i id="loader1" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i>Register</button>

                                
                                </div>
                                <!--end col-md-12-->
                
                            </form></div>

                            <div id="supervisorReg" style="display:none">
                            <h6 style="color: green">Register as a Supervisor</h6>
                            <br>

                            <form class="row" id="supervisorForm" method="post">
                                     <!--begin col-md-6-->
                                     <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>User Data</h6>
                                 <hr class="alert-warning">
                                </div>
                                <div class="col-md-6">

                                    <input class="form-control" required="" style="margin-buttom: 50px" name="username" placeholder="Your Name*" type="text">
                                <br>
                                <label for="">Phone Number(08092992923)</label>
                                    <input class="form-control" required="" id="supervisorphone" name="phone" placeholder="Phone Number*" type="text">
                                <br> 
                                    <label for="">Profile Picture*</label>
                                    <input type="file" name="file_profile" required class="form-control" placeholder="">
                                    <br>
                                  
                                 <hr class="alert-warning">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password" minlength="6" required>
                                </div>
                                 <hr class="alert-warning">
                               
                              
                                <!--end col-md-6-->
        
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="form-control" required="" name="email" placeholder="Email Adress*" type="email">
                                    <br>
                                    <textarea class="form-control" required="" name="address" placeholder="Address*" type="text"></textarea>
                                     <br>   
                                    <div class="form-group">
                                                 <label class="small text-gray-600" for="leadCapEmail">Gender</label>
                                                <select   required   name="gender" class="form-control rounded-pill">
                                                   <option value="Male">Male </option>     
                                                   <option value="Female" > Female</option>     
                                                     
                                                </select>
                                
                                </div>
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <input type="text" id="datepicker1" name="date_of_birth" class="form-control" required placeholder="Choose">
                                </div>
                                <div class="form-group">
                                    <label>years of Experience*</label>
                                    <input type="number"  name="years_of_experience" class="form-control" required placeholder="Years of Experience">
                                </div>
                                </div>
                                
                                <div class="col-md-12"><hr class="alert-warning"></div>

                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="small text-gray-600" for="leadCapEmail">State of Origin</label>
                                    <select required name="state_origin" class="form-control rounded-pill" id="stateselectsupervisororigin">
                                        
                                      </select>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group" style="display: none"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="area_local"  id="areaselectsupervisororigin"   class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                </div>       

                                <div class="col-md-12"><hr class="alert-warning"></div>

                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="small text-gray-600" for="leadCapEmail">State of Residence</label>
                                    <select required name="state" class="form-control rounded-pill" id="stateselectsupervisor">
                                        
                                      </select>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="area"  id="areaselectsupervisor"   class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                </div>
                                
                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>Bank Details</h6>
                                 <hr class="alert-warning">
                                </div>

                                 <div class="col-md-6">
                                 <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Bank Name*</label>
                                 <select required name="bank_name"  id="bank3"  class="form-control rounded-pill">
                                      
                                      </select>
                                            </div>
                                            
                                 </div>               
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Account Number*</label>
                                            <input required type="number" class="form-control" name="account_number"  id="supervisoraccount"  >
                                </div>
                                </div>

                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>Garantor Details</h6>
                                 <hr class="alert-warning">
                                </div>
                                 <div class="col-md-6">
                                 <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Name*</label>
                                    <input  required type="text" class="form-control" name="guarantor_name" >
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Profile Picture*</label>
                                    <input  required type="file" class="form-control" name="file_guarantor_profile" >
                                    </div>
                                 </div>  
                                 <div class="col-md-6">
                                 <label for="">Guarantor Phone Number(08029292929)</label>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Phone Number*</label>
                                    <input  required type="number" id="supervisorgphone" class="form-control" name="guarantor_number" >
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Guarantor Valid ID*</label>
                                    <input  required type="file"  name="file_guarantor_id" class="form-control"  >
                                    </div>
                                 </div> 
                                <div class="col-md-12">
                                 <hr class="alert-warning">
                                <h6>User Valid ID Details</h6>
                                 <hr class="alert-warning">
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">ID Type</label>
                              <select   required name="id_type"    class="form-control rounded-pill">
                                <option   value="Drivers licence">Drivers licence</option>    
                                <option   value="Pvc">Pvc</option>     
                                <option   value="National ID"> National ID</option>     
                                <option   value="International passport">International passport</option>     
                               </select>
                               </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">ID Number</label>
                                            <input  required type="text" name="id_number" class="form-control"  >
                                   </div>    
                                    <label for="">Select the image</label>
                                    <input required  type="file"  name="file_id" />
                              </div>
                             
                              <div class="col-md-12">
                            <hr class="alert-danger">
                            <br>
                            <div id="bodydesc" style="margin-left:auto; margin-right:auto; width:460px; height: 200px; overflow-y: scroll;">

                                    <p id="desc" >
                                    Special Terms and conditions agreement Between Citi design and Esteem supervisor: </p>
                                    <ol>
                                    
                                        <li>1. Desist from every unlawful act that might lead to your profile termination.</li>
                                        <li>2. apply polite ethical standards with client's and Artisans.</li>
                                        <li>3. work professionally. Avoid illegal negotiation or embezzlement of client's properties, the offence is punishable and such can not be penalise but a jail term.  </li>
                                        <li>4.  Additional Part role  if approved to become supervisor is to be on supervission on a merged contracts.</li>
                                        <li>5.  Be on every price negotiation between artisans and clients;  Experts work is auction to ensure it meets  up with client's best interests.</li>
                                    </ol>
                                    
                                                                  

                         
                                    <p class="alert-warning">
                                    NOTE: supervisors earn 43% commission on every contract merge to supervise. Every citi designer contracts Transaction is expected to be under the company's account. Your Withdraw payment to be available only when the work is Completed and confirmed.        
                                    </p>

                            </div>
                            </div>
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                            <label class="form-check-label" for="defaultCheck1">
                                I accept the terms and condition.
                            </label>
                            </div>  
                                <!--begin col-md-12-->
                                <div class="col-md-4 offset-4">

                                      <br><br>          
                                    <button type="submit" class="btn btn-info" id="submit3" ><i id="loader3" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i>Register</button>
                                
                                </div>
                                <!--end col-md-12-->
                
                            </form></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
@endsection
@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
        $( "#datepicker" ).datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-1d'
        });
        $( "#datepicker1" ).datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-1d'
        });
        let banks = [
    { "id": "1", "name": "Access Bank" ,"code":"044" },
    { "id": "2", "name": "Citibank","code":"023" },
    { "id": "3", "name": "Diamond Bank","code":"063" },
    { "id": "4", "name": "Dynamic Standard Bank","code":"" },
    { "id": "5", "name": "Ecobank Nigeria","code":"050" },
    { "id": "6", "name": "Fidelity Bank Nigeria","code":"070" },
    { "id": "7", "name": "First Bank of Nigeria","code":"011" },
    { "id": "8", "name": "First City Monument Bank","code":"214" },
    { "id": "9", "name": "Guaranty Trust Bank","code":"058" },
    { "id": "10", "name": "Heritage Bank Plc","code":"030" },
    { "id": "11", "name": "Jaiz Bank","code":"301" },
    { "id": "12", "name": "Keystone Bank Limited","code":"082" },
    { "id": "13", "name": "Providus Bank Plc","code":"101" },
    { "id": "14", "name": "Polaris Bank","code":"076" },
    { "id": "15", "name": "Stanbic IBTC Bank Nigeria Limited","code":"221" },
    { "id": "16", "name": "Standard Chartered Bank","code":"068" },
    { "id": "17", "name": "Sterling Bank","code":"232" },
    { "id": "18", "name": "Suntrust Bank Nigeria Limited","code":"100" },
    { "id": "19", "name": "Union Bank of Nigeria","code":"032" },
    { "id": "20", "name": "United Bank for Africa","code":"033" },
    { "id": "21", "name": "Unity Bank Plc","code":"215" },
    { "id": "22", "name": "Wema Bank","code":"035" },
    { "id": "23", "name": "Zenith Bank","code":"057" }
]; 
$.each(banks, function(index, value) {
           
            $("#bank1").append(new Option(value.name, value.id));
            $("#bank3").append(new Option(value.name, value.id));
           
            });
        let areas = {};
        $.ajaxSetup({
                headers: { }
            });
$.get('/api/getstates', 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            
            $.each(data.states, function(index, value) {
            console.log(value);
             areas = data.areas;
            $("#stateselect").append(new Option(value.name, value.id));
            if(index == 0){

            $.each(areas, function(index, area_value) {
           if(area_value.state_id == value.id){
            $("#areaselect").append(new Option(area_value.name, area_value.id)); 
           }
            });
    

            }
            });
            
        }else{
         
        }


        }).fail(function(jqxhr, settings, ex) {
          
           alert("An Error Occured on the Server.")
         });


  $( "#stateselect" ).change(function() {
     
   console.log(areas);
   var stateid = $(this). children("option:selected"). val();
   console.log(stateid);
   $("#areaselect").empty();
   $.each(areas, function(index, value) {
           if(value.state_id == stateid){
            $("#areaselect").append(new Option(value.name, value.id)); 
           }
            });
    });   

    $('#user').click(function (e){

        e.preventDefault();
        let element =  document.getElementById('user');
        if(element.classList.contains('active')){
           

        }else{
            $('#user').addClass('active');

        }
        $('#artisan').removeClass('active');
        $('#supervisor').removeClass('active');
        $('#userReg').show();
        $('#artisanReg').hide();
        $('#supervisorReg').hide();

        


    }) ;


 $('#artisan').click(function (e){

e.preventDefault();
let element =  document.getElementById('artisan');
if(element.classList.contains('active')){
   

}else{
    
    populateState('artisan');
    populateState('artisanorigin');
    $('#artisan').addClass('active');

}
$('#user').removeClass('active');
$('#supervisor').removeClass('active');
$('#userReg').hide();
$('#artisanReg').show();
$('#supervisorReg').hide();
}) ;
$('#supervisor').click(function (e){

e.preventDefault();
let element =  document.getElementById('supervisor');
if(element.classList.contains('active')){
   

}else{
    populateState('supervisor');
    populateState('supervisororigin');
    $('#supervisor').addClass('active');

}
$('#user').removeClass('active');
$('#artisan').removeClass('active');
$('#userReg').hide();
$('#artisanReg').hide();
$('#supervisorReg').show();

}) ;

function populateState(userdata){
    let areas = {};
        $.ajaxSetup({
                headers: { }
            });
$.get('/api/getstates', 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            
            $.each(data.states, function(index, value) {
            console.log(value);
             areas = data.areas;
            $("#stateselect" +userdata).append(new Option(value.name, value.id));
            if(index == 0){

            $.each(areas, function(index, area_value) {
           if(area_value.state_id == value.id){
            $("#areaselect" +userdata).append(new Option(area_value.name, area_value.id)); 
           }
            });
    

            }
            });
            
        }else{
         
        }


        }).fail(function(jqxhr, settings, ex) {
          
           alert("An Error Occured on the Server.")
         });


  $( "#stateselect"+userdata ).change(function() {
     
   console.log(areas);
   var stateid = $(this). children("option:selected"). val();
   console.log(stateid);
   $("#areaselect"+userdata).empty();
   $.each(areas, function(index, value) {
           if(value.state_id == stateid){
            $("#areaselect"+userdata).append(new Option(value.name, value.id)); 
           }
            });
    });   

}

        $("#artisanForm").on("submit", function(event){
        event.preventDefault();
        if($('#artisanphone').val().length != 11){
            swal("Error!", 'Enter a Valid 11 degit  phone number', "error"); 

        }else if($('#artisanaccount').val().length != 10){
            swal("Error!", 'Enter a Valid 10 degit  Account number', "error"); 

        }else if($('#artisangphone').val().length != 11){
            swal("Error!", 'Enter a Valid 11 degit guarantor phone number', "error"); 

        }else{
           
        // var formValues= $(this).serialize();   
        // console.log(formValues);
        $('#loader1').show();
       $('#submit1').attr('disabled','disabled');
        var form = $('#artisanForm')[0];
            // Create an FormData object 
            var data = new FormData(form);
            console.log(data);
            $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/api/register/artisan",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

        if(data.code == "200"){
            
            swal("Success!", "Your Artisan Registration is successfull, Please login to Make Payment", "success"); 
          setTimeout(() => {
               window.location.href = '/login';
          }, 3000);

       }else{
          
           
           if(data.email != undefined){
            swal("Error!", data.email[0], "error"); 

           }
          
         
      
       }      $('#loader1').hide();
          $('#submit1').removeAttr('disabled');  
            },
            error: function (e) {
                console.log(e)

            swal("Error!", e.responseJSON.message, "error"); 
            $('#loader1').hide();
             $('#submit1').removeAttr('disabled');


            }
        });

        }
   

        });
        $("#userForm").on("submit", function(event){
        event.preventDefault();
       
        if($('#userphone').val().length == 11){
                $('#loader2').show();
       $('#submit2').attr('disabled','disabled');
        // var formValues= $(this).serialize();   
        // console.log(formValues);

        var form = $('#userForm')[0];
            // Create an FormData object 
            var data = new FormData(form);
            console.log(data);
            $.ajax({
            type: "POST",
            url: "/api/register/user",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

        if(data.code == "200"){
           console.log('correst');
       swal("Success!", "Your User Registration is successfull, Please login to Make Request", "success"); 
       setTimeout(() => {
         window.location.href = '/login';   
       }, 3000);

       }else{
          
           
           if(data.email != undefined){
            swal("Error!", data.email[0], "error"); 

           }
          
         
          $('#loader2').hide();
          $('#submit2').removeAttr('disabled');
       }    
            },
            error: function (e) {
                console.log(e)

            swal("Error!", e.responseJSON.message, "error"); 
            $('#loader2').hide();
             $('#submit2').removeAttr('disabled');
                

            }
        });
        }else{
            swal("Error!", "Please Enter a valid 11 degit number", "error"); 
        }



        });
        $("#supervisorForm").on("submit", function(event){
        event.preventDefault();
        if($('#supervisorphone').val().length != 11){
            swal("Error!", 'Enter a Valid 11 degit  phone number', "error"); 

        }else if($('#supervisoraccount').val().length != 10){
            swal("Error!", 'Enter a Valid 10 degit  Account number', "error"); 

        }else if($('#supervisorgphone').val().length != 11){
            swal("Error!", 'Enter a Valid 11 degit guarantor phone number', "error"); 

        }else{
            $('#loader3').show();
       $('#submit3').attr('disabled','disabled');
        // var formValues= $(this).serialize();   
        // console.log(formValues);

        var form = $('#supervisorForm')[0];
            // Create an FormData object 
            var data = new FormData(form);
            console.log(data);
            $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/api/register/supervisor",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

        if(data.code == "200"){
            
            swal("Success!", "Your Supervisor Registration is successfull, Please login to Make payment", "success"); 
       setTimeout(() => {
         window.location.href = '/login';   
       }, 3000);

       }else{
          
           
           if(data.email != undefined){
            swal("Error!", data.email[0], "error"); 

           }
          
         
        
       }    $('#loader3').hide();
          $('#submit3').removeAttr('disabled');
            },
            error: function (e) {
                console.log(e)
            swal("Error!", e.responseJSON.message, "error"); 
            $('#loader3').hide();
             $('#submit3').removeAttr('disabled');


            }
        });
        }
        



        });  
    });
   </script>

@endsection