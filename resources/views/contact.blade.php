@extends('layouts.front')

@section('content')
<header class="page-header page-header-dark bg-gradient-primary-to-secondary">
    <div class="page-header-content pt-10">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <h1 class="page-header-title mb-3">Contact US</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-rounded text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor"><path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0" /></svg>
    </div>
</header>
<section class="bg-light">
    <div class="container">
        <hr class="my-10" />
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2>Can&apos;t find the What you need?</h2>
                <p class="lead mb-5">Contact us and we&apos;ll get back to you as soon as possible.</p>
            </div>
        </div>
        <div class="row align-items-center mb-10">
            <div class="col-lg-4 text-center mb-5 mb-lg-0">
                <div class="section-preheading">Call Anytime</div>
                <a href="#!">+2347085376346 /  +234906 857 6172</a>
            </div>
            <div class="col-lg-4 text-center mb-5 mb-lg-0">
               
            </div>
            <div class="col-lg-4 text-center">
                <div class="section-preheading">Email Us</div>
                <a href="#!">madeklassic@gmail.com</a>
            </div>
        </div>
        <form>
            <div class="form-row">
                <div class="form-group col-md-6"><label class="text-dark" for="inputName">Full name</label><input class="form-control py-4" id="inputName" type="text" placeholder="Full name" /></div>
                <div class="form-group col-md-6"><label class="text-dark" for="inputEmail">Email</label><input class="form-control py-4" id="inputEmail" type="email" placeholder="name@example.com" /></div>
            </div>
            <div class="form-group"><label class="text-dark" for="inputMessage">Message</label><textarea class="form-control py-3" id="inputMessage" type="text" placeholder="Enter your message..." rows="4"></textarea></div>
            <div class="text-center"><button class="btn btn-primary btn-marketing mt-4" type="submit">Submit Request</button></div>
        </form>
     
    </div>
    <div class="svg-border-rounded text-dark">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor"><path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0" /></svg>
    </div>
</section>
@endsection