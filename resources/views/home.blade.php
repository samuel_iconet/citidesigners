@extends('layouts.homelayout')

@section('content')


<section class="home-section" id="home">


<!--begin container -->
<div class="container">

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="/front/images/slider8.jpg" style="height: 600px;"  alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="/front/images/slider41.jpg" style="height: 600px;"  alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="/front/images/slider3.jpg" style="height: 600px;" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    

</div>
<!--end container -->

</section>


<section class="section-grey" >

<!--begin container -->
<div class="container">

    <!--begin row -->
    <div class="row">

        <!--begin col-md-12 -->
        <div class="col-md-12 text-center margin-bottom-10">

        <h2 class="section-title">How it Works</h2>

<p class="section-subtitle">Citi Designers finds you affordable, trained and highly vetted Artisans for your Domestic and Commercial needs.</p>


        </div>
        <!--end col-md-12 -->

    </div>
    <!--end row -->

</div>
<!--end container -->

<!--begin services-wrapper -->
<div class="services-wrapper">

    <!--begin container -->
    <div class="container">

        <!--begin row -->
        <div class="row">

            <!--begin col-md-4 -->
            <div class="col-md-4">

                <div class="main-services blue">

                    <i class="far fa-chart-bar"></i>

                    <h4>Request An Artisan</h4>

<p>Choose from a variety of  Artisan Skills Available.</p>

                </div>

            </div>
            <!--end col-md-4 -->

            <!--begin col-md-4 -->
            <div class="col-md-4">

                <div class="main-services red">

                    <i class="far fa-heart"></i>

                    <h4>Your Request is Merged</h4>

                    <p>Our system automatically merges your request with an artisan and a supervisor that would professionally do your job.</p>
                    
                </div>

            </div>
            <!--end col-md-4 -->

            <!--begin col-md-4 -->
            <div class="col-md-4">

                <div class="main-services green">

                    <i class="fas fa-anchor"></i>

                    <h4>Get it Done</h4>

                    <p>your service agent and supervisor arrives and gets the job done.  Payments are seamless.</p>
                    
                </div>

            </div>
            <!--end col-md-4 -->
            <div class="col-md-5"></div>
       <div class="col-md-4"><hr class="alert-success"><p><a href="/login" class="btn btn-info" style="margin-right:30px">Login</a><a href="/register" class="btn btn-info">Register</a></p></div>
        </div>
        <!--end row -->
        
    </div>
    <!--end container -->

</div>
<!--end services-wrapper -->

</section>
<section class="section-white" id="about" >
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row align-items-center">
            
                <!--begin col-md-6-->
                <div class="col-md-4 wow slideInLeft" data-wow-delay="0.25s" style="visibility: visible; animation-delay: 0.25s; animation-name: slideInLeft;">

                    <div class="margin-right-25">

                        <img src="/front/images/logo.png" class="width-100 " alt="pic">

                    </div>
                    
                </div>
                <!--end col-sm-6-->
                
                <!--begin col-md-6-->
                <div class="col-md-8">

                    <h3>About Us.</h3>

                    <p>CITI DESIGNER is a  construction management content tools, a plug hub that allows Artisans connect directly to their clients conveniently all over the states. Citi designers has been restructured to enhance the lifestyles of the users.</p>
                    <p>We aim to offer the best Artisans services at the best negotiatable  price all around the globe, with a suitable investment platforms that gives you the best for landed properties.</p>
                    <p>our goals concepts are to bring experts Artisans together, making their services easily accessible, reliable and functional.</p>              
                    <ul class="benefits">
                    
                                <li><i class="fas fa-check"></i>  Merging artisans with Daily contracts notification and Connections that enables artisans to live good and Earn more with their skills</li>
                                <li><i class="fas fa-check"></i>  Get artisans contract fee fast, reliable and smooth financial support systems that will enable every expert artisans work comfortably within and outside their states.</li>
                                <li><i class="fas fa-check"></i> CITI Design; Always ready to serve you perfect on every project, give you the best safe working mechanism suitable for every users. Try us today; Always at your service</li>
                            </ul>
                </div>
                <!--end col-md-6-->
            
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
<section class="section-white" id="portfolio">

<!--begin container -->
<div class="container">

    <!--begin row -->
    <div class="row">

        <!--begin col-md-12 -->
        <div class="col-md-12 text-center margin-bottom-30">

            <h2 class="section-title">Our Artisans</h2>

            <p class="section-subtitle">Dicover the list of Artisans categories available  <a href="/artisans">View all</a>.</p>
            
        </div>
        <!--end col-md-12 -->

    </div>
    <!--end row -->

</div>
<!--end container -->

<!--begin container -->
<div class="container">

    <!--begin row-->
    <div class="row wow fadeIn" data-wow-delay="0.25s" style="visibility: visible; animation-delay: 0.25s; animation-name: fadeIn;">

        <!--begin col-md-4 -->
         
        @if(count($skills) > 0)
                @foreach ($skills as $skill)
                 
              
            <div class="col-md-4 col-sm-12 p-0 m-0">

                <figure class="gallery-insta">
                    <!--begin popup-gallery-->
                    <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                        <a  >
                            <img src="/storage/skill/{{$skill->image}}" class="width-100" alt="pic">
                            <span class="eye-wrapper">{{$skill->name}}</span>
                        </a>
                    </div>
                    <!--end popup-gallery-->
                </figure>
                </div>
                @endforeach 
                 
                @else
               
                
                <h3>No Skills Found!!</h3>
                
                <hr><hr><br><br><br><br><br>
                @endif
       

    </div>
    <!--end row -->

</div>
<!--end container -->

</section>
<section class="section-bg-1">
        
        <div class="section-bg-overlay"></div>
    
        <!--begin container-->
        <div class="container">
    
            <!--begin row-->
            <div class="row">
            
                <!--begin col md 7 -->
                <div class="col-md-7 mx-auto margin-bottom-20 text-center">
    
                    <h2 class="white-text">Fun Facts About Our Agency</h2>
    
                </div>
                <!--end col md 7-->
            
            </div>
            <!--end row-->
    
            <!--begin row-->
            <div class="row">
        
                <!--begin fun-facts-box -->
                <div class="col-md-2 offset-md-2 fun-facts-box text-center">
                    
                    <i class="far fa-gem"></i>
                    
                    <p class="fun-facts-title"><span class="facts-numbers">1050+</span><br>Projects Completed</p>
                    
                </div>
                <!--end fun-facts-box -->
    
                <!--begin fun-facts-box -->
                <div class="col-md-2 fun-facts-box text-center">
                    
                    <i class="far fa-heart"></i>
                                               
                    <p class="fun-facts-title"><span class="facts-numbers">217k</span><br>Happy Clients</p>
                        
                </div>
                <!--end fun-facts-box -->
    
                <!--begin fun-facts-box -->
                <div class="col-md-2 fun-facts-box text-center">
                    
                    <i class="fas fa-award"></i>
                                               
                    <p class="fun-facts-title"><span class="facts-numbers">1210</span><br>Design Awards</p>
                        
                </div>
                <!--end fun-facts-box -->
    
                <!--begin fun-facts-box -->
                <div class="col-md-2 fun-facts-box text-center">
                    
                    <i class="fas fa-anchor"></i>
                                               
                    <p class="fun-facts-title"><span class="facts-numbers">2137</span><br>Line Of Codes</p>
                        
                </div>
                <!--end fun-facts-box -->
    
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
<section class="section-white">
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row align-items-center">
            
                <!--begin col-md-6-->
                <div class="col-md-6 wow slideInLeft" data-wow-delay="0.25s" style="visibility: visible; animation-delay: 0.25s; animation-name: slideInLeft;">

                    <div class="margin-right-15">

                        <img src="/front/images/app.jpg" class="width-100 " alt="pic">

                    </div>
                    
                </div>
                <!--end col-sm-6-->
                
                <!--begin col-md-6-->
                <div class="col-md-6">

                    <h3>Download our APP.</h3>

                    <p>access our services with ease, from your mobile phone</p>
                    
                    <a href=""><img src="/front/images/playstore.png" style="height: 200px ; width: 200px" alt=""></a>
                   


                </div>
                <!--end col-md-6-->
            
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>

    <section class="section-grey" id="team">

<!--begin container-->
<div class="container">

    <!--begin row-->
    <div class="row">
  
        <!--begin col-md-12 -->
        <div class="col-md-12 text-center">

            <h2 class="section-title">Market Place</h2>
                <a href="/register"   class="btn btn-success"  >SELL FOR FREE</a>
              <a href="/marketplace" class="btn btn-success">View All Ads</a>
            
        </div>
        @if(count($products) > 0)
           @foreach($products as $product)

           <div class="col-sm-12 col-md-4 margin-top-30">

              <img src="/storage/product/{{$product->images[0]->image}}" style="height: 250px;" class="team-img width-100" alt="pic">

              <div class="team-item">

                  <h3> <a href="/itemdetail/{{$product->id}}">{{$product->name}} </a><span style="color:orange; font-size:8px">( {{$product->created_at->diffForHumans()}} )</span></h3>
                  <hr>
                  <div class="team-info"><h4 style=" color: green;" class="amount"> {{$product->price}}</h4></div>
                  <p>{{$product->description}}</p>
                  
              </div>

              </div>
           @endforeach
           @else
           <div class="col-lg-4 mb-5 mb-lg-0">
               <hr>
           <h3>No Item Found!!</h3>
           <hr><br><br>
           </div>
           @endif
      
      
    
    </div>
    <!--end row-->

</div>
<!--end container-->

</section>

<section class="section-white" id="features">

<!--begin container -->
<div class="container">

    <!--begin row -->
    <div class="row">

        <!--begin col-md-12-->
        <div class="col-md-12 text-center padding-bottom-10">

            <h2 class="section-title">Amazing Features</h2>


        </div>
        <!--end col-md-12 -->

    </div>
    <!--end row -->

    <!--begin row -->
    <div class="row">

        <!--begin col-md-4-->
        <div class="col-md-4">

            <div class="feature-box light-green wow fadeIn" data-wow-delay="0.25s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                <i class="fas fa-tools"></i>

                <div class="feature-box-text">

                    <h4>Support 24/7</h4>

                    <p>have access to our 24/7 support team, ready to help out.</p>

                </div>

            </div>

        </div>
        <!--end col-md-4 -->

        <!--begin col-md-4-->
        <div class="col-md-4">

            <div class="feature-box light-blue wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                <i class="fas fa-users"></i>

                <div class="feature-box-text">

                    <h4>User Friendly</h4>

                    <p>Enjoy our Userfriendly web application and mobile application.</p>

                </div>

            </div>

        </div>
        <!--end col-md-4 -->

        <!--begin col-md 4-->
        <div class="col-md-4">

            <div class="feature-box orange wow fadeIn" data-wow-delay="0.75s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                <i class="far fa-bell"></i>

                <div class="feature-box-text">

                    <h4>Notifications</h4>

                    <p>Recieve email and Sms notifications for various events on the plateform.</p>


                </div>

            </div>

        </div>
        <!--end col-md-4 -->

    </div>
    <!--end row -->

    <!--begin row -->
    <div class="row">

        <!--begin col-md-4-->
        <div class="col-md-4">

            <div class="feature-box dark-blue wow fadeIn" data-wow-delay="1s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                <i class="fas fa-gift"></i>

                <div class="feature-box-text">

                    <h4>Accesibility</h4>

                    <p>Access all our features with ease.</p>

                </div>

            </div>

        </div>
        <!--end col-md-4 -->

        <!--begin col-md-4-->
        <div class="col-md-4">

            <div class="feature-box light-red wow fadeIn" data-wow-delay="1.25s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                <i class="fas fa-piggy-bank"></i>

                <div class="feature-box-text">

                    <h4>Affordable</h4>

                    <p>Our services are very affordable for user. and also highly rewarding for artisans and supervisors.</p>

                </div>

            </div>

        </div>
        <!--end col-md-4 -->

        <!--begin col-md-4-->
        <div class="col-md-4">

            <div class="feature-box dark-green wow fadeIn" data-wow-delay="1.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">


            <i class="fas fa-robot"></i>


                <div class="feature-box-text">

                    <h4>Automated </h4>

                    <p>Enjoy our smart and automated margin system that connects you to the best artisan available.</p>

                </div>

            </div>

        </div>
        <!--end col-md-4 -->

    </div>
    <!--end row -->

</div>
<!--end container -->

</section>
<section class="section-white">
        
        <!--begin container-->
        <div class="container">
            <hr class="alert-danger">
            <div class="row">

                <!--begin col-md-12-->
                <div class="col-md-12 text-center padding-bottom-10">

                    <h2 class="section-title">LATEST INFO!!!</h2>

                    
                </div>
                <!--end col-md-12 -->

            </div>
            <hr class="alert-danger">
            <!--begin row-->
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators" style="margin-top: 30px">
                <?php $count = 0 ?>
                  
                    @foreach($tips as $tip)
                    @if($count == 0)
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

                    @else
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$count}}"></li>
                    @endif
                    <?php $count++ ?>
                    @endforeach
                </ol>
                <?php $count = 0 ?>
                <div class="carousel-inner">
                @foreach($tips as $tip)
                @if($count == 0)
                  <div class="carousel-item active">
                  @else
                    <div class="carousel-item"  >
                    @endif
                    <?php $count++ ?>
                    @if($tip->image == "none")
                    <div class="row align-items-center">
            
                        <div class="col-md-12">
        
                            <h3>{{$tip->title}}.</h3>
        
                            <p>{{$tip->body}}</p>
                            
                        
        
        
                        </div>
                        <!--end col-md-6-->
                    
                    </div>

                    @else
                    <div class="row align-items-center">
            
                        <!--begin col-md-6-->
                        <div class="col-md-6 " >
        
                            <div class="margin-right-15">
        
                                <img src="/storage/tips/{{$tip->image}}" class="width-100 " alt="pic">
        
                            </div>
                            
                        </div>
                        <!--end col-sm-6-->
                        
                        <!--begin col-md-6-->
                        <div class="col-md-6">
        
                        <h3>{{$tip->title}}.</h3>
        
                            <p>{{$tip->body}}</p>
                            
        
        
                        </div>
                        <!--end col-md-6-->
                    
                    </div>
                  

                    @endif
                  </div>
                 @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
           
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
    <section class="section-bg-2" id="contact">
        
        <div class="section-bg-overlay"></div>

        <!--begin container-->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12-->
                <div class="col-md-12 text-center padding-bottom-10">

                    <h2 class="section-title white-text">Contact Us</h2>
                    <hr class="alert-warning">
                    <div class="row" style="color:white">
                      <div class="col-lg-4">
                      Call Anytime:
                      +2347085376346 / +1249061167673
                      </div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-4" >
                      Email Us: 
                      madeklassic@gmail.com
                      </div>
                    </div>
                    <hr class="alert-warning">

                    <p class="section-subtitle white">Have any queries? Get in touch today.</p>

                </div>
                <!--end col-md-12 -->

            </div>
            <!--end row -->

            <!--begin row-->
            <div class="row justify-content-md-center">
            
                <!--begin col-md-8-->
                <div class="col-md-8 text-center margin-top-10">

                    <!--begin contact-form-wrapper-->
                    <div class="contact-form-wrapper wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">

                        <!--begin form-->
                        <div>
                             
                            <!--begin success message -->
                            @if(Session::has('contactus'))
                            <p class="contact_success_box alert alert-success">We received your message and you will hear from us soon. Thank You!</p>
                           @endif
                            <!--end success message -->
                            
                            <!--begin contact form -->
                            <form class="row contact-form contact" action="/contactus" method="post">
                            @csrf 
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="contact-input" required="" name="contact_names" placeholder="Your Name*" type="text">
                                
                                    <input class="contact-input" required="" name="contact_phone" placeholder="Phone Number*" type="text">
                                
                                </div>
                                <!--end col-md-6-->
        
                                <!--begin col-md-6-->
                                <div class="col-md-6">

                                    <input class="contact-input" required="" name="contact_email" placeholder="Email Adress*" type="email">
                                
                                    
                                
                                </div>
                                <!--end col-md-6-->
        
                                <!--begin col-md-12-->
                                <div class="col-md-12">

                                    <textarea  class="contact-message" rows="2" cols="20" name="contact_message" placeholder="Your Message..."></textarea>

                                    <button type="submit" class="btn btn-info" >Get In Touch</button>
                                
                                </div>
                                <!--end col-md-12-->
                
                            </form>
                            <!--end contact form -->

                        </div>
                        <!--end form-->

                    </div>
                    <!--end contact-form-wrapper-->

                </div>
                <!--end col-md-8-->
            
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
<!-- //old content  -->

@endsection