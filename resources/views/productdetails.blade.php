@extends('layouts.front')

@section('content')
<br><br><br><br><br>
<hr class="alert-warning">

<!--Section: Block Content-->
<section class="mb-5">

  <div class="row">
    <div class="col-md-6 mb-4 mb-md-0">

      <div id="mdb-lightbox-ui"></div>

      <div class="mdb-lightbox">

        <div class="row product-gallery mx-1">

          <div class="col-12 mb-0">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                 
                  @foreach($product->images as $key => $image)
                  @if($key == '0')
                  <div class="carousel-item active">
                    @else
                  <div class="carousel-item ">
                    @endif
                    <figure class="view overlay rounded z-depth-1 main-img">
                        <a href="/storage/product/{{$image->image}}"
                          data-size="710x823">
                          <img  style="height: 823px; width: 710px;" src="/storage/product/{{$image->image}}"
                            class="img-fluid z-depth-1">
                        </a>
                      </figure>
                  </div>
                  @endforeach
                  
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
          
          </div>
      
        </div>

      </div>

    </div>
    <div class="col-md-6">
    <br><br><br>
      <h5>{{$product->name}}</h5>
      <p class="mb-2 text-muted text-uppercase small"><span class=" alert alert-info ">{{$product->ad_type}} </span><span  class="alert alert-warning">Uploaded :{{$product->created_at->diffForHumans()}}</span> </p>
    
      <p><span class="mr-1"><strong style="color: green;">₦ {{number_format($product->price, 0, '.', ',')}}</strong></span></p>
      <p class="pt-1">{{$product->description}}</p>
      <div class="table-responsive">
        <table class="table table-sm table-borderless mb-0">
          <tbody>
              @foreach($product->props as $property)
            <tr>
              <th class="pl-0 w-25" scope="row"><strong>{{$property['name']}}</strong></th>
              <td>{{$property['value']}}</td>
            </tr>
           @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <div class="table-responsive mb-2">
          <br>
        <h4 ><span class="alert alert-success">Seller`s Contact Details</span> </h4>
        <h6>Name: {{$product->user->name}}</h6>
        <h6>Email: {{$product->user->email}}</h6>
      
        <h6>Phone Number: {{$product->user_data->phone_number}} <button type="button" class=""><i class="fas fa-copy"></i></button></h6>
        <h6><span  class="alert alert-warning">Registered :{{$product->user->created_at->diffForHumans()}}</span> <span  class="alert alert-warning">Last Seen :{{$product->user->updated_at->diffForHumans()}}</span></h6>
      <hr>
      <button class="btn btn-warning" id="chatseller" style="display:none">Chat Seller</button>
      <br>
      <div id="chatform" style="display:none">
      <label for="">Message</label>
      <input type="hidden" id="product_id" value="{{$product->id}}">
      <textarea name="" id="msg" class="form-control"></textarea>
      <button class="btn btn-success" id="send">Send</button></div>
      </div>
     
    </div>
  </div>

</section>
<!-- Classic tabs -->
<div class="classic-tabs border rounded px-4 pt-1">

    <ul class="nav tabs-primary nav-justified" id="advancedTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active show" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews ({{ $product->ReviewCount }})</a>
      </li>
    </ul>
    <div class="tab-content" id="advancedTabContent">
      <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
        <div class="table-responsive">
            <table class="table table-sm table-borderless mb-0">
        <tbody>
            @foreach($product->props as $property)
          <tr>
            <th class="pl-0 w-25" scope="row"><strong>{{$property['name']}}</strong></th>
            <td>{{$property['value']}}</td>
          </tr>
         @endforeach
        </tbody>
        </table>
        </div>
        <hr class="alert-warning"><h2>Description</h2>
           <p class="pt-1">{{$product->description}}</p>
    
      </div>
     
      <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
          <hr>
        <hr class="alert-warning">
        <h5><span>{{ $product->ReviewCount }}</span> review for <span>({{$product->name}})</span></h5>
        <hr class="alert-warning">
      @foreach($product->productreview as $reviewproduct)
      <div class="media mt-3 mb-4">
        <div class="media-body">
          <div class="d-sm-flex justify-content-between">
            <p class="mt-1 mb-2">
              <strong>{{$reviewproduct->name}} </strong>
              <span>– </span><span>{{$reviewproduct->created_at->diffForHumans()}}</span>
            </p>
          </div>
          <p class="mb-0">{{$reviewproduct->review}} </p>
        </div>
      </div>
      <hr class="alert-warning">

      @endforeach
      <div class="row">
          <div class="col-md-4 offset-4">
            <hr class="alert-success">
            <h5 class="mt-4">Add a review</h5>
            <p>Your email address will not be published.</p>
            
            <div>
              <!-- Your review -->
              <form action="" id="reviewform">
                <div class="md-form md-outline">
                    <label for="form76">Your review</label>
        
                    <textarea required  id="review" class="md-textarea form-control pr-6" rows="4"></textarea>
                  </div>
                  <!-- Name -->
                  <div class="md-form md-outline">
                    <label for="form75">Name</label>
        
                    <input type="text" required id="name" class="form-control pr-6">
                    <input type="hidden"  required id="product_id" value="{{$product->id}}"  class="form-control pr-6">
                  </div>
                  <!-- Email -->
                  <div class="md-form md-outline">
                    <label for="form77">Email</label>
                    <input type="email" required id="email" class="form-control pr-6">
                  </div>
                  <div class="text-right pb-2">
                      <br>
                    <button type="submit" class="btn btn-primary" id="submit"><i id="loader" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i> a review</button>
                  </div>
              </form>
          </div>
      </div>
        </div>
      </div>
    </div>
  
  </div>
  <!-- Classic tabs -->
<!--Section: Block Content-->
<!--Section: Block Content-->
<br><br><br>
<section class="text-center">
<h2>Related Products</h2>
    <!-- Grid row -->
    <div class="row">
  
      <!-- Grid column -->
      @foreach($product->relatedProducts as $rproduct)
      <div class="col-md-6 col-lg-3 mb-5">
  
        <!-- Card -->
        <div class="">
  
          <div class="view zoom overlay z-depth-2 rounded">
          
            <a href="/itemdetail/{{$product->id}}">
              <div class="mask">
                <img class="img-fluid w-100"
                  src="/storage/product/{{$rproduct->images[0]->image}}">
                <div class="mask rgba-black-slight"></div>
              </div>
            </a>
          </div>
  
          <div class="pt-4">
  
            <h5>{{$rproduct->name}}</h5>
            <h6 style="color: green;">₦ {{number_format($product->price, 0, '.', ',')}}</h6>
          </div>
  
        </div>
        <!-- Card -->
  
      </div>
      @endforeach
     
      <!-- Grid column -->
  
    </div>
    <!-- Grid row -->
  
  </section>
  <!--Section: Block Content-->
@endsection
@section('script')
<script>
    $(document).ready(function () {
      let token = "Bearer "  + localStorage.getItem('access_token');
      console.log(token);
     $.ajax({
     type: "GET",
     headers: {'Content-Type': 'application/json', Authorization: token  },
     url: "/api/validateSession",
     success: function(patientDTO) {
        $('#chatseller').show();
          },
     error: function(e) {
     console.log("ERROR: ", e);
     e.preventDefault();
    $('#loader1').show();
     }
 });
 $('#chatseller').click( function(e){
  e.preventDefault();
  $('#chatseller').hide();
  $('#chatform').show();
 })
 $('#send').click( function(e){
  e.preventDefault();
  let msg = $('#msg').val();
  let product_id = $('#product_id').val();
           if(msg == ""){
            swal("Error!", "Please Enter Message.", "error"); 

           }

           else{
            let token = "Bearer "  + localStorage.getItem('access_token');
            console.log(token);
            $.ajax({
     type: "GET",
            headers: {'Content-Type': 'application/json', Authorization: token  },
            url: "/api/product/chat/" + msg + "/" + product_id,
            success: function(patientDTO) {
                console.log("SUCCESS: ", patientDTO);
                swal("Success!", "Your Message Have been Submitted Successfully.", "success");
                  },
            error: function(e) {
            console.log("ERROR: ", e);
            e.preventDefault();
            $('#loader1').show();
            }
        });
           }  
 })
 
        $('#reviewform').submit(function(e) {
        e.preventDefault();
        $('#loader').show();
        $('#submit').attr('disabled','disabled');
          let email = $('#email').val();  
          let name = $('#name').val();  
          let review = $('#review').val();
          let product_id = $('#product_id').val();
          $.ajaxSetup({
                 headers: { }
             });
            $.post('/api/review',   // url
                    {    
                           email : email,
                           review : review,
                           name : name,
                           product_id : product_id,
                    }, // data to be submit
                
                    function(data, status, jqXHR) {// success callback
            
                    console.log(status);
                     if(data.code == 200){
                        swal("Success!", "Your Review Have been Submitted Successfully.", "success"); 
                        setTimeout(() => {location.reload(true);} , 2000)
                     }      
                    else if(data.code == 401){
                            swal("Error!", data.error, "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
            
                        $('#loader').hide();
                        $('#submit').removeAttr('disabled');

                    }).fail(function(jqxhr, settings, ex) {
                        $('#loader').hide();
                        $('#submit').removeAttr('disabled');
                        console.log(jqxhr.status);
                       
                            swal("Error!", "Network Error!", "error"); 
                        
                    });

    });
});
</script>
@endsection