@extends('layouts.front')

@section('content')
<section class="bg-light pt-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5">
<br><br> <br><br><br>

                <h2>Artisan Palace</h2>
                <h3>WHAT DO YOU WANT DONE?</h3>
                <p class="lead">Get the Best Artisans for your Project.</p>

            </div>
        </div>
        <div class="row mb-n10 z-1">

            <div class="card-columns card-columns-portfolio">
              
                @if(count($skills) > 0)
                @foreach ($skills as $skill)
                 
            <a class="card card-portfolio" href="/requestartisan"
            ><input type="hidden" value="{{$skill->id}}"><img class="card-img-top" src="/storage/skill/{{$skill->image}}" alt="..." />
            <div class="card-body"><div class="card-title">{{$skill->name}}</div></div></a
            >
    
                @endforeach 
                 
                @else
               
                
                <h3>No Skills Found!!</h3>
                
                <hr><hr><br><br><br><br><br>
                @endif
                
                
               
               

            </div>
           
        </div>
        
    </div>

   
</section>

@endsection

 