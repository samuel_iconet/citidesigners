@extends('layouts.front')

@section('content')
<section class="section-grey" id="team" style="margin-buttom:40px">
<div class="container">
            <div class="row align-items-center" >
                <div class="col-lg-3">

                  
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Enter New Password</div>
                        <div class="card-body">
                        
                             <br>   
                            @if(isset($user))
                             <form action="" id='savepassword' >
                             <div class="form-group"><input class="form-control rounded-pill" id="password1" type="password" placeholder="Password" required /></div>
                             <div class="form-group"><input class="form-control rounded-pill" id="password2" type="password" placeholder="Re-enter Password" required /></div>
                            <input type="hidden" value="{{$user->email}}" id="email">
                            <input type="hidden" value="{{$user->remember_token}}" id="token">
                            <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4"><span class="pull-right"><button id="submit1" type="submit" class="btn btn-info"><i id="loader1" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i>Save Password</button></span></div>
                            <div class="col-lg-4"></div>
                            </div>
                            </form>
                            @else

                            <h3 class="alert alert-danger"> Invalid Recovery Token</h3>
                            @endif
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
@endsection
@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
     

    

        $( "#savepassword" ).submit(function(e) {

            e.preventDefault();
            $('#loader1').show();
            $('#submit1').attr('disabled','disabled');
          let pass1 = $('#password1').val();
          let pass2 = $('#password2').val();
          let token = $('#token').val();
          let email = $('#email').val();
           
            console.log(pass1);
            console.log(pass2);
            if(pass1.length < 6 && pass2.length < 6){
            swal("Error!", "password less than 6 Characters ", "error"); 
               
            }else{
                if(pass1 != pass2){
            swal("Error!", "password Mismatch ", "error"); 

                }else{
                    $.ajaxSetup({
                 headers: { }
             });
            $.post('/api/recoverpassword/save',   // url
                    {    
                           email : email,
                           password : pass1,
                           token : token,
                    }, // data to be submit
                // $.post('/api/token',   // url
                //    {        email: email, 
                //             password: password
                            
                //    }, // data to be submit
                    function(data, status, jqXHR) {// success callback
            
                    console.log(status);
                     if(data.code == 200){
                        swal("Success!", "Password Changed Successfully!! ", "success"); 

                     }      
                    else if(data.code == 303){
                            swal("Error!", data.error, "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
            
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');

                    }).fail(function(jqxhr, settings, ex) {
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');
                        console.log(jqxhr.status);
                        if(jqxhr.status == 400){
                            swal("Error!", "Invalid data!", "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
                    });     
                }
            }
            $('#loader1').hide();
            $('#submit1').removeAttr('disabled'); 
        });


 });
   </script>

@endsection