@extends('layouts.front')

@section('content')

<header class="page-header page-header-dark bg-img-repeat bg-secondary" style='background-image: url("assets/img/pattern-shapes.png")'>
    <div class="page-header-content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">

                  
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Request For An Artisan Now</div>
                        <div class="card-body">
                            <h3 class="alert alert-success" id="message"  style="display: none"></h3>
                            <h3 class="alert alert-danger" id="error"  style="display: none"></h3>

                            <form id="target" >
                                
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Select An Artisan</label>
                                    <select required name="cars" id="skill" class="form-control rounded-pill">
                                        @if(isset($skills))
                                            @foreach ($skills as $skill)
                                             <option value="{{$skill->id}}">{{$skill->name}}</option>  
                                            @endforeach
                                        @endif
                                      </select>
                                </div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Describe What you want get Done.</label><textarea  required class="form-control"  id="description" cols="30" rows="5"></textarea></div>
                                <hr>
                                <h3>Your Data</h3>
                                <hr>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Name</label><input required class="form-control rounded-pill" id="name" type="text" /></div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email</label><input required class="form-control rounded-pill" id="email" type="email" /></div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Phone Number</label><input required class="form-control rounded-pill" id="number" type="number" /></div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Address</label><input required class="form-control rounded-pill" id="address" type="text" /></div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">State</label>
                                    <select required name="cars" class="form-control rounded-pill" id="stateselect">
                                        
                                      </select>
                                </div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Area</label>
                                    <select required name="cars" id="areaselect"  class="form-control rounded-pill">
                                      
                                      </select>
                                </div>
                                
                                <button class="btn btn-primary btn-marketing btn-block rounded-pill mt-4" type="submit" id="submit"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i> Place Request Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>

@endsection
@section('script')
<script type="text/javascript">

     $( document ).ready(function() {
        let areas = {};
        $.ajaxSetup({
                headers: { }
            });
$.get('/api/getstates', 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            $.each(data.states, function(index, value) {
            console.log(value);
             areas = data.areas;
            $("#stateselect").append(new Option(value.name, value.id));
            if(index == 0){

            $.each(areas, function(index, area_value) {
           if(area_value.state_id == value.id){
            $("#areaselect").append(new Option(area_value.name, area_value.id)); 
           }
            });
    

            }
            });
            
        }else{
         
        }


        }).fail(function(jqxhr, settings, ex) {
          
           alert("An Error Occured on the Server.")
         });


  $( "#stateselect" ).change(function() {
     
   console.log(areas);
   var stateid = $(this). children("option:selected"). val();
   console.log(stateid);
   $("#areaselect").empty();
   $.each(areas, function(index, value) {
           if(value.state_id == stateid){
            $("#areaselect").append(new Option(value.name, value.id)); 
           }
            });
    });     




    $( "#target" ).submit(function( event ) {
       
       event.preventDefault();
       $('#loader').show();
       $('#submit').attr('disabled','disabled');
       let email = $('#email').val();
       let name =  $('#name').val() ;
       let skill = $('#skill').val();
       let description = $('#description').val();
       let number = $('#number').val();
       let address = $('#address').val();
       let stateselect = $('#stateselect').val();
       let areaselect = $('#areaselect').val();

     
       $.ajaxSetup({
               headers: { }
           });
$.post('/api/makeJobrequest',   // url
      {        name: name, 
               skill_type: skill,
               email: email,
               description: description,
               phone_number: number,
               state: stateselect,
               area: areaselect,
               address: address,



               
      }, 
      function(data, status, jqXHR) {// success callback

       console.log(status);
       console.log(data);      
             
       if(data.code == "200"){
           $('#loader').hide();
          $('#submit').removeAttr('disabled');
          $('#message').show(); 
          $('#message').text("Your Request is Successfull Please Check your Mail for Further Details." );
       }else{
           $('#error').show(); 
          $('#error').text(data.email[0]);
          $('#loader').hide();
          $('#submit').removeAttr('disabled');
       }


       }).fail(function(jqxhr, settings, ex) {
          $('#err').show();
          $('#loader').hide();
          $('#submit').removeAttr('disabled');
          alert("An Error Occured on the Server.")
        });




       });
     });
   </script>
@endsection