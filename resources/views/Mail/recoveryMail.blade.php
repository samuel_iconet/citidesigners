<html>
<body>
    <h1>Hello {{$user->name}},</h1>
    <p>Please follow this link to recover your password</p>
    <a href="{{ url('/') }}/recover/{{$user->email}}/{{$user->remember_token}} " class="btn btn-info">Recover Password</a>
    <p>Thank you.</p>
</body>
</html>