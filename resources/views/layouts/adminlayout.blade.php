<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Citi Designers</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/admin/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="shortcut icon" href="/front/images/looogo.ico">
  <link rel="stylesheet" href="/admin/dist/css/adminlte.min.css">
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Image Popup CSS -->
<link rel="stylesheet" href="/imagepop/css/style.css">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- Image Popup JS -->
<script src="/imagepop/js/jquery.image-popup.js"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
.zoom {
  padding: 50px;
  transition: transform .2s; /* Animation */
 
  margin: 0 auto;
}

.zoom:hover {
  transform: scale(1.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper" id="app">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Home</a>
      </li>
    
    </ul>

    <!-- SEARCH FORM -->
  

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      
      
      <li class="nav-item">
        <a class="nav-link"  title="Logout"  href="/logout" role="button"><i class="fas fa-sign-out-alt"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="/front/images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Citi Designer</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        </div>
        <div class="info">
          <p class="d-block" style="color:white" id="userEmail"></p>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <a href="/dashboard" class="nav-link active">
                  <i class="nav-icon far fa-circle text-warning"></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/getuserrequests" class="nav-link">
                  <i class="nav-icon far fa-circle text-warning"></i>
                  My Artisan Request
                </a>
              </li>
             
           
          
          
          <li class="nav-item has-treeview" style="display: none" id="artisan">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Artisan 
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
           
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a class="nav-link active" href="/getmergerequest"> 
               <i class="fa  fa-fw fa-tachometer"></i> Merged Request
               </a>
              </li>
              <li class="nav-item">
              <a class="nav-link active" href="/artisandata"> 
             <i class="fa  fa-fw fa-tachometer"></i> Artisan Data
               </a>
              </li>
              <li class="nav-item">
              <a class="nav-link active" href="/useraccount"> 
               <i class="fa  fa-fw fa-tachometer"></i> Artisan Account
                </a>
              </li>
            
            </ul>
          </li>
       
          <li class="nav-item has-treeview" style="display: none" id="supervisor">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
              Supervisor 
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
           
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a class="nav-link active" href="/supervisormerge"> 
                <i class="fa  fa-fw fa-tachometer"></i> Merged Request
              </a>
              </li> 
              <li class="nav-item">
              <a class="nav-link active" href="/supervisordata"> 
               <i class="fa  fa-fw fa-tachometer"></i> Supervisor Data
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link active" href="/useraccount"> 
               <i class="fa  fa-fw fa-tachometer"></i> Supervisor Account
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link active" href="/referals"> 
               <i class="fa  fa-fw fa-tachometer"></i> Referals
                </a>
              </li>
             
            
            </ul>
          </li>
          <li class="nav-item has-treeview" style="display:none" id="admin">
            <a href="" class="nav-link">
            <i class="nav-icon text-warning fas fa-users-cog"></i>
              <p>
                Admin Controls
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a class="nav-link " href="/companyaccount"> 
                <i class="nav-icon text-warning fas fa-money-bill-alt"></i>  <p>Company Account Management</p>
              </a>
              </li>
              <li class="nav-item has-treeview">
              <a class="nav-link">
                  <i class="fas fa-user nav-icon  text-warning"></i>
                  <p>
                    User Management
                    <i class=" fas fa-arrow-down text-warning"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a  class="nav-link" href="/usermanagement">
                      <i class="far fa-dot-circle  nav-icon"></i>
                      <p>General User Management</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a  class="nav-link" href="/artisanmanagement">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Artisan Management</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a  class="nav-link" href="/supervisormanagement">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Supervisor Management</p>
                    </a>
                  </li>
                
                </ul>
              </li> 
              <li class="nav-item has-treeview">
              <a class="nav-link">
                  <i class="fas fa-calendar-day text-warning nav-icon"></i>
                  <p>
                  Request Management
                  <i class=" fas fa-arrow-down text-warning"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  
                  <li class="nav-item">
                    <a  class="nav-link" href="/requestmanagent">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>New Artisan Requests</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a  class="nav-link" href="/mergedrequest">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Merged Requests</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a  class="nav-link" href="/completedrequest">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Completed Requests</p>
                    </a>
                  </li>
                
                </ul>
              </li> 
              <li class="nav-item has-treeview">
              <a  class="nav-link">
                  <i class="fas fa-calendar-day text-warning nav-icon"></i>
                  <p>
                 Ad Management
                  <i class=" fas fa-arrow-down text-warning"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  
                <li class="nav-item" >
                <a class="nav-link" href="/admin/admanagent">
                  <i class="fas fa-shopping-bag nav-icon text-warning"></i>
                  <p>Ad Management</p>
                </a>
              </li>
              <li class="nav-item" >
                <a class="nav-link" href="/admin/categories">
                  <i class="fas fa-shopping-bag nav-icon text-warning"></i>
                  <p>Ad categories</p>
                </a>
              </li>
              <li class="nav-item" >
                <a class="nav-link" href="/admin/properties/management">
                  <i class="fas fa-shopping-bag nav-icon text-warning"></i>
                  <p>Ad Subcategories Properties</p>
                </a>
              </li>
                
                </ul>
              </li> 
            
              
              <li class="nav-item" >
                <a class="nav-link" href="/artisanskills">
                  <i class="fas fa-user-tie nav-icon text-warning"></i>
                  <p>SKills and Location Management</p>
                </a>
              </li>
              <li class="nav-item" >
                <a class="nav-link" href="/admin/contactus">
                  <i class="fas fa-inbox nav-icon text-warning"></i>
                  <p>Contact Us</p>
                </a>
              </li>
              
              <li class="nav-item">
                    <a  class="nav-link" href="/adminticket">
                      <i class="fas text-warning fa-comments nav-icon"></i>
                      <p> Tickets</p>
                    </a>
                  </li>
              <li class="nav-item" >
                <a class="nav-link" href="/tips">
                  <i class="far fa-circle nav-icon text-warning"></i>
                  <p>Tips</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
                <a href="/admanagent" class="nav-link ">
                <i class="nav-icon fas fa-key text-warning"></i>
                  <p>My ads </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/productchat" class="nav-link ">
                <i class="nav-icon fas fa-key text-warning"></i>
                  <p>Product Chats </p>
                </a>
              </li>
          <li class="nav-item">
                <a href="/newpassword" class="nav-link ">
                <i class="nav-icon fas fa-key text-warning"></i>
                  <p>Change Password</p>
                </a>
              </li>
              
              <li class="nav-item">
                    <a  class="nav-link" href="/supportticket">
                    <i class="nav-icon fas fa-question text-warning"></i>
                      <p>Support Ticket</p>
                    </a>
                  </li>
              <li class="nav-item">
                <a href="/logout" class="nav-link ">
                  <i class="nav-icon fas fa-sign-out-alt text-warning"></i>
                 Logout
                </a>
              </li>
        
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
    
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
       
        <!-- /.row -->
        <router-view></router-view>      
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 Citi Designer</strong>
    All rights reserved.
    
  </footer>
</div>
<!-- ./wrapper -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap -->
<script src="/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- AdminLTE App -->
<script src="/admin/dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="/admin/dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="/admin/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="/admin/plugins/raphael/raphael.min.js"></script>
<script src="/admin/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="/admin/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="/admin/plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->

<script type="text/javascript">
        $(document).ready( function () {
            console.log(localStorage.getItem('email'));
            $('#userEmail').text(localStorage.getItem('email'));
          let role = localStorage.getItem('role');
            if (role == '1' ){
              console.log('user section');
    
            }else if(role == 2){
              $("#admin").show();
              console.log('admin section');
              
            }else if (role == 3){
                $("#artisan").show();
                console.log('artisan section')
            }
            else if (role == 4){
                $("#supervisor").show();
                console.log('supervisor section')
            }
            setInterval(() => {
              let token = "Bearer "  + localStorage.getItem('access_token');
           if(token.length > 15){
             console.log('token valid ');
             $.ajax({
            type: "GET",
            headers: {'Content-Type': 'application/json', Authorization: token  },
            url: "/api/validateSession",
            success: function(patientDTO) {
                // console.log("SUCCESS: ", patientDTO);
                 console.log('token valid');
                 
                  },
            error: function(e) {
                 

            console.log("ERROR: ", e);
            
            }
        });
           }else{
             console.log('token invalid');
           }
            }, 20000);
            

         
        })
        </script>
    <script>
    $(document).ready(function() {
        app.dateRangePicker();
      
    });
  </script>
</body>
</html>
