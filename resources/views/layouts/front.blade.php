<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.epic-webdesign.com/tf-launchpage/v3/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 09:59:04 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <title>Citi Designers</title>
    
    <!-- Loading Bootstrap -->
    <link href="/front/css/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Template CSS -->
    <link href="/front/css/style.css" rel="stylesheet">
    <link href="/front/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="/front/css/pe-icon-7-stroke.css">
    <link href="/front/css/style-magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="/front/css/all.min.css">

    <!-- Fonts -->
    <link href="/front/https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400&amp;display=swap" rel="stylesheet">
    <link href="/front/https://fonts.googleapis.com/css2?family=Nunito:wght@600;700&amp;display=swap" rel="stylesheet">

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="/front/images/looogo.ico">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('style')
</head>

<body>

    <!--begin header -->
    <header class="header">

        <!--begin navbar-fixed-top -->
        <nav class="navbar navbar-expand-lg navbar-default navbar-fixed-top">
            
            <!--begin container -->
            <div class="container">

                    <!--begin logo -->
                    <a class="navbar-brand" href="/">Citi Designers</a>
                    <!--end logo -->

                    <!--begin navbar-toggler -->
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <!--end navbar-toggler -->

                    <!--begin navbar-collapse -->
                    <div class="navbar-collapse collapse"  style="">
                        
                        <!--begin navbar-nav -->
                        <ul class="navbar-nav ml-auto">
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register">Register</a></li>

                        </ul>
                        <!--end navbar-nav -->

                    </div>
                    <!--end navbar-collapse -->

            </div>
    		<!--end container -->
            
        </nav>
    	<!--end navbar-fixed-top -->
        
    </header>
    <!--end header -->

    @yield('content')

    <!--begin footer -->
    <div class="footer" style="background-color:blue;margin-top:30px">
            
        <!--begin container -->
        <div class="container">
        
            <!--begin row -->
            <div class="row">
            
                <!--begin col-md-5 -->
                <div class="col-md-5">
                   <p style="color:white">
                       <a  href="/privacy" style="color:white">Privacy policy</a> || <a  href="/terms" style="color:white">Terms and Condition</a>
                   </p>
                    <p style="color:white">© 2020 <span class="template-name">Citi Designers</span></p>
                    
                </div>
                <!--end col-md-5 -->
                
                <!--begin col-md-2 -->
                <div class="col-md-2"></div>
                <!--end col-md-2 -->
                
                <!--begin col-md-5 -->
                <div class="col-md-5">
                                         
                    <!--begin footer_social -->
                    <ul class="footer_social">

                        <li style="color:white">Follw us:</li>

                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>

                        <li><a href="#"><i class="fab fa-pinterest"></i></a></li>

                        <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>

                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>

                        <li><a href="#"><i class="fab fa-skype"></i></a></li>

                        <li><a href="#"><i class="fab fa-dribble"></i></a></li>

                    </ul>

                    <!--end footer_social -->
                    
                </div>
                <!--end col-md-5 -->
                
            </div>
            <!--end row -->

        </div>
        <!--end container -->
               
    </div>
    <!--end footer -->


<!-- Load JS here for greater good =============================-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/front/js/bootstrap.min.js"></script>
<script src="/front/js/jquery.scrollTo-min.js"></script>
<script src="/front/js/jquery.magnific-popup.min.js"></script>
<script src="/front/js/jquery.nav.js"></script>
<script src="/front/js/wow.js"></script>
<script src="/front/js/plugins.js"></script>
<script src="/front/js/custom.js"></script>
<script>


$( document ).ready(function() {
    
   console.log( $(".amount"));
       $(".amount").each(function(index){
           console.log(new Intl.NumberFormat().format($(this).text()));
           $(this).text('₦'+ new Intl.NumberFormat().format($(this).text()) )
        });

});
</script>
  @yield('script')

</body>

<!-- Mirrored from demo.epic-webdesign.com/tf-launchpage/v3/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 09:59:38 GMT -->
</html>