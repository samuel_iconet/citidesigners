<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Empire Klassic</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.html" type="image/x-icon">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="/admin/assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Fonts  -->
<link rel="stylesheet" href="/admin/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="/admin/assets/css/simple-line-icons.css">
<!-- CSS Animate -->
<link rel="stylesheet" href="/admin/assets/css/animate.css">
<!-- Daterange Picker -->
<link rel="stylesheet" href="/admin/assets/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- Calendar demo -->
<link rel="stylesheet" href="/admin/assets/css/clndr.css">
<!-- Switchery -->
<link rel="stylesheet" href="/admin/assets/plugins/switchery/switchery.min.css">
<!-- Custom styles for this theme -->
<link rel="stylesheet" href="/admin/assets/css/main.css">
<link rel="stylesheet" href="/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- Feature detection -->
<script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/js/vendor/html5shiv.js"></script>
<script src="assets/js/vendor/respond.min.js"></script>
<![endif]-->
</head>


<!-- Mirrored from ignitethemes.net/logistica/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 24 Feb 2020 10:38:59 GMT -->

<body>
    <section id="main-wrapper" class="theme-dark-full">
        <div id="app">
            <header id="header">
                <!--logo start-->
                <div class="brand">
                    <a href="index.html" class="logo">
                        <span>Empire </span> Klassic</a>
                </div>
                <!--logo end-->
                <ul class="nav navbar-nav navbar-left">
                    <li class="toggle-navigation toggle-left">
                        <button class="sidebar-toggle" id="toggle-left">
                            <i class="fa fa-bars"></i>
                        </button>
                    </li>
                    <li class="toggle-profile hidden-xs">
                        <a href="/" class="alert alert-info" id="toggle-profile">
                           Home
                        </a>
                    </li>
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown profile hidden-xs">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                                <span class="avatar">
                                </span>
                            <span class="text" id="user_name" ></span>
                            <span class="caret"></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight" role="menu">
                           
                            <li class="divider"></li>
                           
                          
                           
                            <li class="divider"></li>
                            <li>
                                <a href="/logout">
                                    <span class="icon"><i class="fa fa-sign-out"></i>
                                    </span>Logout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="toggle-fullscreen hidden-xs">
                        <button type="button" class="btn btn-default expand" id="toggle-fullscreen">
                            <i class="fa fa-expand"></i>
                        </button>
                    </li>
                   
                </ul>
            </header>
            <!--sidebar left start-->
            <aside class="sidebar sidebar-left">
                <div class="sidebar-profile">
                    <div class="avatar">
                  
                  <i class="on border-dark animated bounceIn"></i><br>
                        
                    </div>
                    <div class="profile-body dropdown">
                        <h4 id="userEmail"></h4>
                    </div>
                </div>
                <nav>
                    <h5 class="sidebar-header">Navigation</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="/dashboard" title="Dashboard">
                                <i class="fa  fa-fw fa-tachometer"></i> Dashboard
                            </a>
                        </li>
                        <li class="active">
                    <router-link to="/getuserrequests"> 
                            <a href="index.html" title="Dashboard">
                            <i class="fa  fa-fw fa-tachometer"></i> My Artisan Request
                        </a>
                    </router-link>

                            
                        </li>
                        <li class="active">
                            <router-link to="/newpassword"> 
                                    <a href="index.html" title="Dashboard">
                                    <i class="fa  fa-fw fa-tachometer"></i> Change PassWord
                                </a>
                            </router-link>
        
                                    
                                </li>
                        <span style="display: none" id="artisan">
                            <li class="active">
                                <router-link to="/getmergerequest"> 
                                    <a href="index.html" title="Dashboard">
                                    <i class="fa  fa-fw fa-tachometer"></i> Merged Request
                                </a>
                            </router-link>
        
                                    
                                </li>
                                <li class="active" >
                                    <router-link to="/artisandata"> 
                                        <a href="index.html" title="Dashboard">
                                        <i class="fa  fa-fw fa-tachometer"></i> Artisan Data
                                    </a>
                                </router-link>
            
                                        
                                    </li>
                        </span>
                        <div style="display: none" id="supervisor">
                            <li class="active">
                                <router-link to="/supervisormerge"> 
                                    <a href="index.html" title="Dashboard">
                                    <i class="fa  fa-fw fa-tachometer"></i> Merged Request
                                </a>
                            </router-link>
        
                                    
                                </li>
                                <li class="active" >
                                    <router-link to="/supervisordata"> 
                                        <a href="index.html" title="Dashboard">
                                        <i class="fa  fa-fw fa-tachometer"></i> Supervisor Data
                                    </a>
                                </router-link>
            
                                        
                                    </li>
                        </div>
                       
                    
                    </ul>
                </nav>
              
            </aside>
            <!--sidebar left end-->
            <!--main content start-->
            <section class="main-content-wrapper">
                <div class="pageheader dark">
                    <h1>Dashboard</h1>
                    <p class="description">Welcome to Empire Klassic</p>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">You are here:</span>
                        <ol class="breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <router-view></router-view>
            </section>
            <!--main content end-->
        </section>
        </div>
       

    <!--/sidebar right end-->
    
    <!--Global JS-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/admin/assets/js/vendor/jquery-1.11.1.min.js"></script>
    <script src="/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/admin/assets/plugins/navgoco/jquery.navgoco.min.js"></script>
    <script src="/admin/assets/plugins/pace/pace.min.js"></script>
    <script src="/admin/assets/plugins/fullscreen/jquery.fullscreen-min.js"></script>
    <script src="/admin/assets/js/src/app.js"></script>
    <!--Page Level JS-->
    <script src="/admin/assets/plugins/countTo/jquery.countTo.js"></script>
    <script src="/admin/assets/plugins/weather/js/skycons.js"></script>
    <script src="/admin/assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="/admin/assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- ChartJS  -->
    <script src="/admin/assets/plugins/chartjs/Chart.min.js"></script>
    <!-- Morris  -->
    <script src="/admin/assets/plugins/morris/js/morris.min.js"></script>
    <script src="/admin/assets/plugins/morris/js/raphael.2.1.0.min.js"></script>
    <!-- Vector Map  -->
    <script src="/admin/assets/plugins/jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/admin/assets/plugins/jvectormap/js/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Gauge  -->
    <script src="/admin/assets/plugins/gauge/gauge.min.js"></script>
    <script src="/admin/assets/plugins/gauge/gauge-demo.js"></script>
    <!-- Calendar  -->
    <script src="/admin/assets/plugins/calendar/clndr.js"></script>
    <script src="/admin/assets/plugins/calendar/clndr-demo.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <!-- Switch -->
    <script src="/admin/assets/plugins/switchery/switchery.min.js"></script>
   <!-- DataTables -->
        <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        
    <!--Load these page level functions-->
    <script type="text/javascript">
        $(document).ready( function () {
            console.log(localStorage.getItem('email'));
            $('#userEmail').text(localStorage.getItem('email'));
          let role = localStorage.getItem('role');
            if (role == '1' ){
              console.log('user section');
    
            }else if(role == 2){
              console.log('admin section');
              
            }else if (role == 3){
                $("#artisan").show();
                console.log('artisan section')
            }
            else if (role == 4){
                $("#supervisor").show();
                console.log('supervisor section')
            }
           
        })
        </script>
    <script>
    $(document).ready(function() {
        app.dateRangePicker();
        app.chartJs();
        app.weather();
        app.spinStart();
        app.spinStop();
    });
    </script>
</body>


<!-- Mirrored from ignitethemes.net/logistica/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 24 Feb 2020 10:40:03 GMT -->
</html>
