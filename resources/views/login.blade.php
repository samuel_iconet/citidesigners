@extends('layouts.front')

@section('content')
<section class="section-grey" id="team" style="margin-buttom:40px">
<div class="container">
            <div class="row align-items-center" >
                <div class="col-lg-4">

                  
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Login</div>
                        <div class="card-body">
                            <form>
                                
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email address</label><input class="form-control rounded-pill" id="email" type="email" /></div>
                                <div class="form-group">
                                    <label class="small text-gray-600" for="leadCapEmail">Password</label><input class="form-control rounded-pill" id="password" type="password" />
                                </div>
                                <button  class="btn btn-primary btn-marketing btn-block rounded-pill mt-4" id="submit" type="submit"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Login</button>
                            </form>
                             <br>   
                             <div class="row">
                             <div class="col-lg-8">
                            <a href="" id="forgotpassword">Forgot Password?</a>
                             </div>
                             <div class="col-lg-4">
                            <span class="pull-right"><a href="/register" class="btn btn-info">Register</a></span>
                             </div>
                             </div>
                             <br><br>
                             <form action="" id='recoveryform' style="display: none">
                             <div class="form-group"><input class="form-control rounded-pill" id="recoveryemail" type="email" placeholder="Enter Email address" required /></div>
                            <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4"><span class="pull-right"><button id="submit1" class="btn btn-info"><i id="loader1" style="display:none  " class="fa fa-circle-o-notch fa-spin"></i>Recover Password</button></span></div>
                            <div class="col-lg-4"></div>
                            </div>
                            </form>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                <img class="navbar-brand" src="/front/images/logo.png" style="height:300px ; width:400px" />
                </div>
            </div>
        </div>
        </section>
@endsection
@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
     // $( "#err" ).hide();
      let token = "Bearer "  + localStorage.getItem('access_token');
      console.log(token);
     $.ajax({
     type: "GET",
     headers: {'Content-Type': 'application/json', Authorization: token  },
     url: "/api/validateSession",
     success: function(patientDTO) {
         console.log("SUCCESS: ", patientDTO);
          window.location.href = '/dashboard';
          },
     error: function(e) {
     console.log("ERROR: ", e);
     e.preventDefault();
    $('#loader1').show();
     }
 });

    $('#forgotpassword').click(function(e) {
        e.preventDefault();
        $('#recoveryform').show();
        
    });

        $( "#recoveryform" ).submit(function(e) {

            e.preventDefault();
            $('#loader1').show();
            $('#submit1').attr('disabled','disabled');
          let email = $('#recoveryemail').val();
           if(email == ""){
            swal("Error!", "Enter Email ", "error"); 

           }  
           else{
            $.ajaxSetup({
                 headers: { }
             });
            $.post('/api/recover/password',   // url
                    {    
                           email : email
                    }, // data to be submit
                // $.post('/api/token',   // url
                //    {        email: email, 
                //             password: password
                            
                //    }, // data to be submit
                    function(data, status, jqXHR) {// success callback
            
                    console.log(status);
                     if(data.code == 200){
                        swal("Success!", "Recovery Mail have been sent to your email", "success"); 

                     }      
                    else if(data.code == 401){
                            swal("Error!", data.error, "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
            
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');

                    }).fail(function(jqxhr, settings, ex) {
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');
                        console.log(jqxhr.status);
                        if(jqxhr.status == 400){
                            swal("Error!", "Invalid Email and Password Provided!", "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
                    });
           }   
  
        });



    $( "#submit" ).click(function(e) {
         e.preventDefault();
      $('#loader').show();
      $('#submit').attr('disabled','disabled');
     let email = $('#email').val();
     let password = $('#password').val();
     let token2 = '';
     console.log(email);
     console.log(password);
 $.ajaxSetup({
                 headers: { }
             });
 $.post('/oauth/token',   // url
        {        username: email, 
                 password: password,
                 grant_type: "password",
                 client_id: "4",
                 client_secret: "JODf1TSf4Wnc4qo2Clx2XMmNqH7vWxgSVHfpjGuH"
        }, // data to be submit
     // $.post('/api/token',   // url
     //    {        email: email, 
     //             password: password
                
     //    }, // data to be submit
        function(data, status, jqXHR) {// success callback
 
         console.log(status);
                // console.log(data.access_token);
               localStorage.setItem('access_token', data.access_token);
                token2  = data.access_token;
                   console.log(token2);
                  token2 = "Bearer "  + token2;
                   console.log(token2);
                   $.ajax({
                   type: "GET",
                   headers: {'Content-Type': 'application/json', Authorization: token2  },
                   url: "/api/user",
                   success: function(patientDTO) {
                       console.log("SUCCESS: ", patientDTO);
                                   localStorage.setItem('name', patientDTO.username);
                                  localStorage.setItem('email', patientDTO.email);
                                  localStorage.setItem('id', patientDTO.id);
                                  localStorage.setItem('role', patientDTO.role);
                                  localStorage.setItem('status', patientDTO.status);
                                  localStorage.setItem('created_at', patientDTO.created_at);
                                  $.ajax({
                                            type: "GET",
                                            headers: {'Content-Type': 'application/json', Authorization: token  },
                                            url: "/api/validateSession",
                                            success: function(patientDTO) {
                                                // console.log("SUCCESS: ", patientDTO);
                                                console.log('token valid');
                                                
                                                },
                                            error: function(e) {
                                                

                                            console.log("ERROR: ", e);
                                            
                                            }
                                        });
                                 if(patientDTO.role == '1'){
                                    window.location.href = '/userdashboard';
                                 }else if(patientDTO.role == '2'){
                                    window.location.href = '/admindashboard';
                                 }else if(patientDTO.role == '3'){
                                    window.location.href = '/artisandashboard';
                                 }
                                 else if(patientDTO.role == '4'){
                                    window.location.href = '/supervisordashboard';
                                 }else{
                                     console.log("herer")
                                 }
                        },
                   error: function(e) {
                      $('#err').show();
                       $('#loader').hide();
                       $('#submit').removeAttr('disabled');
                   // display(e.responseJSON.message);
                   // alert(e);
                   swal("Error!", e.responseJSON.message, "error"); 

                   }
               });
 
 
 
         }).fail(function(jqxhr, settings, ex) {
            $('#err').show();
            $('#loader').hide();
            $('#submit').removeAttr('disabled');
            console.log(jqxhr.status);
            if(jqxhr.status == 400){
                swal("Error!", "Invalid Email and Password Provided!", "error"); 
            }else{
                swal("Error!", "Network Error!", "error"); 
            }
          });
 
 
 
 
 });
 });
   </script>

@endsection