<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.lionode.com/focus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Nov 2020 11:00:46 GMT -->
<head>
<title>Citi Designers</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="e-commerce site well design with responsive view." />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="/front/images/looogo.ico">

<link href="/market/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="/market/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href="/market/css/stylesheet.css" rel="stylesheet">
<link href="/market/css/responsive.css" rel="stylesheet">
<link href="/market/javascript/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
<link href="/market/javascript/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />
<script src="/market/javascript/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="/market/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/market/javascript/template_js/jstree.min.js"></script>
<script type="text/javascript" src="/market/javascript/template_js/template.js"></script>
<script src="/market/javascript/common.js" type="text/javascript"></script>
<script src="/market/javascript/global.js" type="text/javascript"></script>
<script src="/market/javascript/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
</head>
<body>
<div class="preloader loader" style="display: block;"> <img src="/market/image/loader.gif"  alt="#"/></div>

<nav id="menu" class="navbar">
    <div class="nav-inner container">
        <div class="navbar-header"><span id="category" class="visible-xs">Categories</span>
            <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
        </div>
        <div class="navbar-collapse">
            <ul class="main-navigation">
                <li><a href="/"   class="parent"  >Home</a> </li>
                @foreach($categories as $category)
                <li><a href="/marketplace/category/{{$category->id}}"class="active parent">{{$category->name}} ({{$category->count}})</a>
                    <ul>
                        @foreach($category->subcategories as $subcategory)
                        <li><a href="/marketplace/subcategory/{{$subcategory->id}}">{{$subcategory->name}} ({{$subcategory->count}})</a></li>
                      
                        @endforeach
                    </ul>
                </li>
                @endforeach
               
            </ul>
        </div>
    </div>
</nav>
<div class="container col-12">
    <ul class="breadcrumb">
        <li><a href="/marketplace"><i class="fa fa-home"></i></a></li>
        <li><a href="/marketplace/category/{{$setcategory->id}}">{{$setcategory->name}}</a></li>
        @if(isset($setsubcategory))
        <li><a href="/marketplace/subcategory/{{$setsubcategory->id}}"> {{$setsubcategory->name}}</a></li>
        @endif
      </ul>
    <div class="row">
        <div id="column-left" class="col-sm-3 hidden-xs column-left">
            <div class="column-block">
                <div class="columnblock-title">Categories</div>
                <div class="category_block">
                    <ul class="box-category treeview-list treeview">

                       @foreach($categories as $category) <li >
                       <a href="/marketplace/category/{{$category->id}}" class="activSub">{{$category->name}} ({{$category->count}})</a>
                            <ul>
                            @foreach($category->subcategories as $subcategory)
                                <li><a href="/marketplace/subcategory/{{$subcategory->id}}">{{$subcategory->name}} ({{$subcategory->count}})</a></li>
                                @endforeach
                            </ul>
                        </li>
                       @endforeach
                    </ul>
                </div>
            </div>
        
            <h3 class="productblock-title">Latest</h3>
            <div class="row latest-grid product-grid">
                @foreach($latest3 as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"><a href="/itemdetail/{{$product->id}}"><img  style="height: 100px; width: 60px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" /></a>
                            <!-- <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                            </div> -->
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                            <p class="price product-price" style="color:green">₦ {{number_format($product->price, 0, '.', ',')}}</p>
                            <br><p class="price product-price" >{{$product['statedata']->name}}  >> {{$product['areadata']->name}}</p>
                                    
                        </div>
                        <!-- <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                        </div> -->
                    </div>
                </div>
              @endforeach
            </div>
            <h3 class="productblock-title">Specials</h3>
            <div class="row special-grid product-grid">
                @foreach($special3 as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
                    <div class="product-thumb transition">
                        <div class="image product-imageblock"><a href="/itemdetail/{{$product->id}}"><img style="height: 100px; width: 60px;" src="/storage/product/{{$product->images[0]->image}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" /></a>
                            <!-- <div class="button-group">
                                <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List"><i class="fa fa-heart-o"></i></button>
                                <button type="button" class="addtocart-btn">Add to Cart</button>
                                <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                            </div> -->
                        </div>
                        <div class="caption product-detail">
                            <h4 class="product-name"><a href="#" title="iPod Classic">{{$product->name}}</a></h4>
                            <p class="price product-price" style="color:green">₦ {{number_format($product->price, 0, '.', ',')}}</p>
                            <br><p class="price product-price" >{{$product['statedata']->name}}  >> {{$product['areadata']->name}}</p>
                                    
                        </div>
                        <!-- <div class="button-group">
                            <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                            <button type="button" class="addtocart-btn" >Add to Cart</button>
                            <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                        </div> -->
                    </div>
                </div>
              @endforeach
         
            </div>
        </div>
        <div id="content" class="col-sm-9">
            <h2 class="category-title">{{$setcategory->name}}

                @if(isset($setsubcategory))
                    >> {{$setsubcategory->name}}
                @endif
            </h2>
            <div class="row category-banner">
              <div class="col-sm-12 category-image"><img style="height: 192px; width: 937px;" src="/storage/category/{{$setcategory->image}}" alt="Desktops" title="Desktops" class="img-thumbnail" /></div>
              <hr class="col-md-12 alert-warning">
              <div class="col-sm-12 category-desc">{{$setcategory->description}}</div>
              <hr class="col-md-12 alert-warning">

            </div>
            <div class="category-page-wrapper">
              <div class="col-md-6 list-grid-wrapper">
                <div class="btn-group btn-list-grid">
                  <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                  <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                </div>
                </div>
             <form action="/marketplace/sort/" method="get" >
              <div class="col-md-4 text-right ">
                <label class="control-label" for="input-limit">State:</label>
                <div class="limit">
                <input type="hidden" value="{{$setcategory->id}}" name="category_id">
                @if(isset($setsubcategory))
                <input type="hidden" value="{{$setsubcategory->id}}" name="subcategory_id">
                @endif
                  <select name="state" required class="form-control" id="stateselect">
                    
                  
                  </select>
                </div>
              </div>
              <div class="col-md-4 text-right ">
                <label class="control-label" for="input-sort">Area:</label>
                <div class="sort-inner">
                  <select name='area' required class="form-control" id="areaselect">
                 
                  
                  </select>
                </div>
                @csrf
              </div>
              <div class="col-md-4 text-right ">
            <button class="btn btn-warning" type="submit">search</button>
              </div>
             </form>
            </div>
            <br />
            <div class="grid-list-wrapper">
            @if(count($products)> 0)
              @foreach($products as $product)
              <div class="product-layout product-list col-xs-12">
                <div class="product-thumb">
                  <div class="image product-imageblock"> <a href="/itemdetail/{{$product->id}}"> <img  style="width: 220px; height: 290px;" src="/storage/product/{{$product->images[0]->image}}" alt="iPod Classic" title="{{$product->name}}" class="img-responsive" /> </a>
                    
                  </div>
                  <div class="caption product-detail">
                    <h4 class="product-name"> <a href="/itemdetail/{{$product->id}}" title="iPod Classic">{{$product->name}}</a> </h4>
                    <p class="product-desc"></p>
                    <p class="price product-price" style="color:green">₦ {{number_format($product->price, 0, '.', ',')}} </p>
                    <br><p class="price product-price" >{{$product['statedata']->name}}  >> {{$product['areadata']->name}}</p>
                                    
                  </div>
   
                </div>
              </div>

              @endforeach
           @else
           <h5 class="alert alert-danger">No ads!!!</h5>
           @endif
            </div>
            <div class="category-page-wrapper">
                {!! $products->render() !!}
            </div>
          </div>
    </div>
</div>

<div class="footer-bottom">
    <div class="container">
        <div class="copyright">Powered By<a class="yourstore" href="http://www.lionode.com/">Citi Designers &copy; 2020 </a> </div>
        <div class="footer-bottom-cms">
            <div class="footer-payment">
                <ul>
                    <li class="mastero"><a href="#"><img alt="" src="/market/image/payment/mastero.jpg"></a></li>
                    <li class="visa"><a href="#"><img alt="" src="/market/image/payment/visa.jpg"></a></li>
                    <li class="currus"><a href="#"><img alt="" src="/market/image/payment/currus.jpg"></a></li>
                    <li class="discover"><a href="#"><img alt="" src="/market/image/payment/discover.jpg"></a></li>
                    <li class="bank"><a href="#"><img alt="" src="/market/image/payment/bank.jpg"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready( function () {
      let areas = {};
          $.ajaxSetup({
                  headers: { }
              });
  $.get('/api/getstates', 
         function(data, status, jqXHR) {// success callback
  
          console.log(status);
          console.log(data);      
                
          if(data.code == "200"){
              
              $.each(data.states, function(index, value) {
              console.log(value);
               areas = data.areas;
              $("#stateselect").append(new Option(value.name, value.id));
              if(index == 0){
  
              $.each(areas, function(index, area_value) {
             if(area_value.state_id == value.id){
              $("#areaselect").append(new Option(area_value.name, area_value.id)); 
             }
              });
      
  
              }
              });
              
          }else{
           
          }
  
  
          }).fail(function(jqxhr, settings, ex) {
            
             alert("An Error Occured on the Server.")
           });
  
  
    $( "#stateselect" ).change(function() {
       
     console.log(areas);
     var stateid = $(this). children("option:selected"). val();
     console.log(stateid);
     $("#areaselect").empty();
     $.each(areas, function(index, value) {
             if(value.state_id == stateid){
              $("#areaselect").append(new Option(value.name, value.id)); 
             }
              });
      }); 
  
  })
  </script>
</body>

<!-- Mirrored from html.lionode.com/focus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Nov 2020 11:03:04 GMT -->
</html>
