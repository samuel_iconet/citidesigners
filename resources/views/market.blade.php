@extends('layouts.front')

@section('content')

<section class="section-grey" id="team">

<!--begin container-->
<div class="container">

    <!--begin row-->
    <div class="row">
  
        <!--begin col-md-12 -->
        <div class="col-md-12 text-center">

            <h2 class="section-title" style="margin-top: 30px">Market Place</h2>

            
        </div>
        <!--end col-md-12 -->
        @if(count($items) > 0)
            @foreach ($items as $item)

           <div class="col-sm-12 col-md-4 margin-top-30">

            <img src="/storage/store/{{$item->profile_image}}" class="team-img width-100" alt="pic">
           
            <div class="team-item">
            
                <h3><a href="/itemdetail/{{$item->id}}">{{$item->name}}</a></h3>
                
                <div class="team-info"><h6 style=" color: green;">₦ {{$item->amount}}</h6></div>

                <p>{{$item->description}}.</p>
                
            </div>

        </div> 
            @endforeach 
             
            @else
            <div class="col-lg-4 mb-5 mb-lg-0">
                <hr>
            <h3>No Item Found!!</h3>
            <hr><br><br>
            </div>
            @endif
        <!--begin team-item -->
      
   
<!--end container-->

</section>
<!--end team section-->


@endsection
