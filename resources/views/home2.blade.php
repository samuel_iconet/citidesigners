@extends('layouts.front')

@section('content')

<header class="page-header page-header-dark bg-img-repeat bg-secondary" style='background-image: url("assets/img/pattern-shapes.png")'>
   
  
    <div id="carouselExampleIndicators" class="carousel slide" data-interval="15000" data-ride="carousel">
  
  <div class="carousel-inner">
    <div class="carousel-item active" >
        <div class="row align-items-center">
            <div class="col-lg-1"></div>
            <div class="col-lg-3">
                <img src="/logo.png" alt="">
                </div>
                <div class="col-lg-2"></div>
                  <div class="col-lg-4">
                   
                      <h1 class="page-header-title">EMPIRE KLASSIC</h1>
                      <p class="page-header-text">        welcome to Empire klassic. City designers. Where you get the best expert in building construction and Interior finishes.
.                    </p>
                     
                  </div>
                <div class="col-lg-2"></div>

                  </div>
    </div>
    <div class="carousel-item">
      <div class="row align-items-center">
          <div class="col-lg-3">
              </div>
                <div class="col-lg-6">

                    <h1 class="page-header-title">EMPIRE KLASSIC</h1>
                    <p class="page-header-text">We give the best space design that enhances the lifestyle of the users. Economically and functional worldwide,  let's make your home dreams come alive.</p>
                   
                </div>
                </div>
    </div>
    <div class="carousel-item">
          <div class="row align-items-center">
          <div class="col-lg-3">
              </div>
                <div class="col-lg-6">

                    <h1 class="page-header-title">EMPIRE KLASSIC</h1>
                    <p class="page-header-text">Welcome to City Designers, the EMPIRE of KLASSIC building design.</p>
                   
                </div>
                </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>
<header class="page-header page-header-dark bg-img-repeat bg-secondary" style='background-image: url("assets/img/pattern-shapes.png")'>
    <div class="page-header-content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">

                    <h1 class="page-header-title">EMPIRE KLASSIC</h1>
                    <p class="page-header-text">Welcome to City Designers, the EMPIRE of KLASSIC building design.</p>
                   
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Sign up as An Artisan</div>
                        <div class="card-body">
                            <h3 class="alert alert-success" id="message"  style="display: none"></h3>
                            <h3 class="alert alert-danger" id="error"  style="display: none"></h3>

                            <form id="target" action="/sum">
                                <div class="form-row">
                                    <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapFirstName">First Name</label><input required  class="form-control rounded-pill" id="fname" type="text" /></div>
                                    <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapLastName">Last Name</label><input required  class="form-control rounded-pill" id="lname" type="text" /></div>
                                </div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email address</label><input required  class="form-control rounded-pill" id="email" type="email" /></div>
                                <div class="form-group">
                                    <label class="small text-gray-600" for="leadCapEmail">Password</label><input required minlength="6" class="form-control rounded-pill" id="password" type="password" />
                                </div>
                                <button type="submit" id="submit"  class="btn btn-primary btn-marketing btn-block rounded-pill mt-4"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Sign Up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>
<section class="bg-light pt-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5">
                <h2>OUR MARKET PLACE</h2>
                <p class="lead">A place were the best item Furniture Can be found!</p>
                <div class="row"><div class="col-md-4 offset-4"><a href="/market" class="btn btn-primary ">Visit Market Place </a></div></div>

            </div>
        </div>
        <div class="row mb-n10 z-1">
           @if(count($items) > 0)
           @foreach ($items as $item)
           <div class="col-lg-4 mb-5 mb-lg-0">
           <a class="card lift h-100" href="/itemdetail/{{$item->id}}"
           ><div class="card-flag card-flag-dark card-flag-top-right">{{$item->created_at->diffForHumans()}}</div>
           <img class="card-img-top" src="/storage/store/{{$item->profile_image}}" alt="..."/>
                <div class="card-body">
                    <h3 class="text-primary mb-0 amount">{{$item->amount}}</h3>
                <div class="small text-gray-800 font-weight-500">{{$item->name}}</div>
                <div class="small text-gray-500">{{$item->description}}</div>
                </div>
                <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                    <div class="small text-gray-500">View listing</div>
                    <div class="small text-gray-500"><i data-feather="arrow-right"></i></div></div
            ></a>
        </div>    
           @endforeach 
            
           @else
           <div class="col-lg-4 mb-5 mb-lg-0">
               <hr>
           <h3>No Item Found!!</h3>
           <hr><br><br>
           </div>
           @endif
        </div>
    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</section>
<section class="bg-light pt-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5">
                <h2>Artisan Palace</h2>
                <h3>WHAT DO YOU WANT DONE?</h3>
                <p class="lead">Get the Best Artisans for your Project.</p>
                <div class="row"><div class="col-md-4 offset-4"><a href="/artisans" class="btn btn-primary ">Request For An Artisan </a></div></div>

            </div>
        </div>
        <div class="row mb-n10 z-1">

            <div class="card-columns card-columns-portfolio">
              
                @if(count($skills) > 0)
                @foreach ($skills as $skill)
                 
                <a class="card card-portfolio" href="/requestartisan"
                ><img class="card-img-top" src="/storage/skill/{{$skill->image}}" alt="..." />
            <div class="card-body"><div class="card-title">{{$skill->name}}</div></div></a
            >
            
                @endforeach 
                 
                @else
               
                
                <h3>No Skills Found!!</h3>
                
                <hr><hr><br><br><br><br><br>
                @endif
                
               
            </div>
           
        </div>
        
    </div>
    
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</section>


@endsection
@section('tips')
<br><br>
<h1  style="color: white" >Daily Tips</h1>
<hr><hr>
<div id="carouselExampleIndicators1"  data-interval="15000"  class="carousel slide" data-ride="carousel">
<?php $count = 0 ?>
<div class="carousel-inner">
@foreach($tips as $tip)
@if($count == 0)
<div class="carousel-item active"  >
@else
<div class="carousel-item"  >
@endif
<?php $count++ ?>
<h2 class="alert alert-warning ">{{$tip->title}} <span class="pull-right">{{$tip->created_at->diffForHumans()}}</span></h2>
<p>{{$tip->body}}</p>
</div>
@endforeach

</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div>
@endsection
@section('script')
<script>
    $('.carouselExampleIndicators').carousel({
  interval: 1000 * 15
});
$('.carouselExampleIndicators1').carousel({
  interval: 1000 * 20
});
</script>
<script type="text/javascript">

     $( document ).ready(function() {
       console.log( $(".amount"));
       $(".amount").each(function(index){
           console.log(new Intl.NumberFormat().format($(this).text()));
           $(this).text('₦'+ new Intl.NumberFormat().format($(this).text()) )
        });
        $( "#target" ).submit(function( event ) {
       
        event.preventDefault();
        $('#loader').show();
        $('#submit').attr('disabled','disabled');
        let email = $('#email').val();
        let username =  $('#fname').val() + " " + $('#lname').val();
        let password = $('#password').val();
    
        $.ajaxSetup({
                headers: { }
            });
$.post('/api/register',   // url
       {        username: username, 
                password: password,
                email: email,
                role: 3
                
       }, 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            $('#loader').hide();
           $('#submit').removeAttr('disabled');
           $('#message').show(); 
           $('#message').text("Registration Successfull please Login to your Mail to complete the process." );
        }else{
            $('#error').show(); 
           $('#error').text(data.email[0]);
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
        }


        }).fail(function(jqxhr, settings, ex) {
           $('#err').show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
           alert("An Error Occured on the Server.")
         });




        });

     });
   </script>
@endsection