<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use App\User;
use App\product;
class adcheck 
{
    public function cancelExpiredAds($products){

        foreach($products as $product){
            $ad = product::find($product->id);
            $fdate = $ad->created_at;
            $tdate = now();
            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');
            $daycheck = 7;
            if($ad->ad_type == 'free'){
                $daycheck = 7;
            }else if($ad->ad_type == 'standard'){
                $daycheck = 14;
            }
            else if($ad->ad_type == 'premium'){
                $daycheck = 30;
            }

            if($days > $daycheck){
                $ad->status = 'Expired';
                $ad->save();
            }

        }
        return 'done';
    }
}