<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class isSupervisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role == 2 || Auth::User()->role == 4){
            return $next($request);
        }else{
            return redirect('/api/adminauth/error');
        }
    }
}
