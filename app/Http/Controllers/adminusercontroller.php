<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use App\tip;
use App\supervisorData;
use App\user_data;
use App\querry;
use App\artisan;
use App\payments;
use App\message;
use App\merge;
use App\job;
use App\proof;

class adminusercontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function deleteUser(request $request){
        $validator = Validator::make($request->all(), [
            "user_id" =>  "required",
        
          
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      $user = User::findOrFail($request->user_id);
     
      if($user->role == '3'){
          $userdata = artisan::where('user_id' , $user->id)->first();
          if(isset($userdata)){
              $userdata->status = 'deleted';
              $userdata->address = 'deleted';
              $userdata->area_name = 'deleted';
              $userdata->state_name = 'deleted';
              $userdata->phone_number = 'deleted';
              $userdata->save();
          }
      }
      else if($user->role == '4'){
        $userdata = supervisorData::where('user_id' , $user->id)->first();
        if(isset($userdata)){
            $userdata->status = 'deleted';
            $userdata->address = 'deleted';
            $userdata->area_name = 'deleted';
            $userdata->state_name = 'deleted';
            $userdata->phone_number = 'deleted';
            $userdata->save();
        }
    } else if($user->role == '1'){
        $userdata = user_data::where('user_id' , $user->id)->first();
        if(isset($userdata)){
            $userdata->address = 'deleted';
            $userdata->area = 'deleted';
            $userdata->state = 'deleted';
            $userdata->phone_number = 'deleted';
            $userdata->save();
        }
    }
    $user->email = "deleted".$user->email;
    $user->deleted = "1";
    $user->status = "deleted";
    $user->save();
    $response['code'] = 200;
    return response()->json($response ,200); 
    
    }
    public function rejectUser(request $request){
        $validator = Validator::make($request->all(), [
            "user_id" =>  "required",
           "querry" => "required"
          
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      $querry = new querry;
      $user =  User::findOrFail($request->user_id);
      $querry->user_id = $user->id;
      $querry->user_type = $user->role;
      $querry->message = $request->querry;
      $querry->status = 'active';
      $querry->save();

      if($user->role == '3'){
          $user->status = '0';
          $userdata = artisan::where('user_id' , $user->id)->first();
          if(isset($userdata)){
              $userdata->status = 'rejected';
              $userdata->save();
              $user->save();
              $message = new message;
              $msg = "Dear ".$user->name.", Your Artisan Account with CitiDesigners has been Querried Please Login to confirm.";
              $response['text'] = $message->sendSms($userdata->phone_number , $msg);
              $response['code'] = 200;
              return response()->json($response ,200); 
          }
          $response['code'] = 201;
          return response()->json($response ,200); 
      }
      if($user->role == '4'){
        $user->status = '0';
        $userdata = supervisorData::where('user_id' , $user->id)->first();
        if(isset($userdata)){
            $userdata->status = 'rejected';
            $userdata->save();
            $user->save();
            $message = new message;
            $msg = "Dear ".$user->name.", Your Supervisor Account with CitiDesigners has been Querried please Login to confirm.";
            $response['text'] = $message->sendSms($userdata->phone_number , $msg);
            $response['code'] = 200;
            return response()->json($response ,200); 
        }
        $response['code'] = 201;
        return response()->json($response ,200); 
    }    

    }
    public function approveUser(request $request){
        $validator = Validator::make($request->all(), [
            "user_id" =>  "required",
           
          
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      $user =  User::findOrFail($request->user_id);
      if($user->role == '3'){
          $user->status = '1';
          $userdata = artisan::where('user_id' , $user->id)->first();
          if(isset($userdata)){

              $userdata->status = 'approved';
              $userdata->save();
              $user->save();
              if($userdata->ref_code != null  && $userdata->ref_status == 'UNUSED'){
                  $referal = User::where(['ref_code' => $userdata->ref_code , 'role' => '4'])->first();
                  if(isset($referal)){
                    $payment = new payments;
                    $payment->debitCompany($referal->id , 200 , 'Registration fees Commission for : ' .$user->name);
                    $userdata->ref_status = 'USED';
                    $userdata->save();

                  }
              }
              $this->automergeAll();
              $message = new message;
              $msg = "Dear ".$user->name.", Your Artisan Account with CitiDesigners has been activated successfully.";
              $response['text'] = $message->sendSms($userdata->phone_number , $msg);
              $response['code'] = 200;
              return response()->json($response ,200); 
          }
          $response['code'] = 201;
          return response()->json($response ,200); 
      }
      if($user->role == '4'){
        $user->status = '1';
        $userdata = supervisorData::where('user_id' , $user->id)->first();
        if(isset($userdata)){
            $userdata->status = 'approved';
            $userdata->save();
            $user->save();
            $message = new message;
            $msg = "Dear ".$user->name.", Your Supervisor Account with CitiDesigners has been activated successfully.";
            $response['text'] = $message->sendSms($userdata->phone_number , $msg);
            $this->automergeAll();
            return 'done';
            
        }
        $response['code'] = 201;
        return response()->json($response ,200); 
    }    

    }
    public function automergeAll(){
        $jobs = job::where('status' , 'Pending')->get();
        return $jobs; 
        foreach($jobs as $job){
         $merge = new merge;
         $merge->mergeJob( $job->id);
        }
        $response['code'] = 200;
        return response()->json($response ,200);
        
     }
    public function deletetip(request $request){
        $validator = Validator::make($request->all(), [
            "id" =>  "required",
          
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }  
      $tip = tip::findOrFail($request->id);
      
      $tip->delete();
      $response['code'] = 200;
      return response()->json($response ,200);   
    }
    public function fliptip(request $request){
        $validator = Validator::make($request->all(), [
            "id" =>  "required",
          
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }  
      $tip = tip::findOrFail($request->id);
      if($tip->status  == '1' ){
          $tip->status = '0';
      }else{
          $tip->status = '1';
      }
      $tip->save();
      $response['code'] = 200;
      return response()->json($response ,200);
    }
    //gettips
    public function gettips(){
        $tips = tip::orderBy('created_at', 'DESC')->get();;
        $response['code'] = 200;
      $response['tips'] = $tips;
      return response()->json($response ,200);

    }
    public function addtip(request $request){
        $validator = Validator::make($request->all(), [
            "title" =>  "required",
            "body" =>  "required",
          
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $tip = new tip;
      $tip->title = $request->title;
      $tip->body = $request->body;
      $tip->status = '1';

      if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'tips'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('tips', $filePath, 'public');
        $image_name = $filePath;
      $tip->image = $image_name;

    }
      $tip->save();

      $response['code'] = 200;
      $response['tip'] = $tip;
      return response()->json($response ,200); 
    }
    
    public function saveuser(request $request){
        $user = User::findOrFail($request->id);
        $user->role = $request->role;
        $user->save();
        $response['code'] = 200;
        return response()->json($response ,200); 
    }
    public function getsupervisors(){
    $supervisors = $this->detectActive(User::where(['role' => '4' ,'deleted' => '0'])->get());
    foreach($supervisors as $supervisor){
        $supervisor['data'] = supervisorData::where('user_id' , $supervisor->id)->first();
        $supervisor['proofs'] = proof::where(['item_id' => $supervisor->id , 'item_category' => 'supervisor'])->get();

    }
    $response['code'] = 200;
    $response['supervisors'] = $supervisors;
    return response()->json($response ,200); 
    }
    public function getartisans(){
        $artisans = $this->detectActive(User::where(['role' => '3' ,'deleted' => '0'])->get());
        foreach($artisans as $artisan){
            $artisan['data'] = artisan::where('user_id' , $artisan->id)->first();
            $artisan['proofs'] = proof::where(['item_id' => $artisan->id , 'item_category' => 'artisan'])->get();
        }
        $response['code'] = 200;
        $response['artisans'] = $artisans;
        return response()->json($response ,200); 
        }
    public function getusers(){
        $message = new message;
       
    $response['allusers'] = $this->detectActive(User::where('deleted', '0')->get());
    $response['all_users_contact'] =  $message->getNumbers($response['allusers']);
    $response['all_users_email'] =  $message->getEmail($response['allusers']);
    $response['admins']= $this->detectActive(User::where(['role' =>  '2','deleted' => '0' ])->get());
    $response['admin_contact'] =  $message->getNumbers($response['admins']);
    $response['admin_email'] =  $message->getEmail($response['admins']);
    $response['users'] = $this->detectActive(User::where(['role' => '1' ,'deleted' => '0'])->get());
    $response['user_contact'] =  $message->getNumbers($response['users']);
    $response['user_email'] =  $message->getEmail($response['users']);
    $response['artisans'] = $this->detectActive(User::where(['role' => '3' ,'deleted' => '0'])->get());
    $response['artisan_contact'] =  $message->getNumbers($response['artisans']);
    $response['artisan_email'] =  $message->getEmail($response['artisans']);
    $response['supervisors'] = $this->detectActive(User::where(['role' => '4' ,'deleted' => '0'])->get());
    $response['supervisor_contact'] =  $message->getNumbers($response['supervisors']);
    $response['supervisor_email'] =  $message->getEmail($response['supervisors']);
    $response['code'] = 200;
    return response()->json($response ,200);
    }
    public function detectActive($users){
        foreach($users as $user){
            $diff = strtotime(now()) - strtotime($user->updated_at);
            if($diff < 60)
            {
                $user['active'] = 'active';
            }else{
                $user['active'] = 'inactive'; 
            }

        }
        return $users;
    }
    public function flipuser(request $request){
        $user = User::findOrFail($request->id);
        if($user->status == 1) {
            $user->status = 0;

        }else{
            $user->status = 1;
        }
        $user->save();
        $response['code'] = 200;
        return response()->json($response ,200);
        }
}
