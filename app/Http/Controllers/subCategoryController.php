<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\subcategory;
use Validator;
class subCategoryController extends Controller
{   //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getsubcategories($category_id){
        
            $category['subcategories'] = subcategory::where('category_id' , $category_id)->orderBy('id' ,'DESC')->get();
       
        $response['code'] = 200;
        $response['categories'] = $categories;
        return response()->json($response ,200);
    }
    public function createsubCategory(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required",
             "category_id" => "required"
        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    
    $subcategory = new subcategory;
    $subcategory->name = $request->name;
    $subcategory->category_id = $request->category_id;
    $subcategory->save();

    $response['code'] = 200;
return response()->json($response ,200);
}
public function editsubCategory(request $request){
    $validator = Validator::make($request->all(), [
        "name" =>  "required",
        "subcategory_id" =>  "required",

         
    
]);

if ($validator->fails()) {

     return $validator->messages();
}
$subcategory = subcategory::findOrFail($request->subcategory_id);


$subcategory->name = $request->name;

$subcategory->save();
$response['code'] = 200;
return response()->json($response ,200);

}
public function flipState($id){
    $subcategory = subcategory::findOrFail($id);
    if($subcategory->status == '1'){
        $subcategory->status = '0';
        $subcategory->save();
    }else{
        $subcategory->status = '1';
        $subcategory->save();
    }
    $response['code'] = 200;
    return response()->json($response ,200);
}
}