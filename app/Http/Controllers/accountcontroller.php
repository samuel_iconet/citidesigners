<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\account;
use App\Transaction;
use App\payments;
use App\withdrawalRequest;
use App\User;
use Auth;
use Validator;
class accountcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
   
    public function getAccount(){
        $account = account::where('user_id' , Auth::User()->id)->first();
        if(!isset($account)){
            $payment = new payments;
         $account =    $payment->createAccount(Auth::User()->id);
 
        }
        $transactions = Transaction::where('to_account_id' ,Auth::User()->id )->orWhere('from_account_id' ,Auth::User()->id )->get();
        $withdrawals = withdrawalRequest::where('user_id' ,Auth::User()->id )->get();
    
        foreach($transactions as $transaction){
            if($transaction->to_account_id == Auth::User()->id){
                $transaction['type'] ='credit';
            }else{
                $transaction['type'] ='debit'; 
            }
            if($transaction->to_account_id == '0'){
                $transaction['to_name'] = 'Citi Designers';
            }else{
                $user = User::find($transaction->to_account_id);
                $transaction['to_name'] = $user->name;
            }
            if($transaction->from_account_id == '0'){
                $transaction['from_name'] = 'Citi Designers';
            }else{
                $user = User::find($transaction->from_account_id);
                $transaction['from_name'] = $user->name;
            }
        }
        $response['code'] = 200;
        $response['transactions'] = $transactions;
        $account['total_balance'] = $account->available_balance +  $account->pending_balance;
        $response['account'] = $account;
        $response['withdrawals'] = $withdrawals;
    
        return response()->json($response , 200);  
    }
   public function cancelwithdrawalrequest(request $request){
    $validator = Validator::make($request->all(), [
        "id"=>  "required",
       
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
  $withdrawal_request = withdrawalRequest::findOrFail( $request->id);
  if($withdrawal_request->status == 'Pending'){
      $account = account::where(['id' => $withdrawal_request->account_id , 'user_id' => Auth::User()->id])->first();
      if(isset($account)){
        $account->available_balance = $account->available_balance +  $withdrawal_request->amount;
        $account->pending_balance = $account->pending_balance -  $withdrawal_request->amount;
        $account->save();  
        $withdrawal_request->status = "Cancelled";
        $withdrawal_request->save();
      }
    
  }
  $response['code'] = 200;
  return response()->json($response , 200);  
   }
    public function withdrawalrequest(request $request){
        $validator = Validator::make($request->all(), [
            "amount" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $account = account::where('user_id' , Auth::User()->id)->first();
      if(isset($account)){
        if($request->amount <= $account->available_balance){
            $withdrawal_request =  new withdrawalRequest;
            $withdrawal_request->user_id = Auth::User()->id;
            $withdrawal_request->account_id = $account->id;
            $withdrawal_request->amount = $request->amount;
            $withdrawal_request->status = 'Pending';
            $withdrawal_request->save();
            $account->available_balance = $account->available_balance -  $request->amount;
            $account->pending_balance = $account->pending_balance +  $request->amount;
            $account->save();
            $response['code'] = 200;
            return response()->json($response , 200); 
        }
        $response['code'] = 404;
        $response['error'] = "Insuficient Balance";
            return response()->json($response , 200);
      }
      $response['code'] = 404;
      $response['error'] = "Invalid Transaction";
          return response()->json($response , 200);
    }
}
