<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\job;
use App\item;
use App\order;
use App\jobmerge;
use App\user_data;
use App\artisan;
use App\supervisorData;
use App\contact_us;
use App\message;
use App\merge;

class adminhomecontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getuserEmail($role){
       $merge = new merge;                                                                                                                       
    //  return $artisans =  $merge->getClosestArtisan($role);
//  return $artisans =  $merge->remergeArtisan($role);
         return $getSettings =  $merge->getSettings();
     
    //  return $artisans =  $merge->getClosestSupervisor($role);
    //  return $artisans =  $merge->mergeJob($role);
        // $message = new message;
        
        // if($role == 'all'){
        //     $response['users_contact'] =  $message->getEmail(User::where('deleted' , '0')->get());
        // }else if($role == 'admin'){
        //     $response['users_contact'] =  $message->getEmail(User::where(['deleted' => '0' , 'role' => '2'])->get());

        // }else if($role == 'supervisor'){
        //     $response['users_contact'] =  $message->getEmail(User::where(['deleted' => '0' , 'role' => '4'])->get());

        // }else if($role == 'artisan'){
        //     $response['users_contact'] =  $message->getEmail(User::where(['deleted' => '0' , 'role' => '3'])->get());

        // }else if($role == 'user'){
        //     $response['users_contact'] =  $message->getEmail(User::where(['deleted' => '0' , 'role' => '1'])->get());

        // }
        // $response['code'] = 200;
        // return response()->json($response ,200);
    }
    public function getuserContact($role){
        if($role == 'all'){
            $response['users_contact'] = $this->getNumbers(User::where('deleted' , '0')->get());
        }else if($role == 'admin'){
            $response['users_contact'] = $this->getNumbers(User::where(['deleted' => '0' , 'role' => '2'])->get());

        }else if($role == 'supervisor'){
            $response['users_contact'] = $this->getNumbers(User::where(['deleted' => '0' , 'role' => '4'])->get());

        }else if($role == 'artisan'){
            $response['users_contact'] = $this->getNumbers(User::where(['deleted' => '0' , 'role' => '3'])->get());

        }else if($role == 'user'){
            $response['users_contact'] = $this->getNumbers(User::where(['deleted' => '0' , 'role' => '1'])->get());

        }
        $response['code'] = 200;
        return response()->json($response ,200);
    }
    public function saveSetting(request $request){
        // return $request->all();
        $merge = new merge;                                                                                                                       
         $response['settings'] =  $merge->setSetting($request);
         $response['code'] = 200;
         return response()->json($response ,200);
    }
    public function gethome(){
        $response['admins']  = count(User::where(['role' => '2' , 'deleted' => 0])->get());
        $response['users'] = count(User::where(['role' => '1' , 'deleted' => 0])->get());
        $response['artisans'] = count(  User::where(['role' => '3', 'deleted' => 0])->get());
        $response['supervisors'] = count(  User::where(['role' => '4', 'deleted' => 0])->get());
        $response['pending_requests'] = count(  job::where('status' , 'pending')->get());
        $response['merged_requests'] = count(  jobmerge::where('status' ,'!=', 'Cancelled')->get());
        $response['complete_requests'] = count(  job::where('status' , 'completed')->get());
        $response['orders'] = count(  order::all());
        $response['items'] = count(  item::all());
        $merge = new merge;                                                                                                                       
         $response['settings'] =  $merge->getSettings();
        $response['code'] = 200;
        return response()->json($response ,200);

    }
    public function getcontactus(request $request ){
        $response['contacts']  = contact_us::all();
        $response['code'] = 200;
        $response['ip'] = $this->getIp();
        return response()->json($response ,200);
    }
    public function getClientIps(){ 
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
 

}
