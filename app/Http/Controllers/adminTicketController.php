<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ticket;
use App\chat;
use App\User;
use Validator;
class adminTicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getTickets(){
        $tickets = ticket::orderBy('id', 'DESC')->get();
        if(count($tickets) > 0){
            foreach($tickets as $ticket){
                $ticket['messages'] = chat::where('ticket_id' , $ticket->id)->get();
                $ticket['user'] = user::find( $ticket->user_id)->first();
                if(count( $ticket['messages']) > 0){
                    foreach($ticket['messages'] as $message){
                        $message['user'] = User::find($message->sender_id);
                    }  
                }
                $ticket['unread'] = count(chat::where(['ticket_id' => $ticket->id , 'status' => 'unread'])->where('sender_id' , '<>' , Auth::User()->id)->get());
            }
        }
        $response['code'] = 200;
        $response['tickets'] = $tickets;
        return response()->json($response ,200);
        }
    public function closeTicket($ticket_id){
        $ticket = ticket::findOrFail($ticket_id);
        $ticket->status = 'Closed';
        $ticket->save();
        $response['code'] = 200;
        return response()->json($response ,200);
    }
    public function replyTickets(request $request){
        $validator = Validator::make($request->all(), [
            "ticket_id" =>  "required",
            "message" =>  "required",

        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $ticket =   ticket::findOrFail($request->ticket_id);
    if($ticket->status == 'Pending'){
        $ticket->status = 'Active';
        $ticket->save();
    }

    $message =  new chat;
    $message->ticket_id = $ticket->id;
    $message->sender_id = Auth::User()->id;;
    $message->message = $request->message;
    $message->status = 'unread';
    $message->save();
    
    $response['code'] = 200;
    return response()->json($response ,200);
  
    }
}
