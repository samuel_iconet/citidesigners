<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\property;
use App\option;
use App\subcategory;
use Validator;
use App\user_data;
use App\product;
use App\adcheck;
use App\user;
use App\state;
use App\artisan;
use App\supervisorData;
use App\area;
use App\productReview;
use App\productImage;
use Auth;
class adminAdController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getProducts(){
        $products  = product::orderBy('id' , 'Desc')->get();
        if(count($products)>0){
            $adExpiration = new adcheck;
            $adExpiration->cancelExpiredAds($products);
            foreach($products as $product){

                $product['images'] = productImage::where('product_id' , $product->id)->get();
                $product['state'] = state::find($product->state);
                $product['area'] = area::find($product->area);
                $product['user'] = User::find($product->user_id);
                $product['reviews'] = productReview::where('product_id' , $product->id)->get();
                $product['reviewscount'] = count(productReview::where('product_id' , $product->id)->get());
                $user = User::findOrFail($product->user_id);
                if($user->role == '3' ){
            $product['user_data'] = artisan::where('user_id' , $product->user_id)->first();

        }else if( $user->role == '4'){
           $product['user_data']  = supervisorData::where('user_id' , $product->user_id)->first();
            
        }else{
            $product['user_data']  = user_data::where('user_id' , $product->user_id)->first();

        }
               
                $product['category'] = category::find($product->category_id);
                $product['subcategory'] = subcategory::find($product->subcategory_id);
                $product['features']  = unserialize($product->properties);
            }
  
        }  
          $response['code'] = 200;
          $response['products'] = $products;
          return response()->json($response ,200);
    }
    public function  approveAd(request $request){
        $validator = Validator::make($request->all(), [
            "ad_id" =>  "required",
     
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $product = product::findOrFail($request->ad_id);
    $product->status = 'Approved';
    $product->created_at = now();
    $product->save();
    $response['code'] = 200;
    return response()->json($response ,200);
    }
    public function  rejectAd(request $request){
        $validator = Validator::make($request->all(), [
            "ad_id" =>  "required",
            // "reason" =>  "required",
     
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $product = product::findOrFail($request->ad_id);
    $product->status = 'Rejected';
    $product->extra = $request->reason;
    $product->save();
    $response['code'] = 200;
    return response()->json($response ,200);
    }

}
