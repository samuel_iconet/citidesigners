<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\item;
use App\order;
use App\User;
use App\item_image;
use Validator;
class adminitemcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getItems(){
       
        $items  = item::all();
        $item_images  = item_image::all();
        $response['items'] = $items;
        $response['item_images'] = $item_images;
        $response['code'] = 200;
      return response()->json($response ,200);
    }
    public function deleteimage(request $request){
        $validator = Validator::make($request->all(), [
            "image_id" =>  "required",
           
          
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $image = item_image::findOrFail($request->image_id);
      if(unlink(storage_path('/app/public/store/'.$image->name))){
          $image->delete();
      }
      $response['code'] = 200;
      return response()->json($response ,200);
    }
    public function addimage(request $request)
    {
        $validator = Validator::make($request->all(), [
            "file" =>  "required",
            "item_id" =>  "required",
           
          
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'store'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('store', $filePath, 'public');
        $image_name = $filePath;

       
    }
   $image = new item_image;
   $image->name = $image_name;
   $image->item_id = $request->item_id;
   $image->save();

    $response['code'] = 200;  
    return response()->json($response ,200);
    }
    public function addproduct(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required",
            "amount" =>  "required",
            "description" =>  "required",
            "seller_name" =>  "required",
            "seller_contact" =>  "required",
          
      ]);

      if ($validator->fails()) {

           return $validator->messages();
           
      }
      if(isset($request->id)){
        $item =  item::findOrFail($request->id);
        if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'store'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $request->file->storeAs('store', $filePath, 'public');
            $image_name = $filePath;
        $item->profile_image = $image_name;       

        }
        
        $item->name = $request->name;
        $item->description = $request->description;
        $item->amount = $request->amount;
        $item->seller_name = $request->seller_name;
        $item->seller_contact = $request->seller_contact;
         $item->status = 1;
        $item->save();
        $response['code'] = 200;  
        return response()->json($response ,200);
      }else{
       
      if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'store'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('store', $filePath, 'public');
        $image_name = $filePath;
    }
    $item = new item;
    $item->name = $request->name;
    $item->description = $request->description;
    $item->amount = $request->amount;
    $item->status = 1;   
     $item->profile_image = $image_name;
    $item->save();
    $response['code'] = 200;  
    return response()->json($response ,200);
      }
   
    }
    public function deleteproduct(request $request){
        $validator = Validator::make($request->all(), [
            "item_id" =>  "required",
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $itemimage =  item_image::where('item_id' , $request->item_id)->get();
    if(isset($itemimage)){
        foreach($itemimage as $image){
            unlink(storage_path('app/public/store/'.$image->name));
            $image->delete();
        } 
    }
    $item = item::findOrFail($request->item_id);
    Storage::delete("/store".$item->profile_image);
    unlink(storage_path('/app/public/store/'.$item->profile_image));
    $item->delete();
    $response['code'] = 200;  
        return response()->json($response ,200);
    }
    public function flipitem(request $request){
        $item = item::findOrFail($request->id);
        if($item->status == 1) {
            $item->status = 0;

        }else{
            $item->status = 1;
        }
        $item->save();
        $response['code'] = 200;
        return response()->json($response ,200);
        }
        public function storerequest(){
            $orders = order::all();
            
            foreach($orders as $order){
              $order['user'] = User::find($order->user_id);  
              $order['item'] = item::find($order->item_id);  
            }
            $response['code'] = 200;  
            $response['orders'] = $orders;  
            return response()->json($response ,200);
        }
        public function setProfile(request $request){
            $validator = Validator::make($request->all(), [
                "image_id" =>  "required",
               
              
          ]);
          if ($validator->fails()) {

            return $validator->messages();
       } 
       $image = item_image::findOrFail($request->image_id);
       $product = item::findOrFail($image->item_id);
       $product->profile_image = $image->name;
       $product->save();
       $response['code'] = 200;  
       return response()->json($response ,200);
        }
}
