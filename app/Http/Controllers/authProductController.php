<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\property;
use App\option;
use App\subcategory;
use Validator;
use App\user_data;
use App\User;
use App\state;
use App\area;
use App\product;
use App\productReview;
use App\artisan;
use App\supervisorData;
use App\chatmessage;
use App\productchat;
use App\productImage;
use Auth;
use Image;
class authProductController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function getchats(){
      $chats = productchat::where('seller_id' , Auth::User()->id)->OrWhere('user_id' , Auth::User()->id)->get();
      foreach($chats as $chat){
          if($chat->seller_id == Auth::User()->id){
            $chat['isSeller']   = '1';
          }else{
            $chat['isSeller']   = '0';
          }
          $chat ['user']   = User::find($chat->user_id);
          $chat ['seller'] = User::find($chat->seller_id);
          $chat ['product'] = product::find($chat->product_id);
          $chat['messages'] = chatmessage::where('chat_id' , $chat->id)->get();
      }
       
        $response['code'] = 200;
        $response['chats'] = $chats;
        return response()->json($response ,200);
    }
    public function replychat(request $request){
        $validator = Validator::make($request->all(), [
            "msg" =>  "required",
            "chat_id" =>  "required",
     
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $message = new chatmessage;
    $message->chat_id =$request->chat_id;
    $message->user_id = Auth::User()->id;
    $message->message = $request->msg;
    $message->save();
    $response['code'] = 200;
    return response()->json($response ,200); 

    }
    public function startChat($msg , $product_id){
     $chat = productchat::where(['product_id' =>$product_id , 'user_id' => Auth::User()->id])->first();  
    $product =  product::findOrFail($product_id);
    if(!isset($chat)){
        $chat  = new productchat;
        $chat->product_id =$product_id;
        $chat->seller_id =$product->user_id;
        $chat->user_id = Auth::User()->id;
        $chat->status = 'active';
        $chat->save();
    }
    $message = new chatmessage;
    $message->chat_id =$chat->id;
    $message->user_id = Auth::User()->id;
    $message->message = $msg;
    $message->save();
    $response['code'] = 200;
    return response()->json($response ,200); 

    }
    public function getProperties(){
        $categories = category::where('status', '1')->get();
        if(count($categories) > 0){
            foreach($categories as $category){
                $subcategories = subcategory::where(['category_id' =>  $category->id , 'status' => '1'])->get();
                if(count($subcategories ) > 0){
                    foreach($subcategories as $subcategory){
                         $properties = property::where(['subcategory_id' => $subcategory->id ,'status' => '1' ])->get();
                        if(count($properties ) > 0){
                            foreach($properties as $property){
                                $options = option::where('property_id' , $property->id)->get();
                                $property['options'] = $options;
                            }
                        }
                        $subcategory['properties'] = $properties;
                    }

                }
                $category['subcategories'] = $subcategories;
            }         
        }
        $user = Auth::User();
       if($user->role == '3' ){
            $user['data']  = artisan::where('user_id' , $user->id)->first();

        }else if( $user->role == '4'){
            $user['data']  = supervisorData::where('user_id' , $user->id)->first();
            
        }else{
            $user['data']  = user_data::where('user_id' , $user->id)->first();

        }
        $response['code'] = 200;
        $response['states'] = state::where('status' , '1')->get();
        $response['areas'] = area::where('status' , '1')->get();
        $response['code'] = 200;
        $response['categories'] = $categories;
        $response['user'] = $user;
        return response()->json($response ,200); 
        
    }
    public function createProduct(request $request){
        $validator = Validator::make($request->all(), [
            "images" =>  "required",
            "data" =>  "required",
     
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $data = json_decode($request->data, true);
    $product = new product;
    $product->user_id =Auth::User()->id;
    $product->name = $data['title'];
    $product->category_id = $data['category_id'];
    $product->subcategory_id = $data['subcategory_id'];
    $product->description = $data['description'];
    $product->price = $data['price'];
    $product->ad_type = $data['type'];
    $product->state = $data['state_id'];
    $product->area = $data['area_id'];
    if(isset($request->properties)){
        $product->properties = serialize(json_decode($request->properties, true));

    }else{
        $product->properties =serialize(json_decode('[]', true));;

    }
    $product->save();
    if($files=$request->file('images')){
        foreach($files as $file){
            $productimage  = new productImage;

            $image = $file;
            $name = 'product'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $file->storeAs('product', $filePath, 'public');
           $productimage->image = $filePath; 
           $productimage->product_id = $product->id;
           $productimage->save(); 

           $img = Image::make(public_path('/storage/product/'.$filePath));
           $img->resize(640, 430);
            /* insert watermark at bottom-right corner with 10px offset */
            $img->insert(public_path('/front/images/logo1.png'), 'bottom-right', 2, 2);
        
            $img->save(public_path('/storage/product/'.$filePath)); 
        }
    }
      
        $response['code'] = 200;
        $response['ad_id'] = $product->id;
        return response()->json($response ,200); 
    }
    public function getProducts(){
      $products  = product::where('user_id' , Auth::User()->id)->orderBy('id' , 'Desc')->get();
      if(count($products)>0){

          foreach($products as $product){
            $product['state'] = state::find($product->state);
            $product['area'] = area::find($product->area);
              $product['images'] = productImage::where('product_id' , $product->id)->get();
              $product['reviews'] = productReview::where('product_id' , $product->id)->get();
              $product['reviewscount'] = count(productReview::where('product_id' , $product->id)->get());
              $product['category'] = category::find($product->category_id);
              $product['subcategory'] = subcategory::find($product->subcategory_id);
              $product['features']  = unserialize($product->properties);
          }

      }  
        $response['code'] = 200;
        $response['products'] = $products;
        return response()->json($response ,200);
    }
}
