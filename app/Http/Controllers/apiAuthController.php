<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\tip;
use App\artisan;
use App\supervisorData;
use App\area;
use App\state;
use App\user_data;
use App\Mail\WelcomeMail;
use App\Mail\recoveryMail;
use App\message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class apiAuthController extends Controller
{   
    public function registerUser(request $request){
        $validator = Validator::make($request->all(), [
            "username" =>  "required",
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|',
            'address' => 'required',
            'phone' => 'required',
            'state' =>  'required',
            'area' =>  'required'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $area = area::find($request->area);
      $state = state::find($request->state);
      $user = new User;
      $user->name = $request->username;
      $user->email = $request->email;
      $user->role ='1';
      $user->status ='1';
      $user->password = bcrypt($request->password);
       $user->save();

       $user_data = new user_data;
       $user_data->user_id = $user->id;
       $user_data->address = $request->address;
       $user_data->state_id = $request->state;
       $user_data->area_id = $request->area;
       $user_data->phone_number = $request->phone;
       $user_data->state = $state->name;
       $user_data->area = $area->name;
       $user_data->save();
       $message = new message;
       $msg = "Welcome to Citi Designers ".$user->name.", You can now Log in to you account to make requests. thank you";
       $response['text'] = $message->sendSms($request->phone , $msg);
      
       $response['code'] = 200;
        return response()->json($response ,200);

    }
    public function registerSupervisor(request $request){
        $validator = Validator::make($request->all(), [
            "username" =>  "required",
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|',
            'password' => 'required|string|',
            'file_profile' => 'required|',
            'address' => 'required|',
            'state' => 'required|',
            'area' => 'required|',
            'bank_name' => 'required|',
            'account_number' => 'required|',
            'guarantor_name' => 'required|',
            'file_guarantor_profile' => 'required|',
            'guarantor_number' => 'required|',
            'file_guarantor_id' => 'required|',
            'id_type' => 'required|',
            'id_number' => 'required|',
            'file_id' => 'required|',
            'gender' => 'required|',
            'date_of_birth'=> 'required',
            'years_of_experience' => 'required',
            'state_origin' => 'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $profile_image = "";
      if ($request->has('file_profile')) {
        $image = $request->file('file_profile');
        $name = 'profile'.'_'.time();
          $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_profile->storeAs('profile', $filePath, 'public');
        $profile_image = $filePath;
    }
    $guarantor_profile_image = "";
      if ($request->has('file_guarantor_profile')) {
        $image = $request->file('file_guarantor_profile');
        $name = 'guarantor_profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_guarantor_profile->storeAs('profile', $filePath, 'public');
        $guarantor_profile_image = $filePath;
    }
    $guarantor_id_image = "";
    if ($request->has('file_guarantor_id')) {
      $image = $request->file('file_guarantor_id');
      $name = 'guarantor_id'.'_'.time();
      $filePath =  $name. '.' . $image->getClientOriginalExtension();
      $request->file_guarantor_id->storeAs('id', $filePath, 'public');
      $guarantor_id_image = $filePath;
  }
  $id_image = "";
  if ($request->has('file_id')) {
    $image = $request->file('file_id');
    $name = 'id'.'_'.time();
    $filePath =  $name. '.' . $image->getClientOriginalExtension();
    $request->file_id->storeAs('ids', $filePath, 'public');
    $id_image = $filePath;
}

   $user = new User;
 
   $user->name = $request->username;
   $user->email = $request->email;
   $user->role ='4';
   $user->status ='0';
   $user->password = bcrypt($request->password);
    $user->save();
    $area = area::find($request->area);
    $state = state::find($request->state);
    $stateorigin = state::find($request->state_origin);
    $user_data = new supervisorData;
    $user_data->user_id = $user->id;
    $user_data->phone_number = $request->phone;
    $user_data->account_number = $request->account_number;
    $user_data->account_name = $request->bank_name;
    $user_data->guarantor = $request->guarantor_name;
    $user_data->guarantor_number = $request->guarantor_number;
    $user_data->profile_image = $profile_image;	
    $user_data->guarantor_profile = $guarantor_profile_image;
    $user_data->guarantor_id = $guarantor_id_image;
    $user_data->id_image = $id_image;
    $user_data->address = $request->address;
    $user_data->area_id = $request->area;
    $user_data->state_id = $request->state;
    $user_data->gender = $request->gender;
    $user_data->id_type = $request->id_type;
    $user_data->id_number = $request->id_number;
    $user_data->years_of_experience = $request->years_of_experience;
    $user_data->state_name = $state->name;
    $user_data->area_name = $area->name;
    $user_data->state_origin = $stateorigin->name;
    $user_data->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
    
    $user_data->save();
    $message = new message;
     $msg = "Welcome to Citi Designers ".$user->name.", Login to make payment and activate your Supervisor Account. thank you";
     $response['text'] = $message->sendSms($request->phone , $msg);
    $response['code'] = 200;
   return response()->json($response ,200);
    }
    public function registerArtisan(request $request){
        $validator = Validator::make($request->all(), [
            "username" =>  "required",
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|',
            'password' => 'required|string|',
            'file_profile' => 'required|',
            'address' => 'required|',
            'state' => 'required|',
            'area' => 'required|',
            'bank_name' => 'required|',
            'account_number' => 'required|',
            'guarantor_name' => 'required|',
            'file_guarantor_profile' => 'required|',
            'guarantor_number' => 'required|',
            'file_guarantor_id' => 'required|',
            'id_type' => 'required|',
            'id_number' => 'required|',
            'file_id' => 'required|',
            'gender' => 'required|',
            'skill_id'=> 'required',
            'ref_code' => 'required',
            'date_of_birth'=> 'required',
            'years_of_experience' => 'required',
            'state_origin' => 'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $profile_image = "";
      if ($request->has('file_profile')) {
        $image = $request->file('file_profile');
        $name = 'profile'.'_'.time();
          $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_profile->storeAs('profile', $filePath, 'public');
        $profile_image = $filePath;
    }
    $guarantor_profile_image = "";
      if ($request->has('file_guarantor_profile')) {
        $image = $request->file('file_guarantor_profile');
        $name = 'guarantor_profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_guarantor_profile->storeAs('profile', $filePath, 'public');
        $guarantor_profile_image = $filePath;
    }
    $guarantor_id_image = "";
    if ($request->has('file_guarantor_id')) {
      $image = $request->file('file_guarantor_id');
      $name = 'guarantor_id'.'_'.time();
      $filePath =  $name. '.' . $image->getClientOriginalExtension();
      $request->file_guarantor_id->storeAs('ids', $filePath, 'public');
      $guarantor_id_image = $filePath;
  }
  $id_image = "";
  if ($request->has('file_id')) {
    $image = $request->file('file_id');
    $name = 'id'.'_'.time();
    $filePath =  $name. '.' . $image->getClientOriginalExtension();
    $request->file_id->storeAs('ids', $filePath, 'public');
    $id_image = $filePath;
}
 $ref = User::where('ref_code' , $request->ref_code)->first();
   $user = new User;
   $user->name = $request->username;
   $user->email = $request->email;
   $user->role ='3';
   $user->status ='0';
   $user->password = bcrypt($request->password);
    $user->save();
    $area = area::find($request->area);
    $state = state::find($request->state);
    $stateorigin = state::find($request->state_origin);
    $user_data = new artisan;
    $user_data->user_id = $user->id;
    $user_data->skill_id = $request->skill_id;
    $user_data->phone_number = $request->phone;
    $user_data->account_number = $request->account_number;
    $user_data->account_name = $request->bank_name;
    $user_data->guarantor = $request->guarantor_name;
    $user_data->guarantor_number = $request->guarantor_number;
    $user_data->profile_image = $profile_image;	
    $user_data->guarantor_profile = $guarantor_profile_image;
    $user_data->guarantor_id = $guarantor_id_image;
    $user_data->artisan_id = $id_image;
    $user_data->address = $request->address;
    $user_data->area_id = $request->area;
    $user_data->state_id = $request->state;
    $user_data->gender = $request->gender;
    $user_data->id_type = $request->id_type;
    $user_data->id_number = $request->id_number;
    $user_data->years_of_experience = $request->years_of_experience;
    $user_data->state_name = $state->name;
    $user_data->area_name = $area->name;
    $user_data->state_origin = $stateorigin->name;
    $user_data->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
    if(isset($ref) && $ref->role == '4'){
      $user_data->ref_code = $ref->ref_code;      
      $user_data->ref_status = 'UNUSED';      
    }
    $user_data->save();
    $message = new message;
    $msg = "Welcome to Citi Designers ".$user->name.", Login to make payment and activate your Artisan Account. thank you";
    $response['text'] = $message->sendSms($request->phone , $msg);
    $response['code'] = 200;
   return response()->json($response ,200);
    }
    public function savePassword(request $request){
        $validator = Validator::make($request->all(), [
            "token" =>  "required",
            'email' => 'required|string|email',
            'password' => 'required|string|min:6|'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
        $user = User::where(['email' => $request->email , 'remember_token' => $request->token])->first();
        if(isset($user)){
           $user->password = bcrypt($request->password);
           $user->remember_token = NULL;
           $user->save();
           
           $response['code'] = 200;
          return response()->json($response ,200);

        }else{
            $response['error'] = "invalid token" ;
            $response['code'] = 303;
           return response()->json($response ,200);

        }
   }
    public function recover(request $request){
        $user = User::where('email' , $request->email)->first();
        if(isset($user)){
            $recoverykey = Str::random(40);
            $user->remember_token = $recoverykey;
            $user->save();
             Mail::to($user)->send(new recoveryMail($user));
            $response['code'] = 200;
            return response()->json($response ,200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Email not registered!!!";
            return response()->json($response ,200);   
        }
    }
    public function tips(){
       $tips = tip::where('status' , '1')->get(); 
       $response['code'] = 200;
       $response['tips'] = $tips;
       return response()->json($response ,200);
    }
    public function register(request $request){
        $validator = Validator::make($request->all(), [
                    "username" =>  "required",
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|'
              ]);
       
              if ($validator->fails()) {
       
                   return $validator->messages();
              }
              else {
               $user = new User;
           if(isset($request->role) && $request->role != 2){
           $user->role = $request->role;
       
           }
           else{
               $user->role  = 1;
           }
           $user->status = 0;
           $user->name = $request->username;
           $user->email = $request->email;
           $user->password = bcrypt($request->password);
           $user->save();
       
       
           $response['code'] = 200;
           $response['user'] = $user;
         Mail::to($user)->send(new WelcomeMail($user));
           return response()->json($response ,200);
              }
       
         }
         public function adminAuthError(){
            $response['error'] = "User Not Permitted";
            $response['code']  = "401";
            return response()->json($response ,200);
         }
         public function makeOrder(request $request){
        $validator = Validator::make($request->all(), [
            "item_id" =>  "required",
            'email' => 'required',
            'name' => 'required'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $v_user = User::where('email' , $request->email)->first();
      if(isset($v_user)){
      $user = $v_user;
      }
      else{
      $user = new User;
      $user->role = 1;
      $user->status = 0;
      $user->name = $request->username;
      $user->email = $request->email;
      $user->password = bcrypt("password");
      $user->save();
       Mail::to($user)->send(new WelcomeMail($user));
      }
      $item = item::find($request->item_id);
      $order = new order;
      $order->quantity = 1;
      $order->amount = $item->amount;
      $order->status = "pending";
      $order->message = $request->message();
      $order->user_id = $user->id;
      $order->save();
      Mail::to($user)->send(new orderMail($user));
      
      $response['code'] = 200;
      return response()->json($response , 200);
         }
  
     
}
