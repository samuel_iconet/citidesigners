<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\item;
use App\order;
use App\Mail\orderMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class ordercontroller extends Controller
{
    //
    public function makeOrder(request $request){
        $validator = Validator::make($request->all(), [
            "item_id" =>  "required",
            'email' => 'required',
            'name' => 'required'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $v_user = User::where('email' , $request->email)->first();
      if(isset($v_user)){
      $user = $v_user;
      }
      else{
      $user = new User;
      $user->role = 1;
      $user->status = 0;
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = bcrypt("password");
      $user->save();
      Mail::to($user)->send(new WelcomeMail($user));
      }
      $item = item::find($request->item_id);
      $order = new order;
      $order->quantity = 1;
      $order->amount = $item->amount;
      $order->status = "pending";
      $order->message = $request->message;
      $order->user_id = $user->id;
      $order->item_id = $request->item_id;
      $order->save();
      Mail::to($user)->send(new orderMail($user));
      $response['code'] = 200;
      return response()->json($response , 200);
    }
}
