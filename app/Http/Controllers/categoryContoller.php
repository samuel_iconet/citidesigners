<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\subcategory;
use App\property;
use App\option;

use Validator;
class categoryContoller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getcategories(){
        $categories = category::orderBy('id' ,'DESC')->get();
        foreach($categories as $category){
            $category['subcategories'] = subcategory::where('category_id' , $category->id)->orderBy('id' ,'DESC')->get();
        }
        $response['code'] = 200;
        $response['categories'] = $categories;
        return response()->json($response ,200);
    }
    public function createCategory(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required",
             "description" => "required"
        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $category = new category;
    if ($request->has('image')) {
        $image = $request->file('image');
        $name = 'catergory'.'_'.time();
          $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->image->storeAs('category', $filePath, 'public');
        $category->image =  $filePath ;
 
    }
    
    $category->name = $request->name;
    $category->description = $request->description;
    $category->save();
    $response['code'] = 200;
return response()->json($response ,200);
}
public function deleteCategory($category_id){
    $category = category::findOrFail($category_id);

    $subcategories = subcategory::where('category_id' ,$category->id )->get();
    if(count($subcategories) > 0){
      foreach($subcategories as $subcategory){
    $properties = property::where('subcategory_id' ,$subcategory->id )->get();

        if(count($properties) > 0){
            foreach($properties as $property){
                $options = option::where('property_id' , $property->id)->get();
            if(count($options) > 0){
                foreach($options as $option){
                    $option->delete();
                }
            }
            $property->delete();
            }
        }
      }  
    }
   
    $category->delete();
    $response['code'] = 200;
return response()->json($response ,200); 
}
public function deleteSubcategory($subcategory_id){
    $subcategory = subcategory::findOrFail($subcategory_id);
    $properties = property::where('subcategory_id' ,$subcategory->id )->get();
    if(count($properties) > 0){
        foreach($properties as $property){
            $options = option::where('property_id' , $property->id)->get();
        if(count($options) > 0){
            foreach($options as $option){
                $option->delete();
            }
        }
        $property->delete();
        }
    }
    $subcategory->delete();
    $response['code'] = 200;
    return response()->json($response ,200); 
}
public function editCategory(request $request){
    $validator = Validator::make($request->all(), [
        "name" =>  "required",
        "category_id" =>  "required",
        "description" => "required"

         
    
]);

if ($validator->fails()) {

     return $validator->messages();
}
$category = category::findOrFail($request->category_id);
if ($request->has('image')) {
    $image = $request->file('image');
    $name = 'catergory'.'_'.time();
      $filePath =  $name. '.' . $image->getClientOriginalExtension();
    $request->image->storeAs('category', $filePath, 'public');
    $category->image =  $filePath ;

}

$category->name = $request->name;
$category->description = $request->description;

$category->save();
$response['code'] = 200;
return response()->json($response ,200);

}
public function flipState($id){
    $category = category::findOrFail($id);
    if($category->status == '1'){
        $category->status = '0';
        $category->save();
    }else{
        $category->status = '1';
        $category->save();
    }
    $response['code'] = 200;
    return response()->json($response ,200);
}
}