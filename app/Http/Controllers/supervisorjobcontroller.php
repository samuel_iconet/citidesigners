<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\jobmerge;
use App\User;
use App\user_data;
use App\artisan;
use App\job;
use App\proof;
use App\skill;
use App\getstateareaname;
use App\payments;
class supervisorjobcontroller extends Controller
{
    //
    public function __construct()
    {
       
        $this->middleware('auth:api');
         $this->middleware('Supervisor');

    }
    public function comfirmrequest(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required'
          
         ]); 
         $jobmerge = jobmerge::findOrFail($request->jobmerge_id);
         $job  = job::findOrfail($jobmerge->job_id);
         if(Auth::User()->id == $jobmerge->supervisor_id){
             $jobmerge->status = 'Completed';
             $job->status = 'Completed';
             $jobmerge->save();
             $job->save();
             $artisan = User::find($jobmerge->artisan_id);
             if(isset($artisan)){
                 $payment = new payments;
              $payment->debitCompany($artisan->id , (76.5 /100 * $job->amount_paid) , 'Payment for Job Completed. Job ID: '.$jobmerge->id);
        
             }
             $supervisor = User::find($jobmerge->supervisor_id);
             if(isset($supervisor)){
                 $payment = new payments;
              $payment->debitCompany($supervisor->id , (40/100*(23.5 /100 * $job->amount_paid)) , 'Payment for Job Completed. Job ID: '.$jobmerge->id);
        
             }
             $response['code'] = 200;
          return response()->json($response , 200);   
         }
         $response['code'] = 404;
         return response()->json($response , 200);
    }
    public function getproofs(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required'
          
         ]);
         $proofs  = proof::where('jobmerge_id' , $request->jobmerge_id)->get();
         $response['code'] = 200;
          $response['proofs'] = $proofs;
          return response()->json($response , 200);     

        }
    public function  getmergeRequest(){
        $merges =  jobmerge::where('supervisor_id' , Auth::User()->id)->get();
      foreach($merges as $merge){
          $merge['artisan'] = User::find($merge->artisan_id);
          $merge['artisandata'] = artisan::where('user_id' , $merge->artisan_id)->first();
          $user = User::find($merge->user_id);
         $merge['jobdata'] = job::where('id' , $merge->job_id)->first();
          $merge['user'] = User::find($merge->user_id);
          $stateareaData = new getstateareaname;
          $merge['state_name'] =  $stateareaData->getStateName($merge['jobdata']->state);
          $merge['area_name'] =  $stateareaData->getAreaName($merge['jobdata']->area);
      }
      $response['code'] = 200;
      $response['requests'] = $merges;
      return response()->json($response , 200);  
  }
    public function acceptRequest($id){
        $request = jobmerge::where(['id' => $id , 'supervisor_id' => Auth::User()->id , 'status' => 'Pending'])->first();
        if(isset($request)){
            $request->supervisor_status = "Accepted";
            if($request->artisan_status == "Accepted"){
                $request->status = "Accepted";
            }
            $request->save();
            $response['code'] = 200;
            return response()->json($response , 200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Invalid ID";
            return response()->json($response , 200);
        }
    }
    public function rejectRequest($id){
         $request = jobmerge::where(['id' => $id , 'supervisor_id' => Auth::User()->id])->first();
       
        if(isset($request)){
            $job = job::where('id' , $request->job_id)->first();
            if(isset($job)){

                $new_request = new jobmerge;
                $new_request->job_id = $request->job_id;
                $new_request->job_name =  $request->job_name;
                 $new_request->artisan_id = $request->artisan_id;
                $new_request->supervisor_id = $request->supervisor_id;
                $new_request->artisan_status = $request->artisan_status;
                $new_request->user_id = $request->user_id;
                $new_request->status = $request->status;
                $new_request->quote =$request->quote;
                $new_request->payment_status = $request->payment_status;
                $new_request->supervisor_status = "Rejected";
                $new_request->save();   
                $request->supervisor_status = "Rejected";
                $request->save();
               if($request->artisan_status == 'Rejected'){
                $request->status = "Cancelled";
                $job->status = 'Pending';
                $job->save();
                $request->save();
                $merge = new merge;
               $merge->mergeJob( $job->id);
               }
               $merge = new merge;
               $merge->remergeSupervisor($new_request->id);
                $response['code'] = 200;
            return response()->json($response , 200);

            }else{
                $response['code'] = 400;
                $response['error'] = "Invalid User ID Provided.";
                return response()->json($response , 200);  
            }
            
        }else{
            $response['code'] = 401;
            $response['error'] = "Invalid ID";
            return response()->json($response , 200);
        }
    }
    public function addQuote(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required',
             'amount' => 'required'
         ]);
       $jobmerge =  jobmerge::where(['id' => $request->jobmerge_id , 'supervisor_id' => Auth::User()->id])->first();
       if(isset($jobmerge)){
        $job = job::findOrFail($jobmerge->job_id);
        $jobmerge->quote = $request->amount;
        $jobmerge->status = 'Quoted';
        $jobmerge->save();
        $job->quote  = $request->amount;  
        $job->status  = 'Quoted';  
        $job->save();  
        $response['code'] = 200;
         return response()->json($response , 200);  

       }else{
        $response['code'] = 404;
        $response['error'] = "Invalid Job Data";
        return response()->json($response , 200);  

       }
    }
    public function getNewRequest(){
        $merges =  jobmerge::where(['supervisor_id' => Auth::User()->id , 'supervisor_status' => 'Pending'])->get();
      if(isset($merges)){
        foreach($merges as $merge){
            $merge['user'] = User::find($merge->user_id);
            $merge['artisan'] = User::find($merge->artisan_id);
            $merge['userdata'] = job::where('id' , $merge->job_id)->first();
            $merge['artisandata'] = artisan::where('user_id' , $merge->artisan_id)->first();
            $stateareaData = new getstateareaname;
            $merge['state_name'] =  $stateareaData->getStateName($merge['userdata']->state);
            $merge['area_name'] =  $stateareaData->getAreaName($merge['userdata']->area);
        }
      }
      $response['new_requests'] = count($merges);
      $response['all_requests'] = count(jobmerge::where('supervisor_id' , Auth::User()->id )->get());
      $response['accepted_requests'] = count(jobmerge::where(['supervisor_id' => Auth::User()->id , 'supervisor_status' => 'Accepted'])->get());
      $response['rejected_requests'] = count(jobmerge::where(['supervisor_id' => Auth::User()->id , 'supervisor_status' => 'Rejected'])->get());
      $response['completed_requests'] = count(jobmerge::where(['supervisor_id' => Auth::User()->id , 'status' => 'Completed'])->get());

        $response['code'] = 200;
        $response['jobs'] = $merges;
        return response()->json($response , 200);
    }
}
