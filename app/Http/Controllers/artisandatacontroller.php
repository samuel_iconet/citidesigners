<?php

namespace App\Http\Controllers;
use Auth;
use App\artisan;
use App\state;
use App\skill;
use App\area;
use App\proof;
use App\querry;
use validate;
use Validator;
use Illuminate\Http\Request;

class artisandatacontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('Artisan');

    }
    public function validateArtisan(){
      $status = Auth::User()->status;
      $data = artisan::where('user_id' , Auth::User()->id)->first();
       if($status == '0' || $data->status != 'approved') {
        $response['code'] = 303;
        $response['error'] = 'inactive Profile';
       $response['status'] = $status;
         return response()->json($response,200);
       }
       $response['code'] = 200;
       $response['status'] = $status;
        return response()->json($response,200);
    }
    public function verify(){
        $response['code'] = 200;
         return response()->json($response,200);   
    }
    public function uploadprofile(request $request){
        $this->validate($request,[     
            'file' => 'required',
         ]);
         if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'profile'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $request->file->storeAs('profile', $filePath, 'public');
            $image_name = $filePath;
        }
        $data = artisan::where('user_id' , Auth::user()->id)->first();
        if(isset($data)){
            $data->profile_image =  $image_name;
            $data->save();
            $response['code'] = 200;
            return response()->json($response , 200);  
        }else{
            $response['code'] = 401;
            $response['error'] = "Please provide User Data";
            return response()->json($response , 200);  
        }
        
   
    }
    public function getData(){
       $artisanData = artisan::where('user_id' , Auth::User()->id)->first();
       $querries = querry::where('user_id' , Auth::User()->id)->get();
       $proofs = proof::where(['item_id' => Auth::User()->id , 'type' => 'registration' , 'item_category' => 'artisan'])->get();
        $states = state::all();
        $areas = area::all();
        $skills = skill::all();
       if(isset($artisanData)){
        $response['code'] = 200;
        $response['artisandata'] = $artisanData;
        $response['querries'] = $querries;

       }else{
        $response['code'] = 401;

        $response['error'] = "Artisan Data not set.";
        $response['artisandata'] = Artisan::find(1);
       }
       $response['skills'] = $skills;
       $response['areas'] = $areas;
       $response['proofs'] = $proofs;
       $response['states'] = $states;
     
        return response()->json($response,200);

    }
    public function setArtisanData(request $request){
        $this->validate($request,[     
             'phone_number' => 'required',
             'account_number' => 'required',
             'account_name' => 'required',
             'guarantor' => 'required',
             'guarantor_number' => 'required',
             'address' => 'required',
             'area_id' => 'required',
             'state_id' => 'required', 
             'date_of_birth' => 'required'
         
         ]);
        
         $request['user_id'] = Auth::User()->id;  
         $exist_artisan = artisan::where('user_id' , Auth::User()->id)->first();
          $state = state::find($request->state_id);
         $area = area::find($request->area_id);
         if(isset($exist_artisan)){
            $exist_artisan->phone_number = $request->phone_number;
            $exist_artisan->account_number = $request->account_number;
            $exist_artisan->account_name = $request->account_name;
            $exist_artisan->guarantor = $request->guarantor;
            $exist_artisan->guarantor_number = $request->guarantor_number;
            $exist_artisan->address = $request->address;
            $exist_artisan->area_id = $request->area_id;
            $exist_artisan->state_id = $request->state_id;
            $exist_artisan->state_name = $state->name;
            $exist_artisan->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
            // $exist_artisan->skill_id = $request->skill_id;
            $exist_artisan->area_name = $area->name;
            $exist_artisan->save();
            $artisandata = $exist_artisan;
         }else{
            // $artisandata =  artisan::create($request->all());
            $new_atisan = new artisan;
            $new_atisan->phone_number = $request->phone_number;
            $new_atisan->account_number = $request->account_number;
            $new_atisan->account_name = $request->account_name;
            $new_atisan->guarantor = $request->guarantor;
            $new_atisan->guarantor_number = $request->guarantor_number;
            $new_atisan->address = $request->address;
            $new_atisan->area_id = $request->area_id;
            $new_atisan->state_id = $request->state_id;
            $new_atisan->state_name = $state->name;
            $new_atisan->area_name = $area->name;
            $new_atisan->user_id = $request->user_id;
            $new_atisan->skill_id = $request->skill_id;
            $new_atisan->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
            $new_atisan->save();
            $artisandata = $new_atisan;
            
         }
        $response['code'] = 200;
        $response['artisandata'] = $artisandata;
         return response()->json($response,200);

    }
    public function updateArtisanData(){
        $this->validate($request,[     
             'skill_id' => 'required',
             'phone_number' => 'required',
             'gender' => 'required',
             'account_number' => 'required',
             'account_name' => 'required',
             'guarantor' => 'required',
             'guarantor_number' => 'required',
             'address' => 'required',
             'area_id' => 'required',
             'state_id' => 'required',
             'date_of_birth' => 'required',
             'id_type' => 'required',
             'id_number' => 'required',
         ]);
        $request->date_of__birth = date('Y-m-d',strtotime($request->date_of_birth));
        $artisandata =  artisan::update($request->all());
        $response['code'] = 200;
        $response['artisandata'] = artisandata;
         return response()->json($response,200);
    }
    public function updateArtisan(request $request){
        $validator = Validator::make($request->all(), [
            'phone' => 'required|string|',
            'address' => 'required|',
            'state' => 'required|',
            'area' => 'required|',
            'bank_name' => 'required|',
            'account_number' => 'required|',
            'guarantor' => 'required|',
            'guarantor_number' => 'required|',
            'id_type' => 'required|',
            'id_number' => 'required|',
            'gender' => 'required|',
            'skill_id'=> 'required',
            'date_of_birth'=> 'required'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $user = Auth::User();
    $area = area::find($request->area);
    $state = state::find($request->state);
    $user_data = artisan::where('user_id' , $user->id)->first(); 
     
      if ($request->has('file_profile')) {
        $image = $request->file('file_profile');
        $name = 'profile'.'_'.time();
          $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_profile->storeAs('profile', $filePath, 'public');
        $profile_image = $filePath;
        $user_data->profile_image = $profile_image;	

    }
 
      if ($request->has('file_guarantor_profile')) {
        $image = $request->file('file_guarantor_profile');
        $name = 'guarantor_profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file_guarantor_profile->storeAs('profile', $filePath, 'public');
        $guarantor_profile_image = $filePath;
         $user_data->guarantor_profile = $guarantor_profile_image;

    }
   
    if ($request->has('file_guarantor_id')) {
      $image = $request->file('file_guarantor_id');
      $name = 'guarantor_id'.'_'.time();
      $filePath =  $name. '.' . $image->getClientOriginalExtension();
      $request->file_guarantor_id->storeAs('ids', $filePath, 'public');
      $guarantor_id_image = $filePath;
    $user_data->guarantor_id = $guarantor_id_image;

  }
  
  if ($request->has('file_id')) {
    $image = $request->file('file_id');
    $name = 'id'.'_'.time();
    $filePath =  $name. '.' . $image->getClientOriginalExtension();
    $request->file_id->storeAs('ids', $filePath, 'public');
    $id_image = $filePath;
    $user_data->artisan_id = $id_image;

}

   
    $user_data->skill_id = $request->skill_id;
    $user_data->phone_number = $request->phone;
    $user_data->account_number = $request->account_number;
    $user_data->account_name = $request->bank_name;
    $user_data->guarantor = $request->guarantor;
    $user_data->guarantor_number = $request->guarantor_number;
    $user_data->address = $request->address;
    $user_data->area_id = $request->area;
    $user_data->state_id = $request->state;
    $user_data->gender = $request->gender;
    $user_data->id_type = $request->id_type;
    $user_data->id_number = $request->id_number;
    $user_data->state_name = $state->name;
    $user_data->area_name = $area->name;
    $user_data->status = 'pending';
    $user_data->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
    $user_data->save();
    $querries = querry::where('user_id' , Auth::User()->id)->get();
    foreach($querries as $querry){
        $querry->status = "resolved";
        $querry->save();
    }
    $response['code'] = 200;
   return response()->json($response ,200);
    }
}
