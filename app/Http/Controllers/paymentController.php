<?php

namespace App\Http\Controllers;
use Validator;
use App\registrationPayment;
use App\requestPayment;
use App\artisan;
use App\proof;
use App\supervisorData;
use App\jobmerge;
use App\User;
use App\user_data;
use App\job;
use App\payments;
use App\product;
use App\adPayment;
use Auth;
use Illuminate\Http\Request;

class paymentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function testcredit(){
      $payment = new payments;
     return  $payment->creditCompany('2' , '1000' , 'Registration Fees');
    }
    public function cardrequestPayment(request $request){
      $validator = Validator::make($request->all(), [
        "amount" =>  "required",
        'reference' => 'required',
        'jobmerge_id' => 'required',
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
  $url = "https://api.paystack.co/transaction/verify/".$request->reference;
      $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Authorization: Bearer sk_test_43baab53ab8c7dbd14678e30525ae4949956d544",
      "Cache-Control: no-cache",
    ),
  ));
  
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  
  if ($err) {
    
    $responce['code'] = 404;
    $responce['error'] = "cURL Error #:" . $err;
    return response()->json($responce,200);

  } else {
       $jobmerge = jobmerge::findOrFail($request->jobmerge_id);
       $job = job::findOrFail($jobmerge->job_id);
       
       $requestPayment = new requestPayment;
       $requestPayment->user_id = Auth::User()->id;
       $requestPayment->job_id = $job->id;
       $requestPayment->merge_id = $jobmerge->id;
       $requestPayment->amount = $request->amount;
       $requestPayment->payment_reference = $request->reference;
       $requestPayment->payment_method = 'card';
       $requestPayment->status = 'successful';
       $requestPayment->save();
       $job->amount_paid = $job->amount_paid + $request->amount;
       $job->status = 'Paid';
       $job->save();
       $jobmerge->amount_paid = $jobmerge->amount_paid + $request->amount;
       $jobmerge->payment_status = '1';
       $jobmerge->status = 'Paid';
       $jobmerge->save();
       $payment = new payments;
       $payment->creditCompany($job->jobber_id , $request->amount , 'Payment For Job with ID: '.$job->id);
      $responce['code'] = 200;
       return response()->json($responce,200);
  }

    }
    public function cardregistrationPayment(request $request){
        $validator = Validator::make($request->all(), [
            "amount" =>  "required",
            'reference' => 'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
$url = "https://api.paystack.co/transaction/verify/".$request->reference;
      $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Authorization: Bearer sk_test_43baab53ab8c7dbd14678e30525ae4949956d544",
      "Cache-Control: no-cache",
    ),
  ));
  
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  
  if ($err) {
    
    $responce['code'] = 404;
    $responce['error'] = "cURL Error #:" . $err;
    return response()->json($responce,200);

  } else {
    if(Auth::User()->role == '3'){
      $user_data = artisan::where('user_id' , Auth::User()->id)->first();

    }else {
      $user_data = supervisorData::where('user_id' , Auth::User()->id)->first();

    }
      $payment = new registrationPayment;
      $payment->user_id = Auth::User()->id;
      $payment->user_type = Auth::User()->role;
      $payment->payment_method = 'card';
      $payment->payment_reference = $request->reference;
      $payment->amount = $request->amount;
      $payment->status = 'successful';
      $payment->save();
      $user_data->payment_status = '1';
      $user_data->save();
      $payment = new payments;
      $payment->creditCompany(Auth::User()->id , $request->amount , 'Registration fees for User with ID: '.Auth::User()->id);

      $responce['code'] = 200;
       return response()->json($responce,200);
  }
      

    }
    public function cardadPayment(request $request){
      $validator = Validator::make($request->all(), [
          "amount" =>  "required",
          'reference' => 'required',
          'product_id' => 'required',
    ]);

    if ($validator->fails()) {

         return $validator->messages();
    }
$url = "https://api.paystack.co/transaction/verify/".$request->reference;
    $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer sk_test_43baab53ab8c7dbd14678e30525ae4949956d544",
    "Cache-Control: no-cache",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

if ($err) {
  
  $responce['code'] = 404;
  $responce['error'] = "cURL Error #:" . $err;
  return response()->json($responce,200);

} else {
    $ad = product::findOrFail($request->product_id);

    $payment = new adPayment;
    $payment->product_id = $ad->id;
    $payment->ad_type = $ad->ad_type;
    $payment->payment_method = 'card';
    $payment->payment_reference = $request->reference;
    $payment->amount = $request->amount;
    $payment->status = 'successful';
    $payment->save();
    $ad->payment_id = $payment->id;
    $ad->save();
    $payment = new payments;
    $payment->creditCompany(Auth::User()->id , $request->amount , $ad->type.' Subcribtion Payment for Ad  with ID: '.$ad->id);

    $responce['code'] = 200;
     return response()->json($responce,200);
}
    

  }
    public function dbtregistrationPayment(request $request){
      $validator = Validator::make($request->all(), [
        "amount" =>  "required",
        'file' => 'required',
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }

  if ($request->has('file')) {
    $image = $request->file('file');
    $name = 'proof'.'_'.time();
    $filePath =  $name. '.' . $image->getClientOriginalExtension();
    $request->file->storeAs('proof', $filePath, 'public');
    $image_name = $filePath;
    $user = Auth::User();
    $proof = new proof;
    $proof->image_name =  $image_name;
    $proof->amount =  $request->amount;
    $proof->type =  "registration";

    if($user->role == '3'){
    $proof->item_id = $user->id;
    $proof->item_category = 'artisan'; 
    $user_data = artisan::where('user_id' , Auth::User()->id)->first();

     
    }else if($user->role == '4'){
    $user_data = supervisorData::where('user_id' , Auth::User()->id)->first();

      $proof->item_id = $user->id;
      $proof->item_category = 'supervisor'; 
    }
    $proof->save();
      $payment = new registrationPayment;
      $payment->user_id = Auth::User()->id;
      $payment->user_type = Auth::User()->role;
      $payment->payment_method = 'DBT';
      $payment->payment_reference = $proof->id;
      $payment->amount = $request->amount;
      $payment->status = 'pending';
      $payment->save();
      $user_data->payment_status = '2';
      $user_data->save();
      $responce['code'] = 200;
       return response()->json($responce,200);
    }
  }
  public function dbtrequestPayment(request $request){
    $validator = Validator::make($request->all(), [
      "amount" =>  "required",
      'file' => 'required',
      'jobmerge_id' => 'required',
]);

if ($validator->fails()) {

     return $validator->messages();
}
  $jobmerge = jobmerge::find($request->jobmerge_id);
if ($request->has('file')) {
  $image = $request->file('file');
  $name = 'proof'.'_'.time();
  $filePath =  $name. '.' . $image->getClientOriginalExtension();
  $request->file->storeAs('proof', $filePath, 'public');
  $image_name = $filePath;
  $user = Auth::User();
  $proof = new proof;
  $proof->image_name =  $image_name;
  $proof->amount =  $request->amount;
  $proof->type =  "request";
  $proof->item_id = $jobmerge->id;
  $proof->item_category = 'request';
  
  $proof->save();
  $jobmerge->status = 'Pending Payment Confirmation';
  $jobmerge->payment_status = '2';
  $jobmerge->save();

  $job = job::findOrFail($jobmerge->job_id);
  $job->status = 'Pending Payment Confirmation';
  $job->save();
 
    $responce['code'] = 200;
     return response()->json($responce,200);
  }
}
public function rejectRegPayment($proof_id){
 $proof = proof::findOrFail($proof_id);
 $payment = registrationPayment::where(['payment_reference' => $proof_id])->first();
 $proof->status = 'rejected';
 if(isset($payment)){
  $payment->status = "failed";
  $payment->save();
 }
 $user = User::findOrFail($proof->item_id);
  if($user->role == '3'){
    $artisan = artisan::where('user_id' , $user->id)->first();
    if(isset($artisan)){
      $artisan->payment_status = "0";
      $artisan->save();
    }
  }else if($user->role == '4'){
    $supervisor = supervisorData::where('user_id' , $user->id)->first();
    if(isset($supervisor)){
      $supervisor->payment_status = "0";
      $supervisor->save();
    }
  }
 $proof->save();

 $responce['code'] = 200;
 return response()->json($responce,200);
}
public function acceptRegPayment($proof_id){
  $proof = proof::findOrFail($proof_id);
  $payment = registrationPayment::where(['payment_reference' => $proof_id])->first();
  $proof->status = 'accepted';
  if(isset($payment)){
   $payment->status = "successfull";
   $payment->save();
  }
  $user = User::findOrFail($proof->item_id);
  if($user->role == '3'){
    $artisan = artisan::where('user_id' , $user->id)->first();
    if(isset($artisan)){
      $artisan->payment_status = "1";
      $artisan->save();
    }
  }else if($user->role == '4'){
    $supervisor = supervisorData::where('user_id' , $user->id)->first();
    if(isset($supervisor)){
      $supervisor->payment_status = "1";
      $supervisor->save();
    }
  }
  $proof->save();

  $payment = new payments;
  $payment->creditCompany($proof->item_id , $proof->amount , 'Registration fees for User with ID: '.$proof->item_id);
  $responce['code'] = 200;
  return response()->json($responce,200);
}
public function rejectReqPayment($proof_id){
  $proof = proof::findOrFail($proof_id);
  $jobmerge = jobmerge::findOrFail($proof->item_id);
  $job = job::findOrFail($jobmerge->job_id);
  $proof->status = 'rejected';
  $jobmerge->status = "Quoted";
  $jobmerge->payment_status = "0";
  $job->status = "Quoted";
  $job->save();
  $proof->save();
  $jobmerge->save();
  $responce['code'] = 200;
  return response()->json($responce,200);
}
public function acceptReqPayment($proof_id){
  $proof = proof::findOrFail($proof_id);
  $jobmerge = jobmerge::findOrFail($proof->item_id);
  $job = job::findOrFail($jobmerge->job_id);
  $proof->status = 'accepted';
  $jobmerge->status = "Paid";
  $jobmerge->payment_status = "1";
  $job->status = "Paid";
  $job->amount_paid = $job->amount_paid + $proof->amount;
  $jobmerge->amount_paid = $jobmerge->amount_paid + $proof->amount;

  $job->save();
  $proof->save();
  $jobmerge->save();
  $payment = new payments;
  $payment->creditCompany($job->jobber_id , $proof->amount , 'Registration fees for User with ID: '.$proof->item_id);
  
  $responce['code'] = 200;
  return response()->json($responce,200);
}
}
