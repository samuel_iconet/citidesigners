<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\property;
use App\option;
use App\subcategory;
use Validator;
class categoryPropertiesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function addoption(request $request){
        $validator = Validator::make($request->all(), [
            "property_id" =>  "required",
             "name" => "required",
           
        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $option  = new option;
    $option->property_id = $request->property_id;
    $option->name = $request->name;
    $option->status = '1';
    $option->save();
    $response['code'] = 200;
    return response()->json($response ,200); 
    }
    public function deleteoption($option_id){
        $option = option::findOrFail($option_id);
        $option->delete();
        $response['code'] = 200;
    return response()->json($response ,200); 
    }
    public function deleteproperty($property_id){
        $property = property::findOrFail($property_id);
        $options = option::where('property_id' , $property->id)->get();
        if(count($options) > 0){
            foreach($options as $option){
                $option->delete();
            }
        }
        $property->delete();
        $response['code'] = 200;
        return response()->json($response ,200); 
    }
    public function getProperties(){
        $categories = category::all();
        if(count($categories) > 0){
            foreach($categories as $category){
                $subcategories = subcategory::where('category_id' , $category->id)->get();
                if(count($subcategories ) > 0){
                    foreach($subcategories as $subcategory){
                         $properties = property::where('subcategory_id' , $subcategory->id)->get();
                        if(count($properties ) > 0){
                            foreach($properties as $property){
                                $options = option::where('property_id' , $property->id)->get();
                                $property['options'] = $options;
                            }
                        }
                        $subcategory['properties'] = $properties;
                    }

                }
                $category['subcategories'] = $subcategories;
            }         
        }
        $response['code'] = 200;
        $response['categories'] = $categories;
        return response()->json($response ,200); 
        
    }
    public function addProperty(request $request){
        $validator = Validator::make($request->all(), [
            "subcategory_id" =>  "required",
             "name" => "required",
             "type" => "required",
             "required" => "required",
              "status" => "required"
        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $property = new property;
    $property->name = $request->name;
    $property->type = $request->type;
    $property->subcategory_id = $request->subcategory_id;
    $property->required = $request->required;
    $property->status =  $request->status;
    $property->save();
    $response['code'] = 200;
        return response()->json($response ,200);
    }
    public function editProperty(request $request){
        $validator = Validator::make($request->all(), [
             "name" => "required",
             "type" => "required",
             "required" => "required",
             "property_id" => "required",
              "status" =>  "required",

        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $property =  property::findOrFail($request->property_id);
    $property->name = $request->name;
    $property->type = $request->type;
    $property->required = $request->required;
    $property->status =  $request->status;
    $property->save();
    $response['code'] = 200;
        return response()->json($response ,200);
    }
    
}
