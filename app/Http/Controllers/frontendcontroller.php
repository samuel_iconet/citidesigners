<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\item;
use App\skill;
use App\tip;
use App\item_image;
use App\User;
use App\contact_us;
use App\message;
use App\category;
use App\property;
use App\option;
use App\state;
use App\artisan;
use App\supervisorData;
use App\area;
use App\adcheck;
use App\subcategory;
use Validator;
use App\user_data;
use App\product;
use App\productImage;
use App\productReview;
use App\Mail\orderMail;

use Illuminate\Support\Facades\Mail;
use Session;
class frontendcontroller extends Controller
{
    //
    public function testcredit(request $request){
        return $request->data;
        $payment = new payments;
       return  $payment->creditCompany('2' , '1000' , 'Registration Fees');
      }
    public function testSms(){
       
        $payment = new message;
        return  $payment->sendSms('08169401926' , 'Registration Fees');
        // $curl = curl_init();
        // $data = array("to" => "2348169401926","from" => "CitiDesign",
        // "sms"=>"Hi there, testing Termii","type" => "plain","channel" => "generic","api_key" => "TLieQixVvDnL3BoS5oZRPwyaQmzKzWjcdyNXpu4eHHf2yc4076r27nWwFQUGlJ");
        
        // $post_data = json_encode($data);
        
        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://termii.com/api/sms/send",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 0,
        //   CURLOPT_FOLLOWLOCATION => true,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => $post_data,
        //   CURLOPT_HTTPHEADER => array(
        //     "Content-Type: application/json"
        //   ),
        // ));
        
        // $response = curl_exec($curl);
        
        // curl_close($curl);
//         $curl = curl_init();
// $data = array("to" => "2348169401926","sms"=>"Hi there, testing Termii","api_key" => "TLieQixVvDnL3BoS5oZRPwyaQmzKzWjcdyNXpu4eHHf2yc4076r27nWwFQUGlJ",  );

// $post_data = json_encode($data);

// curl_setopt_array($curl, array(
//   CURLOPT_URL => "https://termii.com/api/sms/number/send",
//   CURLOPT_RETURNTRANSFER => true,
//   CURLOPT_ENCODING => "",
//   CURLOPT_MAXREDIRS => 10,
//   CURLOPT_TIMEOUT => 0,
//   CURLOPT_FOLLOWLOCATION => true,
//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//   CURLOPT_CUSTOMREQUEST => "POST",
//   CURLOPT_POSTFIELDS => $post_data,
//   CURLOPT_HTTPHEADER => array(
//     "Content-Type: application/json"
//   ),
// ));

// $response = curl_exec($curl);

// curl_close($curl);
        // return  $response;  
    }
    public function register(){
    $skills =  skill::where('status' , '1')->get();
     return view('register',  ['skills' => $skills]);   
    }
    public function home(){
       
        $count  = count(product::where('status' , 'Approved' )->get());
        if($count > 9){
           $products = product::where('status' , 'Approved' )->get()->random(9);
        }else{
            $products = product::where('status' , 'Approved' )->get(); 
        }
        $products = $this->populateImage($products);
        // $items  = item::where('status' , '1')->get();
        $skills =  skill::where('status' , '1')->get();
        $tips = tip::where('status' , '1')->get();
        // if(count($items) > 9 ){
        //     $items  = item::where('status' , '1')->get()->random(9);
        // }
        if(count($skills) > 9){
            $skills =  skill::where('status' , '1')->get()->random(9);
        }

        return view('home' , ['tips' => $tips , 'products' => $products , 'skills' => $skills]);
    }
    public function sortarea(request $request){
       if(isset($request->category_id)){
        $setcategory = category::findOrFail($request->category_id);
        if($setcategory->status  == '1'){
            $categories = category::where('status' , '1')->get();
            if(count($categories) > 0){
                foreach($categories as $category){
                    $category['count'] = count(product::where(['status' => 'Approved'  , 'category_id' => $category->id])->get());
                    $subcategories = subcategory::where(['category_id' =>  $category->id , 'status' => '1'])->get();
                    foreach( $subcategories as $subcategory){
                        $subcategory['count'] = count(product::where(['status' => 'Approved'  , 'subcategory_id' => $subcategory->id])->get());  
                    }
                    $category['subcategories'] = $subcategories;
                }
                
                
            } 

            $count = count(product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get());
              $specialProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random($count);

              $specialProduct = $this->populateImage($specialProduct);
              if(count($specialProduct) > 3){
                 $special3  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random(3);

              }else{
                  $special3 = $specialProduct;
              }

              $special3 = $this->populateImage($special3);
              $count  = count(product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get());
              $premiumProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get()->random($count);
              $premiumProduct = $this->populateImage($premiumProduct);
             $latestProducts = product::where('status' , 'Approved')->orderBy('id', 'desc')->get();
             $latestProducts = $this->populateImage($latestProducts);
              if(count($latestProducts) > 3) {
                $latest3 = product::where('status' , 'Approved')->orderBy('id', 'desc')->take(3)->get();
              }else{
                $latest3 = $latestProducts;
              }
             $latest3 = $this->populateImage($latest3);

           if(isset($request->subcategory_id)){
            $setsubcategory = subcategory::findOrFail($request->subcategory_id);
            $products = product::where(['status' => 'Approved' , 'subcategory_id' => $request->subcategory_id,'category_id' => $request->category_id , 'state' => $request->state ,'area' => $request->area])->paginate(8);
            $products = $this->populateImage($products);
                return view('productlist' ,
                    [
                'categories' => $categories , 
                'setcategory' => $setcategory , 
                'setsubcategory' => $setsubcategory , 
                    'special3' => $special3,
                    'latest3' => $latest3,
                    'products' => $products,
                    ]);
           } else{
            $products = product::where(['status' => 'Approved' ,'category_id' => $request->category_id , 'state' => $request->state ,'area' => $request->area])->paginate(8);
            $products = $this->populateImage($products);
                return view('productlist' ,
                    [
                'categories' => $categories , 
                'setcategory' => $setcategory , 
                    'special3' => $special3,
                    'latest3' => $latest3,
                    'products' => $products,
                    ]);
           }
           
        }else{
            return redirect()->back();
        } 
       }
    }
    public function registerRef($ref_code){
        $user = User::where('ref_code' , $ref_code)->first();
        if(isset($user) && $user->role == '4'){
            $skills =  skill::where('status' , '1')->get();
            $ref_code = $ref_code;
          return view('register',  ['skills' => $skills , 'ref_code' => $ref_code]); 
        }
        return view('notfound');
    }
    public function setnewpassword($email , $token){
        $user = User::where(['email' => $email , 'remember_token' => $token])->first();
        if(isset($user)){
         return view('recoverpassword' , ['user' => $user ]); 
 
        }
        return view('recoverpassword' , ['error' => "Invalid Recovery Token" ]);
    }
    public function review(request $request){
        $validator = Validator::make($request->all(), [
            "review" =>  "required",
            'email' => 'required',
            'name' => 'required',
            'product_id' => 'required',
           

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $review  = new productReview;
      $review->name = $request->name;
      $review->email = $request->email;
      $review->review = $request->review;
      $review->product_id = $request->product_id;
      $review->save();
      $response['code'] = 200;
      return response()->json($response ,200);
    }
    public function getItem($id){
        $product = product::findOrFail($id);
        $product['user'] = User::findOrFail($product->user_id);
        $product['props'] = unserialize($product->properties);
        $product['user'] = User::findOrFail($product->user_id); 
       
         $user = User::findOrFail($product->user_id);
                if($user->role == '3' ){
            $product['user_data'] = artisan::where('user_id' , $product->user_id)->first();

        }else if( $user->role == '4'){
           $product['user_data']  = supervisorData::where('user_id' , $product->user_id)->first();
            
        }else{
            $product['user_data']  = user_data::where('user_id' , $product->user_id)->first();

        }
        $product['images'] = productImage::where('product_id' , $product->id)->get();
        $count = count(product::where('subcategory_id' , $product->subcategory_id)->where('id' , '<>' ,$product->id)->get());
        $product['relatedProducts'] = product::where('subcategory_id' , $product->subcategory_id)->where('id' , '<>' ,$product->id)->get();
        if($count > 0 && $count < 5){

        }else if($count > 0){
            $product['relatedProducts'] = product::where('subcategory_id' , $product->subcategory_id)->where('id' , '<>' ,$product->id)->get()->random(4);
            
        }

        $product['relatedProducts'] = $this->populateImage($product['relatedProducts']);
        $product['productreview'] = productReview::where('product_id' , $product->id)->orderBy('id' ,'desc')->get();
        $product['ReviewCount'] = count(productReview::where('product_id' , $product->id)->get());
        $count = count(product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get());
        $specialProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random($count);
        return view('productdetails' ,
        ['product' => $product , 
       
        ]);
    }
    public function subcategoryset($id){
        $setsubcategory = subcategory::findOrFail($id);
        $setcategory = category::findOrFail($setsubcategory->category_id);
        if($setcategory->status  == '1'){
            $categories = category::where('status' , '1')->get();
            if(count($categories) > 0){
                foreach($categories as $category){
                    $category['count'] = count(product::where(['status' => 'Approved'  , 'category_id' => $category->id])->get());
                    $subcategories = subcategory::where(['category_id' =>  $category->id , 'status' => '1'])->get();
                    foreach( $subcategories as $subcategory){
                        $subcategory['count'] = count(product::where(['status' => 'Approved'  , 'subcategory_id' => $subcategory->id])->get());  
                    }
                    $category['subcategories'] = $subcategories;
                }
                
                
            } 

            $count = count(product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get());
              $specialProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random($count);

              $specialProduct = $this->populateImage($specialProduct);
              if(count($specialProduct) > 3){
                 $special3  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random(3);

              }else{
                  $special3 = $specialProduct;
              }

              $special3 = $this->populateImage($special3);
              $count  = count(product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get());
              $premiumProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get()->random($count);
              $premiumProduct = $this->populateImage($premiumProduct);
             $latestProducts = product::where('status' , 'Approved')->orderBy('id', 'desc')->get();
             $latestProducts = $this->populateImage($latestProducts);
              if(count($latestProducts) > 3) {
                $latest3 = product::where('status' , 'Approved')->orderBy('id', 'desc')->take(3)->get();
              }else{
                $latest3 = $latestProducts;
              }
             $latest3 = $this->populateImage($latest3);

             $products = product::where(['status' => 'Approved' , 'subcategory_id' => $id])->paginate(8);
             $products = $this->populateImage($products);
        return view('productlist' ,
         [
        'categories' => $categories , 
        'setcategory' => $setcategory , 
        'setsubcategory' => $setsubcategory , 
         'special3' => $special3,
         'latest3' => $latest3,
         'products' => $products,
         ]); 
        }else{
            return redirect()->back();
        }
    }
    public function populateImage($products){
        if(count($products) > 0) {
            $adExpiration = new adcheck;
            $adExpiration->cancelExpiredAds($products);
            foreach($products as $product){
                $product['statedata'] = state::find($product->state);
                $product['areadata'] = area::find($product->area);
                $product['images'] = productImage::where('product_id' , $product->id)->get();
                // $a = new \NumberFormatter("it-IT", \NumberFormatter::CURRENCY);
                // $product->price = $a->formatCurrency( $product->price , "NGN");
            }
        }
        return $products;
    }
    public function categoryset($id){
        $setcategory = category::findOrFail($id);
        if($setcategory->status  == '1'){
            $categories = category::where('status' , '1')->get();
            if(count($categories) > 0){
                foreach($categories as $category){
                    $category['count'] = count(product::where(['status' => 'Approved'  , 'category_id' => $category->id])->get());
                    $subcategories = subcategory::where(['category_id' =>  $category->id , 'status' => '1'])->get();
                    foreach( $subcategories as $subcategory){
                        $subcategory['count'] = count(product::where(['status' => 'Approved'  , 'subcategory_id' => $subcategory->id])->get());  
                    }
                    $category['subcategories'] = $subcategories;
                }
                
                
            } 

            $count = count(product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get());
              $specialProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random($count);

              $specialProduct = $this->populateImage($specialProduct);
              if(count($specialProduct) > 3){
                 $special3  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random(3);

              }else{
                  $special3 = $specialProduct;
              }

              $special3 = $this->populateImage($special3);
              $count  = count(product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get());
              $premiumProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get()->random($count);
              $premiumProduct = $this->populateImage($premiumProduct);
             $latestProducts = product::where('status' , 'Approved')->orderBy('id', 'desc')->get();
             $latestProducts = $this->populateImage($latestProducts);
              if(count($latestProducts) > 3) {
                $latest3 = product::where('status' , 'Approved')->orderBy('id', 'desc')->take(3)->get();
              }else{
                $latest3 = $latestProducts;
              }
             $latest3 = $this->populateImage($latest3);

             $products = product::where(['status' => 'Approved' , 'category_id' => $id])->paginate(8);
             $products = $this->populateImage($products);
        return view('productlist' ,
         [
        'categories' => $categories , 
        'setcategory' => $setcategory , 
         'special3' => $special3,
         'latest3' => $latest3,
         'products' => $products,
         ]); 
        }else{
            return redirect()->back();
        }

    }
    public function market(){
        $categories = category::where('status' , '1')->get();
        if(count($categories) > 0){
            foreach($categories as $category){
                $category['count'] = count(product::where(['status' => 'Approved'  , 'category_id' => $category->id])->get());
                $subcategories = subcategory::where(['category_id' =>  $category->id , 'status' => '1'])->get();
                foreach( $subcategories as $subcategory){
                    $subcategory['count'] = count(product::where(['status' => 'Approved'  , 'subcategory_id' => $subcategory->id])->get());  
                }
                $category['subcategories'] = $subcategories;
            }         
        }
              $count = count(product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get());
              $specialProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random($count);

              $specialProduct = $this->populateImage($specialProduct);
              if(count($specialProduct) > 3){
                 $special3  = product::where(['status' => 'Approved' , 'ad_type' => 'standard'])->get()->random(3);

              }else{
                  $special3 = $specialProduct;
              }

              $special3 = $this->populateImage($special3);
              $count  = count(product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get());
              $premiumProduct  = product::where(['status' => 'Approved' , 'ad_type' => 'premium'])->get()->random($count);
              $premiumProduct = $this->populateImage($premiumProduct);
             $latestProducts = product::where('status' , 'Approved')->orderBy('id', 'desc')->get();
             $latestProducts = $this->populateImage($latestProducts);
              if(count($latestProducts) > 3) {
                $latest3 = product::where('status' , 'Approved')->orderBy('id', 'desc')->take(3)->get();
              }else{
                $latest3 = $latestProducts;
              }
             $latest3 = $this->populateImage($latest3);
        return view('marketplace' ,
         ['categories' => $categories , 
         'specialProducts' => $specialProduct  , 
         'special3' => $special3,
         'latestProducts' => $latestProducts,
         'latest3' => $latest3,
         'premiumProducts'=> $premiumProduct
         ]);    
    }
    public function skills(){
        $skills  = skill::where('status' , '1')->get();
        return view('artisan' , ['skills' => $skills ]);    
    }
    public function requestartisan(){
        $skills  = skill::where('status' , '1')->get();
        return view('requestartisan' , ['skills' => $skills ]);    
    }
    public function contactus(request $request){
        $contact = new contact_us;
        $contact->name = $request->contact_names;
        $contact->email = $request->contact_email;
        $contact->message = $request->contact_message;
        $contact->number = $request->contact_phone;
        $contact->save();
        Session::flash('contactus', 'true'); 

        return redirect('/#contact');

    }
    
}
