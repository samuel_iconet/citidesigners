<?php

namespace App\Http\Controllers;
use App\state;
use App\area;
use App\job;
use App\User;
use App\skill;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Mail\adminjobMail;
use App\Mail\jobRequestMail;
use Validator;
use Illuminate\Http\Request;

class jobcontroller extends Controller
{
    public function getstatesData(){
        $states = state::all();
        $areas = area::all();
        $skills = skill::where('status' , '1')->get();
        $response['code'] = 200;
        $response['states'] = $states;
        $response['areas'] = $areas;
        $response['skills'] = $skills;
        return response()->json($response ,200);
    }
    public function makeJobrequest(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required",
            'email' => 'required',
            'skill_type' => 'required',
            'description' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'state' => 'required',
            'area' => 'required'

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $v_user = User::where('email' , $request->email)->first();
      if(isset($v_user)){
      $user = $v_user;
      }
      else{
      $user = new User;
      $user->role = 1;
      $user->status = 0;
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = bcrypt("password");
      $user->save();
      Mail::to($user)->send(new WelcomeMail($user));
      }

     $job = new job;
      $job->jobber_id = $user->id;
      $job->skills_id = $request->skill_type;
      $job->description = $request->description;
      $job->address = $request->address;
      $job->phone_number = $request->phone_number;
      $job->area = $request->area;
      $job->state = $request->state;
      $job->status = 'Pending';
      $job->save();
      $admins = User::where('role' , '2')->get();
      foreach($admins as $admin){
      Mail::to($admin)->send(new adminjobMail($admin));

      }
      Mail::to($user)->send(new jobRequestMail($user));
      $response['code'] = 200;
        return response()->json($response ,200);
      


    }
}
