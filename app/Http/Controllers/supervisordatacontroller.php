<?php

namespace App\Http\Controllers;
use App\supervisorData;
use Auth;
use App\state;
use App\skill;
use App\area;
use App\proof;
use App\artisan;
use App\User;
use validate;
use Str;
use Illuminate\Http\Request;

class supervisordatacontroller extends Controller
{
    //
   
    public function __construct()
    {
       
        $this->middleware('auth:api');
         $this->middleware('Supervisor');

    }
    public function updatesupervisor(){
        $this->validate($request,[     
            'skill_id' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'account_number' => 'required',
            'account_name' => 'required',
            'guarantor' => 'required',
            'guarantor_number' => 'required',
            'address' => 'required',
            'area_id' => 'required',
            'state_id' => 'required',
            'date_of_birth' => 'required',
            'id_type' => 'required',
            'id_number' => 'required',
        ]);
       $request->date_of__birth = date('Y-m-d',strtotime($request->date_of_birth));
       $artisandata =  artisan::update($request->all());
       $response['code'] = 200;
       $response['artisandata'] = artisandata;
        return response()->json($response,200);
   }
   public function updateArtisan(request $request){
       $validator = Validator::make($request->all(), [
           'phone' => 'required|string|',
           'address' => 'required|',
           'state' => 'required|',
           'area' => 'required|',
           'bank_name' => 'required|',
           'account_number' => 'required|',
           'guarantor' => 'required|',
           'guarantor_number' => 'required|',
           'id_type' => 'required|',
           'id_number' => 'required|',
           'gender' => 'required|',
           'skill_id'=> 'required',
           'date_of_birth'=> 'required'
     ]);

     if ($validator->fails()) {

          return $validator->messages();
     }
     $user = Auth::User();
   $area = area::find($request->area);
   $state = state::find($request->state);
   $user_data = supervisorData::where('user_id' , $user->id)->first(); 
    
     if ($request->has('file_profile')) {
       $image = $request->file('file_profile');
       $name = 'profile'.'_'.time();
         $filePath =  $name. '.' . $image->getClientOriginalExtension();
       $request->file_profile->storeAs('profile', $filePath, 'public');
       $profile_image = $filePath;
       $user_data->profile_image = $profile_image;	

   }

     if ($request->has('file_guarantor_profile')) {
       $image = $request->file('file_guarantor_profile');
       $name = 'guarantor_profile'.'_'.time();
       $filePath =  $name. '.' . $image->getClientOriginalExtension();
       $request->file_guarantor_profile->storeAs('profile', $filePath, 'public');
       $guarantor_profile_image = $filePath;
        $user_data->guarantor_profile = $guarantor_profile_image;

   }
  
   if ($request->has('file_guarantor_id')) {
     $image = $request->file('file_guarantor_id');
     $name = 'guarantor_id'.'_'.time();
     $filePath =  $name. '.' . $image->getClientOriginalExtension();
     $request->file_guarantor_id->storeAs('ids', $filePath, 'public');
     $guarantor_id_image = $filePath;
   $user_data->guarantor_id = $guarantor_id_image;

 }
 
 if ($request->has('file_id')) {
   $image = $request->file('file_id');
   $name = 'id'.'_'.time();
   $filePath =  $name. '.' . $image->getClientOriginalExtension();
   $request->file_id->storeAs('ids', $filePath, 'public');
   $id_image = $filePath;
   $user_data->artisan_id = $id_image;

}

  
   $user_data->skill_id = $request->skill_id;
   $user_data->phone_number = $request->phone;
   $user_data->account_number = $request->account_number;
   $user_data->account_name = $request->bank_name;
   $user_data->guarantor = $request->guarantor;
   $user_data->guarantor_number = $request->guarantor_number;
   $user_data->address = $request->address;
   $user_data->area_id = $request->area;
   $user_data->state_id = $request->state;
   $user_data->gender = $request->gender;
   $user_data->id_type = $request->id_type;
   $user_data->id_number = $request->id_number;
   $user_data->state_name = $state->name;
   $user_data->area_name = $area->name;
   $user_data->status = 'pending';
   $user_data->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
   $user_data->save();
   $querries = querry::where('user_id' , Auth::User()->id)->get();
   foreach($querries as $querry){
       $querry->status = "resolved";
       $querry->save();
   }
   $response['code'] = 200;
  return response()->json($response ,200);
    }
    public function getData(){
        $supervisor_data = supervisorData::where('user_id' , Auth::User()->id)->first();
        $user = User::findOrFail($supervisor_data->user_id);
        if($user->ref_code == null){
          $user->ref_code = $user->id.'_'.Str::random(5);
          $user->save();
        }
       $proofs = proof::where(['item_id' => Auth::User()->id , 'type' => 'registration' , 'item_category' => 'supervisor'])->get();
         $states = state::where('status' , '1')->get();
         $areas = area::where('status' , '1')->get();
         $skills = skill::all();
        if(isset($supervisor_data)){
         $response['code'] = 200;
         $response['supervisor_data'] = $supervisor_data;
 
        }else{
         $response['code'] = 401;
 
         $response['error'] = "Supervisor Data not set.";
         $response['supervisor_data'] = supervisorData::find(1);
        }
        $response['ref_code'] = $user->ref_code;
        $response['skills'] = $skills;
        $response['areas'] = $areas;
        $response['states'] = $states;
        $response['proofs'] = $proofs;
      
         return response()->json($response,200);
 
     }
     public function validateSupervisor(){
        $status = Auth::User()->status;
        $data = supervisorData::where('user_id' , Auth::User()->id)->first();
         if($status == '0' || $data->status != 'approved') {
          $response['code'] = 303;
          $response['error'] = 'inactive Profile';
         $response['status'] = $status;
           return response()->json($response,200);
         }
         $response['code'] = 200;
         $response['status'] = $status;
          return response()->json($response,200);
     }
     public function uploadid(request $request){
        $this->validate($request,[     
            'file' => 'required',
            'id_type' => 'required',
            'id_number' => 'required',
         ]);
         if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'profile'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $request->file->storeAs('ids', $filePath, 'public');
            $image_name = $filePath;
            $data = supervisorData::where('user_id' , Auth::user()->id)->first();
            if(isset($data)){
                $data->id_image =  $image_name;
                $data->id_type =  $request->id_type;
                $data->id_number =  $request->id_number;
                $data->save();
                $response['code'] = 200;
                return response()->json($response , 200);  
            }else{
                $response['code'] = 401;
                $response['error'] = "Please provide User Data";
                return response()->json($response , 200);  
            }
        }  else{
            $response['code'] = 401;
                $response['error'] = "Please provide User Data";
                return response()->json($response , 200); 
        }
     }
     public function uploadprofile(request $request){
        $this->validate($request,[     
            'file' => 'required',
         ]);
         if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'profile'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $request->file->storeAs('profile', $filePath, 'public');
            $image_name = $filePath;
        }
        $data = supervisorData::where('user_id' , Auth::user()->id)->first();
        if(isset($data)){
            $data->profile_image =  $image_name;
            $data->save();
            $response['code'] = 200;
            return response()->json($response , 200);  
        }else{
            $response['code'] = 401;
            $response['error'] = "Please provide User Data";
            return response()->json($response , 200);  
        }
        
   
    }
    public function getrefferals(){
      $refferals = artisan::where('ref_code' , Auth::User()->ref_code)->get();
      foreach($refferals as $refs){
        $refs['user'] = User::find($refs->user_id);
      }
      $response['code'] = 200;
      $response['refferals'] = $refferals;
      return response()->json($response , 200); 
    }
     public function setSupervisorData(request $request){
       // return $request->all();
           $this->validate($request,[     
                'phone_number' => 'required',
                'account_number' => 'required',
                'account_name' => 'required',
                'guarantor' => 'required',
                'guarantor_number' => 'required',
                'address' => 'required',
                'area_id' => 'required',
                'state_id' => 'required', 
                'date_of_birth' => 'required', 
                'gender' => 'required', 
                
            
            ]);
            $request['user_id'] = Auth::User()->id;  
            $exist_supervisor = supervisorData::where('user_id' , Auth::User()->id)->first();
             $state = state::find($request->state_id);
            $area = area::find($request->area_id);
            if(isset($exist_supervisor)){
               $exist_supervisor->phone_number = $request->phone_number;
               $exist_supervisor->account_number = $request->account_number;
               $exist_supervisor->account_name = $request->account_name;
               $exist_supervisor->guarantor = $request->guarantor;
               $exist_supervisor->guarantor_number = $request->guarantor_number;
               $exist_supervisor->address = $request->address;
               $exist_supervisor->area_id = $request->area_id;
               $exist_supervisor->state_id = $request->state_id;
               $exist_supervisor->state_name = $state->name;
               $exist_supervisor->gender = $request->gender;
               $exist_supervisor->area_name = $area->name;
               $exist_supervisor->date_of_birth = date('Y-m-d',strtotime($request->date_of_birth));
               $exist_supervisor->save();
               $supervisordata = $exist_supervisor;
            }else{
               // $artisandata =  artisan::create($request->all());
               $exist_supervisor = new supervisorData;
               $exist_supervisor->phone_number = $request->phone_number;
               $exist_supervisor->account_number = $request->account_number;
               $exist_supervisor->account_name = $request->account_name;
               $exist_supervisor->guarantor = $request->guarantor;
               $exist_supervisor->guarantor_number = $request->guarantor_number;
               $exist_supervisor->address = $request->address;
               $exist_supervisor->area_id = $request->area_id;
               $exist_supervisor->state_id = $request->state_id;
               $exist_supervisor->state_name = $state->name;
               $exist_supervisor->area_name = $area->name;
               $exist_supervisor->user_id = $request->user_id;
               $exist_supervisor->gender = $request->gender;
               $exist_supervisor->date_of_birth =  date('Y-m-d',strtotime($request->date_of_birth));

               $exist_supervisor->save();
               $supervisordata = $exist_supervisor;
               
            }
           $response['code'] = 200;
           $response['supervisordata'] = $supervisordata;
            return response()->json($response,200);
   
       }
}
