<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\jobmerge;
use App\user_data;
use App\job;
use App\skill;
use App\area;
use App\artisan;
use App\state;
use App\getstateareaname;
use App\supervisorData;
use App\proof;
use Validator;

use App\Mail\artisanjobmergeMail;
use App\Mail\supervisorjobmergeMail;
use Illuminate\Support\Facades\Mail;
class adminjobcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function cancelMerge(request $request){
        $validator = Validator::make($request->all(), [
            "merge_id" =>  "required",
          
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $merge = jobmerge::findOrFail($request->merge_id);
      $job  = job::findOrFail($merge->job_id);
      $merge->status = "Cancelled";
      $job->status = "pending";
      $merge->save();
      $job->save();
      $response['code'] = 200;
      return response()->json($response ,200);
    }
    public function getJobs(){
     $newJobs = job::where("status" , "Pending")->get();
     $merge_jobs = jobmerge::all();
     $completed_jobs = jobmerge::where("status" , "Completed")->get();
     $states = state::all();
     $areas = area::all();
     $skills  = skill::all();
     $artisans = artisan::where('status' , 'approved')->get();
     $supervisors = supervisorData::where('status' , 'approved')->get();
     foreach($artisans as $artisan){
         $artisan['user'] = User::find($artisan->user_id);
         foreach($skills as $skill){
            if($skill->id == $artisan->skill_id){
                $artisan['skill_name'] = $skill->name;
            }
        }
     }
     foreach($supervisors as $supervisor){
        $supervisor['user'] = User::find($supervisor->user_id);
        
    }
     foreach($newJobs as $jobs){
         foreach($skills as $skill){
             if($skill->id == $jobs->skills_id){
                 $jobs['skill_name'] = $skill->name;
             }
            
         }
         $jobs['user'] = User::find($jobs->jobber_id);
         $stateareaData = new getstateareaname;
         $jobs['state_name'] =  $stateareaData->getStateName($jobs->state);
         $jobs['area_name'] =  $stateareaData->getAreaName($jobs->area);
     }
     foreach($merge_jobs as $jobs){
        $jobs['job'] = job::find($jobs->job_id);
        $jobs['user'] = User::find($jobs->user_id);
        $jobs['artisan'] = User::find($jobs->artisan_id);
        $jobs['supervisor'] = User::find($jobs->supervisor_id);
        $jobs['artisan_data']= artisan::where('user_id' , $jobs->artisan_id)->first();
        $jobs['supervisor_data']= supervisorData::where('user_id' , $jobs->supervisor_id)->first();
        $jobs['proofs']= proof::where(['type' => 'request' , 'item_id' => $jobs->id])->get();
     
        $stateareaData = new getstateareaname;
        $jobs['state_name'] =  $stateareaData->getStateName($jobs['job']->state);
        $jobs['area_name'] =  $stateareaData->getAreaName($jobs['job']->area);
        $skill_id = $jobs['artisan_data']['skill_id'];
        $skil_name = skill::find($skill_id);
        $name = "";
        $name = $skil_name['name'];
         $jobs['artisan_skills'] =  $name;

     }
     foreach($completed_jobs as $jobs){
        $jobs['job'] = job::find($jobs->job_id);
        $jobs['user'] = User::find($jobs->user_id);
        $jobs['artisan'] = User::find($jobs->artisan_id);
        $jobs['supervisor'] = User::find($jobs->supervisor_id);
        $jobs['artisan_data']= artisan::where('user_id' , $jobs->artisan_id)->first();
        $jobs['supervisor_data']= supervisorData::where('user_id' , $jobs->supervisor_id)->first();
     
        $stateareaData = new getstateareaname;
        $jobs['state_name'] =  $stateareaData->getStateName($jobs['job']->state);
        $jobs['area_name'] =  $stateareaData->getAreaName($jobs['job']->area);
        $skill_id = $jobs['artisan_data']['skill_id'];
        $skil_name = skill::find($skill_id);
        $name = "";
        $name = $skil_name['name'];
         $jobs['artisan_skills'] =  $name;
        

     }
     $this->automergeAll();
     $response['newjobs'] = $newJobs;
     $response['merge_jobs'] = $merge_jobs;
     $response['completed_jobs'] = $completed_jobs;
     $response['areas'] = $areas;
     $response['states'] = $states;
     $response['artisans'] = $artisans;
     $response['supervisors'] = $supervisors;
     $response['code'] = 200;
    return response()->json($response ,200);
    }
    public function automergeAll(){
        $jobs = job::where('status' , 'Pending')->get();
        return $jobs; 
        foreach($jobs as $job){
         $merge = new merge;
         $merge->mergeJob( $job->id);
        }
        return "done";
        
     }
    public function remergejob(request $request){
        // return $request->all();
        $validator = Validator::make($request->all(), [
            "jobmerge_id" =>  "required",
            "user_id" =>  "required",
            "user_type" =>  "required",
          
      ]);

      if ($validator->fails()) {

      return $validator->messages();
      }   
      $merged_job = jobmerge::findOrFail($request->jobmerge_id);
       $user = User::findOrFail($request->user_id);
      if($request->user_type == 'Artisan'){
         $merged_job->artisan_id = $user->id;
         $merged_job->artisan_status = 'Pending';
         Mail::to($user)->send(new artisanjobmergeMail($user));


      }else if($request->user_type == 'Supervisor'){
          $merged_job->supervisor_id = $user->id;
          $merged_job->supervisor_status = 'Pending';
     Mail::to($user)->send(new supervisorjobmergeMail($user));

      }
     
      $merged_job->save();
      $response['code'] = 200;
    return response()->json($response ,200);
    }
    public function mergeJobRepairs(request $request){
        $validator = Validator::make($request->all(), [
            "job_id" =>  "required",
            "artisan_id" =>  "required",
      ]);

      if ($validator->fails()) {

      return $validator->messages();
      }
      $job = job::findOrFail($request->job_id);
      $skill = skill::findOrFail($job->skills_id);
      $artisan = artisan::findOrFail($request->artisan_id);
      $jobmerge = new jobmerge;
      $jobmerge->job_id = $job->id;
      $jobmerge->job_name = $skill->name;
      $jobmerge->artisan_id = $artisan->user_id;
      $jobmerge->supervisor_id = 'null';
      $jobmerge->user_id = $job->jobber_id;
      $jobmerge->status = "Pending";
      $jobmerge->quote = "0";
      $jobmerge->payment_status = "0";
      $jobmerge->save();
      $job->status = "merged";
      $job->save();
      $art = User::find($artisan->user_id);
      Mail::to($art)->send(new artisanjobmergeMail($art));
      $response['code'] = 200;
      return response()->json($response ,200);
    }
    public function mergejobComplete(request $request){
        $validator = Validator::make($request->all(), [
            "job_id" =>  "required",
            "artisan_id" =>  "required",
            "supervisor_id" =>  "required",
          
      ]);

      if ($validator->fails()) {

      return $validator->messages();
      }
     $job = job::findOrFail($request->job_id);
     $skill = skill::findOrFail($job->skills_id);
     $artisan = artisan::findOrFail($request->artisan_id);
     $supervisor = supervisorData::findOrFail($request->supervisor_id);
     $jobmerge = new jobmerge;
     $jobmerge->job_id = $job->id;
     $jobmerge->job_name = $skill->name;
     $jobmerge->artisan_id = $artisan->user_id;
     $jobmerge->supervisor_id = $supervisor->user_id;
     $jobmerge->user_id = $job->jobber_id;
     $jobmerge->status = "Pending";
     $jobmerge->quote = "0";
     $jobmerge->payment_status = "0";
     $jobmerge->save();
     $job->status = "merged";
     $job->save();
     $art = User::find($artisan->user_id);
     $sup = User::find($supervisor->user_id);
     Mail::to($art)->send(new artisanjobmergeMail($art));
     Mail::to($sup)->send(new supervisorjobmergeMail($sup));
     $response['code'] = 200;
     return response()->json($response ,200);
    }

}
