<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class usercontroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function validateSession(){
        $user = User::find(Auth::USer()->id);
        $user->updated_at = now();
        $user->logged_in = '1';
        $user->save();
        $response['code'] = 200;
        return response()->json($response ,200);

    }
    public function logout(){
        $user = User::find(Auth::USer()->id);
        $user->logged_in = '0';
        $user->save();
        $response['code'] = 200;
        return response()->json($response ,200);
        
    }
   public function attemptlogin(){
      $result['data'] = 'success';
                $result['code'] = 200;
                return response()->json( $result,200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatepassword(request $request){
      $user =  User::findOrFail(Auth::User()->id);
      $user->password =  bcrypt($request->password); 
      $user->save();
      $result['code'] = 200;
      return response()->json( $result,200);
    }
    public function getUser()
    {
        $response['code'] = 200;
        $response['user'] = Auth::User();
         return response()->json($response,200);
    }
    public function EditUser(request $request){
        $this->validate($request,[     
            'name' => 'required'
            
        ]);

        $user = User::findOrFail(Auth::User()->id);
        $user->name = $request->name;
        $user->save();
        $response['code'] = 200;
        $response['user'] = $user;
         return response()->json($response,200);

    }
    
}
