<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\job;
use App\jobmerge;
use App\User;
use App\artisan;
use App\skill;
use App\user_data;
use App\supervisorData;
use App\proof;
use App\state;
use App\payments;
use App\area;
use App\merge;
use Auth;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Mail\adminjobMail;
use App\Mail\jobRequestMail;
class authjobcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
  
    public function makeJobrequest(request $request){
 
      $validator = Validator::make($request->all(), [
          "name" =>  "required",
            "email" => "required",
            "job_type" => "required",
            "skill_id" => "required",
            "description" => "required",
            "phone_number" => "required",
            "address" => "required",
            "state_id" => "required",
            "area_id" => "required"
      
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
      $user = Auth::User();
     

     $job = new job;
      $job->jobber_id = $user->id;
      $job->skills_id = $request->skill_id;
      $job->description = $request->description;
      $job->address = $request->address;
      $job->phone_number = $request->phone_number;
      $job->area = $request->area_id;
      $job->state = $request->state_id;
      $job->job_type = $request->job_type;
      $job->status = 'Pending';
      $job->save();
      $merge = new merge;

      $response['merge report'] = $merge->mergeJob( $job->id);
      $response['jobspending'] = $this->automergeAll();

    //   $admins = User::where('role' , '2')->get();
    //   foreach($admins as $admin){
    //   Mail::to($admin)->send(new adminjobMail($admin));

    //   }
    //   Mail::to($user)->send(new jobRequestMail($user));
      $response['code'] = 200;
        return response()->json($response ,200);
      


    }
    public function automergeAll(){
       $jobs = job::where('status' , 'Pending')->get();
       return $jobs; 
       foreach($jobs as $job){
        $merge = new merge;
        $merge->mergeJob( $job->id);
       }
      return 'done';
       
    }
    public function cancelRequest(request $request){
        $validator = Validator::make($request->all(), [
            "request_id" =>  "required",
            "reason" =>  "required",

        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $jobrequest = job::findOrFail($request->request_id);
    $jobmerge = jobmerge::where(['job_id' => $request->request_id ] )->get();
    if(isset($jobmerge)){
        foreach($jobmerge as $job){
            $job->status = 'Cancelled';
            $job->artisan_status = 'Cancelled';
            $job->supervisor_status = 'Cancelled';
            $job->save();
            
        }
    }
    if($jobrequest->status == 'merged' || $jobrequest->status == 'pending' || $jobrequest->status == 'Pending' || $jobrequest->status == 'Quoted')  {
        $jobrequest->status = 'Cancelled';
        $jobrequest->extra = $request->reason;
        $jobrequest->save();

    }
    $response['code'] = 200;
   
    return response()->json($response , 200);
    }
    public function userRequestData(){
        $response['total_request'] = count(job::where('jobber_id' , Auth::User()->id)->get());
        $response['merged_request'] = count(job::where(['jobber_id' => Auth::User()->id, 'status' => 'merged'])->get());
        $response['pending_request'] = count(job::where(['jobber_id' => Auth::User()->id, 'status' => 'Pending'])->get());
        $response['completed_request'] = count(job::where(['jobber_id' => Auth::User()->id, 'status' => 'Completed'])->get());
        $response['cancelled_request'] = count(job::where(['jobber_id' => Auth::User()->id, 'status' => 'Cancelled'])->get());
        $response['user'] = Auth::user();
        $response['userdata']  = user_data::where('user_id' , Auth::User()->id)->first();
        $response['skills'] = skill::where('status' , '1')->get();
        $response['areas'] = area::all();
        $response['states'] = state::all();
        
        return response()->json($response , 200);

    }
    public function getallRequest(){
    
    $requests = job::where('jobber_id' , Auth::user()->id)->get();
    $user = User::where('id' , Auth::User()->id)->first();
    $area  = area::all();
   $state = state::all();

    foreach($requests as $request){
        
        $merges = jobmerge::where(['job_id' => $request->id ])->where( 'status' ,'<>' , 'cancelled' )->get();
        if(count($merges) > 0){
                $request['proofs'] = proof::where(['item_id' => $merges[0]->id , 'type' => 'request' ])->get();
                $request['artisan'] = User::find($merges[0]->artisan_id);
                $request['jobmerge_id'] = $merges[0]->id;
                $request['payment_status'] = $merges[0]->payment_status;
                $request['supervisor'] = User::find($merges[0]->supervisor_id);
                $request['artisan_data'] = artisan::where('user_id' , $merges[0]->artisan_id)->first();
                $request['supervisor_data'] = supervisorData::where('user_id' , $merges[0]->supervisor_id)->first();
            
        }
        foreach($area as $lg){
            if($lg->id == $request->area){
              $request['area_name'] = $lg->name;
            }
        }
        foreach($state as $st){
            if($st->id == $request->state){
              $request['state_name'] = $st->name;
            }
        }
        $jobber = User::find($request->jobber_id);
        $skill = skill::find($request->skills_id);
        $request['jobber'] = $jobber;
        $request['skill'] = $skill;
    }
    $response['skills'] = skill::where('status' , '1')->get();
    $response['areas'] = area::all();
    $response['states'] = state::all();
    $response['code'] = 200;
    $response['requests'] = $requests;
    $response['user'] = $user;
    return response()->json($response , 200);
    }
    public function confirmrequest(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required'
          
         ]); 
         $jobmerge = jobmerge::findOrFail($request->jobmerge_id);
         $job  = job::findOrfail($jobmerge->job_id);
         if(Auth::User()->id == $job->jobber_id){
             $jobmerge->status = 'Completed';
             $job->status = 'Completed';
             $jobmerge->save();
             $job->save();
            $artisan = User::find($jobmerge->artisan_id);
            if(isset($artisan)){
                $payment = new payments;
             $payment->debitCompany($artisan->id , (76.5 /100 * $job->amount_paid) , 'Payment for Job Completed. Job ID: '.$jobmerge->id);
       
            }
            $supervisor = User::find($jobmerge->supervisor_id);
            if(isset($supervisor)){
                $payment = new payments;
             $payment->debitCompany($supervisor->id , (40/100*(23.5 /100 * $job->amount_paid)) , 'Payment for Job Completed. Job ID: '.$jobmerge->id);
       
            }
             $response['code'] = 200;
          return response()->json($response , 200);   
         }
         $response['code'] = 404;
         return response()->json($response , 200);
    }
}
