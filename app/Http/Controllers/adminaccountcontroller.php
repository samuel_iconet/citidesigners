<?php

namespace App\Http\Controllers;
use App\account;
use App\Transaction;
use App\payments;
use App\withdrawalRequest;
use App\User;
use App\artisan;
use App\supervisorData;
use Auth;
use Validator;
use Illuminate\Http\Request;

class adminaccountcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function confirmWithdrawal(request $request){
        $validator = Validator::make($request->all(), [
            "id"=>  "required",
           
      ]);
    
      if ($validator->fails()) {
    
           return $validator->messages();
      }
      $withdrawal_request =  withdrawalRequest::findOrFail($request->id);
      $payment = new payments;
       $data = $payment->WithdrawFromUser( $withdrawal_request->user_id, $withdrawal_request->amount , 'Withdrawal Request');
      if($data){
         
        $withdrawal_request->status = 'Completed';  
        $withdrawal_request->save();  
        $response['code'] = 200;
        return response()->json($response , 200); 
      }
      $response['code'] = 404;
        return response()->json($response , 200);
    }
    public function getAccount(){
        $account = account::where('user_id' , '0')->first();
        
        $transactions = Transaction::where('to_account_id' ,'0' )->orWhere('from_account_id' ,'0' )->get();
        $withdrawals = withdrawalRequest::all();
        foreach($withdrawals as $withdrawal){
            $user = User::find($withdrawal->user_id);
            if(isset($user)){
                $withdrawal['user'] = $user;
                if($user->role == '3'){
                    $withdrawal['user_data'] = artisan::where('user_id' , $user->id)->first();
                }else if($user->role == '4'){
                    $withdrawal['user_data'] = supervisorData::where('user_id' , $user->id)->first();

                } 
            }
           
            
        }
        foreach($transactions as $transaction){
            if($transaction->to_account_id == '0'){
                $transaction['type'] ='credit';
            }else{
                $transaction['type'] ='debit'; 
            }
            if($transaction->to_account_id == '0'){
                $transaction['to_name'] = 'Citi Designers';
            }else{
                $user = User::find($transaction->to_account_id);
                $transaction['to_name'] = $user->name;
            }
            if($transaction->from_account_id == '0'){
                $transaction['from_name'] = 'Citi Designers';
            }else{
                $user = User::find($transaction->from_account_id);
                $transaction['from_name'] = $user->name;
            }
        }
        $response['code'] = 200;
        $response['transactions'] = $transactions;
        $account['total_balance'] = $account->available_balance +  $account->pending_balance;
        $response['account'] = $account;
        $response['withdrawals'] = $withdrawals;
    
        return response()->json($response , 200); 
    }
}
