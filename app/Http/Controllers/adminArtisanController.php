<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\skill;
use App\state;
use App\area;
use App\artisan;
use App\querry;
use App\supervisorData;
use Validator;
class adminArtisanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin');

    }
    public function getsupervisorData($id){
      $supervisor_data = supervisorData::where('user_id' , $id)->first();
      $skills = skill::all();
      $querries  = querry::where('user_id' , $id)->get();
      if(isset($supervisor_data)){
       
        
         
        
       $response['code'] = 200;
       $response['supervisor_data'] = $supervisor_data;
       $response['querries'] = $querries;
       return response()->json($response , 200);  
      } 
      else{
       $response['code'] = 401;
       $response['error'] = "Artisan not found";
       return response()->json($response , 200);  
      } 
    }
    public function getartisanData($id){
      $artisan_data = artisan::where('user_id' , $id)->first();
      $skills = skill::all();
     $querries  = querry::where('user_id' , $id)->get();
      if(isset($artisan_data)){
       
        
          foreach($skills as $skill){
            if($artisan_data->skill_id == $skill->id){
              $artisan_data['skill_name'] = $skill->name;
            }
          }
        
       $response['code'] = 200;
       $response['artisan_data'] = $artisan_data;
       $response['querries'] = $querries;
       return response()->json($response , 200);  
      } 
      else{
       $response['code'] = 401;
       $response['error'] = "Artisan not found";
       return response()->json($response , 200);  
      } 
   }
    public function getskills(){
        $skills = skill::all();
        $areas = area::all();
        $states = state::all();

        foreach($areas as $area){
            foreach($states as $state){
                if($state->id  == $area->state_id){
                    $area['state_name'] = $state->name;
                }
            }
        }
        $response['code'] = 200;
        $response['skills'] = $skills;
        $response['areas'] = $areas;
        $response['states'] = $states;
    
        return response()->json($response ,200);

    }
    public function addskill(request $request){
        $this->validate($request,[     
            'file' => 'required',
            'skill_name' => 'required'
         ]);
         if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'profile'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            $request->file->storeAs('skill', $filePath, 'public');
            $image_name = $filePath;
            $skills = new skill;
            $skills->name = $request->skill_name;
            $skills->image = $image_name;
            $skills->status = '1';
            $skills->save();
        }
        $response['code'] = 200;
         return response()->json($response ,200);

    }
    public function addstate(request $request){
        $validator = Validator::make($request->all(), [
            "state" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $state = new state;
      $state->name = $request->state;
      $state->status = 1;
      $state->save();

      $response['code'] = 200;
      return response()->json($response ,200);

    }
    public function addarea(request $request){
        $validator = Validator::make($request->all(), [
            "state_id" =>  "required",
            "area" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $state =  state::findOrFail($request->state_id);
      $area = new area;
      
      $area->name = $request->area;
      $area->state_id = $request->state_id;
      $area->status = 1;
      $area->save();

      $response['code'] = 200;
      return response()->json($response ,200);

    }
    public function fliparea(request $request){
        $validator = Validator::make($request->all(), [
            "area_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $area = area::findOrFail($request->area_id);
      if($area->status == '1'){
          $area->status = '0';

      }else if($area->status == '0'){
        $area->status = '1';

    }
    $area->save();

      $response['code'] = 200;
      return response()->json($response ,200);  
    }
    public function flipstate(request $request){
        $validator = Validator::make($request->all(), [
            "state_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $state = state::findOrFail($request->state_id);
      if($state->status == '1'){
          $state->status = '0';

      }else if($state->status == '0'){
        $state->status = '1';

    }
    $state->save();
    
      $response['code'] = 200;
      return response()->json($response ,200);  
    }
    public function flipskill(request $request){
        $validator = Validator::make($request->all(), [
            "skill_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $skill = skill::findOrFail($request->skill_id);
      if($skill->status == '1'){
          $skill->status = '0';

      }else if($skill->status == '0'){
        $skill->status = '1';

    }
    $skill->save();
    
      $response['code'] = 200;
      return response()->json($response ,200);  
    }


    public function deletearea(request $request){
        $validator = Validator::make($request->all(), [
            "area_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $area = area::findOrFail($request->area_id);
     
    $area->delete();

      $response['code'] = 200;
      return response()->json($response ,200);  
    }
    public function deletestate(request $request){
        $validator = Validator::make($request->all(), [
            "state_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $state = state::findOrFail($request->state_id);
     
    $state->delete();
    
      $response['code'] = 200;
      return response()->json($response ,200);  
    }
    public function deleteskill(request $request){
        $validator = Validator::make($request->all(), [
            "skill_id" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $skill = skill::findOrFail($request->skill_id);
  
    $skill->delete();
    
      $response['code'] = 200;
      return response()->json($response ,200);  
    }
}
