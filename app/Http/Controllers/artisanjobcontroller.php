<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\jobmerge;
use App\User;
use App\user_data;
use App\job;
use App\supervisorData;
use App\getstateareaname;
use App\proof;
use App\merge;

use Str;
class artisanjobcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('Artisan');

    }
    public function addQuote(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required',
             'amount' => 'required'
         ]);
       $jobmerge =  jobmerge::where(['id' => $request->jobmerge_id , 'artisan_id' => Auth::User()->id])->first();
       if(isset($jobmerge)){
        $job = job::findOrFail($jobmerge->job_id);
        $jobmerge->quote = $request->amount;
        $jobmerge->status = 'Quoted';
        $jobmerge->save();
        $job->quote  = $request->amount;  
        $job->status  = 'Quoted';  
        $job->save();  
        $response['code'] = 200;
         return response()->json($response , 200);  

       }else{
        $response['code'] = 404;
        $response['error'] = "Invalid Job Data";
        return response()->json($response , 200);  

       }
    }
    public function getproofs(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required'
          
         ]);
         $proofs  = proof::where('jobmerge_id' , $request->jobmerge_id)->get();
         $response['code'] = 200;
          $response['proofs'] = $proofs;
          return response()->json($response , 200);     

        }
        public function appUploadproof(request $request){
            $this->validate($request,[     
                'jobmerge_id' => 'required',
                'file' => 'required',
                 'name' => 'required'
             ]);
             $realImage = base64_decode($request->file);
             file_put_contents($request->name , $realImage);
            
             return 'done';

       }
    public function uploadproof(request $request){
        $this->validate($request,[     
            'jobmerge_id' => 'required',
            'file' => 'required',
             
         ]);
         if ($request->has('file')) {
            $image = $request->file('file');
            $name = 'proof'.'_'.time();
            $filePath =  $name. '.' . $image->getClientOriginalExtension();
            // $this->uploadOne($image, $folder, 'public', $name);
            $request->file->storeAs('proof', $filePath, 'public');
            $image_name = $filePath;
        }
        $merge = jobmerge::where(['artisan_id' => Auth::user()->id , 'id' => $request->jobmerge_id])->first();
        $merge->status = 'Payment Processed';
        $merge->save();
        if(isset($merge)){
          $proof = new proof;
          $proof->name = $image_name;
          $proof->jobmerge_id =  $request->jobmerge_id;
          $proof->save();
          $response['code'] = 200;
          $response['proof'] = $proof;
          return response()->json($response , 200);  

        }
        else{
            $response['code'] = 401;
            $response['error'] = "You are not merged to this Job";
            return response()->json($response , 200);  
        }
        
    }
    public function  getmergeRequest(){
          $merges =  jobmerge::where('artisan_id' , Auth::User()->id)->get();
        foreach($merges as $merge){
            $user = User::find($merge->user_id);
           $merge['jobdata'] = job::where('id' , $merge->job_id)->first();
            $merge['user'] = User::find($merge->user_id);
            $merge['supervisor'] = User::find($merge->supervisor_id);
            $merge['supervisor_data'] = supervisorData::where('user_id' , $merge->supervisor_id)->first();
            $stateareaData = new getstateareaname;
            $merge['state_name'] =  $stateareaData->getStateName($merge['jobdata']->state);
            $merge['area_name'] =  $stateareaData->getAreaName($merge['jobdata']->area);
        }
        $response['code'] = 200;
        $response['requests'] = $merges;
        return response()->json($response , 200);  
    }
    public function getNewRequest(){
        $merges =  jobmerge::where(['artisan_id' => Auth::User()->id , 'artisan_status' => 'pending'])->get();
        foreach($merges as $merge){
            $merge['user'] = User::find($merge->user_id);
            $merge['userdata'] = job::where('id' , $merge->job_id)->first();
            $stateareaData = new getstateareaname;
            $merge['state_name'] =  $stateareaData->getStateName($merge['userdata']->state);
            $merge['area_name'] =  $stateareaData->getAreaName($merge['userdata']->area);
        }
        $response['new_requests'] = count($merges);
        $response['all_requests'] = count(jobmerge::where('artisan_id' , Auth::User()->id )->get());
        $response['accepted_requests'] = count(jobmerge::where(['artisan_id' => Auth::User()->id , 'artisan_status' => 'Accepted'])->get());
        $response['rejected_requests'] = count(jobmerge::where(['artisan_id' => Auth::User()->id , 'artisan_status' => 'Rejected'])->get());
        $response['completed_requests'] = count(jobmerge::where(['artisan_id' => Auth::User()->id , 'status' => 'Completed'])->get());

        $response['code'] = 200;
        $response['jobs'] = $merges;
        return response()->json($response , 200);
    }
    public function acceptRequest($id){
        $request = jobmerge::where(['id' => $id , 'artisan_id' => Auth::User()->id , 'status' => 'Pending'])->first();
       $job = job::find($request->job_id);
        if(isset($request) && isset($job)){
            $request->artisan_status = "Accepted";
            if($job->job_type == 'repairs'){
                $request->status = "Accepted";   
            }else{
                if($request->supervisor_status == "Accepted"){
                    $request->status = "Accepted";
                }
            }
            $request->save();
            $response['code'] = 200;
            return response()->json($response , 200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Invalid ID";
            return response()->json($response , 200);
        }
    }
    public function rejectRequest($id){
         $request = jobmerge::where(['id' => $id , 'artisan_id' => Auth::User()->id])->first();
       
        if(isset($request)){
            $job = job::where('id' , $request->job_id)->first();
            if(isset($job)){
                $new_request = new jobmerge;
                $new_request->job_id = $request->job_id;
                $new_request->job_name =  $request->job_name;
                 $new_request->artisan_id = $request->artisan_id;
                $new_request->supervisor_id = $request->supervisor_id;
                $new_request->supervisor_status = $request->supervisor_status;
                $new_request->user_id = $request->user_id;
                $new_request->status = $request->status;
                $new_request->quote =$request->quote;
                $new_request->payment_status = $request->payment_status;
                $new_request->artisan_status = "Rejected";
                $new_request->save();   
                $request->artisan_status = "Rejected";
                $request->status = "Cancelled";
                $request->save();
               if($request->supervisor_status == 'Rejected'){
                $new_request->status = "Cancelled";
                $new_request->save();
                $job->status = 'Pending';
                $job->save();
                $merge = new merge;
               $merge->mergeJob( $job->id);
               }
               $merge = new merge;
              $data =  $merge->remergeArtisan( $new_request->id );
                $response['code'] = 200;
                $response['res'] =  $data;
            return response()->json($response , 200);

            }else{
                $response['code'] = 400;
                $response['error'] = "Invalid User ID Provided.";
                return response()->json($response , 200);  
            }
            
        }else{
            $response['code'] = 401;
            $response['error'] = "Invalid ID";
            return response()->json($response , 200);
        }
    }
    public function getAllRequest(){
        $merges =  jobmerge::where('artisan_id' , Auth::User()->id )->get();
        foreach($merges as $merge){
            $merge['user'] = User::find($merge->user_id);
            $merge['userdata'] = user_data::where('user_id' , $merge->user_id)->first();
        }
        $response['code'] = 200;
        $response['jobs'] = $merges;
        return response()->json($response , 200);
    }
}
