<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\account;
use App\Transaction;
use App\withdrawalRequest;
use App\User;
use App\artisan;
use App\supervisorData;
use App\state;
use App\area;
use App\user_data;
use App\skill;
use App\setting;

use App\Mail\artisanjobmergeMail;
use App\Mail\supervisorjobmergeMail;
use Illuminate\Support\Facades\Mail;
class merge 
{
    public function setSetting(request $request){
        
        $settings['artisansingleMerge'] = $request->artisansingleMerge;
        $settings['artisansingleJob'] = $request->artisansingleJob;
        $settings['artisansameState'] = $request->artisansameState;
        $settings['artisansameArea'] = $request->artisansameArea;
        $settings['artisansameSkills'] = $request->artisansameSkills;
        $settings['artisanapproved'] = $request->artisanapproved;
        $settings['supervisorsingleMerge'] = $request->supervisorsingleMerge;
        $settings['supervisorsingleJob'] = $request->supervisorsingleJob;
        $settings['supervisorsameState'] = $request->supervisorsameState;
        $settings['supervisorsameArea'] = $request->supervisorsameArea;
        $settings['supervisorapproved'] = $request->supervisorapproved;
        $serialSetting  = serialize($settings);
        $mergeSetting = setting::where('name' , 'merge')->first();
        $mergeSetting->settings = $serialSetting;
        $mergeSetting->save();
        return $settings;
    }
    public function getSettings(){
      $mergeSetting = setting::where('name' , 'merge')->first();
      if(isset($mergeSetting)){
        $settings = unserialize($mergeSetting->settings);
        return $settings; 
      }
      else {
        $settings['artisansingleMerge'] = true;
        $settings['artisansingleJob'] = false;
        $settings['artisansameState'] = true;
        $settings['artisansameArea'] = false;
        $settings['artisansameSkills'] = true;
        $settings['artisanapproved'] = true;
        $settings['supervisorsingleMerge'] = true;
        $settings['supervisorsingleJob'] = false;
        $settings['supervisorsameState'] = true;
        $settings['supervisorsameArea'] = false;
        $settings['supervisorapproved'] = true; 
        
        $serialSetting  = serialize($settings);
        $mergeSetting = new setting;
        $mergeSetting->name = 'merge';
        $mergeSetting->settings = $serialSetting;
        $mergeSetting->save();
        return $settings;
      }
    }
    public function getClosestArtisan($job_id ){
        $settings  = $this->getSettings();
        $singleMerge =  $settings['artisansingleMerge'];
        $singleJob = $settings['artisansingleJob'];
        $sameState =  $settings['artisansameState'];
        $sameArea = $settings['artisansameArea'];
        $sameSkills = $settings['artisansameSkills'];
        $approved =  $settings['artisanapproved'];
        $loggedin = false;
        $artisans = [];
         $job = job::find($job_id);

         if(!isset($job)){
             return 'invalid job';
         }
        
         

         if($approved){
            $artisans = artisan::where('status' ,  'approved')->get(); 
         }else{
            $artisans = artisan::where('status' , '<>' , 'deleted')->get();
         }
       
         if(count($artisans) < 1){
            return 'empty by now 1';
        }
        if($sameSkills){
            $collection = new Collection();
            foreach($artisans as $key => $artisan ){
                if($artisan->skill_id == $job->skills_id){
                    $collection->push($artisan);
                }
          }
          $artisans =  $collection;
        }
        if(count($artisans) < 1){
            return 'empty by now 2';
        }
        if($singleMerge){

            $collection = new Collection();
            foreach($artisans as $key => $artisan ){
                $response['SMArtisan'.$key] = $artisan;
                $response['SMjobmerge'.$key] = $jobmerge = jobmerge::where(['job_id' => $job->id , 'artisan_id' => $artisan->user_id] )->get();
                if(count($jobmerge) < 1){
                    $collection->push($artisan);      
                }
          }
          $artisans =  $collection;
        }
        if(count($artisans) < 1){
            return 'empty by now 3';
        }
        if($singleJob){
            $collection = new Collection();
            foreach($artisans as $key => $artisan ){
                $response['artisan'.$key]  =$artisan;
               $response['jobmerge'.$key]  = $jobmerge = jobmerge::where( 'artisan_id' ,$artisan->user_id )->where('status' , '<>' , 'Completed')->where('status' , '<>' , 'Cancelled')->get();
                if(count($jobmerge) < 1){
                    $collection->push($artisan);      
                }
          }
          $artisans =  $collection;
        }
        if(count($artisans) < 1){
            return 'empty by now 4';
        }
        if($sameState){
            $collection = new Collection();
            foreach($artisans as $key => $artisan ){
                if($artisan->state_id == $job->state){
                    $collection->push($artisan);
                }
          }
          $artisans =  $collection;
          if($sameArea){
            $collection = new Collection();
            foreach($artisans as $key => $artisan ){
                if($artisan->area_id == $job->area){
                    $collection->push($artisan);
                }
          }
          $artisans =  $collection;
          if(count($artisans) < 1){
            return 'empty by now 5';
        }
        $selected = rand(1 , count($artisans));
        return $artisans[$selected-1];
          }
          else{
            
            
          if(count($artisans) < 1){
                    return 'empty by now 6';
           }else{
               $areas = area::where(['state_id' => $job->state , 'status' => '1'])->get();
               $area_count = count($areas);
               $job_area = area::find($job->area);
               $area_key = 0;
               foreach($areas as $key => $area){
                   if($job_area->id == $area->id ){
                       $area_key = $key;
                   }
               }
            //    return $areas[$area_key];
               $area_count = count($areas);
               $found = false;
               $left = 0;
               $right = 0;
               $rightArtisan =  new Collection();
               $leftArtisan =  new Collection();
               $rightArtisans =  new Collection();
               $leftArtisans =  new Collection();
               $count = 0;
               for($i = $area_key; $i < $area_count ; $i++ ){
                   $area = $areas[$i];
                   foreach($artisans as $artisan){
                       if(!$found){
                         if($artisan->area_id == $area->id){
                            $rightArtisan->push($artisan);  
                            $found = true;
                            $right = $count;   
                         }  
                       }
                     
                   }
                   $count++;
               }
               $count = 0;
               $found = false;

               for($i = $area_key; $i >= 0 ; $i--){
               $response['id'.$i] = $area = $areas[$i];
                foreach($artisans as $artisan){
                    if(!$found){
                      if($artisan->area_id == $area->id){
                         $leftArtisan->push($artisan);  
                         $found = true; 
                         $left = $count;  
                      }  
                    }

                    
                }
                $count++;
            }
            $response['leftArtisan']  = $leftArtisan;
            $response['rightArtisan']  = $rightArtisan;
            $response['right']  = $right;
            $response['left']  = $left;
            if(count($leftArtisan) > 0 && count($rightArtisan) > 0){
             $response['msg']  = "found on both side";
             if($left < $right){
                 return $leftArtisan[0];
             }else{
                 return $rightArtisan[0];
             }
            
            }else if(count($leftArtisan) > 0 ){
                $response['msg']  = "found on left side";  
                return $leftArtisan[0];
             
            }
            else if(count($rightArtisan) > 0){
                $response['msg']  = "found on right side"; 
                return $rightArtisan[0];
            }else{
                $response['msg']  = "none found";  
            }
            return $response;

           }
          }
          
         
         }
         else{
            $selected = rand(1 , count($artisans));
            return $artisans[$selected-1];  
         }
        

        



    }
    public function getClosestSupervisor($job_id){
        $settings  = $this->getSettings();
        $singleMerge =  $settings['supervisorsingleMerge'];
        $singleJob = $settings['supervisorsingleJob'];
        $sameState =  $settings['supervisorsameState'];
        $sameArea = $settings['supervisorsameArea'];
        $approved =  $settings['supervisorapproved'];
     
        $loggedin = false;
        $supervisors = [];
         $job = job::find($job_id);

         if(!isset($job)){
             return 'invalid job';
         }
        
         

         if($approved){
            $supervisors = supervisorData::where('status' ,  'approved')->get(); 
         }else{
            $supervisors = supervisorData::where('status' , '<>' , 'deleted')->get();
         }

         if(count($supervisors) < 1){
            return 'empty by now';
        }
       
        if($singleMerge){

            $collection = new Collection();
            foreach($supervisors as $key => $supervisor ){
                $response['SMsupervisor'.$key] = $supervisor;
                $response['SMjobmerge'.$key] = $jobmerge = jobmerge::where(['job_id' => $job->id , 'supervisor_id' => $supervisor->user_id] )->get();
                if(count($jobmerge) < 1){
                    $collection->push($supervisor);      
                }
          }
          $supervisors =  $collection;
        }
        if(count($supervisors) < 1){
            return 'empty by now';
        }
        if($singleJob){
            $collection = new Collection();
            foreach($supervisors as $key => $supervisor ){
                $response['supervisor'.$key]  =$supervisor;
               $response['jobmerge'.$key]  = $jobmerge = jobmerge::where( 'supervisor_id' ,$supervisor->user_id )->where('status' , '<>' , 'Completed')->where('status' , '<>' , 'Cancelled')->get();
                if(count($jobmerge) < 1){
                    $collection->push($supervisor);      
                }
          }
          $supervisors =  $collection;
        }
        if(count($supervisors) < 1){
            return 'empty by now';
        }
        if($sameState){
            $collection = new Collection();
            foreach($supervisors as $key => $supervisor ){
                if($supervisor->state_id == $job->state){
                    $collection->push($supervisor);
                }
          }
          $supervisors =  $collection;
          if($sameArea){
            $collection = new Collection();
            foreach($supervisors as $key => $supervisor ){
                if($supervisor->area_id == $job->area){
                    $collection->push($supervisor);
                }
          }
          $supervisors =  $collection;
          if(count($supervisors) < 1){
            return 'empty by now';
        }
        $selected = rand(1 , count($supervisors));
        return $supervisors[$selected-1];
          }
          else{
            
            
          if(count($supervisors) < 1){
                    return 'empty by now';
           }else{
               $areas = area::where(['state_id' => $job->state , 'status' => '1'])->get();
               $area_count = count($areas);
               $job_area = area::find($job->area);
               $area_key = 0;
               foreach($areas as $key => $area){
                   if($job_area->id == $area->id ){
                       $area_key = $key;
                   }
               }
            //    return $areas[$area_key];
               $area_count = count($areas);
               $found = false;
               $left = 0;
               $right = 0;
               $rightsupervisor =  new Collection();
               $leftsupervisor =  new Collection();
               $rightsupervisors =  new Collection();
               $leftsupervisors =  new Collection();
               $count = 0;
               for($i = $area_key; $i < $area_count ; $i++ ){
                   $area = $areas[$i];
                   foreach($supervisors as $supervisor){
                       if(!$found){
                         if($supervisor->area_id == $area->id){
                            $rightsupervisor->push($supervisor);  
                            $found = true;
                            $right = $count;   
                         }  
                       }
                     
                   }
                   $count++;
               }
               $count = 0;
               $found = false;

               for($i = $area_key; $i >= 0 ; $i--){
               $response['id'.$i] = $area = $areas[$i];
                foreach($supervisors as $supervisor){
                    if(!$found){
                      if($supervisor->area_id == $area->id){
                         $leftsupervisor->push($supervisor);  
                         $found = true; 
                         $left = $count;  
                      }  
                    }

                    
                }
                $count++;
            }
            $response['leftsupervisor']  = $leftsupervisor;
            $response['rightsupervisor']  = $rightsupervisor;
            $response['right']  = $right;
            $response['left']  = $left;
            if(count($leftsupervisor) > 0 && count($rightsupervisor) > 0){
             $response['msg']  = "found on both side";
             if($left < $right){
                 return $leftsupervisor[0];
             }else{
                 return $rightsupervisor[0];
             }
            
            }else if(count($leftsupervisor) > 0 ){
                $response['msg']  = "found on left side";  
                return $leftsupervisor[0];
             
            }
            else if(count($rightsupervisor) > 0){
                $response['msg']  = "found on right side"; 
                return $rightsupervisor[0];
            }else{
                $response['msg']  = "none found";  
            }
            return $response;

           }
          }
          
         
         }
         else{
            $selected = rand(1 , count($supervisors));
            return $supervisors[$selected-1];  
         }
        

        



    }
    public function mergeSupervisor(){
        
    }
    public function mergeJob($job_id){
        
        $job = job::find($job_id);
        
        if(isset($job) && $job->status == 'Pending'){
            if($job->job_type  == 'repairs'){
                 $artisan = $this->getClosestArtisan($job_id);
                if(isset($artisan->id)){
                   $skill = skill::findOrFail($job->skills_id);
                  $jobmerge = new jobmerge;
                  $jobmerge->job_id = $job->id;
                  $jobmerge->job_name = $skill->name;
                  $jobmerge->artisan_id = $artisan->user_id;
                  $jobmerge->supervisor_id = 'null';
                  $jobmerge->user_id = $job->jobber_id;
                  $jobmerge->status = "Pending";
                  $jobmerge->quote = "0";
                  $jobmerge->payment_status = "0";
                  $jobmerge->save();
                  $job->status = "merged";
                  $job->save();
                  $art = User::find($artisan->user_id);
                  Mail::to($art)->send(new artisanjobmergeMail($art));
                //   $response['code'] = 200;
                //   return response()->json($response ,200);


                    return 'done merging repairs';





                }else{
                    return 'no artisan to merge';
                }        
            }else if($job->job_type  == 'new' || $job->job_type == 'old'){
                $artisan = $this->getClosestArtisan($job_id);
                $supervisor = $this->getClosestSupervisor($job_id);
                if(isset($artisan->id) && isset($supervisor->id)){
                    $skill = skill::findOrFail($job->skills_id);
                    $jobmerge = new jobmerge;
                    $jobmerge->job_id = $job->id;
                    $jobmerge->job_name = $skill->name;
                    $jobmerge->artisan_id = $artisan->user_id;
                    $jobmerge->supervisor_id = $supervisor->user_id;
                    $jobmerge->user_id = $job->jobber_id;
                    $jobmerge->status = "Pending";
                    $jobmerge->quote = "0";
                    $jobmerge->payment_status = "0";
                    $jobmerge->save();
                    $job->status = "merged";
                    $job->save();
                    $art = User::find($artisan->user_id);
                    $sup = User::find($supervisor->user_id);
                    $sup_data = supervisorData::where('user_id' ,$sup->id)->first();
                    $art_data = artisan::where('user_id' ,$sup->id)->first();
                     $message = new message;
                   
                 $msg = "Hello ".$sup->name.", A request has been merged to you.";
                 $response['text'] = $message->sendSms($sup_data->phone , $msg);
                  
                 $msg = "Hello ".$art->name.", A request has been merged to you.";
                 $response['text'] = $message->sendSms($art_data->phone , $msg);
                $response['code'] = 200;
                    Mail::to($art)->send(new artisanjobmergeMail($art));
                    Mail::to($sup)->send(new supervisorjobmergeMail($sup));
                     return 'done merging new old';
                }else {
                    $res['ar'] = $artisan ;
                    $res['su'] = $supervisor ;
                    $res['mes'] = 'cant merge' ;
                    return $res;
                }
            }
            else{
                return 'no type';
            }
             
        }else {
            return 'invalid job';
        }
        

        
    }
    public function remergeArtisan($jobmerge_id){
        $jobmerge = jobmerge::find($jobmerge_id);
        if(isset($jobmerge) && $jobmerge->artisan_status == 'Rejected'){
         $job = job::find($jobmerge->job_id);
        if(isset($job) && $job->status == 'merged'){
              $artisan = $this->getClosestArtisan($job->id);
            if(isset($artisan->id)){
            $user = user::find($artisan->user_id);
            $jobmerge->artisan_id = $artisan->user_id;
            $jobmerge->artisan_status = 'Pending';
            $jobmerge->save();
            Mail::to($user)->send(new artisanjobmergeMail($user));
            }
            return 'done';            
        }else{
            return 'invalid job';
        }
        }else{
            return 'invalid id';
        }
        
        
    }
    public function remergeSupervisor(){
        $jobmerge = jobmerge::find($jobmerge_id);
        if(isset($jobmerge) && $jobmerge->artisan_status == 'Rejected'){
        $job = job::find($jobmerge->job_id);
        if(isset($job) && $job->status == 'merged'){
            $supervisor = $this->getClosestSupervisor($job->id);
            if(isset($supervisor->id)){
             $user = user::find($supervisor->user_id);
            $jobmerge->supervisor_id = $supervisor->user_id;
            $jobmerge->supervisor_status = 'Pending';
            Mail::to($user)->send(new artisanjobmergeMail($user));
            Mail::to($user)->send(new supervisorjobmergeMail($user));
            }
            
        }
        }
    } 
}
