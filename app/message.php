<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\account;
use App\Transaction;
use App\withdrawalRequest;
use App\User;
use App\artisan;
use App\supervisorData;
use App\user_data;
class message 
{
  public function sendSms($number, $message){
    if(strlen($number) == 11){
      $contact = substr($number , 1);
      if(substr($number , 1 ,1) == '8' || substr($number , 1 ,1) == '7'){
        $contact = '234'.$contact;
        $curl = curl_init();
        $data = array("to" => $contact,"from" => "CitiDesign",
        "sms"=> $message,"type" => "plain","channel" => "generic","api_key" => "TLieQixVvDnL3BoS5oZRPwyaQmzKzWjcdyNXpu4eHHf2yc4076r27nWwFQUGlJ");
        
        $post_data = json_encode($data);
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://termii.com/api/sms/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $post_data,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return  $response; 
      }else{
        return 'false1';
      }

    }else{
      return 'false2';
    }

  }
  public function getNumbers($users){
    $numbers = [];
    $count = 0;
  foreach($users as $user){
      if($user->role == '1' || $user->role == '2'){
          $user_data = user_data::where('user_id' , $user->id)->first();
      }
      else if($user->role == '3'){
        $user_data = artisan::where('user_id' , $user->id)->first();
      }
      else if($user->role == '4'){
        $user_data = supervisorData::where('user_id' , $user->id)->first();
      }
      if(isset($user_data)){
          $numbers[$count] = $user_data->phone_number;
          $count++;
      }
  }
  return $numbers;
}
public function getEmail($users){
    $emails = [];
    $count = 0;
    foreach($users as $user){
        $emails[$count] = $user->email;
        $count++;

    }
    return $emails;
}
 
}
