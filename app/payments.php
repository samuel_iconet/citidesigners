<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\account;
use App\Transaction;
use App\withdrawalRequest;
use App\User;
class payments 
{
  public function WithdrawFromUser($user_id , $amount , $description ){
    $userAccount = account::where('user_id' , $user_id )->first();
    $user = User::find($user_id);
    if(!isset($userAccount) ){
      return false;
    }
    if(isset($user) && $amount <= $userAccount->pending_balance){
        $userAccount->pending_balance = $userAccount->pending_balance - $amount;
        $userAccount->save();
        $transaction = new Transaction;
        $transaction->to_account_id =  '0';
        $transaction->from_account_id =  $user->id;
        $transaction->amount =  $amount;
        $transaction->description = $description ;
        $transaction->status = '1' ;
        $transaction->save();
      return true;
      }
      return  false;
} 
  public function debitCompany($to_id , $amount , $description ){ 
    $companyAccount = account::where('user_id' , '0')->first();
    $userAccount = account::where('user_id' , $to_id )->first();
    $user = User::find($to_id);
    if(!isset($userAccount) && isset($user)){
   return false;
    }
    if(isset($companyAccount)  && isset($user)){
        $companyAccount->available_balance = $companyAccount->available_balance - $amount;
        $companyAccount->save();
        $userAccount->available_balance = $userAccount->available_balance +$amount;
        $userAccount->save();
        $transaction = new Transaction;
        $transaction->to_account_id =   $user->id;
        $transaction->from_account_id = '0';
        $transaction->amount =  $amount;
        $transaction->description = $description ;
        $transaction->status = '1' ;
        $transaction->save();
      return true;
      }
      return  false;
} 
  public function creditCompany($from_id , $amount , $description ){
      $companyAccount = account::where('user_id' , '0')->first();
      $userAccount = account::where('user_id' , $from_id )->first();
      $user = User::find($from_id);
      if(!isset($userAccount) && isset($user)){
        $userAccount = new account;
        $userAccount->user_id = $user->id;
        $userAccount->status = '1';
        $userAccount->user_type = $user->role;
        $userAccount->save();
      }
      if(isset($companyAccount)  && isset($user)){
          $companyAccount->available_balance = $companyAccount->available_balance + $amount;
          $companyAccount->save();
          $transaction = new Transaction;
          $transaction->to_account_id =  '0';
          $transaction->from_account_id =  $user->id;
          $transaction->amount =  $amount;
          $transaction->description = $description ;
          $transaction->status = '1' ;
          $transaction->save();
        return true;
        }
        return  false;
  }  
  public function createAccount($user_id){
    $userAccount = account::where('user_id' , $user_id )->first();
      $user = User::find($user_id); 
      if(!isset($userAccount) && isset($user)){
        $userAccount = new account;
        $userAccount->user_id = $user->id;
        $userAccount->status = '1';
        $userAccount->user_type = $user->role;
        $userAccount->save();
      }
      return $userAccount;  
  }
 
}
