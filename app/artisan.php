<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class artisan extends Model
{
    //
    protected $fillable = [
         'skill_id','user_id','phone_number','area_id', 'address', 'state_id', 'account_number', 'account_name' , 'guarantor' , 'guarantor_number'
    ];
}
